/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ProcessClassMap.java $
 * created by Owner Apr 28, 2006
 */
package org.yuwa.util.smallBind;

import java.util.HashMap;
import java.util.Map;

import org.yuwa.util.namevalueparser.NameValueParser;

/**
 * @author Owner
 * 
 */
public class ProcessClassMap
{
    public static final String NAME = "classmap";

    private final SaxBindHarness harness;
    private final String data;


    /**
     * @param harness
     * @param data
     */
    public ProcessClassMap(SaxBindHarness harness, String data)
    {
        this.harness = harness;
        this.data = data;
    }


    /**
     * parse the data into a map, and then use that as the class map
     * 
     * The processing instruction should appear as:
     * 
     * <?classmap /root : org.blah.Blah ; /root/zing : org.blah.Zing ; ?>
     */
    @SuppressWarnings("unchecked")
    public void process() throws Exception
    {
        NameValueParser parser = new NameValueParser(this.data);
        parser.parse();
        Map<String, String> map = parser.getMap();
        Map<String, Class<?>> newMap = new HashMap();

        for (String key : map.keySet())
        {
            if (key != null && key.length() > 0)
            {
                this.harness.debug("{create [" + key + "] [" + map.get(key) + "]}");
                Class c = Class.forName(map.get(key));
                newMap.put(key, c);
            }
        }

        this.harness.setClassMap(newMap);
    }
}
