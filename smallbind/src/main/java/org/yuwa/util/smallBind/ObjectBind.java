/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/* Generated by Together */

package org.yuwa.util.smallBind;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Collections;
import java.util.Map;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.yuwa.util.uriresolver.SimpleUriResolver;
import org.yuwa.util.uriresolver.UriResolver;


public class ObjectBind
{
    @SuppressWarnings("unchecked")
    private Map<String, Class<?>> classMap = (Map<String, Class<?>>) Collections.EMPTY_MAP;
    @SuppressWarnings("unchecked")
    private Map<String, String> parameterMap = (Map<String, String>) Collections.EMPTY_MAP;
    private SaxBindHarness harness = new SaxBindHarness();


    public Binder from(String uri) throws BinderException
    {
        Binder result = null;
        
        try
        {
            UriResolver locator = new SimpleUriResolver();
            InputStream stream = locator.resolveAsStream(uri);
            result = this.fromInputStream(stream);
        }
        catch (Exception e)
        {
            throw new BinderException("could not bind [" + uri  + "]", e);
        }
        
        return result;
    }
    
    
    /**
     * this method binds a file from the given file. Pass the file path, and the
     * xpath to the root node
     * 
     * @param fileName
     * @param root
     * @return
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws SAXException
     */
    public Binder fromFile(String fileName) throws ClassNotFoundException, InstantiationException, IOException,
            IllegalAccessException, SAXException
    {
        this.harness.setClassMap(this.classMap);
        this.harness.setPropertyMap(this.parameterMap);

        XMLReader parser = this.harness.makeParser();
        parser.parse(fileName);

        return this.harness;
    }


    /**
     * read in the XML from an input stream
     * 
     * this method binds a file from the given file. Pass the file path, and the
     * xpath to the root node
     * 
     */
    public Binder fromInputStream(InputStream stream) throws ClassNotFoundException, InstantiationException,
            IOException, IllegalAccessException, SAXException
    {
        this.harness.setClassMap(this.classMap);
        this.harness.setPropertyMap(this.parameterMap);

        XMLReader parser = this.harness.makeParser();
        InputSource xmlSource = new InputSource(stream);
        parser.parse(xmlSource);

        return this.harness;
    }


    /**
     * take a xml string and parse it into an object structure.
     * 
     * this method binds a file from the given file. Pass the file path, and the
     * xpath to the root node
     * 
     * @param the
     *            uri to the XML file to be parsed
     * @return the root object of the resulting object structure
     */
    public Binder bindString(String xml) throws ClassNotFoundException, InstantiationException,
            IllegalAccessException, IOException, SAXException
    {
        this.harness.setClassMap(this.classMap);
        this.harness.setPropertyMap(this.parameterMap);

        StringReader reader = new StringReader(xml);
        InputSource xmlSource = new InputSource(reader);
        XMLReader parser = this.harness.makeParser();
        parser.parse(xmlSource);

        return this.harness;
    }


    /**
     * allow the user to set an object for the binder to use
     * 
     * @param id the id of the object
     * @param object the object to reference
     */
	public void putById(String id, Object object) { harness.putById(id, object); }

}
