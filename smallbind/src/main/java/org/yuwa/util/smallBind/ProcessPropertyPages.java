/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ProcessPropertyPages.java $
 * created by Owner May 4, 2006
 */
package org.yuwa.util.smallBind;

import java.io.InputStream;

import org.yuwa.util.namevalueparser.InputStreamPeeler;
import org.yuwa.util.namevalueparser.NameValueLexer;
import org.yuwa.util.namevalueparser.NameValueParserException;
import org.yuwa.util.namevalueparser.StringPeeler;
import org.yuwa.util.propertypages.parser.PropertyPageParser;

/**
 * @author Owner
 * 
 */
public class ProcessPropertyPages
{
    public static final String NAME = "ppages";

    private final SaxBindHarness harness;
    private final String data;


    /**
     * @param harness
     * @param data
     */
    public ProcessPropertyPages(SaxBindHarness harness, String data)
    {
        this.harness = harness;
        this.data = data;
    }


    /**
     * 
     */
    @SuppressWarnings("unchecked")
    public void process() throws Exception
    {
        String uri = this.uri();

        InputStream stream = this.harness.getUriResolver().resolveAsStream(uri);

        if (stream == null) throw new RuntimeException("unable to open stream for [" + uri + "]");
        
        InputStreamPeeler peeler = new InputStreamPeeler(stream);
        PropertyPageParser parser = new PropertyPageParser(peeler);

        parser.parse();

        this.harness.setPropertyPages(parser.getPropertyPages());
    }


    /**
     * @return the uri specified in the data
     * @throws NameValueParserException
     */
    private String uri() throws NameValueParserException
    {
        StringPeeler dataPeeler = new StringPeeler(this.data);
        NameValueLexer lexer = new NameValueLexer(dataPeeler);

        lexer.whiteSpace();

        if (!lexer.uri()) throw new RuntimeException("no property page uri specified");
        String uri = lexer.getValue();

        return uri;
    }

}
