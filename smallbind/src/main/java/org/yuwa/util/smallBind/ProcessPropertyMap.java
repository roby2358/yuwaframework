/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ProcessPropertyMap.java $
 * created by Owner Apr 28, 2006
 */
package org.yuwa.util.smallBind;

import java.util.Map;

import org.yuwa.util.namevalueparser.NameValueParser;

/**
 * @author Owner
 *
 */
public class ProcessPropertyMap
{
    public static final String NAME = "propertymap";

    private final SaxBindHarness harness;
    private final String data;

    /**
     * @param harness
     * @param data
     */
    public ProcessPropertyMap(SaxBindHarness harness, String data)
    {
        this.harness = harness;
        this.data = data;
    }

    /**
     * 
     */
    @SuppressWarnings("unchecked")
    public void process() throws Exception
    {
        NameValueParser parser = new NameValueParser(this.data);
        parser.parse();
        Map<String, String> map = parser.getMap();
        this.harness.setPropertyMap(map);
    }

}
