/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: Binder.java $
 * created by Owner Apr 26, 2006
 */
package org.yuwa.util.smallBind;



/**
 * The binder interface what the user interacts with
 * after the binding is done.
 * 
 * @author Owner
 * 
 */
public interface Binder
{


    /**
     * before binding
     *
     * Let the user shove objects into the builder,
     * so we'll use those instead of constructing new ones.
     * 
     * This identifies the object by id rather than xpath
     *
     * Note that in parsing the file, it's possible this object
     * could be replaced by a newly generated one, since the
     * id may be redefined.
     * 
     * @param xpath
     */
    void putById(String id, Object object);


    /**
     * after binding
     * 
     * gets the object associated with an xpath, or null if not found
     * 
     * @param xpath
     * @return
     */
    Object getObject(String xpath);


    /**
     * after binding
     * 
     * if an object has an id= attribute, we can fetch it from the objectsByID
     * map
     * 
     * @param id
     * @return the request object, or null if no object with that ID
     */
    Object getObjectByID(String id);

    /**
     * 
     * @param name
     * @return the property value for the given name, or null
     */
    String getProperty(String name);

}
