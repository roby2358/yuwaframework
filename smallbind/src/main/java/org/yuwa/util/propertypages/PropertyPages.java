/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: PropertyPages.java $
 * created by Owner Apr 27, 2006
 */
package org.yuwa.util.propertypages;

import org.yuwa.util.workingobject.WorkingObject;


/**
 * @author Owner
 * 
 */
public interface PropertyPages
{
    /**
     * apply property pages to the given object
     * @param o
     */
    void apply(Object o);

    /**
     * apply property pages to the given object using the given id & ppages
     * 
     * @param o
     * @param id
     * @param ppage
     */
    void apply(Object o, String id, String ppage);

    /**
     * apply property pages to the given object using the given id, ppages and
     * path
     * 
     * @param o
     * @param id
     * @param ppage
     */
    void apply(Object o, String path, String id, String ppage);

    /**
     * apply property pages to the given working object using the given id,
     * ppages and path
     * 
     * @param o
     * @param id
     * @param ppage
     */
    void apply(WorkingObject o, String path, String id, String ppage);

    
    /**
     * a do-nothing implementation to use as a placeholder
     */
    PropertyPages NO_PAGES = new PropertyPages() {

        public void apply(Object o) { }

        public void apply(Object o, String id, String ppage) { }

        public void apply(Object o, String path, String id, String ppage) { }

        public void apply(WorkingObject o, String path, String id, String ppage) { }
    };
}
