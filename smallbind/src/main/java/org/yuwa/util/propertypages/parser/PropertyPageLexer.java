/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: PropertyPageLexer.java $
 * created by Owner May 3, 2006
 */
package org.yuwa.util.propertypages.parser;

import org.yuwa.util.namevalueparser.NameValueParserException;
import org.yuwa.util.namevalueparser.Peeler;

/**
 * @author Owner
 * 
 */
public class PropertyPageLexer
{
    private final Peeler peeler;


    public PropertyPageLexer(Peeler peeler)
    {
        this.peeler = peeler;
    }


    public boolean idDelimiter() throws NameValueParserException
    {
        return this.character('@');
    }


    public boolean baseDelimiter() throws NameValueParserException
    {
        return this.character(':');
    }


    public boolean nameValueSeparator() throws NameValueParserException
    {
        return this.character(':');
    }


    public boolean startPage() throws NameValueParserException
    {
        return this.character('{');
    }


    public boolean endPage() throws NameValueParserException
    {
        return this.character('}');
    }


    private boolean character(char c) throws NameValueParserException
    {
        boolean ok = false;

        this.resetValue();

        if ((char) this.getC() == c)
        {
            this.take();
            ok = true;
        }

        return ok;
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // delegate methods
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    public String getValue()
    {
        return this.peeler.getValue();
    }


    private int getC()
    {
        return this.peeler.getC();
    }


    private void resetValue()
    {
        this.peeler.resetValue();
    }


    public void take() throws NameValueParserException
    {
        this.peeler.take();
    }
}
