/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: SimplePropertyPages.java $
 * created by Owner Apr 27, 2006
 */
package org.yuwa.util.propertypages;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.yuwa.util.namevalueparser.NameListParser;
import org.yuwa.util.namevalueparser.NameValueParserException;
import org.yuwa.util.workingobject.WorkingObject;
import org.yuwa.util.workingobject.WorkingObjectObject;

/**
 * @author Owner
 * 
 */
public class SimplePropertyPages implements PropertyPages
{
    protected Logger log = Logger.getLogger(getClass());

    /** pages mapped by name */
    private Map<String, PropertyPage> pagesByKey = new HashMap<String, PropertyPage>();


    /**
     * apply with an object and a ppages string
     * 
     * apply all the property pages that apply, in order, to this object
     */
    public void apply(Object o)
    {
        try
        {
            WorkingObject wip = new WorkingObjectObject(o);
            String id = wip.getId();
            String pp = wip.getPPages();
            this.applyPages(wip, null, id, pp);
        }
        catch (Throwable t)
        {
            throw new PropertyPagesException("failed to apply", t);
        }
    }

    /**
     * apply to the given object with the given id & property page string
     */
    public void apply(Object o, String id, String pp)
    {
        try
        {
            WorkingObject wip = new WorkingObjectObject(o);
            this.applyPages(wip, null, id, pp);
        }
        catch (Throwable t)
        {
            throw new PropertyPagesException("failed to apply", t);
        }
    }

    /**
     * apply using the given ppages string instead of using introspection
     * 
     * @param o
     * @param pp
     */
    public void apply(Object o, String path, String id, String pp)
    {
        WorkingObject wip = new WorkingObjectObject(o);
        this.apply(wip,path,id,pp);
    }

    /**
     * do the same when we already have a working object
     */
    public void apply(WorkingObject wip, String path, String id, String pp)
    {
        try
        {
            this.applyPages(wip, path, id, pp);
        }
        catch (Throwable t)
        {
            throw new PropertyPagesException("failed to apply", t);
        }
    }


    /**
     * @param wip
     * @param pp
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NameValueParserException
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private void applyPages(WorkingObject wip, String path, String id, String pp) throws IllegalAccessException, InvocationTargetException,
            NameValueParserException, Exception
    {
        List<String> ppages = Collections.EMPTY_LIST;

        if (pp != null)
        {
            NameListParser parser = new NameListParser(pp);
            parser.parse();
            ppages = parser.getList();
        }

        List<String> pageKeys = this.findPageNames(wip.getType(), path, ppages, id);

        for (String key : pageKeys)
        {
            PropertyPage page = (PropertyPage) this.pagesByKey.get(key);
            if (page != null)
            {
                this.applyBasePages(wip, page);
                page.apply(wip);
            }
        }
    }


    /**
     * figure out all the pages we need to apply
     * 
     * @param o
     * @return a list of page names in the order to apply them
     */
    private List<String> findPageNames(String className, String path, List<String> ppages, String id) throws Exception
    {
        List<String> keys = new ArrayList<String>();

        // 1) the class name goes first and gets a free ride
        this.addName(keys, className);

        if (id != null && id.length() > 0) this.addName(keys, className + "@" + id);

        // hm... iterate through implemented interfaces?

        // hm... look at annotations?

        // 2) iterate through the page names and add each name, and name@id if
        // valid
        for (String pagename : ppages)
        {
            this.addName(keys, pagename);

            if (id != null && id.length() > 0) this.addName(keys, pagename + "@" + id);
        }
        
        // 2.5) add the path, raw, and then the path@id
        if ( path != null && path.length() > 0 )
        {
            this.addName(keys,path);
            if ( id != null && id.length() > 0 ) this.addName(keys,path + "@" + id);
        }

        // 3) finally add just the id if valid
        if (id != null && id.length() > 0) this.addName(keys, "@" + id);

        return keys;
    }


    /**
     * @param keys
     * @param used
     * @param key
     */
    private void addName(List<String> keys, String key)
    {
        // if it's there, take it out & move it up the list
        if (keys.contains(key)) keys.remove(key);

        keys.add(key);
    }


    /**
     * @param page
     * @param o
     */
    private void applyBasePages(WorkingObject wip, PropertyPage page)
    {
        List<String> basePages = page.getBasePages();
        for (String basePageName : basePages)
        {
            this.applyOnly(wip, basePageName);
        }
    }


    /**
     * apply the named page to this object
     */
    private void applyOnly(WorkingObject wip, String name)
    {
        PropertyPage page = (PropertyPage) this.pagesByKey.get(name);
        if (page != null)
        {
            page.apply(wip);
        }
    }


    public Map<String, PropertyPage> getPagesByKey()
    {
        return this.pagesByKey;
    }


    public void setPagesByKey(Map<String, PropertyPage> pagesByName)
    {
        this.pagesByKey = pagesByName;
    }


    public void add(PropertyPage page)
    {
        this.pagesByKey.put(page.key(), page);
    }

}
