/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: PropertyPage.java $
 * created by Owner Apr 27, 2006
 */
package org.yuwa.util.propertypages;

import java.util.Collections;
import java.util.List;

import org.yuwa.util.workingobject.WorkingObject;


/**
 * @author Owner
 *
 */
public interface PropertyPage
{

    /**
     * @return the name for this page
     */
    String getName();

    /**
     * @return the ID for this page
     */
    String getID();


    /**
     * @return the key for this page
     */
    String key();

    /**
     * @return the list of base page names for this page
     */
    List<String> getBasePages();
    
    /**
     * apply this propery to the given object
     *
     * does not verify that the object matches against the name,
     * it just applies the properties
     * 
     * @param o
     */
    void apply(Object o) throws PropertyPagesException;
    
    /**
     * apply this propery to the given working object
     *
     * does not verify that the object matches against the name,
     * it just applies the properties
     * 
     * @param o
     */
    void apply(WorkingObject wip) throws PropertyPagesException;

    
    /**
     * a null object in case we need to hold an object reference
     */
    PropertyPage NO_PROPERTY_PAGE = new PropertyPage() {

        public String getName() { return ""; }
        
        public String getID() { return ""; }

        public String key() { return ""; }

        public void apply(Object o) throws PropertyPagesException { }

        public void apply(WorkingObject wip) throws PropertyPagesException { }
        
        @SuppressWarnings("unchecked")
        public List<String> getBasePages()
        {
            return Collections.EMPTY_LIST;
        }
        
    };
}
