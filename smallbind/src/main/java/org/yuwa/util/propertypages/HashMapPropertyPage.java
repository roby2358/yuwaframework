/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: PropertyPage.java $
 * created by Owner Apr 27, 2006
 */
package org.yuwa.util.propertypages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.yuwa.util.workingobject.WorkingObject;
import org.yuwa.util.workingobject.WorkingObjectObject;

/**
 * @author Owner
 * 
 */
public class HashMapPropertyPage implements PropertyPage
{

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // attributes
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    private String name;
    private String id;
    private List<String> basePages = new ArrayList<String>();
    private Map<String, String> propertiesByName = new HashMap<String, String>();


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // implementation
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * create a working object & apply properties
     * 
     * @param wip
     * @throws PropertyPagesException
     * @see org.yuwa.util.propertypages.PropertyPage#apply(java.lang.Object)
     */
    public void apply(Object o) throws PropertyPagesException
    {
        WorkingObject wip = new WorkingObjectObject(o);
        this.apply(wip);
    }

    /**
     * apply to a working object
     * 
     * @param wip
     * @throws PropertyPagesException
     */
    public void apply(WorkingObject wip) throws PropertyPagesException
    {
        try
        {
            for (String key : this.propertiesByName.keySet())
            {
                String value = this.propertiesByName.get(key);
                wip.set(key, value);
            }
        }
        catch (PropertyPagesException ppe)
        {
            throw ppe;
        }
        catch (Throwable t)
        {
            throw new PropertyPagesException("failed to apply properties", t);
        }
    }

    public String key()
    {
        StringBuilder key = new StringBuilder();

        if (this.name != null && this.name.length() > 0)
        {
            key.append(this.name);
        }

        if (this.id != null && this.id.length() > 0)
        {
            key.append('@');
            key.append(this.id);
        }

        return key.toString();
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    public List<String> getBasePages()
    {
        return this.basePages;
    }


    public void setBasePages(List<String> basePages)
    {
        this.basePages = basePages;
    }


    public String getName()
    {
        return this.name;
    }


    public void setName(String name)
    {
        this.name = name;
    }


    public String getID()
    {
        return this.id;
    }


    public void setID(String id)
    {
        this.id = id;
    }


    public Map<String, String> getPropertiesByName()
    {
        return this.propertiesByName;
    }


    public void setPropertiesByName(Map<String, String> propertiesByName)
    {
        this.propertiesByName = propertiesByName;
    }

}
