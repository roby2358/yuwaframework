/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: PropertyPageParser.java $
 * created by Owner May 3, 2006
 */
package org.yuwa.util.propertypages.parser;

import org.yuwa.util.namevalueparser.NameValueLexer;
import org.yuwa.util.namevalueparser.NameValueParserException;
import org.yuwa.util.namevalueparser.Peeler;
import org.yuwa.util.propertypages.HashMapPropertyPage;
import org.yuwa.util.propertypages.PropertyPages;
import org.yuwa.util.propertypages.SimplePropertyPages;

/**
 * @author Owner
 * 
 */
public class PropertyPageParser
{
    private final Peeler peeler;
    private final SimplePropertyPages propertyPages;
    private final NameValueLexer nameValueLexer;
    private final PropertyPageLexer propertyPageLexer;


    public PropertyPageParser(Peeler p) throws NameValueParserException
    {
        this.peeler = p;
        this.nameValueLexer = new NameValueLexer(p);
        this.propertyPageLexer = new PropertyPageLexer(p);
        this.propertyPages = new SimplePropertyPages();

    }


    public void parse() throws NameValueParserException
    {
        while (!this.peeler.done())
        {
            HashMapPropertyPage page = this.pageNameID();
            
            if ( page != null)
            {
                this.nameValueLexer.whiteSpace();

                this.baseNames(page);

                this.nameValueLexer.whiteSpace();

                if (!this.propertyPageLexer.startPage()) throw new NameValueParserException("expected start page \"{\"");

                this.nameValues(page);

                this.nameValueLexer.whiteSpace();

                if (!this.propertyPageLexer.endPage()) throw new NameValueParserException("expected end page \"}\"");
                
                // add in the page we built
                this.propertyPages.add(page);
            }
        }
    }


    /**
     * @param page 
     * @throws NameValueParserException
     */
    private HashMapPropertyPage pageNameID() throws NameValueParserException
    {
        HashMapPropertyPage page = null;
        String pageName = "";
        String pageID = "";

        this.nameValueLexer.whiteSpace();
        if (this.nameValueLexer.path())
        {
            pageName = this.nameValueLexer.getValue();
        }

        if (!this.propertyPageLexer.idDelimiter())
        {
            // nop... next
        }
        else if (!this.nameValueLexer.value())
        {
            throw new NameValueParserException("expected page id after \"@\"");
        }
        else
        {
            pageID = this.nameValueLexer.getValue();
        }
        
        if ( pageName.length() > 0 || pageID.length() > 0 )
        {
            page = new HashMapPropertyPage();
            page.setName(pageName);
            page.setID(pageID);
        }
        
        return page;
    }


    /**
     * @param page 
     * @throws NameValueParserException
     */
    private void baseNames(HashMapPropertyPage page) throws NameValueParserException
    {
        if (this.propertyPageLexer.baseDelimiter())
        {
            boolean ok = true;

            while (ok)
            {
                this.nameValueLexer.whiteSpace();

                ok = this.nameValueLexer.value();

                if (ok)
                {
                    String name = this.nameValueLexer.getValue();
                    page.getBasePages().add( name );
                }
            }
        }
    }


    /**
     * @param page 
     * @throws NameValueParserException
     */
    private void nameValues(HashMapPropertyPage page) throws NameValueParserException
    {
        boolean ok = true;

        while (ok)
        {
            this.nameValueLexer.whiteSpace();

            ok = nameValue(page);
        }
    }


    /**
     * @param page 
     * @param ok
     * @return
     * @throws NameValueParserException
     */
    private boolean nameValue(HashMapPropertyPage page) throws NameValueParserException
    {
        boolean ok = false;

        if (this.nameValueLexer.value())
        {
            ok = true;

            String name = this.nameValueLexer.getValue();

            this.nameValueLexer.whiteSpace();

            if (!this.nameValueLexer.nameValueSeparator())
                throw new NameValueParserException("expected name value separator \":\"");

            this.nameValueLexer.whiteSpace();

            if (!this.nameValueLexer.string()) throw new NameValueParserException("expected property value");
            String value = this.nameValueLexer.getValue();

            this.nameValueLexer.whiteSpace();

            if (!this.nameValueLexer.nameValueTerminator()) throw new NameValueParserException("expected value terminator \";\"");
            
            page.getPropertiesByName().put(name,value);
        }

        return ok;
    }


    public PropertyPages getPropertyPages()
    {
        return this.propertyPages;
    }

}
