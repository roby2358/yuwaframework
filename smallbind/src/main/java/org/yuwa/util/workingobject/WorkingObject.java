/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.workingobject;

import java.lang.reflect.InvocationTargetException;


public interface WorkingObject
{
    /**
     * create a working object for the given type
     * 
     * @param type
     * @return a working object
     */
    WorkingObject create(String type);
    
    /**
     * set the value for the named property
     * 
     * @param property
     * @param value
     * @return true if successful
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     */
    boolean set(String property, Object value) throws java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException;

    /**
     * get the value for the named property
     *  
     * @param property
     * @return the value for the named property
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     */
    Object get(String property) throws java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException;

    /**
     * get the id value of this object
     * 
     * @return the id value
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    String getId() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException;

    /**
     * get the property pages value
     * 
     * @return the ppages value
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    String getPPages() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException;

    /**
     * get the type of the underlying object
     * 
     * @return the type of the underlying object
     */
    String getType();
    
    /**
     * return the object underlying this working object
     * <p>
     * may return null if there is no underlying object, and the implementation
     * abstracts the properties (e.g. as a Map)
     * 
     * @return the underlying object
     */
    Object getObject();

    /**
     * get an alternate id for when the id is missing
     * 
     * @return the alternate id
     */
    String getAltId();

    /**
     * supply an alternate id for when the id is missing
     * 
     * @param altId
     */
    void setAltId(String altId);

    /**
     * get the class maps object for this class
     * 
     * @return the class maps object
     */
    ClassMaps getClassMaps();
}
