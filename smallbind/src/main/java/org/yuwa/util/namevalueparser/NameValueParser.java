/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: Parser.java $
 * created by Owner Apr 29, 2006
 */
package org.yuwa.util.namevalueparser;

import java.util.HashMap;
import java.util.Map;

/**
 * usage is
 * 
 * @author Owner
 */
public class NameValueParser
{
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // attributes & properties
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    private final Peeler peeler;
    private final NameValueLexer lexer;
    private final Map<String, String> map = new HashMap<String, String>();

    private final NameValueParser THIS = this;


    public Map<String, String> getMap()
    {
        return this.map;
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // constructor
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * constructor starts with a string
     */
    public NameValueParser(String s) throws NameValueParserException
    {
        this.peeler = new StringPeeler(s);
        this.lexer = new NameValueLexer(this.peeler);
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // implementation
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    public void parse() throws NameValueParserException
    {
        while (!this.done())
        {
            this.lexer.whiteSpace();

            if ( this.lexer.nameValueTerminator() ) {
                // ignore & loop
            }
            else if (this.lexer.quotedValue() || this.lexer.path())
            {
                String name = this.getValue();

                this.lexer.whiteSpace();
                if (!this.lexer.nameValueSeparator()) throw new NameValueParserException("expected separator");

                this.lexer.whiteSpace();
                if (!this.lexer.string()) throw new NameValueParserException("expected string");
                String value = this.getValue();

                this.map.put(name, value);

                this.lexer.whiteSpace();
                if (!this.lexer.nameValueTerminator()) throw new NameValueParserException("expected terminator");
            }
        }
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // delegate methods
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    private boolean done()
    {
        return this.peeler.done();
    }


    private String getValue()
    {
        return this.peeler.getValue();
    }


    /**
     * the tester object exposes some methods we dont usually want to expose
     * 
     * @author Owner
     * 
     */
    public class Tester
    {

        public String getValue()
        {
            return THIS.getValue();
        }

    }

}
