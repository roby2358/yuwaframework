/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: NameValueLexer.java $
 * created by Owner May 1, 2006
 */
package org.yuwa.util.namevalueparser;

/**
 * @author Owner
 * 
 */
public class NameValueLexer
{
    
    public static final String VALUE_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.$";
    public static final String PATH_CHARACTERS = "/\\";
    public static final String URI_CHARACTERS = "/\\@:#";

    private final Peeler peeler;


    /**
     * constructor takes a peeler
     * 
     * @param p
     * @throws NameValueParserException
     */
    public NameValueLexer(Peeler p) throws NameValueParserException
    {
        this.peeler = p;
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // parse out separator & terminator
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    public boolean nameValueSeparator() throws NameValueParserException
    {
        boolean ok = false;
        
        this.resetValue();

        if ((char) this.getC() == ':')
        {
            ok = true;
            this.take();
        }
        
        return ok;
    }


    public boolean nameValueTerminator() throws NameValueParserException
    {
        boolean ok = false;
        
        this.resetValue();

        if ((char) this.getC() == ';')
        {
            ok = true;
            this.take();
        }
        
        return ok;
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // parse out a string, quoted or not
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    public boolean string() throws NameValueParserException
    {
        boolean ok = false;
        
        if (this.isQuote((char) this.getC()))
            ok = this.quotedValue();
        else
            ok = this.value();
        
        return ok;
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // parse out a quoted value
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    public boolean quotedValue() throws NameValueParserException
    {
        boolean ok = true;
        
        this.resetValue();

        // munch the opening quote
        if (this.isQuote((char) this.getC())) this.next();
        else ok = false;

        while ( ok && !done() && !this.isQuote((char) this.getC()))
        {
            if ((char) this.getC() == '\\')
            {
                // munch escape character & take the next one literally
                this.next();
                if (!this.done()) this.take();
            }
            else if ((char) this.getC() == '\r')
            {
                // munch CRs
                this.next();
            }
            else
            {
                this.take();
            }

        }

        // munch the closing quote
        if (this.isQuote((char) this.getC())) this.next();
        else ok = false;

        return ok;
    }


    /**
     * @return
     */
    public boolean isQuote(char c)
    {
        return (c == '"' || c == '\'');
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // parse out a value
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    public boolean value() throws NameValueParserException
    {
        boolean ok = false;
        
        this.resetValue();

        while (this.isValueCharacter((char) this.getC()))
        {
            this.take();
            ok = true;
        }
        
        return ok;
    }

    /**
     * @param c
     * @return
     */
    public boolean isValueCharacter(char c)
    {
        boolean ok = VALUE_CHARACTERS.indexOf(c) > -1;

        return ok;
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // parse out a path
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    public boolean path() throws NameValueParserException
    {
        boolean ok = false;
        
        this.resetValue();

        while (this.isPathCharacter((char) this.getC()))
        {
            this.take();
            ok = true;
        }
        
        return ok;
    }

    
    /**
     * 
     * @param c
     * @return true if this is a valid character for a uri
     */
    public boolean isPathCharacter(char c)
    {
        boolean ok = false;

        if ( PATH_CHARACTERS.indexOf(c) > -1 ) {
            ok = true;
        }
        else {
            ok = this.isValueCharacter(c);
        }
        
        return ok;
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // parse out a uri
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    public boolean uri() throws NameValueParserException
    {
        boolean ok = false;
        
        this.resetValue();

        while (this.isUriCharacter((char) this.getC()))
        {
            this.take();
            ok = true;
        }
        
        return ok;
    }

    
    /**
     * 
     * @param c
     * @return true if this is a valid character for a uri
     */
    public boolean isUriCharacter(char c)
    {
        boolean ok = false;

        if ( URI_CHARACTERS.indexOf(c) > -1 ) {
            ok = true;
        }
        else {
            ok = this.isValueCharacter(c);
        }
        
        return ok;
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // parse out whitespace
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    public boolean whiteSpace() throws NameValueParserException
    {
        boolean ok = false;
        
        this.resetValue();

        while (this.isWhitespace((char) this.getC()))
        {
            this.take();
            ok = true;
        }

        return ok;
    }


    /**
     * @param c
     * @return
     */
    public boolean isWhitespace(char c)
    {
        return (c == ' ' || c == '\n' || c == '\t' || c == '\r');
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // delegate methods
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    public String getValue()
    {
        return this.peeler.getValue();
    }


    private boolean done()
    {
        return this.peeler.done();
    }


    private int getC()
    {
        return this.peeler.getC();
    }


    private void next() throws NameValueParserException
    {
        this.peeler.next();
    }


    private void resetValue()
    {
        this.peeler.resetValue();
    }


    public void take() throws NameValueParserException
    {
        this.peeler.take();
    }

}
