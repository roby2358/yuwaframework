/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: NameListParser.java $
 * created by Owner May 3, 2006
 */
package org.yuwa.util.namevalueparser;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Owner
 * 
 */
public class NameListParser
{
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // attributes & properties
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    private final Peeler peeler;
    private final NameValueLexer lexer;
    private final List<String> list = new ArrayList<String>();

    private boolean done = false;


    public List<String> getList()
    {
        return this.list;
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // constructor
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * constructor starts with a string
     */
    public NameListParser(String s) throws NameValueParserException
    {
        this.peeler = new StringPeeler(s);
        this.lexer = new NameValueLexer(this.peeler);
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // implementation
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    public void parse() throws NameValueParserException
    {
        while (!this.done())
        {
            this.lexer.whiteSpace();
            this.lexer.string();

            String name = this.getValue();

            if (name.length() == 0)
            {
                this.done = true;
            }
            else
            {
                this.list.add(name);
            }
        }
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // delegate methods
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    private boolean done()
    {
        return (this.done || this.peeler.done() );
    }


    private String getValue()
    {
        return this.peeler.getValue();
    }

}
