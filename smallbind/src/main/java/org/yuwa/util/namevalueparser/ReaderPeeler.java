/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: Peeler.java $
 * created by Owner May 1, 2006
 */
package org.yuwa.util.namevalueparser;

import java.io.Reader;

/**
 * @author Owner
 * 
 */
public class ReaderPeeler implements Peeler
{
    private final Reader reader;
    private StringBuilder value = new StringBuilder();
    private int c;


    /**
     * constructor takes a string & sets up the reader
     * 
     * @param source
     * @throws NameValueParserException
     */
    public ReaderPeeler(Reader source) throws NameValueParserException
    {
        this.reader = source;
        this.next();
    }


    /**
     * reset the current value
     *
     */
    public void resetValue()
    {
        this.value.delete(0, this.value.length());
    }


    /**
     * consume the current character; save it in the value, and get the next
     * 
     * @throws NameValueParserException
     */
    public void take() throws NameValueParserException
    {
        this.value.append((char) this.c);
        this.next();
    }


    /**
     * get the next character
     * 
     * @throws NameValueParserException
     */
    public void next() throws NameValueParserException
    {
        try
        {
            this.c = this.reader.read();
        }
        catch (Throwable t)
        {
            throw new NameValueParserException("failed to read", t);
        }
    }


    /**
     * @return true if no more input
     */
    public boolean done()
    {
        return (this.c < 0);
    }


    /**
     * @return the current character
     */
    public int getC()
    {
        return this.c;
    }


    /**
     * @return the current value
     */
    public String getValue()
    {
        return this.value.toString();
    }
}
