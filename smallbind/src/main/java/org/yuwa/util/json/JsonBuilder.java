/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.json;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.yuwa.util.workingobject.WorkingObjectObject;


/**
 * A utility class for converting Java object structures to JSON for
 * transmitting to an AJAX-driven web page
 * 
 * <p>
 * for information on JSON, see http://www.json.org/
 * 
 * <p>
 * This class is intended to bundle up a single object in the form of name/value
 * or name/list pairs. Each value is just a string, created with the object's
 * toString method.
 * 
 * <p>
 * To have finer control of the value, the client can generate it's own string
 * and put that in with the name. Also, if the value is a JsonBuilder itself,
 * then its ToString method will encode the data as a string and use that for a
 * value. So to get nested JSON heirarchies, just use nested instances of
 * JsonBuilders.
 * 
 * <p>
 * Aug 13, 2007
 * 
 * @author royoung
 */
public class JsonBuilder
{
    /** our logger */
    protected Logger log = Logger.getLogger(getClass());

    /** a list of names -- can't be a hash, because we want to preserve order (tho I'm not sure that matters) */
    private List<String> names = new ArrayList<String>();

    /** a list of the objects to go with the names */
    private List<Object> values = new ArrayList<Object>();

    /** a default array of types we consider to be scalar */
    private String[] defaultScalars = new String[] {
            "java.lang.String",
            "java.lang.Integer",
            "java.lang.Boolean",
            "java.lang.Long",
            "java.lang.Double",
            "java.lang.Float",
            "int",
            "boolean",
            "long",
            "double",
            "float"
    };

    /** a set of types that we consider scalar */
    private Set<String> scalars = new HashSet<String>(Arrays.asList(defaultScalars));

    /** a switch to include public fields */
    private boolean includeFields = false;

    /** add newlines to the output for readability? */
    private boolean newlines = false;

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // constructors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * default, no-arg constructor
     */
    public JsonBuilder()
    {
    }


    /**
     * for convenience, a builder that encodes the public properties of o
     * 
     * @param o
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
     */
    public JsonBuilder(Object o)
    throws IllegalAccessException, InvocationTargetException
    {
        this.put(o);
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // implementation
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * put a name/value pair
     *
     * @param name
     * @param value
     */
    public void put(String name, Object value)
    {
        this.names.add(name);
        this.values.add(value);
    }

    /**
     * add the given object to a named list
     * 
     * @param name
     * @param o
     */
    @SuppressWarnings("unchecked")
    public void add(String name, Object o)
    {
        int i = this.names.indexOf(name);
        if ( i < 0 )
        {
            if ( log.isDebugEnabled() )
                log.debug("********* new list " + i);
            this.names.add(name);
            List<Object> list = new ArrayList<Object>();
            list.add(o);
            this.values.add(list);
        }
        else
        {
            if ( log.isDebugEnabled() )
                log.debug("********* got list " + i);
            List<Object> list = (List<Object>) this.values.get(i);
            list.add(o);
        }
    }

    /**
     * add an empty list; not required, but handy if the list may be empty
     * 
     * @param name
     */
    public void addEmptyList(String name)
    {
        int i = this.names.indexOf(name);
        if ( i < 0 )
        {
            if ( log.isDebugEnabled() )
                log.debug("********* new list " + i);
            this.names.add(name);
            List<Object> list = new ArrayList<Object>();
            this.values.add(list);
        }
    }


    /**
     * uses reflection to grab properties from the object and add them
     * 
     * @param o
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
     */
    public void put(Object o)
    throws IllegalAccessException, InvocationTargetException
    {
        WorkingObjectObject wo = new WorkingObjectObject(o);

        Set<String> properties = wo.getClassMaps().getTypesByPropertyName().keySet();
        TreeSet<String> sortedProperties = new TreeSet<String>(properties);

        for ( String property : sortedProperties )
        {
            String type = wo.getClassMaps().getTypesByPropertyName().get(property).getName();
            Object value = wo.get(property);

            // TODO: allow maps as well, treat as Object
            // TODO: flag to traverse object structure?

            if ( ! this.includeFields
                    && ! wo.getClassMaps().getGettersByName().containsKey(property) )
            {
                if ( log.isDebugEnabled() )
                    log.debug(" ... exclude field " + property);
            }
            else if ( value instanceof List<?> )
            {
                for ( Object oo : (List<?>)value )
                {
                    // TODO: won't handle lists of objects
                    if ( log.isDebugEnabled() )
                        log.debug(property + " ... " + oo.toString());
                    this.add(property, oo.toString());
                }
            }
//          TODO: we know it's an array, but how do we iterate over it generically?
//          else if ( value.getClass().isArray() )
//          {
//          Array a = new Array(value);

//          for ( Object op : Arrays.asList(value) )
//          {
//          // TODO: won't handle arrays of objects
//          if ( log.isDebugEnabled() )
//          log.debug(p + " ... " + op.toString());
//          this.add(p, op.toString());
//          }
//          }
            else if ( ! this.scalars.contains( type ) )
            {
                if ( log.isDebugEnabled() )
                    log.debug("... not scalar; skipping " + property + " " + type );
                // TODO: add traverse flag?
            }
            else
            {
                if ( log.isDebugEnabled() )
                    log.debug("... scalar " + property + " = " + type);
                this.put( property, wo.get(property) );
            }

        }
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // render as JSON string
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * render this object as a JSON string
     * 
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        StringBuilder builder = new StringBuilder("{");

        for ( int i = 0 ; i < this.names.size() ; i++ )
        {
            String name = this.names.get(i);
            Object value = this.values.get(i);

            // TODO: treat a Map as an object

            if ( value instanceof List )
            {
                this.buildArray(builder,name, (List<?>)value);
            }
            else
            {
                this.buildNameValue(builder,name,value);
            }
        }

        builder.append("}");

        return builder.toString();
    }

    /**
     * this method for creating JSON that is intended to be embeded in a page at
     * load time. Apostrophes and quotes need extra layers of escaping so
     * javascript doesn't get confused.
     * 
     * <p>
     * Apostrophes and alreadhy-escaped quotes are rendered as \\\' and \\\".
     * It's possible to do this using JSTL tags, but it's pretty hideous. This
     * method does that work in advance.
     * 
     * <p>
     * The return value is an "eval" statement that can be embedded directly
     * into javascript, such as var s = <c:out value='${json.embedded}' escapeXml='false' />
     * 
     * @return a json string encoded so it can be embeded directly in a page's
     *         javascript
     */
    public String getEmbedded()
    {
        StringBuilder eval = new StringBuilder("eval('(")
        .append( this.toString()
                .replaceAll("'","\\\\\\\\\\\\'")
                .replaceAll("\\\\\"","\\\\\\\\\\\\\"") )
                .append(")')");

        return eval.toString();
    }

    /**
     * build the JSON for a name/value pair
     * @param builder
     * @param name
     * @param value
     */
    private void buildNameValue(StringBuilder builder, String name, Object value)
    {
        if ( builder.length() > 1 ) builder.append( this.comma() );
        builder.append(name);
        builder.append(":");
        this.buildValue(builder, value);
    }


    /**
     * @param builder
     * @param value
     */
    private void buildValue(StringBuilder builder, Object value) {

        if ( value == null )
        {
            builder.append("null");
        }
        else if ( ! this.scalars.contains(value.getClass().getName()) )
        {
            builder.append( value.toString() );
        }
        else
        {
            builder.append("\"");
            builder.append(value.toString().replaceAll("\"","\\\\\""));
            builder.append("\"");
        }
    }


    /**
     * build the JSON for an array of items
     * 
     * @param builder
     * @param name
     * @param value
     */
    private void buildArray(StringBuilder builder, String name, List<?> value)
    {
        if ( builder.length() > 1 ) builder.append( this.comma());

        builder.append(name);
        builder.append(":[");
        for ( int i = 0 ; i < value.size() ; i++ )
        {
            if ( i > 0 ) builder.append(",");

            Object object = value.get(i);

            // TODO: awrk... lists of lists

            this.buildValue(builder, object);
        }
        builder.append("]");
    }


    /**
     * @return
     */
    private String comma() {

        return (this.newlines) ? ",\n" : ",";
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    /**
     * @return the scalars
     */
    public Set<String> getScalars() { return this.scalars; }

    /**
     * @return the includeFields
     */
    public boolean isIncludeFields() { return this.includeFields; }

    /**
     * @param includeFields the includeFields to set
     */
    public void setIncludeFields(boolean includeFields) { this.includeFields = includeFields; }

    /**
     * @param newlines the newlines to set
     */
    public void setNewlines(boolean newlines) { this.newlines = newlines; }

}
