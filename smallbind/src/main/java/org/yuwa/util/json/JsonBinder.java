/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.json;

import java.net.BindException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

/**
 * the binder is the flip side of the builder; read arbitrary JSON and render it
 * as an object structure
 * 
 * <p>
 * the default behavior is to bind everything to Maps and Lists of String; the fancy stuff will come later
 * 
 * @author Owner
 */
public class JsonBinder
{
    /** our logger */
    protected Logger log = Logger.getLogger(getClass());

    /** we're past the end of the input */
    public static final int EOF = -1;

    private static final String CHARS_END_OF_STRING = " \n\r\t:,{}[]";

    /** the source string */
    private String input;

    /** the index of where we are in the string */
    private int index;

    /** the object we're building */
    private Map<String,Object> wip = new TreeMap<String,Object>();

    /**
     * default constructor
     *
     */
    public JsonBinder()
    {
        // nop
    }

    /**
     * constructor takes a string
     * 
     * @param s
     */
    public JsonBinder(String s)
    {
        this.input = s;
    }

    /**
     * parse the input string into a top-level object
     * 
     * @return the top-level object
     * @throws BindException
     */
    public Map<String,?> parse()
    throws BindException
    {
        if ( this.parseDataStart() ) /* ok */ ;
        
        if ( ! this.parseObjectStart() ) throw this.buildException("missing object start");

        this.parseObject(this.wip); 

        if ( this.parseDataEnd() ) /* ok */ ;
        
        return this.wip;
    }

    /**
     * a holder to hold an object
     * 
     * @author Owner
     */
    public static class SomeObject
    {
        public Object object;
    }

    /**
     * parse the input and return a Map (Object)
     * 
     * @return a map of the input
     */
    public boolean parseObject(Map<String,Object> wip)
    throws BindException
    {
        boolean ok = true;

        this.noEndHere();
        
        if ( this.parseObjectEnd() )
        {
            // done
        }
        else
        {
            boolean done = false;

            while ( ! done )
            {
                this.parseNameValue(wip);

                if ( ! this.parseSeparator() ) done = true;
            }

            if ( ! this.parseObjectEnd() ) throw this.buildException("object end missing");
        }

        return ok;
    }

    /**
     * parse out a name value pair
     * 
     * @param wip
     * @throws BindException
     */
    private boolean parseNameValue(Map<String, Object> wip) throws BindException
    {
        boolean ok = true;
        
        StringBuilder name = new StringBuilder();

        if ( ! this.parseString(name) ) throw this.buildException("name expected");

        if ( ! this.parsePairSeparator() ) throw this.buildException("name value pair separator expected");

        // TODO: parse out null as special value
        
        SomeObject value = new SomeObject();
        if ( ! this.parseSomeValue(value) ) throw this.buildException("value expected");

        wip.put(name.toString(), value.object);
        
        return ok;
    }

    /**
     * parse the input into the given list
     * 
     * @param list
     */
    public boolean parseList(List<Object> list)
    throws BindException
    {
        boolean ok = true;

        this.noEndHere();

        if ( this.parseListEnd() )
        {
            // done
        }
        else
        {
            boolean done = false;

            while ( ! done )
            {
                SomeObject value = new SomeObject();

                this.parseSomeValue(value);

                list.add(value.object);

                if ( ! this.parseSeparator() ) done = true;
            }

            if ( ! this.parseListEnd() ) throw this.buildException("list end missing");
        }

        return ok;
    }

    /**
     * parse a value, which could be a string, list or map
     * 
     * @param value
     * @return
     * @throws BindException
     */
    private boolean parseSomeValue(SomeObject value) throws BindException
    {
        boolean ok = true;

        if ( this.parseObjectStart() )
        {
            Map<String,Object> o = new TreeMap<String,Object>();

            this.parseObject(o);

            value.object = o;
        }
        else if ( this.parseListStart() )
        {
            List<Object> list = new ArrayList<Object>();

            this.parseList(list);

            value.object = list;
        }
        else if ( this.isEndOfInput() )
        {
            throw this.buildException("value expected");
        }
        else
        {
            StringBuilder valueString = new StringBuilder();

            if ( ! this.parseString(valueString) ) throw this.buildException("value expected");

            value.object = valueString.toString();
        }

        return ok;
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // parsing methods
    //
    // these are public for testing, but not exposed through the interface ;)
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * parse a string, fail if invalid input
     * 
     * @param value
     * @return true if we parsed the string
     * @throws BindException
     */
    public boolean parseString(StringBuilder value)
    throws BindException
    {
        boolean ok = true;

        this.parseWhitespace();

        if ( this.isEndOfInput() ) throw this.buildException("unexpected end of input");
        else if ( this.parseObjectStart() ) throw this.buildException("unexpected start of object");
        else if ( this.parseListStart() ) throw this.buildException("unexpected start of list");
        else if ( this.parseQuotedString(value) ) { /* done */ }
        else if ( this.parseUnquotedString(value) ) { /* done */ }
        else
        {
            ok = false;
        }

        return ok;
    }


    /**
     * parse a string that starts with a quote
     * 
     * <p>
     * ends when we hit the close quote ... everything else goes in
     * 
     * @param value
     * @return
     * @throws BindException
     */
    public boolean parseQuotedString(StringBuilder value)
    throws BindException
    {
        boolean ok = true;

        if ( ! this.parseQuote() )
        {
            ok = false;
        }
        else
        {
            boolean done = false;

            do
            {
                if ( this.isEndOfInput() ) throw this.buildException("unexpected end of file in string");
                else if ( this.parseQuote() ) done = true;
                else if ( this.parseLiteralQuote() ) value.append('"');
                else
                {
                    value.append((char)this.current());
                    this.consume();
                }
            }
            while ( ! done );
        }
        return ok;
    }


    /**
     * ends when we hit whitespace or a special character
     * 
     * @param value
     * @return
     * @throws BindException
     */
    public boolean parseUnquotedString(StringBuilder value)
    throws BindException
    {
        boolean done = false;

        do
        {
            if ( this.isEndOfInput() ) done = true;
            else if ( this.isEndOfString() ) done = true;
            else if ( this.parseLiteralQuote() ) value.append('"');
            else
            {
                value.append((char)this.current());
                this.consume();
            }
        }
        while ( ! done );

        return true;
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // low-level parsing methods
    //
    // these are public for testing, but not exposed through the interface ;)
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * return true if we hit a character that can't be a part of an unquoted string
     * 
     * <p>
     * don't consume the character, because it's probably part of the next token
     */
    public boolean isEndOfString()
    {
        char c = (char)this.current();

        int i = CHARS_END_OF_STRING.indexOf(c);

        return i > -1;
    }

    /**
     * return true if we're at the end of file
     */
    public boolean isEndOfInput()
    {
        return this.index >= this.input.length();
    }

    /**
     * we're still expecting more...
     * 
     * @throws BindException
     */
    private void noEndHere() throws BindException
    {
        if ( this.isEndOfInput() ) throw this.buildException("unexpected end of input in object");
    }

    /**
     * munch as much whitespace as we can
     * 
     * @return true if we parsed some whitespace
     */
    public boolean parseWhitespace()
    {
        int count = 0;

        while ( (char)this.current(count) == ' '
            || (char)this.current(count) == '\n'
                || (char)this.current(count) == '\r'
                    || (char)this.current(count) == '\t'
        ) count++;

        if ( count > 0 ) this.consume(count);

        return count > 0;
    }

    public boolean parseQuote()
    {
        return this.parseChar('"');
    }

    public boolean parseLiteralQuote()
    {
        final boolean ok = ( (char)this.current() == '\\' && (char)this.current(1) == '\"' );

        if ( ok ) this.consume(2);

        return ok;
    }

    public boolean parseSeparator()
    {
        this.parseWhitespace();
        return this.parseChar(',');
    }


    private boolean parsePairSeparator()
    {
        this.parseWhitespace();
        return this.parseChar(':');
    }

    public boolean parseStringStart()
    {
        this.parseWhitespace();
        return this.parseChar('"');
    }

    public boolean parseStringEnd()
    {
        return this.parseChar('"');
    }

    public boolean parseListStart()
    {
        this.parseWhitespace();
        return this.parseChar('[');
    }

    public boolean parseListEnd()
    {
        this.parseWhitespace();
        return this.parseChar(']');
    }

    public boolean parseDataStart()
    {
        this.parseWhitespace();
        return this.parseChar('(');
    }

    public boolean parseDataEnd()
    {
        this.parseWhitespace();
        return this.parseChar(')');
    }

    public boolean parseObjectStart()
    {
        this.parseWhitespace();
        return this.parseChar('{');
    }

    public boolean parseObjectEnd()
    {
        this.parseWhitespace();
        return this.parseChar('}');
    }

    public boolean parseChar(char c)
    {
        final boolean ok = ( this.current() == c );

        if ( ok ) this.consume();

        return ok;
    }

    /**
     * the current character; return -1 if we're past the end
     */
    public int current()
    {
        return this.current(0);
    }

    /**
     * the current character plus n; return -1 if we're past the end
     */
    public int current(int n)
    {
        int at = this.index + n;

        final int c = (at >= this.input.length() )
        ? EOF : this.input.charAt(at);

        return c;
    }

    /**
     * consume the current character
     */
    public void consume() { this.consume(1); }

    /**
     * consume a number of characters
     * 
     * @param n
     */
    public void consume(int n) { this.index += n; }

    /**
     * build an exception with some data on hand
     * 
     * @param message
     * @return
     */
    private BindException buildException(String message)
    {
        int start = this.index - 10;
        int end = this.index;
        if ( start < 0 ) start = 0;

        String hint = this.input.substring(start,end);

        String fullMessage = message + " at " + this.index + " ...\"" + hint + "\"";

        return new BindException(fullMessage );
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // utilities for the client
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * return the given object as a map of objects, or null if not possible
     */
    @SuppressWarnings("unchecked")
    public Map<String,Object> asObject(Object o)
    {
        final Map<String,Object> map;
        
        if ( o != null && o instanceof Map )
        {
            map = (Map<String,Object>)o;
        }
        else
        {
            map = null;
        }
        
        return map;
    }
    

    /**
     * return the object as a list of objects, or null if not possible
     * 
     * @param o
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object> asList(Object o)
    {
        final List<Object> list;
        
        if ( o != null && o instanceof List )
        {
            list = (List<Object>)o;
        }
        else
        {
            list = null;
        }
        
        return list;
    }
    
    /**
     * return the object as a string value, or null if not possible
     * 
     * @param o
     * @return the object as a string value
     */
    public String asValue(Object o)
    {
        final String string;

        if ( o == null )
        {
            string = null;
        }
        else if ( o instanceof Map )
        {
            string = null;
        }
        else if ( o instanceof List )
        {
            string = null;
        }
        else
        {
            string = o.toString();
        }
        
        return string;
    }
    
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    public void setInput(String input) { this.input = input; }

    public String getInput() { return input; }

}
