
simple { value:simple; }

part1 { part1:woo; }
part2 { part2:wa; }
parts : part1 part2 { }

org.yuwa.util.propertypages.test.Test3
{
	value1:class;
	value2:class;
	value3:class;
	value4:class;
	value5:class;
	value6:class;
	value7:class;
	value8:class;
}

org.yuwa.util.propertypages.test.Test3@test3
{
	value2:'class@id';
	value3:'class@id';
	value4:'class@id';
	value5:'class@id';
}

stacked1 : dep1
{
	value3:stacked1;
}

stacked2
{
	value4:stacked2;
	value5:stacked2;
	value6:stacked2;
}

stacked3
{
	value5:stacked3;
}

stacked2@test3 {
	value6:'stacked2@test3';
}

dep1
{
	value7:dep1;
	value8:dep1;
}

dep1@test3
{
	value8:'dep1@test3';
}

a {
	value1:a;
}

b : a {
	value2:b;
}

b@test3 : a {
	value4:'b@test3';
}

c : b {
	value3:c;
}

@pong { value:pong; }

/zing/zot { value:path; }

/zing/zot@woop { value:pathAndID; }
