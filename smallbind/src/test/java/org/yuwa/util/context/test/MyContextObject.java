/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/*
 * Created on Mar 21, 2005
 * 
 * *$Id: MyContextObject.java,v 1.1.1.1 2005/09/17 06:42:16 Owner Exp $
 *
 *@author Owner
 */
package org.yuwa.util.context.test;

import org.yuwa.util.context.BasicFactory;
import org.yuwa.util.context.ContextCreationException;
import org.yuwa.util.context.ContextObject;

/**
 * @author Owner
 */
public class MyContextObject implements ContextObject
{
    public String value = "Value for " + this.getClass().getName();
    private BasicFactory factory = new BasicFactory(this);
    
    public Object create(Class<? extends Object> c) throws ContextCreationException
    {
        return this.factory.create(c);
    }

}
