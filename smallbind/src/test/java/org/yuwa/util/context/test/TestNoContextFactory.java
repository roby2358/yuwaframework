/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/*
 * Created on Mar 21, 2005
 * 
 * *$Id: TestBasicFactory.java,v 1.1.1.1 2005/09/17 06:42:16 Owner Exp $
 *
 *@author Owner
 */
package org.yuwa.util.context.test;


import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.util.context.NoContextFactory;

/**
 * @author Owner
 */
public class TestNoContextFactory extends TestCase
{
    MyContextObject context;
    NoContextFactory noContextFactory;
    MyFactory customFactory;


    protected void setUp()
    {
        this.context = new MyContextObject();
        this.noContextFactory = new NoContextFactory();
    }


    public void testFactoryCreateContextInitialized()
    {
        MyContextInitialized o = (MyContextInitialized) this.noContextFactory.create(this.context, MyContextInitialized.class);

        Assert.assertNotNull(o);
        Assert.assertEquals(this.context, o.context);
        Assert.assertEquals(context.value, o.value);
    }


    public void testFactoryCreateEmptyInitialized()
    {
        MyEmptyInitialized o = (MyEmptyInitialized) this.noContextFactory.create(this.context, MyEmptyInitialized.class);

        Assert.assertNotNull(o);
        Assert.assertEquals("value for org.yuwa.util.context.test.MyEmptyInitialized", o.value);
    }


    public void testFactoryCreateGeneric()
    {
        MyGenericObject o = (MyGenericObject) this.noContextFactory.create(this.context, MyGenericObject.class);

        Assert.assertNotNull(o);
        Assert.assertEquals("value for org.yuwa.util.context.test.MyGenericObject", o.value);
    }


    public void testCreateContextInitialized()
    {
        MyContextInitialized o = (MyContextInitialized) this.noContextFactory.create(this.context, MyContextInitialized.class);

        Assert.assertNotNull(o);
        Assert.assertEquals(this.context, o.context);
        Assert.assertEquals(context.value, o.value);
    }


    public void testCreateEmptyInitialized()
    {
        MyEmptyInitialized o = (MyEmptyInitialized) this.noContextFactory.create(this.context, MyEmptyInitialized.class);

        Assert.assertNotNull(o);
        Assert.assertEquals("value for org.yuwa.util.context.test.MyEmptyInitialized", o.value);
    }


    public void testCreateGeneric()
    {
        MyGenericObject o = (MyGenericObject) this.noContextFactory.create(this.context, MyGenericObject.class);

        Assert.assertNotNull(o);
        Assert.assertEquals("value for org.yuwa.util.context.test.MyGenericObject", o.value);
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // run the same tests, without the context argument as a parameter
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    public void testFactoryCreateContextInitialized2()
    {
        MyContextInitialized o = (MyContextInitialized) this.noContextFactory.create(MyContextInitialized.class);

        Assert.assertNotNull(o);
    }


    public void testFactoryCreateEmptyInitialized2()
    {
        MyEmptyInitialized o = (MyEmptyInitialized) this.noContextFactory.create(MyEmptyInitialized.class);

        Assert.assertNotNull(o);
        Assert.assertEquals("value for org.yuwa.util.context.test.MyEmptyInitialized", o.value);
    }


    public void testFactoryCreateGeneric2()
    {
        MyGenericObject o = (MyGenericObject) this.noContextFactory.create(MyGenericObject.class);

        Assert.assertNotNull(o);
        Assert.assertEquals("value for org.yuwa.util.context.test.MyGenericObject", o.value);
    }


    public void testCreateContextInitialized2()
    {
        MyContextInitialized o = (MyContextInitialized) this.noContextFactory.create(MyContextInitialized.class);

        Assert.assertNotNull(o);
    }


    public void testCreateEmptyInitialized2()
    {
        MyEmptyInitialized o = (MyEmptyInitialized) this.noContextFactory.create(MyEmptyInitialized.class);

        Assert.assertNotNull(o);
        Assert.assertEquals("value for org.yuwa.util.context.test.MyEmptyInitialized", o.value);
    }


    public void testCreateGeneric2()
    {
        MyGenericObject o = (MyGenericObject) this.noContextFactory.create(MyGenericObject.class);

        Assert.assertNotNull(o);
        Assert.assertEquals("value for org.yuwa.util.context.test.MyGenericObject", o.value);
    }

}
