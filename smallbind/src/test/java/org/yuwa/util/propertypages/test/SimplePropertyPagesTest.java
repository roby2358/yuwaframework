/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: SimplePropertyPagesTest.java $
 * created by Owner May 4, 2006
 */
package org.yuwa.util.propertypages.test;

import java.io.InputStream;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.util.namevalueparser.InputStreamPeeler;
import org.yuwa.util.propertypages.HashMapPropertyPage;
import org.yuwa.util.propertypages.SimplePropertyPages;
import org.yuwa.util.propertypages.parser.PropertyPageParser;

/**
 * @author Owner
 * 
 */
public class SimplePropertyPagesTest extends TestCase
{
    private SimplePropertyPages pages;


    public void setUp() throws Exception
    {
        super.setUp();

        InputStream stream = this.getClass().getResourceAsStream("test1.pp");
        InputStreamPeeler peeler = new InputStreamPeeler(stream);
        PropertyPageParser parser = new PropertyPageParser(peeler);

        parser.parse();
        this.pages = (SimplePropertyPages) parser.getPropertyPages();
    }


    /*
     * Test method for
     * 'org.yuwa.util.propertypages.SimplePropertyPages.apply(Object)'
     */
    public void testApplyObject()
    {
        Test2 test = new Test2();
        
        this.pages.apply(test);
        
        Assert.assertEquals("woo",test.getPart1());
        Assert.assertEquals("wa",test.getPart2());
    }


    /*
     * Test method for
     * 'org.yuwa.util.propertypages.SimplePropertyPages.apply(Object)'
     */
    public void testApplyObjectClassString()
    {
        Test3 test = new Test3();
        
        this.pages.apply(test,"test3","");
        
        Assert.assertEquals("class",test.getValue1());
        Assert.assertEquals("class@id",test.getValue2());
    }


    /*
     * Test method for
     * 'org.yuwa.util.propertypages.SimplePropertyPages.apply(Object)'
     */
    public void testApplyObjectStacked()
    {
        Test3 test = new Test3();
        
        this.pages.apply(test);
        
        this.assertTest3OK(test);
    }


    /*
     * Test method for
     * 'org.yuwa.util.propertypages.SimplePropertyPages.apply(Object)'
     */
    public void testApplyObjectStackedWParams()
    {
        Test3 test = new Test3();
        
        this.pages.apply(test,test.getId(),test.getPpages());
        
        this.assertTest3OK(test);
    }


    /**
     * @param test
     */
    private void assertTest3OK(Test3 test)
    {
        Assert.assertEquals("class",test.getValue1());
        Assert.assertEquals("class@id",test.getValue2());
        Assert.assertEquals("stacked1",test.getValue3());
        Assert.assertEquals("stacked2",test.getValue4());
        Assert.assertEquals("stacked3",test.getValue5());
        Assert.assertEquals("stacked2@test3",test.getValue6());
        Assert.assertEquals("dep1",test.getValue7());
        Assert.assertEquals("dep1",test.getValue8());
    }


    /*
     * Test method for
     * 'org.yuwa.util.propertypages.SimplePropertyPages.apply(Object, String)'
     */
    public void testApplyObjectString()
    {
        Test1 test = new Test1();
        
        this.pages.apply(test,"","simple");
        
        Assert.assertEquals("simple",test.getValue());
    }


    /*
     * Test method for
     * 'org.yuwa.util.propertypages.SimplePropertyPages.add(PropertyPage)'
     */
    public void testAddNameID()
    {
        HashMapPropertyPage page = new HashMapPropertyPage();

        page.setID("id");
        page.setName("name");
        pages.add(page);

        Assert.assertEquals(page, pages.getPagesByKey().get(page.key()));
    }


    /*
     * Test method for
     * 'org.yuwa.util.propertypages.SimplePropertyPages.add(PropertyPage)'
     */
    public void testAddName()
    {
        HashMapPropertyPage page = new HashMapPropertyPage();

        page.setName("name");
        pages.add(page);

        Assert.assertEquals(page, pages.getPagesByKey().get(page.key()));
    }


    /*
     * Test method for
     * 'org.yuwa.util.propertypages.SimplePropertyPages.add(PropertyPage)'
     */
    public void testAddID()
    {
        HashMapPropertyPage page = new HashMapPropertyPage();

        page.setID("id");
        pages.add(page);

        Assert.assertEquals(page, pages.getPagesByKey().get(page.key()));
    }
    
    /**
     * verifuy that all base pages are not recursed, and not applied with @id
     *
     */
    public void testNoRecursion()
    {
        Test3 test = new Test3();
        this.pages.apply(test,"test3","c");
        
        Assert.assertEquals("class",test.getValue1());
        Assert.assertEquals("b",test.getValue2());
        Assert.assertEquals("c",test.getValue3());
        Assert.assertEquals("class@id",test.getValue4());
        Assert.assertEquals("class@id",test.getValue5());
        Assert.assertEquals("class",test.getValue6());
        Assert.assertEquals("class",test.getValue7());
        Assert.assertEquals("class",test.getValue8());
    }
    
    public void testNoPath()
    {
        Test1 test = new Test1();
        this.pages.apply(test,null,"pong","");
        
        Assert.assertEquals("pong",test.getValue());
    }
    
    public void testPath()
    {
        Test1 test = new Test1();
        this.pages.apply(test,"/zing/zot","","");
        
        Assert.assertEquals("path",test.getValue());
    }
    
    public void testPathAndID()
    {
        Test1 test = new Test1();
        this.pages.apply(test,"/zing/zot","woop","");
        
        Assert.assertEquals("pathAndID",test.getValue());
    }

}
