/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: PropertyPageParserTest.java $
 * created by Owner May 4, 2006
 */
package org.yuwa.util.propertypages.parser.test;

import java.io.InputStream;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.util.namevalueparser.InputStreamPeeler;
import org.yuwa.util.propertypages.HashMapPropertyPage;
import org.yuwa.util.propertypages.SimplePropertyPages;
import org.yuwa.util.propertypages.parser.PropertyPageParser;

/**
 * @author Owner
 *
 */
public class PropertyPageParserTest extends TestCase
{
    public PropertyPageParser parser;
    public SimplePropertyPages pages;

    public void setUp() throws Exception
    {
        super.setUp();

        InputStream stream = this.getClass().getResourceAsStream("test1.pp");
        InputStreamPeeler peeler = new InputStreamPeeler(stream);

        this.parser = new PropertyPageParser(peeler);
        this.parser.parse();
        this.pages = (SimplePropertyPages) this.parser.getPropertyPages();
    }

    /*
     * Test method for 'org.yuwa.util.propertypages.parser.PropertyPageParser.parse()'
     */
    public void testParseBing()
    {
        HashMapPropertyPage page = (HashMapPropertyPage) this.pages.getPagesByKey().get("bing");
        
        Assert.assertEquals("bing",page.getName());
        Assert.assertEquals("",page.getID());
        Assert.assertEquals(0,page.getBasePages().size());
        
        Assert.assertEquals("a", page.getPropertiesByName().get("a"));
        Assert.assertEquals("c.c", page.getPropertiesByName().get("b.b"));
    }

    /*
     * Test method for 'org.yuwa.util.propertypages.parser.PropertyPageParser.parse()'
     */
    public void testParseAtWhoop()
    {
        HashMapPropertyPage page = (HashMapPropertyPage) this.pages.getPagesByKey().get("@whoop");
        
        Assert.assertEquals("",page.getName());
        Assert.assertEquals("whoop",page.getID());
        Assert.assertEquals(0,page.getBasePages().size());
        
        Assert.assertEquals("z", page.getPropertiesByName().get("z"));
    }

    /*
     * Test method for 'org.yuwa.util.propertypages.parser.PropertyPageParser.parse()'
     */
    public void testParseBoopAtBong()
    {
        HashMapPropertyPage page = (HashMapPropertyPage) this.pages.getPagesByKey().get("boop@bong");
        
        Assert.assertEquals("boop",page.getName());
        Assert.assertEquals("bong",page.getID());
        Assert.assertEquals(0,page.getBasePages().size());
        
        Assert.assertEquals("b", page.getPropertiesByName().get("b"));
    }

    /*
     * Test method for 'org.yuwa.util.propertypages.parser.PropertyPageParser.parse()'
     */
    public void testParseZoop()
    {
        HashMapPropertyPage page = (HashMapPropertyPage) this.pages.getPagesByKey().get("zoop");
        
        Assert.assertEquals("zoop",page.getName());
        Assert.assertEquals("",page.getID());
        Assert.assertEquals(4,page.getBasePages().size());
        
        Assert.assertEquals("zip", page.getBasePages().get(0));
        Assert.assertEquals("zot", page.getBasePages().get(1));
        Assert.assertEquals("zig", page.getBasePages().get(2));
        Assert.assertEquals("zag", page.getBasePages().get(3));
        
        Assert.assertEquals("b", page.getPropertiesByName().get("a"));
    }

    /*
     * Test method for 'org.yuwa.util.propertypages.parser.PropertyPageParser.parse()'
     */
    public void testParsePongo()
    {
        HashMapPropertyPage page = (HashMapPropertyPage) this.pages.getPagesByKey().get("pongo");
        
        Assert.assertEquals("pongo",page.getName());
        Assert.assertEquals("",page.getID());
        Assert.assertEquals(0,page.getBasePages().size());
        Assert.assertEquals(0,page.getPropertiesByName().keySet().size());
    }

    public void testParseZing1()
    {
        HashMapPropertyPage page = (HashMapPropertyPage) this.pages.getPagesByKey().get("/zing/zot");
        
        Assert.assertNotNull(page);
        Assert.assertEquals("/zing/zot",page.getName());
        Assert.assertEquals("",page.getID());
        Assert.assertEquals(0,page.getBasePages().size());
        Assert.assertEquals(0,page.getPropertiesByName().keySet().size());
    }

    public void testParseZing2()
    {
        HashMapPropertyPage page = (HashMapPropertyPage) this.pages.getPagesByKey().get("/zing/zot@woop");
        
        Assert.assertNotNull(page);
        Assert.assertEquals("/zing/zot",page.getName());
        Assert.assertEquals("woop",page.getID());
        Assert.assertEquals(0,page.getBasePages().size());
        Assert.assertEquals(0,page.getPropertiesByName().keySet().size());
    }
    
}
