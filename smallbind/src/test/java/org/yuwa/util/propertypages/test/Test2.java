/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: Test2.java $
 * created by Owner May 4, 2006
 */
package org.yuwa.util.propertypages.test;

/**
 * @author Owner
 * 
 */
public class Test2
{
    public String ppages = "parts";
    public String part1 = "what";
    public String part2 = "what";


    public String getPpages()
    {
        return this.ppages;
    }


    public void setPpages(String ppages)
    {
        this.ppages = ppages;
    }


    public String getPart1()
    {
        return this.part1;
    }


    public void setPart1(String part1)
    {
        this.part1 = part1;
    }


    public String getPart2()
    {
        return this.part2;
    }


    public void setPart2(String part2)
    {
        this.part2 = part2;
    }

}
