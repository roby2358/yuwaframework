/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: Test3.java $
 * created by Owner May 4, 2006
 */
package org.yuwa.util.propertypages.test;

/**
 * @author Owner
 * 
 */
public class Test3
{
    String id = "test3";
    String ppages = "stacked1 stacked3 stacked2 stacked3";
    String value1 = "what";
    String value2 = "what";
    String value3 = "what";
    String value4 = "what";
    String value5 = "what";
    String value6 = "what";
    String value7 = "what";
    String value8 = "what";


    public String getId()
    {
        return this.id;
    }


    public void setId(String id)
    {
        this.id = id;
    }


    public String getPpages()
    {
        return this.ppages;
    }


    public void setPpages(String ppages)
    {
        this.ppages = ppages;
    }


    public String getValue1()
    {
        return this.value1;
    }


    public void setValue1(String value1)
    {
        this.value1 = value1;
    }


    public String getValue2()
    {
        return this.value2;
    }


    public void setValue2(String value2)
    {
        this.value2 = value2;
    }


    public String getValue3()
    {
        return this.value3;
    }


    public void setValue3(String value3)
    {
        this.value3 = value3;
    }


    public String getValue4()
    {
        return this.value4;
    }


    public void setValue4(String value4)
    {
        this.value4 = value4;
    }


    public String getValue5()
    {
        return this.value5;
    }


    public void setValue5(String value5)
    {
        this.value5 = value5;
    }


    public String getValue6()
    {
        return this.value6;
    }


    public void setValue6(String value6)
    {
        this.value6 = value6;
    }


    public String getValue7()
    {
        return this.value7;
    }


    public void setValue7(String value7)
    {
        this.value7 = value7;
    }


    public String getValue8()
    {
        return this.value8;
    }


    public void setValue8(String value8)
    {
        this.value8 = value8;
    }


}
