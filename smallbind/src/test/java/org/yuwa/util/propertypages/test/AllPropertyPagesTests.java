/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/*
 * Created on Mar 16, 2005
 * 
 * *$Id: TestSmallBind.java,v 1.2 2005/09/23 19:04:35 Owner Exp $
 *
 *@author Owner
 */
package org.yuwa.util.propertypages.test;

import junit.framework.TestSuite;

import org.yuwa.util.namevalueparser.test.NameListParserTest;
import org.yuwa.util.namevalueparser.test.NameValueLexerTest;
import org.yuwa.util.namevalueparser.test.NameValueParserTest;
import org.yuwa.util.namevalueparser.test.StringPeelerTest;
import org.yuwa.util.propertypages.parser.test.PropertyPageLexerTest;
import org.yuwa.util.propertypages.parser.test.PropertyPageParserTest;
import org.yuwa.util.uriresolver.test.SimpleUriResolverTest;

/**
 * @author Owner
 */
public class AllPropertyPagesTests extends TestSuite
{
    
    public static TestSuite suite() { return new AllPropertyPagesTests(); }
    
    public AllPropertyPagesTests()
    {
        super();
        this.addTestSuite( HashMapPropertyPageTest.class );
        this.addTestSuite( StringPeelerTest.class );
        this.addTestSuite( NameValueLexerTest.class );
        this.addTestSuite( NameValueParserTest.class );
        this.addTestSuite( NameListParserTest.class );
        this.addTestSuite( PropertyPageLexerTest.class );
        this.addTestSuite( PropertyPageParserTest.class );
        this.addTestSuite( SimplePropertyPagesTest.class );
        this.addTestSuite( SimpleUriResolverTest.class );
    }
}
