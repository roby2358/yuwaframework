/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: HashMapPropertyPageTest.java $
 * created by Owner Apr 27, 2006
 */
package org.yuwa.util.propertypages.test;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.util.propertypages.HashMapPropertyPage;

/**
 * @author Owner
 *
 */
public class HashMapPropertyPageTest extends TestCase
{
    public HashMapPropertyPage pp;

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        
        this.pp = new HashMapPropertyPage();
    }


    /*
     * Test method for 'org.yuwa.util.propertypages.HashMapPropertyPage.apply(Object)'
     */
    public void testApply()
    {
        Test1 t1 = new Test1();
        
        Map<String,String> map = this.pp.getPropertiesByName();
        
        map.put("name","zoop");
        map.put("count","1234");
        map.put("active","true");
        
        this.pp.apply(t1);
        
        Assert.assertEquals("zoop",t1.name);
        Assert.assertEquals(1234,t1.count);
        Assert.assertTrue(t1.active);
    }


    /*
     * Test method for 'org.yuwa.util.propertypages.HashMapPropertyPage.getFullName()'
     */
    public void testGetName()
    {
        String fullName = ".bing.bong.boop#whiz";
        this.pp.setName(fullName);
        
        Assert.assertEquals(fullName,this.pp.getName());
    }


    /*
     * Test method for 'org.yuwa.util.propertypages.HashMapPropertyPage.getPropertiesByName()'
     */
    public void testGetPropertiesByName()
    {
        HashMap<String,String> newMap = new HashMap<String,String>();
        this.pp.setPropertiesByName(newMap);
        
        Assert.assertTrue(newMap == this.pp.getPropertiesByName());
    }

    
    public void testKey1()
    {
        HashMapPropertyPage page = new HashMapPropertyPage();
        
        page.setName("bing");
        page.setID("bong");
        Assert.assertEquals("bing@bong",page.key());
    }

    
    public void testKey2()
    {
        HashMapPropertyPage page = new HashMapPropertyPage();
        
        page.setName("");
        page.setID("bong");
        Assert.assertEquals("@bong",page.key());
    }

    
    public void testKey3()
    {
        HashMapPropertyPage page = new HashMapPropertyPage();
        
        page.setName("bing");
        page.setID("");
        Assert.assertEquals("bing",page.key());
    }

    
    public void testKey4()
    {
        HashMapPropertyPage page = new HashMapPropertyPage();
        
        page.setName("");
        page.setID("");
        Assert.assertEquals("",page.key());
    }


    
    public void testKey2null()
    {
        HashMapPropertyPage page = new HashMapPropertyPage();
        
        page.setName(null);
        page.setID("bong");
        Assert.assertEquals("@bong",page.key());
    }

    
    public void testKey3null()
    {
        HashMapPropertyPage page = new HashMapPropertyPage();
        
        page.setName("bing");
        page.setID(null);
        Assert.assertEquals("bing",page.key());
    }

    
    public void testKey4null()
    {
        HashMapPropertyPage page = new HashMapPropertyPage();
        
        page.setName(null);
        page.setID(null);
        Assert.assertEquals("",page.key());
    }

}
