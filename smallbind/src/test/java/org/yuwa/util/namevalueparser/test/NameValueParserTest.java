/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: NameValueParserTest.java $
 * created by Owner Apr 29, 2006
 */
package org.yuwa.util.namevalueparser.test;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.util.namevalueparser.NameValueParser;

/**
 * @author Owner
 * 
 */
public class NameValueParserTest extends TestCase
{

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }


    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameValueParser.parse()'
     */
    public void testParse1() throws Exception
    {
        NameValueParser parser = new NameValueParser("abc-123_ABC:_xyz098ZYX;");

        parser.parse();
        Assert.assertEquals("_xyz098ZYX", parser.getMap().get("abc-123_ABC"));
    }


    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameValueParser.parse()'
     */
    public void testParse2() throws Exception
    {
        NameValueParser parser = new NameValueParser("abc:123;xyz:\"1 2\";");

        parser.parse();
        Assert.assertEquals("123", parser.getMap().get("abc"));
        Assert.assertEquals("1 2", parser.getMap().get("xyz"));
    }


    public void testParse3() throws Exception
    {
        NameValueParser parser = new NameValueParser("  \n\r  abc\n  :\n  123\n  ; xyz  :   \"1 2\";");

        parser.parse();
        Assert.assertEquals("123", parser.getMap().get("abc"));
        Assert.assertEquals("1 2", parser.getMap().get("xyz"));
    }


    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameValueParser.parse()'
     */
    public void testParse4() throws Exception
    {
        NameValueParser parser = new NameValueParser("\n\nabc-123_ABC\n\n:\n\n_xyz098ZYX\n\n;\n\n");

        parser.parse();
        Assert.assertEquals("_xyz098ZYX", parser.getMap().get("abc-123_ABC"));
    }


    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameValueParser.parse()'
     */
    public void testParse5() throws Exception
    {
        NameValueParser parser = new NameValueParser("\n/a/b.c:zing;\n\n\\d$e$f  : zot;\n\n");

        parser.parse();
        Assert.assertEquals("zing", parser.getMap().get("/a/b.c"));
        Assert.assertEquals("zot", parser.getMap().get("\\d$e$f"));
    }


    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameValueParser.parse()'
     */
    public void testParse6() throws Exception
    {
        NameValueParser parser = new NameValueParser("a:b; ; ;; ; c:d;\n\n\n\n;");

        parser.parse();
        Assert.assertEquals("b", parser.getMap().get("a"));
        Assert.assertEquals("d", parser.getMap().get("c"));
    }


    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameValueParser.parse()'
     */
    public void testParse7() throws Exception
    {
        NameValueParser parser = new NameValueParser("\n\na:b; ; ; ; ; c:d;\n\n\n\n;\n\n");

        parser.parse();
        Assert.assertEquals("b", parser.getMap().get("a"));
        Assert.assertEquals("d", parser.getMap().get("c"));
    }

}
