/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: NameValueLexerTest.java $
 * created by Owner May 1, 2006
 */
package org.yuwa.util.namevalueparser.test;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.util.namevalueparser.NameValueLexer;
import org.yuwa.util.namevalueparser.Peeler;
import org.yuwa.util.namevalueparser.StringPeeler;

/**
 * @author Owner
 *
 */
public class NameValueLexerTest extends TestCase
{
    private Peeler peeler;
    private NameValueLexer lexer;
    
    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }


    public void testNameValueSeparator() throws Exception
    {
        this.peeler = new StringPeeler(":");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.nameValueSeparator();
        Assert.assertTrue(ok);
        Assert.assertEquals(":", this.lexer.getValue());
    }


    public void testNameValueSeparatorNo() throws Exception
    {
        this.peeler = new StringPeeler("x");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.nameValueSeparator();
        Assert.assertFalse(ok);
        Assert.assertEquals(0, this.lexer.getValue().length());
    }


    public void testNameValueTerminator() throws Exception
    {
        this.peeler = new StringPeeler(";");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.nameValueTerminator();
        Assert.assertTrue(ok);
        Assert.assertEquals(";", this.lexer.getValue());
    }


    public void testNameValueTerminatorNo() throws Exception
    {
        this.peeler = new StringPeeler("x");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.nameValueTerminator();
        Assert.assertFalse(ok);
        Assert.assertEquals(0, this.lexer.getValue().length());
    }


    public void testString() throws Exception
    {
        this.peeler = new StringPeeler("a.b-c_d1234");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.string();
        Assert.assertTrue(ok);
        Assert.assertEquals("a.b-c_d1234", this.lexer.getValue());
    }


    public void testStringQuoted() throws Exception
    {
        this.peeler = new StringPeeler("\"a b c d\"");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.string();
        Assert.assertTrue(ok);
        Assert.assertEquals("a b c d", this.lexer.getValue());
    }


    public void testQuotedValue() throws Exception
    {
        this.peeler = new StringPeeler("\"a b c d\"");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.quotedValue();
        Assert.assertTrue(ok);
        Assert.assertEquals("a b c d", this.lexer.getValue());
    }


    public void testQuotedValueSquote() throws Exception
    {
        this.peeler = new StringPeeler("'a b c d'");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.quotedValue();
        Assert.assertTrue(ok);
        Assert.assertEquals("a b c d", this.lexer.getValue());
    }


    public void testQuotedValueEmbeddedStuff() throws Exception
    {
        this.peeler = new StringPeeler("\"a.a\\\"b\\\nc\\\\d\"");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.quotedValue();
        Assert.assertTrue(ok);
        Assert.assertEquals("a.a\"b\nc\\d", this.lexer.getValue());
    }


    public void testQuotedValueNo() throws Exception
    {
        this.peeler = new StringPeeler("a b c d");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.quotedValue();
        Assert.assertFalse(ok);
        Assert.assertEquals("", this.lexer.getValue());
    }


    public void testIsQuote(char c) throws Exception
    {
        this.peeler = new StringPeeler("!abc-ABC_123!");
        this.lexer = new NameValueLexer(peeler);

        Assert.assertTrue(this.lexer.isWhitespace('"'));
        Assert.assertTrue(this.lexer.isWhitespace('\''));

        Assert.assertFalse(this.lexer.isWhitespace('a'));
    }


    public void testValue() throws Exception
    {
        this.peeler = new StringPeeler("a.bc-ABC_123!");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.value();
        Assert.assertTrue(ok);
        Assert.assertEquals("a.bc-ABC_123", this.lexer.getValue());
    }


    public void testValueNo() throws Exception
    {
        this.peeler = new StringPeeler("!abc-ABC_123!");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.value();
        Assert.assertFalse(ok);
        Assert.assertEquals(0, this.lexer.getValue().length());
    }


    public void testPath() throws Exception
    {
        this.peeler = new StringPeeler("/a.bc/-ABC_1/23!");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.path();
        Assert.assertTrue(ok);
        Assert.assertEquals("/a.bc/-ABC_1/23", this.lexer.getValue());
    }


    public void testPathNo() throws Exception
    {
        this.peeler = new StringPeeler(":/ab/c-A/BC_1/23!");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.path();
        Assert.assertFalse(ok);
        Assert.assertEquals(0, this.lexer.getValue().length());
    }


    public void testUri() throws Exception
    {
        this.peeler = new StringPeeler("file:/a.bc/-ABC_1/23!");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.uri();
        Assert.assertTrue(ok);
        Assert.assertEquals("file:/a.bc/-ABC_1/23", this.lexer.getValue());
    }


    public void testUriNo() throws Exception
    {
        this.peeler = new StringPeeler("!/ab/c-A/BC_1/23!");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.uri();
        Assert.assertFalse(ok);
        Assert.assertEquals(0, this.lexer.getValue().length());
    }


    public void testIsValueCharacter() throws Exception
    {
        this.peeler = new StringPeeler("!abc-ABC_123!");
        this.lexer = new NameValueLexer(peeler);

        Assert.assertTrue(this.lexer.isValueCharacter('a'));
        Assert.assertTrue(this.lexer.isValueCharacter('z'));
        Assert.assertTrue(this.lexer.isValueCharacter('A'));
        Assert.assertTrue(this.lexer.isValueCharacter('Z'));
        Assert.assertTrue(this.lexer.isValueCharacter('0'));
        Assert.assertTrue(this.lexer.isValueCharacter('9'));
        Assert.assertTrue(this.lexer.isValueCharacter('-'));
        Assert.assertTrue(this.lexer.isValueCharacter('_'));
        Assert.assertTrue(this.lexer.isValueCharacter('.'));
        Assert.assertTrue(this.lexer.isValueCharacter('$'));

        Assert.assertFalse(this.lexer.isValueCharacter('!'));
        Assert.assertFalse(this.lexer.isValueCharacter(' '));
        Assert.assertFalse(this.lexer.isValueCharacter('/'));
        Assert.assertFalse(this.lexer.isValueCharacter(':'));
        Assert.assertFalse(this.lexer.isValueCharacter('@'));
        Assert.assertFalse(this.lexer.isValueCharacter('['));
        Assert.assertFalse(this.lexer.isValueCharacter('`'));
        Assert.assertFalse(this.lexer.isValueCharacter('{'));
    }


    public void testIsPathCharacter() throws Exception
    {
        this.peeler = new StringPeeler("!abc-ABC_123!");
        this.lexer = new NameValueLexer(peeler);

        Assert.assertTrue(this.lexer.isPathCharacter('a'));
        Assert.assertTrue(this.lexer.isPathCharacter('z'));
        Assert.assertTrue(this.lexer.isPathCharacter('A'));
        Assert.assertTrue(this.lexer.isPathCharacter('Z'));
        Assert.assertTrue(this.lexer.isPathCharacter('0'));
        Assert.assertTrue(this.lexer.isPathCharacter('9'));
        Assert.assertTrue(this.lexer.isPathCharacter('-'));
        Assert.assertTrue(this.lexer.isPathCharacter('_'));
        Assert.assertTrue(this.lexer.isPathCharacter('.'));
        Assert.assertTrue(this.lexer.isPathCharacter('/'));
        Assert.assertTrue(this.lexer.isPathCharacter('\\'));
        Assert.assertTrue(this.lexer.isPathCharacter('$'));

        Assert.assertFalse(this.lexer.isPathCharacter('!'));
        Assert.assertFalse(this.lexer.isPathCharacter(' '));
        Assert.assertFalse(this.lexer.isPathCharacter('['));
        Assert.assertFalse(this.lexer.isPathCharacter('`'));
        Assert.assertFalse(this.lexer.isPathCharacter('{'));
        Assert.assertFalse(this.lexer.isPathCharacter(':'));
        Assert.assertFalse(this.lexer.isPathCharacter('@'));
        Assert.assertFalse(this.lexer.isPathCharacter('#'));
    }


    public void testIsUriCharacter() throws Exception
    {
        this.peeler = new StringPeeler("!abc-ABC_123!");
        this.lexer = new NameValueLexer(peeler);

        Assert.assertTrue(this.lexer.isUriCharacter('a'));
        Assert.assertTrue(this.lexer.isUriCharacter('z'));
        Assert.assertTrue(this.lexer.isUriCharacter('A'));
        Assert.assertTrue(this.lexer.isUriCharacter('Z'));
        Assert.assertTrue(this.lexer.isUriCharacter('0'));
        Assert.assertTrue(this.lexer.isUriCharacter('9'));
        Assert.assertTrue(this.lexer.isUriCharacter('-'));
        Assert.assertTrue(this.lexer.isUriCharacter('_'));
        Assert.assertTrue(this.lexer.isUriCharacter('.'));
        Assert.assertTrue(this.lexer.isUriCharacter('/'));
        Assert.assertTrue(this.lexer.isUriCharacter('\\'));
        Assert.assertTrue(this.lexer.isUriCharacter(':'));
        Assert.assertTrue(this.lexer.isUriCharacter('@'));
        Assert.assertTrue(this.lexer.isUriCharacter('$'));
        Assert.assertTrue(this.lexer.isUriCharacter('#'));

        Assert.assertFalse(this.lexer.isUriCharacter('!'));
        Assert.assertFalse(this.lexer.isUriCharacter(' '));
        Assert.assertFalse(this.lexer.isUriCharacter('['));
        Assert.assertFalse(this.lexer.isUriCharacter('`'));
        Assert.assertFalse(this.lexer.isUriCharacter('{'));
    }


    public void testWhiteSpace() throws Exception
    {
        this.peeler = new StringPeeler("  \n  \r  \t  ");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.whiteSpace();
        Assert.assertTrue(ok);
        Assert.assertEquals("  \n  \r  \t  ", this.lexer.getValue());
    }


    public void testWhiteSpaceNo() throws Exception
    {
        this.peeler = new StringPeeler("!  \n  \r  \t  ");
        this.lexer = new NameValueLexer(peeler);

        boolean ok = this.lexer.whiteSpace();
        Assert.assertFalse(ok);
        Assert.assertEquals(0, this.lexer.getValue().length());
    }


    public void testIsWhitespace() throws Exception
    {
        this.peeler = new StringPeeler("!abc-ABC_123!");
        this.lexer = new NameValueLexer(peeler);

        Assert.assertTrue(this.lexer.isWhitespace(' '));
        Assert.assertTrue(this.lexer.isWhitespace('\n'));
        Assert.assertTrue(this.lexer.isWhitespace('\r'));
        Assert.assertTrue(this.lexer.isWhitespace('\t'));

        Assert.assertFalse(this.lexer.isWhitespace('a'));
    }
    
    public void testMultipleLexers() throws Exception
    {
        this.peeler = new StringPeeler("abc xyz '123' \"zoop\"");
        NameValueLexer lex1 = new NameValueLexer(this.peeler);
        NameValueLexer lex2 = new NameValueLexer(this.peeler);
        
        lex1.whiteSpace();
        lex1.string();
        Assert.assertEquals("abc",lex1.getValue());
        
        lex2.whiteSpace();
        lex2.string();
        Assert.assertEquals("xyz",lex2.getValue());
        
        lex1.whiteSpace();
        lex1.string();
        Assert.assertEquals("123",lex1.getValue());
        
        lex2.whiteSpace();
        lex2.string();
        Assert.assertEquals("zoop",lex1.getValue());
    }

}
