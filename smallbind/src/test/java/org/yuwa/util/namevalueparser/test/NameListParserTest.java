/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: NameListParserTest.java $
 * created by Owner May 3, 2006
 */
package org.yuwa.util.namevalueparser.test;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.util.namevalueparser.NameListParser;

/**
 * @author Owner
 *
 */
public class NameListParserTest extends TestCase
{

    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameListParser.NameListParser(String)'
     */
    public void testNameListParser() throws Exception
    {
        NameListParser parser = new NameListParser("a b c");
        parser.parse();
        Assert.assertEquals(3,parser.getList().size());
        Assert.assertEquals("a",parser.getList().get(0).toString());
        Assert.assertEquals("b",parser.getList().get(1).toString());
        Assert.assertEquals("c",parser.getList().get(2).toString());
    }

    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameListParser.NameListParser(String)'
     */
    public void testNameListParserWhitespace() throws Exception
    {
        NameListParser parser = new NameListParser("      a\n\n\nb\n\r\t\t\tc   ");
        parser.parse();
        Assert.assertEquals(3,parser.getList().size());
        Assert.assertEquals("a",parser.getList().get(0).toString());
        Assert.assertEquals("b",parser.getList().get(1).toString());
        Assert.assertEquals("c",parser.getList().get(2).toString());
    }

    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameListParser.NameListParser(String)'
     */
    public void testNameListParserQuotes() throws Exception
    {
        NameListParser parser = new NameListParser(" 'a' \"b\" c");
        parser.parse();
        Assert.assertEquals(3,parser.getList().size());
        Assert.assertEquals("a",parser.getList().get(0).toString());
        Assert.assertEquals("b",parser.getList().get(1).toString());
        Assert.assertEquals("c",parser.getList().get(2).toString());
    }

    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameListParser.NameListParser(String)'
     */
    public void testNameListParserFunkyChars() throws Exception
    {
        NameListParser parser = new NameListParser(" '\\'a\\\\' \"\\\"b\" c");
        parser.parse();
        Assert.assertEquals(3,parser.getList().size());
        Assert.assertEquals("'a\\",parser.getList().get(0).toString());
        Assert.assertEquals("\"b",parser.getList().get(1).toString());
        Assert.assertEquals("c",parser.getList().get(2).toString());
    }

    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameListParser.NameListParser(String)'
     */
    public void testNameListParserTerminator() throws Exception
    {
        NameListParser parser = new NameListParser("a b c!!!! d e f");
        parser.parse();
        Assert.assertEquals(3,parser.getList().size());
        Assert.assertEquals("a",parser.getList().get(0).toString());
        Assert.assertEquals("b",parser.getList().get(1).toString());
        Assert.assertEquals("c",parser.getList().get(2).toString());
    }

    /*
     * Test method for 'org.yuwa.util.namevalueparser.NameListParser.NameListParser(String)'
     */
    public void testNameListParserPeriods() throws Exception
    {
        NameListParser parser = new NameListParser("a.a b.b c.c!!!! d.d e.e f.f");
        parser.parse();
        Assert.assertEquals(3,parser.getList().size());
        Assert.assertEquals("a.a",parser.getList().get(0).toString());
        Assert.assertEquals("b.b",parser.getList().get(1).toString());
        Assert.assertEquals("c.c",parser.getList().get(2).toString());
    }

}
