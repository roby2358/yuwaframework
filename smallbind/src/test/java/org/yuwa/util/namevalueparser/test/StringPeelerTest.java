/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: PeelerTest.java $
 * created by Owner May 1, 2006
 */
package org.yuwa.util.namevalueparser.test;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.util.namevalueparser.Peeler;
import org.yuwa.util.namevalueparser.StringPeeler;

/**
 * @author Owner
 *
 */
public class StringPeelerTest extends TestCase
{

    private static final String SOURCE = "abc-ABC_123!";
    
    public Peeler peeler;
    
    /**
     * create a peeler to work with
     */
    public void setUp() throws Exception
    {
        this.peeler = new StringPeeler(SOURCE);
    }
    
    /*
     * Test method for 'org.yuwa.util.namevalueparser.Peeler.Peeler(String)'
     */
    public void testPeeler() throws Exception
    {
        Assert.assertEquals('a', (char)this.peeler.getC() );
    }


    /*
     * Test method for 'org.yuwa.util.namevalueparser.Peeler.done()'
     */
    public void testDone() throws Exception
    {
        for ( int i = 0 ; i < SOURCE.length() ; i++ ) {
            Assert.assertFalse(this.peeler.done());
            this.peeler.next();
        }
        Assert.assertTrue(this.peeler.done());
    }


    /*
     * Test method for 'org.yuwa.util.namevalueparser.Peeler.getC()'
     */
    public void testGetC()
    {
        Assert.assertEquals('a', (char)this.peeler.getC() );
    }


    /*
     * Test method for 'org.yuwa.util.namevalueparser.Peeler.getValue()'
     */
    public void testGetValue() throws Exception
    {
        peeler.take();
        Assert.assertEquals("a", peeler.getValue());
    }


    public void testResetValue() throws Exception
    {
        peeler.take();
        peeler.take();
        peeler.take();
        Assert.assertEquals("abc", peeler.getValue().toString());

        peeler.resetValue();
        Assert.assertEquals(0, peeler.getValue().length());
    }


    public void testTake(int c) throws Exception
    {
        peeler.take();
        Assert.assertEquals("a", peeler.getValue());
        Assert.assertEquals('b', (char) peeler.getC());

        peeler.take();
        Assert.assertEquals("ab", peeler.getValue());
        Assert.assertEquals('c', (char) peeler.getC());

        peeler.take();
        Assert.assertEquals("abc", peeler.getValue());
        Assert.assertEquals('-', (char) peeler.getC());
    }


    public void testNext() throws Exception
    {
        this.peeler = new StringPeeler("abc");

        Assert.assertEquals('a', (char) peeler.getC());
        peeler.next();
        Assert.assertEquals('b', (char) peeler.getC());
        peeler.next();
        Assert.assertEquals('c', (char) peeler.getC());
        peeler.next();
        Assert.assertEquals(-1, peeler.getC());
    }

}
