/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: SimpleUriResolverTest.java $
 * created by Owner May 4, 2006
 */
package org.yuwa.util.uriresolver.test;

import java.io.InputStream;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.util.uriresolver.SimpleUriResolver;
import org.yuwa.util.uriresolver.UriResolver;

/**
 * @author Owner
 *
 */
public class SimpleUriResolverTest extends TestCase
{
    public UriResolver resolver = new SimpleUriResolver();
    
    /*
     * Test method for 'org.yuwa.util.uriresolver.SimpleUriResolver.resolveAsStream(String)'
     */
    public void testResolveAsStream() throws Exception
    {
        InputStream stream = this.resolver.resolveAsStream("/org/yuwa/util/uriresolver/test/test1.txt");
        
        Assert.assertNotNull(stream);
        
        byte[] bytes = new byte[100];
        
        int n = stream.read(bytes);
        
        Assert.assertEquals(2,n);
        Assert.assertEquals(104,bytes[0]);
        Assert.assertEquals(105,bytes[1]);
    }

    public void testResolveAsStreamNo()
    {
        InputStream stream = this.resolver.resolveAsStream("/org/yuwa/util/uriresolver/test/test1x.txt");
        
        Assert.assertNull(stream);
    }

}
