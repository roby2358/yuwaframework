/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.json;

import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

public class JsonBinderAsATest extends TestCase
{
    public JsonBinder binder = new JsonBinder();
    
    protected void setUp() throws Exception
    {
        super.setUp();
    }


    public void testAsObject()
    throws Exception
    {
        this.binder.setInput("{info:{woo:wa}}");
        Map<String,?> o = this.binder.parse();
        
        Map<String,Object> info = this.binder.asObject( o.get("info") );
        assertNotNull(info);
        assertEquals( "wa",info.get("woo") );
    }


    public void testAsObjectList()
    throws Exception
    {
        this.binder.setInput("{info:[0,1,2]}");
        Map<String,?> o = this.binder.parse();
        
        Map<String,?> info = this.binder.asObject( o.get("info") );
        assertNull(info);
    }


    public void testAsObjectValue()
    throws Exception
    {
        this.binder.setInput("{info:woo}");
        Map<String,?> o = this.binder.parse();
        
        Map<String,?> info = this.binder.asObject( o.get("info") );
        assertNull(info);
    }


    public void testAsObjectNull()
    throws Exception
    {
        this.binder.setInput("{info:null}");
        Map<String,?> o = this.binder.parse();
        
        Map<String,Object> info = this.binder.asObject( o.get("info") );
        assertNull(info);
    }


    public void testAsList()
    throws Exception
    {
        this.binder.setInput("{info:[woo,wa]}");
        Map<String,?> o = this.binder.parse();
        
        List<?> info = this.binder.asList( o.get("info") );
        assertNotNull(info);
        assertEquals( "woo",info.get(0) );
    }


    public void testAsListObject()
    throws Exception
    {
        this.binder.setInput("{info:{woo:wa}}");
        Map<String,?> o = this.binder.parse();
        
        List<?> info = this.binder.asList( o.get("info") );
        assertNull(info);
    }


    public void testAsListNull()
    throws Exception
    {
        this.binder.setInput("{info:null}");
        Map<String,?> o = this.binder.parse();
        
        List<?> info = this.binder.asList( o.get("info") );
        assertNull(info);
    }


    public void testAsListValue()
    throws Exception
    {
        this.binder.setInput("{info:woo}");
        Map<String,?> o = this.binder.parse();
        
        List<?> info = this.binder.asList( o.get("info") );
        assertNull(info);
    }


    public void testAsValue()
    throws Exception
    {
        this.binder.setInput("{info:woo}");
        Map<String,?> o = this.binder.parse();
        
        String info = this.binder.asValue( o.get("info") );
        assertNotNull(info);
        assertEquals( "woo",info );
    }


    public void testAsValueObject()
    throws Exception
    {
        this.binder.setInput("{info:{woo:wa}}");
        Map<String,?> o = this.binder.parse();
        
        String info = this.binder.asValue( o.get("info") );
        assertNull(info);
    }


    public void testAsValueList()
    throws Exception
    {
        this.binder.setInput("{info:[woo,wa]}");
        Map<String,?> o = this.binder.parse();
        
        String info = this.binder.asValue( o.get("info") );
        assertNull(info);
    }


    public void testAsValueNull()
    throws Exception
    {
// TODO: need to parse out null as special value
//        this.binder.setInput("{info:null}");
//        Map<String,Object> o = this.binder.parse();
//        
//        String info = this.binder.asValue( o.get("info") );
//        assertNull(info);
    }

}
