/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.json;

import java.net.BindException;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

public class JsonBinderTest extends TestCase
{
    Logger log = Logger.getLogger(getClass());
    
    public JsonBinder binder = new JsonBinder();

    public void setUp() throws Exception
    {
    }

    public void testCurrentAndConsume()
    {
        this.binder.setInput("abc");
        assertEquals('a',(char)this.binder.current());

        this.binder.consume();
        assertEquals('b',(char)this.binder.current());

        this.binder.consume();
        assertEquals('c',(char)this.binder.current());
    }

    public void testCurrentNAndConsumeN()
    {
        this.binder.setInput("abcdefghijkl");
        assertEquals('a',(char)this.binder.current(0));
        assertEquals('b',(char)this.binder.current(1));
        assertEquals('c',(char)this.binder.current(2));

        this.binder.consume(2);
        assertEquals('c',(char)this.binder.current(0));
        assertEquals('d',(char)this.binder.current(1));
        assertEquals('e',(char)this.binder.current(2));
        assertEquals('f',(char)this.binder.current(3));

        this.binder.consume(3);
        assertEquals('f',(char)this.binder.current());
    }

    public void testParseChar()
    {
        this.binder.setInput("abcdef");

        assertTrue(this.binder.parseChar('a'));
        assertFalse(this.binder.parseChar('x'));
        assertTrue(this.binder.parseChar('b'));
        assertFalse(this.binder.parseChar('C'));
        assertTrue(this.binder.parseChar('c'));
        assertFalse(this.binder.parseChar('D'));
        assertTrue(this.binder.parseChar('d'));
        assertFalse(this.binder.parseChar('D'));
        assertTrue(this.binder.parseChar('e'));
        assertFalse(this.binder.parseChar('E'));
        assertTrue(this.binder.parseChar('f'));
        assertFalse(this.binder.parseChar('x'));
    }

    public void testParseLiteralQuote()
    {
        this.binder.setInput("a\\\"bcd\\\"ef");

        assertTrue(this.binder.parseChar('a'));
        assertTrue(this.binder.parseLiteralQuote());
        assertTrue(this.binder.parseChar('b'));
        assertFalse(this.binder.parseLiteralQuote());
        assertTrue(this.binder.parseChar('c'));
        assertFalse(this.binder.parseLiteralQuote());
        assertTrue(this.binder.parseChar('d'));
        assertTrue(this.binder.parseLiteralQuote());
        assertFalse(this.binder.parseLiteralQuote());
        assertTrue(this.binder.parseChar('e'));
        assertFalse(this.binder.parseLiteralQuote());
        assertTrue(this.binder.parseChar('f'));
        assertFalse(this.binder.parseLiteralQuote());
    }


    public void testParseWhitespace()
    {
        this.binder.setInput("    \n\ta  \n \t  b\n\n\n\t\r\n     c");

        assertTrue(this.binder.parseWhitespace());
        assertTrue(this.binder.parseChar('a'));
        assertTrue(this.binder.parseWhitespace());
        assertTrue(this.binder.parseChar('b'));
        assertTrue(this.binder.parseWhitespace());
        assertTrue(this.binder.parseChar('c'));
    }


    public void testParseWhitespaceToEof()
    {
        this.binder.setInput("    \n\t   ");

        assertTrue(this.binder.parseWhitespace());
        assertTrue(this.binder.isEndOfInput());
    }

    public void testIsEndOfInput()
    {
        this.binder.setInput("a");

        assertTrue(this.binder.parseChar('a'));
        assertTrue(this.binder.isEndOfInput());
    }

    public void testIsEndOfInputEmptyString()
    {
        this.binder.setInput("");

        assertTrue(this.binder.isEndOfInput());
    }

    public void testIsEndOfInputPastEnd()
    {
        this.binder.setInput("abc");

        this.binder.consume(6);
        assertTrue(this.binder.isEndOfInput());
    }

    public void testIsEndOfString()
    {
        this.binder.setInput("a b\nc\rd\te:f,g{h}i[j]k");

        assertFalse(this.binder.isEndOfString());
        this.binder.consume();
        assertTrue(this.binder.isEndOfString());
        this.binder.consume();
        assertFalse(this.binder.isEndOfString());
        this.binder.consume();
        assertTrue(this.binder.isEndOfString());
        this.binder.consume();
        assertFalse(this.binder.isEndOfString());
        this.binder.consume();
        assertTrue(this.binder.isEndOfString());
        this.binder.consume();
        assertFalse(this.binder.isEndOfString());
        this.binder.consume();
        assertTrue(this.binder.isEndOfString());
        this.binder.consume();
        assertFalse(this.binder.isEndOfString());
        this.binder.consume();
        assertTrue(this.binder.isEndOfString());
        this.binder.consume();
        assertFalse(this.binder.isEndOfString());
        this.binder.consume();
        assertTrue(this.binder.isEndOfString());
        this.binder.consume();
        assertFalse(this.binder.isEndOfString());
        this.binder.consume();
        assertTrue(this.binder.isEndOfString());
        this.binder.consume();
        assertFalse(this.binder.isEndOfString());
        this.binder.consume();
        assertTrue(this.binder.isEndOfString());
        this.binder.consume();
        assertFalse(this.binder.isEndOfString());
        this.binder.consume();
        assertTrue(this.binder.isEndOfString());
        this.binder.consume();
        assertFalse(this.binder.isEndOfString());
        this.binder.consume();
        assertTrue(this.binder.isEndOfString());
        this.binder.consume();
        assertFalse(this.binder.isEndOfString());
        this.binder.consume();
    }

    public void testParseUnquotedString()
    throws Exception
    {
        this.binder.setInput("abcdef");

        StringBuilder value = new StringBuilder();

        assertTrue(this.binder.parseUnquotedString(value));

        assertEquals("abcdef",value.toString());
    }

    public void testParseUnquotedStringComma()
    throws Exception
    {
        this.binder.setInput("abc,def");

        StringBuilder value = new StringBuilder();

        assertTrue(this.binder.parseUnquotedString(value));

        assertEquals("abc",value.toString());
        assertTrue(this.binder.parseSeparator());
    }

    public void testParseUnquotedStringSpace()
    throws Exception
    {
        this.binder.setInput("abc def");

        StringBuilder value = new StringBuilder();

        assertTrue(this.binder.parseUnquotedString(value));

        assertEquals("abc",value.toString());
        assertTrue(this.binder.parseWhitespace());
    }

    public void testParseUnquotedStringLiteralQuote()
    throws Exception
    {
        this.binder.setInput("ab\\\"c,def");

        StringBuilder value = new StringBuilder();

        assertTrue(this.binder.parseUnquotedString(value));

        assertEquals("ab\"c",value.toString());
        assertTrue(this.binder.parseSeparator());
    }

    /**
     * hmmmmmm....
     * 
     * @throws Exception
     */
    public void testParseUnquotedStringQuote()
    throws Exception
    {
        this.binder.setInput("ab\"c,def");

        StringBuilder value = new StringBuilder();

        assertTrue(this.binder.parseUnquotedString(value));

        assertEquals("ab\"c",value.toString());
        assertTrue(this.binder.parseSeparator());
    }

    public void testParseQuotedString()
    throws Exception
    {
        this.binder.setInput("\"a{b},c [d]:ef\"");

        StringBuilder value = new StringBuilder();

        assertTrue(this.binder.parseQuotedString(value));

        assertEquals("a{b},c [d]:ef",value.toString());

    }

    public void testParseQuotedStringNoStart()
    throws Exception
    {
        this.binder.setInput("a{b},c[d]:ef\"");

        StringBuilder value = new StringBuilder();

        assertFalse( this.binder.parseQuotedString(value) );

        assertEquals("",value.toString());
        assertTrue(this.binder.parseChar('a'));
    }

    public void testParseQuotedStringNoEnd()
    {
        this.binder.setInput("\"a{b},c[d]:ef");

        StringBuilder value = new StringBuilder();

        try
        {
            this.binder.parseQuotedString(value);
            fail("should have failed");
        }
        catch (BindException e)
        {
            // ok
        }
    }
    
    public void testParseStringOK()
    throws Exception
    {
        this.binder.setInput("a{b},c [d]:ef");

        StringBuilder value = new StringBuilder();
        
        assertTrue(this.binder.parseString(value));
        
        assertEquals("a",value.toString());
        assertTrue(this.binder.parseChar('{'));
    }
    
    public void testParseStringOKQuoted()
    throws Exception
    {
        this.binder.setInput("\"a{b},c [d]:ef\" ");

        StringBuilder value = new StringBuilder();
        
        assertTrue(this.binder.parseString(value));
        
        assertEquals("a{b},c [d]:ef",value.toString());
        assertTrue(this.binder.parseChar(' '));
    }
    
    public void testParseStringFailCurly()
    throws Exception
    {
        this.binder.setInput("{a{b},c [d]:ef");

        StringBuilder value = new StringBuilder();

        try
        {
            this.binder.parseString(value);
            fail("should have failed");
        }
        catch (BindException e)
        {
            // ok
        }
    }
    
    public void testParseStringFailBracket()
    throws Exception
    {
        this.binder.setInput("[a{b},c [d]:ef");

        StringBuilder value = new StringBuilder();

        try
        {
            this.binder.parseString(value);
            fail("should have failed");
        }
        catch (BindException e)
        {
            // ok
        }
    }
    
    public void testParseObjectStrings()
    throws Exception
    {
        this.binder.setInput(" { aaa : bbb , ccc:ddd, \"eee\":\"fff\" } " );
        
        Map<String,?> m = this.binder.parse();
        
        assertEquals(3,m.size());
        assertEquals("bbb",m.get("aaa"));
        assertEquals("ddd",m.get("ccc"));
        assertEquals("fff",m.get("eee"));
    }
    
    @SuppressWarnings("unchecked")
    public void testParseObjectObject()
    throws Exception
    {
        this.binder.setInput(" { aaa : { ccc:ddd, \"eee\":\"fff\" } } " );
        
        Map<String,?> m = this.binder.parse();
        
        assertEquals(1,m.size());
        
        Map<String,?> aaa = (Map<String, ?>) m.get("aaa");
        
        assertEquals(2,aaa.size());
        assertEquals("ddd",aaa.get("ccc"));
        assertEquals("fff",aaa.get("eee"));
    }
    
    public void testParseList()
    throws Exception
    {
        this.binder.setInput(" { aaa : [ ccc, ddd, \"eee\",\"fff\" ] } " );
        
        Map<String,?> m = this.binder.parse();
        
        assertEquals(1,m.size());
        
        List<?> aaa = (List<?>) m.get("aaa");
        
        assertEquals(4,aaa.size());
        assertEquals("ccc",aaa.get(0));
        assertEquals("ddd",aaa.get(1));
        assertEquals("eee",aaa.get(2));
        assertEquals("fff",aaa.get(3));
    }
    
    @SuppressWarnings("unchecked")
    public void testParseListObject()
    throws Exception
    {
        this.binder.setInput(" { aaa : [ { ccc: ddd, \"eee\":\"fff\" } ] } " );
        
        Map<String,?> m = this.binder.parse();
        
        log.debug(m.toString());
        
        assertEquals(1,m.size());
        
        List<?> aaa = (List<?>) m.get("aaa");
        
        assertEquals(1,aaa.size());
        
        Map<String,?> map = (Map<String, ?>) aaa.get(0);
        
        assertEquals(2,map.size());
        assertEquals("ddd",map.get("ccc"));
        assertEquals("fff",map.get("eee"));
    }
    
    public void testParseListList()
    throws Exception
    {
        this.binder.setInput(" { aaa : [ [ccc , ddd] , [ \"eee\", \"fff\" ] ] } " );
        
        Map<String,?> m = this.binder.parse();
        
        log.debug(m.toString());
        
        assertEquals(1,m.size());
        
        List<?> aaa = (List<?>) m.get("aaa");
        
        assertEquals(2,aaa.size());
        
        List<?> list0 = (List<?>) aaa.get(0);
        List<?> list1 = (List<?>) aaa.get(1);
        
        assertEquals(2,list0.size());
        assertEquals(2,list1.size());
        assertEquals("ccc",list0.get(0));
        assertEquals("ddd",list0.get(1));
        assertEquals("eee",list1.get(0));
        assertEquals("fff",list1.get(1));
    }
    
    @SuppressWarnings("unchecked")
    public void testParseListMixed()
    throws Exception
    {
        this.binder.setInput(" { aaa : [ zzz, [ccc , ddd] , { \"eee\": \"fff\" } ] } " );
        
        Map<String,?> m = this.binder.parse();
        
        log.debug(m.toString());
        
        assertEquals(1,m.size());
        
        List<?> aaa = (List<?>) m.get("aaa");
        
        assertEquals(3,aaa.size());
        
        String value0 = (String)aaa.get(0);
        List<?> list0 = (List<?>) aaa.get(1);
        Map<String,?> map0 = (Map<String,?>) aaa.get(2);
        
        assertEquals("zzz",value0);
        assertEquals(2,list0.size());
        assertEquals("ccc",list0.get(0));
        assertEquals("ddd",list0.get(1));
        assertEquals(1,map0.size());
        assertEquals("fff",map0.get("eee"));
    }
    
    public void testParseEmpty()
    throws Exception
    {
        this.binder.setInput(" { } " );
        
        Map<String,?> m = this.binder.parse();
        
        log.debug(m.toString());
    }
    
    public void testParseEmptyList()
    throws Exception
    {
        this.binder.setInput(" { aaa : [ ] } " );
        
        Map<String,?> m = this.binder.parse();
        
        log.debug(m.toString());
    }
    
    public void testParseEmptyObject()
    throws Exception
    {
        this.binder.setInput(" { aaa : { } } " );
        
        Map<String,?> m = this.binder.parse();
        
        log.debug(m.toString());
    }
    
    
}
