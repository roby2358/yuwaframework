/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: TestBind.java $
 * created by Owner May 20, 2006
 */
package org.yuwa.util.smallBind.test;

import java.io.StringReader;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.yuwa.util.smallBind.SaxBindHarness;

/**
 * @author Owner
 * 
 */
public class TestBind extends TestCase
{
    public SaxBindHarness harness = new SaxBindHarness();


    public void setUp()
    {
        WithID.reset();
        WithoutID.reset();
        this.harness.startDocument();
    }


    public void testBindWithoutID() throws Exception
    {
        StringBuilder classmap = new StringBuilder("<?classmap");
        classmap.append(" /t1 : org.yuwa.util.smallBind.test.WithoutID;");
        classmap.append(" /t1/next : org.yuwa.util.smallBind.test.WithoutID;");
        classmap.append(" /t1/aa : org.yuwa.util.smallBind.test.WithoutID;");
        classmap.append(" /t1/bb : org.yuwa.util.smallBind.test.WithoutID;");
        classmap.append(" /t1/cc : org.yuwa.util.smallBind.test.WithoutID;");
        classmap.append(" /t1/dd : org.yuwa.util.smallBind.test.WithoutID;");
        classmap.append(" ?>");

        String toParse = classmap.toString()
                + "<t1 name='t1'><next id='next' name='bing'/><aa id='a'/><bb ref='a'/><cc ref='a' name='poop'/><dd/></t1>";

        StringReader reader = new StringReader(toParse);
        InputSource xmlSource = new InputSource(reader);

        XMLReader parser = this.harness.makeParser();
        parser.parse(xmlSource);

        WithName aa = (WithName) harness.getObject("/t1/aa");
        WithName bb = (WithName) harness.getObject("/t1/bb");
        WithName cc = (WithName) harness.getObject("/t1/cc");

        Assert.assertTrue(aa == cc);
        Assert.assertTrue(aa == bb);
        Assert.assertEquals("t1", ((WithName) harness.getObject("/t1")).getName());
        Assert.assertEquals("bing", ((WithName) harness.getObject("/t1/next")).getName());
		Assert.assertEquals("poop", aa.getName());
        Assert.assertEquals("poop", bb.getName());
		Assert.assertEquals("poop", cc.getName());
        Assert.assertEquals("withoutID3", ((WithName) harness.getObject("/t1/dd")).getName());
    }


    public void testBindWithID() throws Exception
    {
        String classmap = "<?classmap /t1 : org.yuwa.util.smallBind.test.WithID; /t1/next : org.yuwa.util.smallBind.test.WithID; /t1/aa : org.yuwa.util.smallBind.test.WithID; /t1/bb : org.yuwa.util.smallBind.test.WithID; /t1/cc : org.yuwa.util.smallBind.test.WithID; /t1/dd : org.yuwa.util.smallBind.test.WithID; ?>";

        String toParse = classmap + "<t1 name='t1'><next id='next' name='bing'/><aa id='a'/><bb ref='a'/><cc ref='a'/><dd/></t1>";

        StringReader reader = new StringReader(toParse);
        InputSource xmlSource = new InputSource(reader);

        XMLReader parser = this.harness.makeParser();
        parser.parse(xmlSource);

        Assert.assertEquals("t1", ((WithName) harness.getObject("/t1")).getName());
        Assert.assertEquals("bing", ((WithName) harness.getObject("/t1/next")).getName());
        Assert.assertEquals("a", ((WithID) harness.getObject("/t1/aa")).getID());
        Assert.assertEquals("withID2", ((WithName) harness.getObject("/t1/aa")).getName());
        Assert.assertEquals("withID2", ((WithName) harness.getObject("/t1/bb")).getName());
        Assert.assertEquals("withID2", ((WithName) harness.getObject("/t1/cc")).getName());
        Assert.assertEquals("withID3", ((WithName) harness.getObject("/t1/dd")).getName());

    }


    public void testIDNotFound() throws Exception
    {
        String classmap = "<?classmap /t1 : org.yuwa.util.smallBind.test.WithoutID; /t1/next : org.yuwa.util.smallBind.test.WithoutID; /t1/aa : org.yuwa.util.smallBind.test.WithoutID; /t1/bb : org.yuwa.util.smallBind.test.WithoutID; /t1/cc : org.yuwa.util.smallBind.test.WithoutID; /t1/dd : org.yuwa.util.smallBind.test.WithoutID; ?>";

        String toParse = classmap + "<t1 name='t1'><next id='next' name='bing'/><aa id='a'/><bb ref='b'/><cc ref='a'/><dd/></t1>";

        StringReader reader = new StringReader(toParse);
        InputSource xmlSource = new InputSource(reader);

        XMLReader parser = this.harness.makeParser();

        try
        {
            parser.parse(xmlSource);
            Assert.fail("should have thrown object reference not found");
        }
        catch (Exception e)
        {
            Assert.assertTrue(-1 != e.getMessage().indexOf("object reference not found"));
        }

    }
}
