/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/*
 * Created on Mar 16, 2005
 * 
 * *$Id: TestWorkingObject.java,v 1.1.1.1 2005/09/17 06:42:17 Owner Exp $
 *
 *@author Owner
 */
package org.yuwa.util.smallBind.test;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.util.workingobject.WorkingObject;
import org.yuwa.util.workingobject.WorkingObjectObject;


/**
 * @author Owner
 */
public class TestWorkingObject extends TestCase
{
    private WorkingObjectObject wip;
    private TestObject tester;
    private TestObject testee;
    
    
    protected void setUp()
    {
        try
        {
            this.tester = new TestObject();
            this.testee = new TestObject();
            this.wip = new WorkingObjectObject(this.testee);
            
            this.tester.name = "name";
            this.tester.value = 5;
            this.tester.active = true;
            this.tester.sequence = new Integer(20);
            this.tester.testo = new TestObject();
            
            this.wip.set("name",this.tester.name);
            this.wip.set("value",new Integer(this.tester.value) );
            this.wip.set("active",new Boolean(this.tester.active) );
            this.wip.set("sequence",this.tester.sequence );
            this.wip.set("use add",this.tester.testo);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Assert.fail("should not throw any errors " + e + " " + e.getStackTrace() );
        }
    }
    
    
    public void testSet()
    {
        Assert.assertEquals(this.tester.name,this.testee.name);
        Assert.assertEquals(this.tester.value,this.testee.value);
        Assert.assertEquals(this.tester.active,this.testee.active);
        Assert.assertEquals(this.tester.sequence,this.testee.sequence);
        Assert.assertTrue(this.tester.testo == this.testee.testo);
    }
    
    
    public void testSetSuper()
    {
        try
        {
            this.tester.add(new TestObject4());
            this.wip.set("use add", this.tester.testo);
            Assert.assertTrue( this.tester.testo == this.testee.testo );
            
            this.tester.add(new TestObject3());
            this.wip.set("use add", this.tester.testo);
            Assert.assertTrue( this.tester.testo == this.testee.testo );
            
            this.tester.add(new TestObject2());
            this.wip.set("use add", this.tester.testo);
            Assert.assertTrue( this.tester.testo == this.testee.testo );
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Assert.fail("should not throw any errors " + e + " " + e.getStackTrace() );
        }
    }
    
    
    public void testAddObject()
    {
        try
        {
            TestObject4 t = new TestObject4();
            WorkingObject w = new WorkingObjectObject(t);
            
            w.set("use add object", "zing");
            w.set("use add object", new Integer(0));
            
            String expected = "|java.lang.String|java.lang.Integer";
            Assert.assertEquals(expected,t.buffer.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Assert.fail("should not throw any errors " + e + " " + e.getStackTrace() );
        }
    }
    
    
    public void testNoAddObject()
    {
        try
        {
            TestObject3 t = new TestObject3();
            WorkingObject w = new WorkingObjectObject(t);
            
            w.set("use add object", "zing");
            w.set("use add object", new Integer(0));
            
            String expected = "";
            Assert.assertEquals(expected,t.buffer.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Assert.fail("should not throw any errors " + e + " " + e.getStackTrace() );
        }
    }
    
    
    public void testToString()
    {
        Assert.assertEquals(this.tester.toString(), this.wip.toString() );
        Assert.assertEquals(this.testee.toString(), this.wip.toString() );
    }
    
    
    public void testGetObject()
    {
        Assert.assertTrue( this.testee == this.wip.getObject() );
    }
    
    public void testGetId() throws Exception
    {
        Test3 t = new Test3();
        WorkingObject wo = new WorkingObjectObject( t );
        
        String what = "what";
        t.setId(what);
        Assert.assertEquals(what,t.getId());
        Assert.assertEquals(what,wo.getId());
    }
    
    public void testGetIdNo() throws Exception
    {
        Test1 t = new Test1();
        WorkingObject wo = new WorkingObjectObject( t );
        
        Assert.assertEquals(null,wo.getId());
    }
    
    public void testGetPPages() throws Exception
    {
        Test3 t = new Test3();
        WorkingObject wo = new WorkingObjectObject( t );
        
        String zoop = "zoop bing pop";
        t.setPPages(zoop);
        Assert.assertEquals(zoop,t.getPPages());
        Assert.assertEquals(zoop,wo.getPPages());
    }
    
    public void testGetPPagesNo() throws Exception
    {
        Test1 t = new Test1();
        WorkingObject wo = new WorkingObjectObject( t );
        
        Assert.assertEquals(null,wo.getId());
    }
    
    public void testMixedCase() throws Exception
    {
        TestObject5 t5 = new TestObject5();
        WorkingObject wo = new WorkingObjectObject( t5 );
        
        wo.set("mixEdCaSe","mixEdCaSe");
        Assert.assertEquals("mixEdCaSe",t5.mixEdCaSe);
        
        wo.set("MiXedCAse","mixEdCaSe");
        Assert.assertEquals("mixEdCaSe",t5.mixEdCaSe);
        
        wo.set("lowercase","lowercase");
        Assert.assertEquals("lowercase",t5.lowercase);
        
        wo.set("LOWERCASE","lowercase");
        Assert.assertEquals("lowercase",t5.lowercase);
        
        wo.set("uppercase","uppercase");
        Assert.assertEquals("uppercase",t5.uppercase);
        
        wo.set("UPPERCASE","uppercase");
        Assert.assertEquals("uppercase",t5.uppercase);
    }
    
    public void testIntField() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("intfield",1);
        assertEquals(1,f.intfield);
    }
    
    public void testIntFieldMixed() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("iNtFieLd",1);
        assertEquals(1,f.intfield);
    }
    
    public void testLongField() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("longfield",1);
        assertEquals(1,f.longfield);
    }
    
    public void testBooleanField() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        f.booleanfield = true;
        wo.set("booleanfield",true);
        assertTrue(f.booleanfield);
        
        f.booleanfield = true;
        wo.set("booleanfield",false);
        assertFalse(f.booleanfield);
    }
    
    public void testFloatField() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("floatfield",1.0f);
        assertEquals(1.0f,f.floatfield);
    }
    
    public void testDoubleField() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("doublefield",1.0);
        assertEquals(1.0,f.doublefield);
    }
    
    public void testByteField() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("bytefield",(byte)1);
        assertEquals((byte)1,f.bytefield);
        
        wo.set("bytefield",2);
        assertEquals((byte)2,f.bytefield);
    }
    
    public void testIntFieldString() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("intfield","1");
        assertEquals(1,f.intfield);
    }
    
    public void testIntFieldMixedString() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("iNtFieLd","1");
        assertEquals(1,f.intfield);
        
        wo.set("INTFIELD","2");
        assertEquals(2,f.intfield);
    }
    
    public void testLongFieldString() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("longfield","1");
        assertEquals(1L,f.longfield);
    }
    
    public void testBooleanFieldString() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        f.booleanfield = true;
        wo.set("booleanfield","true");
        assertTrue(f.booleanfield);
        
        f.booleanfield = true;
        wo.set("booleanfield","false");
        assertFalse(f.booleanfield);
    }
    
    public void testFloatFieldString() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("floatfield","1.0");
        assertEquals(1.0f,f.floatfield);
    }
    
    public void testDoubleFieldString() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("doublefield","1.0");
        assertEquals(1.0,f.doublefield);
    }
    
    public void testByteFieldString() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("bytefield","1");
        assertEquals((byte)1,f.bytefield);
    }
    
    public void testTest1Field() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        Test1 t = new Test1();
        wo.set("test1field",t);
        assertEquals(t,f.test1field);
    }
    
    public void testDupField() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("dupfield",1);
        assertEquals(2,f.dupfield);
    }
    
    public void testMixedField() throws Exception
    {
        FieldObject f = new FieldObject();
        WorkingObject wo = new WorkingObjectObject(f);
        
        wo.set("MIXED",1);
        assertEquals(1,f.mIxeD);
        
        wo.set("mixed",2);
        assertEquals(2,f.mIxeD);
        
        wo.set("miXEd",3);
        assertEquals(3,f.mIxeD);
    }
    
    
    public void testWrongType() throws Exception
    {
        
        try
        {
            FieldObject f = new FieldObject();
            WorkingObject wo = new WorkingObjectObject(f);
            
            wo.set("intfield","bang!");
            fail();
        }
        catch(Exception e)
        {
            // it should throw some sort of exception
        }
    }
    
    
    public void testSup1() throws Exception
    {
        SupTester tester = new SupTester();
        WorkingObject wo = new WorkingObjectObject(tester);
        Sup1 target = new Sup1();
        
        wo.set("bop",target);
        Assert.assertEquals("sup1",tester.result);
    }
    
    
    public void testSup2() throws Exception
    {
        SupTester tester = new SupTester();
        WorkingObject wo = new WorkingObjectObject(tester);
        Sup2 target = new Sup2();
        
        wo.set("bop",target);
        Assert.assertEquals("sup2",tester.result);
    }
    
    
    public void testSup2A() throws Exception
    {
        SupTester tester = new SupTester();
        WorkingObject wo = new WorkingObjectObject(tester);
        Sup2 target = new Sup2() { };
        
        wo.set("bop",target);
        Assert.assertEquals("sup2",tester.result);
    }
    
    
    public void testInt1() throws Exception
    {
        SupTester tester = new SupTester();
        WorkingObject wo = new WorkingObjectObject(tester);
        Int1 target = new Int1() { };
        
        wo.set("bop",target);
        Assert.assertEquals("int1",tester.result);
    }
    
    
    public void testInt2() throws Exception
    {
        SupTester tester = new SupTester();
        WorkingObject wo = new WorkingObjectObject(tester);
        Int2 target = new Int2() { };
        
        wo.set("bop",target);
        Assert.assertEquals("int2",tester.result);
    }
    
    
    public void testSup3() throws Exception
    {
        SupTester tester = new SupTester();
        WorkingObject wo = new WorkingObjectObject(tester);
        Sup3 target = new Sup3();
        
        wo.set("bop",target);
        Assert.assertEquals("int1",tester.result);
    }
    
    
    public void testSup3a() throws Exception
    {
        SupTester tester = new SupTester();
        WorkingObject wo = new WorkingObjectObject(tester);
        Sup3 target = new Sup3() { };
        
        wo.set("bop",target);
        Assert.assertEquals("int1",tester.result);
    }
    
    public static class TestObject
    {
        public StringBuffer buffer = new StringBuffer();
        
        public String name;
        public int value;
        public boolean active;
        public Integer sequence;
        public TestObject testo;
        
        public String toString()
        {
            StringBuffer result = new StringBuffer();
            
            result.append("[test name=");
            result.append(name);
            result.append("] value=[");
            result.append(value);
            result.append("] active=[");
            result.append(active);
            result.append("] sequence=[");
            result.append(sequence);
            result.append("] testo=[");
            result.append((testo == null) ? "null" : testo.getClass().getName());
            result.append("]");
            
            return result.toString();
        }
        
        public void add(TestObject o)
        {
            this.testo = o;
        }
        
        public void addTestObject3(TestObject3 o)
        {
            this.testo = o;
        }
        
        public boolean isActive() { return active; }
        public void setActive(boolean active) { this.active = active; }
        public String getName() { return name; }
        public void setName(String name) { this.name = name; }
        public Integer getSequence() { return sequence; }
        public void setSequence(Integer sequence) { this.sequence = sequence; }
        public int getValue() { return value; }
        public void setValue(int value) { this.value = value; }
    }
    
    public static class TestObject2 extends TestObject
    {
        public String toString()
        {
            return "t2 " + super.toString();
        }
    }
    
    public static class TestObject3 extends TestObject2
    {
        public String toString()
        {
            return "t3 " + super.toString();
        }
    }
    
    
    public static class TestObject4 extends TestObject3
    {
        public String toString()
        {
            return "t4 " + super.toString();
        }
        
        public void add(Object o)
        {
            this.buffer.append("|");
            this.buffer.append(o.getClass().getName());
        }
    }
    
    public static class TestWhakyCase
    {
        public String value = "TestWhakyCase";
        
    }
    
    public static class TestObject5
    {
        public String mixEdCaSe = "nope";
        public String lowercase = "nope";
        public String uppercase = "nope";
        public TestWhakyCase testWhakyCase;
        
        public String toString()
        {
            return "t5 " + super.toString();
        }
        
        public void setTestWhakyCase(TestWhakyCase t)
        {
            this.testWhakyCase = t;
        }
        
        public void setMixEdCaSe(String s)
        {
            this.mixEdCaSe = s;
        }
        
        public void setLowercase(String s)
        {
            this.lowercase = s;
        }
        
        public void setUPPERCASE(String s)
        {
            this.uppercase = s;
        }
    }
}
