/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.smallBind.test;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.yuwa.util.smallBind.SaxBindHarness;

public class TestPropertyInspector
extends TestCase
{

    public SaxBindHarness harness = new SaxBindHarness();
    public SaxBindHarness.Tester tester = harness.new Tester();


    public void setUp()
    {
        this.harness.startDocument();
    }
    
    // ~-~- tests for properties (get & set)
    
    public static class TestBind1
    {
        public String whoop = "woo";
        public int zing = -1;
        public Integer zot = -1;
        public int a;
        public Integer b;
        public long c;
        public Long d;
        // TODO: float, double, etc.
        
        public int getZing() { return zing; }
        public void setZing(int zing) { this.zing = zing; }
        public int getZot() { return zot; }
        public void setZot(int zot) { this.zot = zot; }
        public String getWhoop() { return whoop; }
        public void setWhoop(String whoop) { this.whoop = whoop; }
    }
    

    public void testBind1()
    {
        String toParse = "<t1 zot='2' whoop='wooga'><zing>1</zing></t1>";

        Map<String, Class<?>> classMap = new HashMap<String, Class<?>>();
        Map<String, String> propertiesMap = new HashMap<String, String>();

        classMap.put("/t1", TestBind1.class);

        this.harness.setClassMap(classMap);
        this.harness.setPropertyMap(propertiesMap);

        StringReader reader = new StringReader(toParse);
        InputSource xmlSource = new InputSource(reader);
        try
        {
            XMLReader parser = this.harness.makeParser();
            parser.parse(xmlSource);
        }
        catch (Throwable t)
        {
            throw new RuntimeException("failed", t);
        }

        TestBind1 t1 = (TestBind1) this.harness.getObject("/t1");

        Assert.assertNotNull(t1);
        Assert.assertEquals("wooga", t1.whoop);
        Assert.assertEquals(1, t1.zing);
        Assert.assertEquals(2, t1.zot.intValue());
    }
    
    public static class TestBind2
    {
        public TestBind1 t1;
        public Aa aa;
        public Bb bb;
        public Cc cc;

        public TestBind1 getT1() { return t1; }
        public void setT1(TestBind1 t1) { this.t1 = t1; }
        public Aa getAa() { return aa; }
        public void setAa(Aa aa) { this.aa = aa; }
        public Bb getBb() { return bb; }
        public void setBb(Bb bb) { this.bb = bb; }
        public Cc getCc() { return cc; }
        public void setCc(Cc cc) { this.cc = cc; }
    }
    
    public static class Aa
    {
        
    }
    
    public static class Bb
    {
        
    }
    
    public static class Cc
    {
        
    }

    public void testBind2()
    {
        String toParse = "<t2><aa /><bb /><cc /><t1 zot='20' whoop='wooga'><zing>10</zing></t1></t2>";

        Map<String, Class<?>> classMap = new HashMap<String, Class<?>>();
        Map<String, String> propertiesMap = new HashMap<String, String>();

        classMap.put("/t2", TestBind2.class);

        this.harness.setClassMap(classMap);
        this.harness.setPropertyMap(propertiesMap);

        StringReader reader = new StringReader(toParse);
        InputSource xmlSource = new InputSource(reader);
        try
        {
            XMLReader parser = this.harness.makeParser();
            parser.parse(xmlSource);
        }
        catch (Throwable t)
        {
            throw new RuntimeException("failed", t);
        }

        TestBind1 t1 = (TestBind1) this.harness.getObject("/t2/t1");
        TestBind2 t2 = (TestBind2) this.harness.getObject("/t2");

        Assert.assertNotNull(t1);
        Assert.assertEquals("wooga", t1.whoop);
        Assert.assertEquals(10, t1.zing);
        Assert.assertEquals(20, t1.zot.intValue());
        
        Assert.assertTrue( t2.t1 == t1);
        
        Assert.assertNotNull( t2.aa );
        Assert.assertNotNull( t2.bb );
        Assert.assertNotNull( t2.cc );
    }
    
    // ~-~- tests for setter-only attributes
    
    public static class TestSet1
    {
        public String whoop = "woo";
        public int zing = -1;
        public Integer zot = -1;
        public int a;
        public Integer b;
        public long c;
        public Long d;
        // TODO: float, double, etc.
        
        public Aa aa;
        public Bb bb;
        public Cc cc;

        public void setZing(int zing) { this.zing = zing; }
        public void setZot(int zot) { this.zot = zot; }
        public void setWhoop(String whoop) { this.whoop = whoop; }
        public void setAa(Aa aa) { this.aa = aa; }
        public void addBb(Bb bb) { this.bb = bb; }
        public void setCc(Cc cc) { this.cc = cc; }
    }

    public void testSet1()
    {
        String toParse = "<t1 zot='2' whoop='wooga'><zing>1</zing><aa/><bb/></t1>";

        Map<String, Class<?>> classMap = new HashMap<String, Class<?>>();
        Map<String, String> propertiesMap = new HashMap<String, String>();

        classMap.put("/t1", TestSet1.class);

        this.harness.setClassMap(classMap);
        this.harness.setPropertyMap(propertiesMap);

        StringReader reader = new StringReader(toParse);
        InputSource xmlSource = new InputSource(reader);
        try
        {
            XMLReader parser = this.harness.makeParser();
            parser.parse(xmlSource);
        }
        catch (Throwable t)
        {
            throw new RuntimeException("failed", t);
        }

        TestSet1 t1 = (TestSet1) this.harness.getObject("/t1");

        Assert.assertNotNull(t1);
        Assert.assertEquals("wooga", t1.whoop);
        Assert.assertEquals(1, t1.zing);
        Assert.assertEquals(2, t1.zot.intValue());
        Assert.assertNotNull( t1.aa );
        Assert.assertNotNull( t1.bb );
        Assert.assertNull( t1.cc );
    }
}
