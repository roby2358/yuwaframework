/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.smallBind.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.yuwa.util.context.ContextInitialized;
import org.yuwa.util.context.ContextObject;

public class Test2 implements ContextInitialized<ContextObject>
{
    public ContextObject context = ContextObject.NO_CONTEXT;
    public String name = "hoozat";
    public String property = "huh";
    public List<Test3> t3s = new ArrayList<Test3>();
    
    public void add(Test3 t) { this.t3s.add(t); }
    public void setProperty(String s) { this.property = s; }
    public void set(Map<String, String> m) { }
    public void setAbc(int a, int b, int c) { }
    public void setInt(Integer i) { }
    public void add(int a, int b, int c) { }

    public void initialize(ContextObject c)
    {
        this.context = c;
    }

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

}
