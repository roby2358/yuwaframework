/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: WithoutID.java $
 * created by Owner May 20, 2006
 */
package org.yuwa.util.smallBind.test;

/**
 * @author Owner
 * 
 */
public class WithoutID implements WithName
{
    public static int count = 0;
    public String name = "withoutID" + count++;
    public WithName next;
    public WithName a;
    public WithName b;
    public WithName c;
    public WithName d;
    
    public static void reset()
    {
        count = 0;
    }


    public WithName getNext()
    {
        return this.next;
    }


    public void setNext(WithName next)
    {
        this.next = next;
    }


    public String getName()
    {
        return this.name;
    }


    public void setName(String name)
    {
        this.name = name;
    }


    public void setA(WithName a)
    {
        this.a = a;
    }


    public void setB(WithName b)
    {
        this.b = b;
    }


    public void setC(WithName c)
    {
        this.c = c;
    }


    public void setD(WithName d)
    {
        this.d = d;
    }


}
