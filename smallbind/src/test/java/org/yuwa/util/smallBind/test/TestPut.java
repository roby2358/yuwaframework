/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.smallBind.test;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.yuwa.util.smallBind.SaxBindHarness;

public class TestPut extends TestCase
{
    public SaxBindHarness harness = new SaxBindHarness();
    public SaxBindHarness.Tester tester = harness.new Tester();


    public void setUp()
    {
        this.harness.startDocument();
    }


    public void testPutById1()
    {
        String toParse = "<t1 ref='myT1' name='zoop' ><t2 id='a' ><name>whoop</name><t3 ref='myT3' ><name>bongo</name></t3><t3 id='b' name='b'><bing>BONG</bing></t3></t2></t1>";

        Map<String, Class<?>> classMap = new HashMap<String, Class<?>>();
        Map<String, String> propertiesMap = new HashMap<String, String>();

        classMap.put("/t1", Test1.class);
        classMap.put("/t1/t2", Test2.class);
        classMap.put("/t1/t2/t3", Test3.class);

        propertiesMap.put("/t1/t2/t3/bing", "BING");
        propertiesMap.put("/t1/@name", "T1NAME");
        propertiesMap.put("/t1/t2/t3/@name", "T3NAME");

        this.harness.setClassMap(classMap);
        this.harness.setPropertyMap(propertiesMap);

        Test1 myT1 = new Test1();
        myT1.setName("pow-boom");
        myT1.setInt(1234);
        this.harness.putById("myT1", myT1);
        
        Test3 myT3 = new Test3();
        myT3.setName("wingo-zowie");
        this.harness.putById("myT3", myT3);
        
        StringReader reader = new StringReader(toParse);
        InputSource xmlSource = new InputSource(reader);
        try
        {
            XMLReader parser = this.harness.makeParser();
            parser.parse(xmlSource);
        }
        catch (Throwable t)
        {
            throw new RuntimeException("failed", t);
        }

        Test1 t1 = (Test1) this.harness.getObject("/t1");
        Test2 t2 = (Test2) this.harness.getObject("/t1/t2");
//        Test3 t3a = (Test3) this.harness.getObject("/t1/t2/t3");
        Test3 t3a = (Test3) this.harness.getObjectByID("myT3");
        Test3 t3b = (Test3) this.harness.getObjectByID("b");

        Assert.assertTrue(t1 == myT1);
        Assert.assertTrue(t3a == myT3);
        Assert.assertNotNull(t1);
        Assert.assertEquals("zoop", t1.name);
        Assert.assertEquals(1234, myT1.i);
        Assert.assertNotNull(t2);
        Assert.assertEquals("whoop", t2.name);
        Assert.assertNotNull(t3a);
        Assert.assertEquals("bongo", myT3.name);
        Assert.assertEquals("bongo", t3a.name);
        Assert.assertNotNull(t3b);
        Assert.assertEquals("b", t3b.id);
        Assert.assertEquals("b", t3b.name);
        
        Assert.assertTrue( t2.t3s.contains(t3a) );
        Assert.assertTrue( t2.t3s.contains(myT3) );
        Assert.assertTrue( t2.t3s.contains(t3b) );

        Assert.assertEquals("BONG", this.harness.getProperty("BING"));
        Assert.assertEquals("zoop", this.harness.getProperty("T1NAME"));
        Assert.assertEquals("b", this.harness.getProperty("T3NAME"));
    }


}
