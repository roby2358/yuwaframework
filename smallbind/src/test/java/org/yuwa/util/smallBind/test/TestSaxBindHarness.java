/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.smallBind.test;

import java.io.InputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.yuwa.util.context.ContextObject;
import org.yuwa.util.smallBind.SaxBindHarness;
import org.yuwa.util.uriresolver.SimpleUriResolver;
import org.yuwa.util.uriresolver.UriResolver;
import org.yuwa.util.workingobject.WorkingObject;
import org.yuwa.util.workingobject.WorkingObjectObject;

public class TestSaxBindHarness extends TestCase
{
    public SaxBindHarness harness = new SaxBindHarness();
    public SaxBindHarness.Tester tester = harness.new Tester();


    public void setUp()
    {
        this.harness.startDocument();
    }


    public void testBind1()
    {
        String toParse = "<t1 name='zoop' ><t2 id='a' ><name>whoop</name><t3 id='b' name='t3b' /><t3 name='b'><bing>BONG</bing></t3></t2></t1>";

        Map<String, Class<?>> classMap = new HashMap<String, Class<?>>();
        Map<String, String> propertiesMap = new HashMap<String, String>();

        classMap.put("/t1", Test1.class);
        classMap.put("/t1/t2", Test2.class);
        classMap.put("/t1/t2/t3", Test3.class);

        propertiesMap.put("/t1/t2/t3/bing", "BING");
        propertiesMap.put("/t1/@name", "T1NAME");
        propertiesMap.put("/t1/t2/t3/@name", "T3NAME");

        this.harness.setClassMap(classMap);
        this.harness.setPropertyMap(propertiesMap);

        StringReader reader = new StringReader(toParse);
        InputSource xmlSource = new InputSource(reader);
        try
        {
            XMLReader parser = this.harness.makeParser();
            parser.parse(xmlSource);
        }
        catch (Throwable t)
        {
            throw new RuntimeException("failed", t);
        }

        Test1 t1 = (Test1) this.harness.getObject("/t1");
        Test2 t2 = (Test2) this.harness.getObject("/t1/t2");
        Test3 t3a = (Test3) this.harness.getObject("/t1/t2/t3");
        Test3 t3b = (Test3) this.harness.getObjectByID("b");

        Assert.assertNotNull(t1);
        Assert.assertEquals("zoop", t1.name);
        Assert.assertNotNull(t2);
        Assert.assertEquals("whoop", t2.name);
        Assert.assertNotNull(t3a);
        Assert.assertEquals("b", t3a.name);
        Assert.assertNotNull(t3b);
        Assert.assertEquals("b", t3b.id);
        Assert.assertEquals("t3b", t3b.name);

        Assert.assertEquals("BONG", this.harness.getProperty("BING"));
        Assert.assertEquals("zoop", this.harness.getProperty("T1NAME"));
        Assert.assertEquals("b", this.harness.getProperty("T3NAME"));
    }


    public void testBindId()
    {
        String toParse = "<xx name='xz' ><xy id='a' /><xz name='b'/><xa /></xx>";

        Map<String, Class<?>> classMap = new HashMap<String, Class<?>>();
        Map<String, String> propertiesMap = new HashMap<String, String>();

        classMap.put("/xx", Test1.class);
        classMap.put("/xx/xy", Test1.class);
        classMap.put("/xx/xz", Test1.class);
        classMap.put("/xx/xa", Test3.class);

        this.harness.setClassMap(classMap);
        this.harness.setPropertyMap(propertiesMap);

        StringReader reader = new StringReader(toParse);
        InputSource xmlSource = new InputSource(reader);
        try
        {
            XMLReader parser = this.harness.makeParser();
            parser.parse(xmlSource);
        }
        catch (Throwable t)
        {
            throw new RuntimeException("failed", t);
        }

        Test1 t1 = (Test1) this.harness.getObjectByID("xx");
        Test1 t2 = (Test1) this.harness.getObjectByID("a");
        Test1 t3 = (Test1) this.harness.getObjectByID("xz");
        Test3 t4 = (Test3) this.harness.getObjectByID("what");

        Assert.assertNotNull(t1);
        Assert.assertEquals("xz", t1.name);
        Assert.assertNotNull(t2);
        Assert.assertEquals("huh", t2.name);
        Assert.assertNotNull(t3);
        Assert.assertEquals("b", t3.name);
        Assert.assertNotNull(t4);
        Assert.assertEquals("b", t3.name);
    }


    public void testBind2() throws Exception
    {
        String classmap = "<?classmap '/t1' : org.yuwa.util.smallBind.test.Test1;\"/t1/t2\" : org.yuwa.util.smallBind.test.Test2;\n\t/t1/t2/t3\n\t:\n\torg.yuwa.util.smallBind.test.Test3\n\t;\n\n\n\n ?>";

        String propertymap = "<?propertymap '/t1/t2/t3/bing':'BING'; \"/t1/@name\":\"T1NAME\"; '/t1/t2/t3/@name':T3NAME; ?>";

        String toParse = ""
                + classmap
                + propertymap
                + "<t1 name='zoop' ><t2 id='a' ><name>whoop</name><t3 id='b' name='t3b' /><t3 name='b'><bing>BONG</bing></t3></t2></t1>";

        StringReader reader = new StringReader(toParse);
        InputSource xmlSource = new InputSource(reader);

        XMLReader parser = this.harness.makeParser();
        parser.parse(xmlSource);

        Test1 t1 = (Test1) this.harness.getObject("/t1");
        Test2 t2 = (Test2) this.harness.getObject("/t1/t2");
        Test3 t3a = (Test3) this.harness.getObject("/t1/t2/t3");
        Test3 t3b = (Test3) this.harness.getObjectByID("b");

        Assert.assertNotNull(t1);
        Assert.assertEquals("zoop", t1.name);
        Assert.assertNotNull(t2);
        Assert.assertEquals("whoop", t2.name);
        Assert.assertNotNull(t3a);
        Assert.assertEquals("b", t3a.name);
        Assert.assertNotNull(t3b);
        Assert.assertEquals("b", t3b.id);
        Assert.assertEquals("t3b", t3b.name);

        Assert.assertEquals("BONG", this.harness.getProperty("BING"));
        Assert.assertEquals("zoop", this.harness.getProperty("T1NAME"));
        Assert.assertEquals("b", this.harness.getProperty("T3NAME"));
    }


    public void testBind3() throws Exception
    {
        UriResolver resolver = new SimpleUriResolver();
        InputStream stream = resolver.resolveAsStream("/org/yuwa/util/smallBind/test/bind3.xml");
        InputSource xmlSource = new InputSource(stream);
        XMLReader parser = this.harness.makeParser();

        parser.parse(xmlSource);

        Test1 test1 = (Test1) this.harness.getObject("/t1");
        Test2 test2 = (Test2) this.harness.getObject("/t1/t2");

        Assert.assertEquals("zoop", test1.name);
        Assert.assertEquals("ppage3", test1.property);

        Assert.assertEquals("test2class", test2.name);
        Assert.assertEquals("ppage3-2", test2.property);
    }


    public void testProcessingInstruction()
    {
        String toParse = "<?bing bong ?><t1 name='zoop' ><t2 id='a' ><name>whoop</name><t3 id='b' name='t3b' /><t3 name='b'><bing>BONG</bing></t3></t2></t1>";

        Map<String, Class<?>> classMap = new HashMap<String, Class<?>>();
        Map<String, String> propertiesMap = new HashMap<String, String>();

        classMap.put("/t1", Test1.class);
        classMap.put("/t1/t2", Test2.class);
        classMap.put("/t1/t2/t3", Test3.class);

        propertiesMap.put("/t1/t2/t3/bing", "BING");
        propertiesMap.put("/t1/@name", "T1NAME");
        propertiesMap.put("/t1/t2/t3/@name", "T3NAME");

        this.harness.setClassMap(classMap);
        this.harness.setPropertyMap(propertiesMap);

        StringReader reader = new StringReader(toParse);
        InputSource xmlSource = new InputSource(reader);
        try
        {
            XMLReader parser = this.harness.makeParser();
            parser.parse(xmlSource);
        }
        catch (Throwable t)
        {
            throw new RuntimeException("failed", t);
        }

    }


    public void testMakeParser()
    {
        XMLReader reader;

        try
        {
            reader = this.harness.makeParser();
        }
        catch (Throwable e)
        {
            throw new RuntimeException("failed", e);
        }

        Assert.assertEquals(org.apache.xerces.parsers.SAXParser.class, reader.getClass());
    }


    public void testPush()
    {
        String xpath = "";
        xpath = this.harness.push("zing");
        Assert.assertEquals("/zing", xpath);
        xpath = this.harness.push("zot");
        Assert.assertEquals("/zing/zot", xpath);
        xpath = this.harness.push("zoop");
        Assert.assertEquals("/zing/zot/zoop", xpath);
        Assert.assertEquals("", this.harness.getWorkingText().toString());
    }


    public void testPop()
    {
        String xpath = "";
        xpath = this.harness.push("zing");
        xpath = this.harness.push("zot");
        xpath = this.harness.push("zoop");

        xpath = this.harness.pop();
        Assert.assertEquals("/zing/zot", xpath);
        xpath = this.harness.pop();
        Assert.assertEquals("/zing", xpath);
        xpath = this.harness.pop();
        Assert.assertEquals("", xpath);
        xpath = this.harness.pop();
        Assert.assertEquals("", xpath);
        xpath = this.harness.pop();
        Assert.assertEquals("", xpath);
    }


    public void testGetClass()
    {

    }


    public void testGetObject()
    {

    }


    public void testGetWorkingObject()
    {

    }


    public void testSetParameterFromWorkingText()
    {

    }


    public void testCreateNewObjectNoParent()
    {
        WorkingObject o;

        try
        {
            o = this.harness.createNewObject("/zing", Test1.class);
        }
        catch (Throwable e)
        {
            throw new RuntimeException("failed", e);
        }

        Assert.assertEquals(o, this.tester.getWorkingObject());
        Assert.assertEquals(o, this.harness.getWorkingObject("/zing"));
        Assert.assertEquals(Test1.class, o.getObject().getClass());
    }


    public void testCreateNewObjectContextCreatedNoContext()
    {
        WorkingObject o;

        try
        {
            o = this.harness.createNewObject("/zing", Test2.class);
        }
        catch (Throwable e)
        {
            throw new RuntimeException("failed", e);
        }

        Assert.assertEquals(o, this.tester.getWorkingObject());
        Assert.assertEquals(o, this.harness.getWorkingObject("/zing"));
        Assert.assertEquals(Test2.class, o.getObject().getClass());
        Test2 t2 = (Test2) o.getObject();
        Assert.assertEquals(ContextObject.NO_CONTEXT, t2.context);
    }


    public void testCreateNewObjectContextCreated()
    {
        WorkingObject o;
        Test1 t1 = new Test1();

        try
        {
            this.tester.setWorkingObject(new WorkingObjectObject(t1));
            o = this.harness.createNewObject("/zing", Test2.class);
        }
        catch (Throwable e)
        {
            throw new RuntimeException("failed", e);
        }

        Assert.assertEquals(o, this.tester.getWorkingObject());
        Assert.assertEquals(o, this.harness.getWorkingObject("/zing"));
        Assert.assertEquals(Test2.class, o.getObject().getClass());
        Test2 t2 = (Test2) o.getObject();
        Assert.assertEquals(t1, t2.context);
    }


    public void testCreateNewObjectContextCreatedEmptyInitialized()
    {
        WorkingObject o;
        Test1 t1 = new Test1();

        try
        {
            this.tester.setWorkingObject(new WorkingObjectObject(t1));
            o = this.harness.createNewObject("/zing", Test3.class);
        }
        catch (Throwable e)
        {
            throw new RuntimeException("failed", e);
        }

        Assert.assertEquals(o, this.tester.getWorkingObject());
        Assert.assertEquals(o, this.harness.getWorkingObject("/zing"));
        Assert.assertEquals(Test3.class, o.getObject().getClass());
        Test3 t3 = (Test3) o.getObject();
        Assert.assertTrue(t3.ready);
    }


    public void testCreateNewObjectEmptyInitialized()
    {
        WorkingObject o;

        try
        {
            o = this.harness.createNewObject("/zing", Test3.class);
        }
        catch (Throwable e)
        {
            throw new RuntimeException("failed", e);
        }

        Assert.assertEquals(o, this.tester.getWorkingObject());
        Assert.assertEquals(o, this.harness.getWorkingObject("/zing"));
        Assert.assertEquals(Test3.class, o.getObject().getClass());
        Test3 t3 = (Test3) o.getObject();
        Assert.assertTrue(t3.ready);
    }


}
