/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/*
 * Created on Mar 22, 2005
 * 
 * *$Id: TestSetterMapByClass.java,v 1.2 2005/09/23 19:04:35 Owner Exp $
 *
 *@author Owner
 */
package org.yuwa.util.smallBind.test;

import java.util.Map;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.util.workingobject.ClassMaps;

/**
 * @author Owner
 */
public class TestClassMaps extends TestCase
{

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    public void testBuildMaps()
    {
        ClassMaps maps = new ClassMaps(Test2.class);

        Assert.assertEquals( 3, maps.getSettersByName().keySet().size() );
        Assert.assertEquals( 2, maps.getAddersByClass().keySet().size());
        Assert.assertEquals( ClassMaps.NO_METHOD, maps.getIdMethod() );
        Assert.assertEquals( ClassMaps.NO_METHOD, maps.getPropertyPagesMethod() );
    }

    public void testBuildMapsTest3()
    {
        ClassMaps maps = new ClassMaps(Test3.class);

        Assert.assertEquals( 3, maps.getSettersByName().keySet().size() );
        Assert.assertEquals( 0, maps.getAddersByClass().keySet().size());
        Assert.assertNotSame( ClassMaps.NO_METHOD, maps.getIdMethod() );
        Assert.assertNotSame( ClassMaps.NO_METHOD, maps.getPropertyPagesMethod() );
    }

    public void testBuildMapsFields()
    {
        ClassMaps maps = new ClassMaps(FieldObject.class);
        
        Assert.assertEquals(9, maps.getFieldsByName().keySet().size());
        Assert.assertNotNull(maps.getFieldsByName().get("intfield"));
        Assert.assertNotNull(maps.getFieldsByName().get("longfield"));
        Assert.assertNotNull(maps.getFieldsByName().get("booleanfield"));
        Assert.assertNotNull(maps.getFieldsByName().get("floatfield"));
        Assert.assertNotNull(maps.getFieldsByName().get("doublefield"));
        Assert.assertNotNull(maps.getFieldsByName().get("bytefield"));
        Assert.assertNotNull(maps.getFieldsByName().get("test1field"));
        Assert.assertNotNull(maps.getFieldsByName().get("dupfield"));
        Assert.assertNotNull(maps.getFieldsByName().get("mixed"));
        Assert.assertNull(maps.getFieldsByName().get("mIxeD"));
    }
    
    public void testGetSetterMap()
    {
     ClassMaps maps = new ClassMaps(Test1.class);
     
     Assert.assertEquals( 3, maps.getSettersByName().keySet().size() );
     Assert.assertTrue( maps.getSettersByName().keySet().contains("property"));
     Assert.assertTrue( maps.getSettersByName().keySet().contains("int"));
     Assert.assertFalse( maps.getSettersByName().keySet().contains("abc"));
     
     Assert.assertEquals( "public void org.yuwa.util.smallBind.test.Test1.setProperty(java.lang.String)",maps.getSettersByName().get("property").toString());
     Assert.assertEquals( "public void org.yuwa.util.smallBind.test.Test1.setInt(java.lang.Integer)",maps.getSettersByName().get("int").toString());
    }

    
    public void testGetAdderMap()
    {
        ClassMaps maps = new ClassMaps(Test1.class);

        Assert.assertEquals(2,maps.getAddersByClass().keySet().size());
        Assert.assertTrue( maps.getAddersByClass().keySet().contains(Test1.class) );
        Assert.assertTrue( maps.getAddersByClass().keySet().contains(Map.class) );
        Assert.assertEquals( "public void org.yuwa.util.smallBind.test.Test1.add(org.yuwa.util.smallBind.test.Test1)", maps.getAddersByClass().get(Test1.class).toString() );
        Assert.assertEquals( "public void org.yuwa.util.smallBind.test.Test1.set(java.util.Map)", maps.getAddersByClass().get(Map.class).toString() );
    }

    
    public static class HasOverloadedSet
    {
        public void set(String s) { }
        public void set(Map<String, String> m) { }
    }
    
    public void testGetAdderMapOverloadedSet()
    {
        ClassMaps maps = new ClassMaps(HasOverloadedSet.class);

        Assert.assertEquals(2,maps.getAddersByClass().keySet().size());
        Assert.assertTrue( maps.getAddersByClass().keySet().contains(String.class) );
        Assert.assertTrue( maps.getAddersByClass().keySet().contains(Map.class) );
        Assert.assertEquals( "public void org.yuwa.util.smallBind.test.TestClassMaps$HasOverloadedSet.set(java.lang.String)", maps.getAddersByClass().get(String.class).toString() );
        Assert.assertEquals( "public void org.yuwa.util.smallBind.test.TestClassMaps$HasOverloadedSet.set(java.util.Map)", maps.getAddersByClass().get(Map.class).toString() );
    }

}
