/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.smallBind.test;

import java.util.Map;

import org.yuwa.util.context.ContextInitialized;
import org.yuwa.util.context.ContextObject;
import org.yuwa.util.context.EmptyInitialized;

public class Test1 implements ContextObject
{
    public String name = "huh";
    public String property = "what";
	public Object i;
    
    public void add(Test1 t) { }
    public void setProperty(String s) { this.property = s; }
    public void set(Map<String, String> m) { }
    public void setAbc(int a, int b, int c) { }
    public void setInt(Integer i) { this.i = i; }
    public void add(int a, int b, int c) { }

    public String getName() { return this.name; }
    public void setName(String name) { this.name = name; }
    
    @SuppressWarnings("unchecked")
    public Object create(Class<?> c)
    {
        try
        {
            Object newObject = c.newInstance();
            if ( newObject instanceof ContextInitialized ) ((ContextInitialized<ContextObject>)newObject ).initialize(this);
            else if ( newObject instanceof EmptyInitialized ) ((EmptyInitialized)newObject ).initialize();
            return newObject;
        }
        catch (Throwable e)
        {
            throw new RuntimeException("failed",e);
        }
    }

}
