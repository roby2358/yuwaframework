/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Oct 29, 2007 $
 */
package org.yuwa.technology.testtools;

/**
 *
 * <p>
 * Oct 29, 2007
 *
 * @author royoung
 */
public class Woo
{
	public static int count;

	/**
	 * get a new unique number each time this is called
	 * 
	 * <p>
	 * Note: to make it really unique, I should mix in the thread ID and the
	 * system IP
	 * 
	 * @return the unique number
	 */
	public static long get()
	{
		return ( System.currentTimeMillis() % 1000000 ) * 100 + ( count++ % 100 );
	}

	/**
	 * @param woo
	 * @return
	 */
	public static long small()
	{
		return ( get() % 1000L );
	}

}
