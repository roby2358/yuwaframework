/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jul 10, 2007 $
 */
package org.yuwa.technology.testtools;

import java.util.Random;


/**
 * OK so this isn't random... you just tell it what value to return
 *
 * <p>
 * Jul 10, 2007
 *
 * @author royoung
 */
public class DummyRandom extends Random {

	/** default serial version ID */
	private static final long serialVersionUID = 1L;
	
	/** the byte array to supply */
	public byte[] bytes;
	
	/**
	 * make sure you've set .bytes, and the array you set is long enough ;)
	 * 
	 * @see java.util.Random#nextBytes(byte[])
	 */
	@Override
	public void nextBytes(byte[] bytes) {

		for ( int i = 0 ; i < bytes.length ; i++ ) {
			
			bytes[i] = this.bytes[i];
		}
	}
	
}
