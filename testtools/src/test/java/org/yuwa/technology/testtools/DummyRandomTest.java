/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Dec 21, 2007 $
 */
package org.yuwa.technology.testtools;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;


/**
 *
 * <p>
 * Dec 21, 2007
 *
 * @author royoung
 */

public class DummyRandomTest
{
	public DummyRandom random = new DummyRandom();

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// tests

	@Test
	public void testNextBytes()
	{
		this.random.bytes = new byte[] { (byte)1, (byte)2, (byte)3, (byte)4, (byte)5, (byte)6 };
		byte[] woo = new byte[this.random.bytes.length];
		this.random.nextBytes(woo);
		
		assertNotNull(woo);
		assertEquals(this.random.bytes.length,woo.length);
		
		for ( int i = 0 ; i < woo.length ; i++ )
		{
		    assertEquals(this.random.bytes[i],woo[i]);
		}
		
	}
}
