package org.yuwa.util.worker;


/**
 * @author Owner
 *
 */
public aspect WorkerAspect
{
    private WorkerQueue queue = new WorkerQueue();
    private Worker worker = new Worker(queue);
        
    {
        this.worker.begin();
    }
    
    pointcut workerMethod() : execution( @RunWorker void *.*() );
    
    void around() : workerMethod()
    {
        Runnable r = new Runnable()
        {
            public void run() { proceed(); }
        };
        this.queue.add(r);
    }
    
    pointcut workerClass(Runnable r) : this(r) && execution ( @RunWorker *.new(..) );
    
    after(Runnable r) : workerClass(r) 
    {
        this.queue.add(r);
    }
  
    pointcut done() : execution( @EndWorker * *.*(..) );
    
    after() : done()
    {
        this.queue.halt();
    }
}
