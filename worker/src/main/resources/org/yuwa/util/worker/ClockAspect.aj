package org.yuwa.util.worker;


/**
 * @author Owner
 * 
 */
public aspect ClockAspect
{

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // attributes & methods
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    private Clock clock;
    
    private ClockAspect THIS = this;

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // pointcuts & advice
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    
    pointcut startOfTime(Clock clock) : this(clock) && execution(Clock.new());

    after (Clock clock) : startOfTime(clock)
    {
        this.clock = clock;
    }
    
    pointcut begin(Ticker ticker) : target(ticker) && call(void begin());
    
    after(Ticker ticker) : begin(ticker)
    {
        final Ticker TICKER = ticker;
        
        new RunWorkerable() {
            
            public void run()
            {
                THIS.clock.add(TICKER);                
            }
        };
    }
    
    pointcut end(Ticker ticker) : target(ticker) && call(void end());
    
    before(Ticker ticker) : end(ticker)
    {
        final Ticker TICKER = ticker;
        
        new RunWorkerable() {
            
            public void run()
            {
                THIS.clock.remove(TICKER);                
            }
        };
    }

}
