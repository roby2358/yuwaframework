/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: Worker.java $
 * created by Owner Jul 13, 2006
 */
package org.yuwa.util.worker;

/**
 * @author Owner
 *
 */
public class Worker implements Runnable
{
    private String name = "workerthread";
    
    private Thread thread;

    private final WorkerQueue queue;
    
    /**
     * the object gets a thread and starts going on instantiation
     *
     */
    public Worker(WorkerQueue queue)
    {
        this.queue = queue;
    }

    
    public void start()
    {
		this.thread = new Thread(this,name);
        this.thread.start();
    }
    
    
    /**
     * the queue sits around waiting for a request to
     * come in, then executes it
     */
    public void run()
    {
        try
        {
            while ( this.queue.isRunning() )
            {
                Runnable r = this.queue.next();

                if ( r != null )
                {
                    synchronized(r)
                    {
                        r.run();
                        r.notifyAll();

                        if ( r instanceof Repeater && ((Repeater)r).repeat() )
                        {
                            this.queue.add(r);
                        }
                    }
                }
            }
        }
        catch (InterruptedException e)
        {
            // nop.. fall through
        }
        
    }

    public void setName(String name) { this.name = name; }
}
