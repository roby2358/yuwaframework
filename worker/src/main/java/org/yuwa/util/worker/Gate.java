/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: Block.java $
 * created by Owner Nov 3, 2006
 */
package org.yuwa.util.worker;


/**
 * @author Owner
 *
 */
public class Gate
{
    private int counter = 0;
    
    public synchronized int close() { return this.counter; }

    public synchronized void open()
    {
        if ( ++this.counter > 32768 ) this.counter = 0;
        this.notifyAll();
    }
    
    public synchronized int pass(int c)
    {
        if ( c == this.counter )
        {
            try
            {
                this.wait();
            }
            catch ( InterruptedException ie )
            {
                // done
            }
        }
        return this.counter;
    }
    
    public synchronized int getCounter()
    {
        return this.counter;
    }
}
