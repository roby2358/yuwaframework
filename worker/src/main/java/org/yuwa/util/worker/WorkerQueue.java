/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: WorkerQueue.java $
 * created by Owner Jul 6, 2006
 */
package org.yuwa.util.worker;

import java.util.LinkedList;

/**
 * The worker queue holds a list of runnables, spawns a thread
 * to process the runnables as they come in
 * 
 * @author Owner
 *
 */
public class WorkerQueue
{
    private LinkedList<Runnable> queue = new LinkedList<Runnable>();
    
    private boolean stop = false;
    
    /**
     * add a runnable to the queue
     * @param r
     */
    public synchronized void add(Runnable r)
    {
        this.queue.add(r);
        this.notifyAll();
    }
    
    
    /**
     * @return a runnable if we're running & there is something to do
     * @throws InterruptedException
     */
    public synchronized Runnable next() throws InterruptedException
    {
        Runnable result = null;
        
        while ( ! stop && this.queue.size() == 0 )
        {
            this.wait();
        }
        
        if ( this.isRunning() && this.queue.size() > 0 ) result = this.queue.removeFirst();
        
        return result;
    }
    
    
    /**
     * add a task that stops the queue
     *
     */
    public synchronized void halt()
    {
        this.stop = true;
        this.notifyAll();
    }
    
    /**
     * indicates if the queue is still running
     * @return false if the queue has stopped
     */
    public boolean isRunning() { return !this.stop; }
}
