/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: Clock.java $
 * created by Owner Mar 10, 2006
 */
package org.yuwa.util.worker;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Owner
 * 
 */
public class Clock implements Runnable, Repeater
{

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// attributes

	private static final long MILLIS_TO_NANOS = 1000000L;

	// time between ticks
	private long tickLength = 40L;

	// time of the last tick -- (relative) time in nanos
	private long nanos = System.nanoTime();

	// elapsed time since last tick
	private long elapsed = 0;

	// pause?
	private boolean paused = true;

	// stop?
	private boolean halt;

	private List<Ticker> tickers = new ArrayList<Ticker>();

	private final Clock THIS = this;


	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// control methods

	/**
	 * marker to add the clock to the queue
	 */
	public void start() { this.halt = false; }


	/**
	 * needed for Repeater... return true if not stopped
	 */
	public boolean repeat() { return ! this.halt; }


	/**
	 * stop the clock!
	 */
	public void stop()
	{
		this.halt = true;
		synchronized (this) { this.notifyAll(); }
	}


	/**
	 * add a ticker to the clock
	 * 
	 * @param ticker
	 */
	public void add(Ticker ticker)
	{
		this.tickers.add(ticker);
	}


	/**
	 * remove a ticker from the clock
	 * @param ticker
	 */
	public void remove(Ticker ticker)
	{
		this.tickers.remove(ticker);
	}


	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// implementation methods

	public void run()
	{
		try
		{
			if (!this.halt) this.tock();
			if (!this.halt) this.tick();
		}
		catch (InterruptedException e)
		{
			// nop
		}
	}


	/**
	 * the tock is the delay before the tick
	 * 
	 * @throws InterruptedException
	 */
	private void tock() throws InterruptedException
	{
		do
		{
			long n = System.nanoTime();
			long delay = this.tickLength - (n - this.nanos) / MILLIS_TO_NANOS;

			if (delay > 0)
			{
				synchronized( this) {
					this.wait(delay);
				}
			}

			n = System.nanoTime();
			this.elapsed = n - this.nanos;
			this.nanos = n;
		}
		while ( ! this.halt && this.paused );
	}


	/**
	 * tick the clock and all its tickers
	 *
	 */
	private void tick()
	{
		for ( Ticker t : THIS.tickers )
		{
			t.tick();
		}
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors

	/**
	 * @param tickLength the elapsed time between ticks in milliseconds
	 */
	public void setTickLength(long tickLength) { this.tickLength = tickLength; }

	/**
	 * set or disable the paused flag
	 * 
	 * @param pause
	 */
	public void setPaused(boolean pause) { this.paused = pause; }

	/**
	 * This is a helper method tickers can use to both refer to
	 * a common time, as well as saving cycles calling the (expensive)
	 * nanoTime function.
	 * 
	 * @return the time at the start of the last clock tick, in nanoseconds
	 */
	public long getNanos() { return this.nanos; }


	/**
	 * A conveience method so tickers can tell how long it's been
	 * since the last tick.
	 * 
	 * Note that this is relative clock time, so it doesn't include
	 * time that passed while the clock was paused.
	 * 
	 * @return the elapsed nanoseconds since the last clock tick
	 */
	public long getElapsed() { return this.elapsed; }

}
