/**
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files '',, to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * The Software shall be used for Good, not Evil.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * @author CW-BRIGHT
 */
package org.yuwa.singletonusurper;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.apache.log4j.Logger;

/**
 * @author CW-BRIGHT
 *
 */
public class SingletonUsurper
{
    protected Logger LOG = Logger.getLogger(SingletonUsurper.class);

    static Field MODIFIERS;

    static
    {
        try
        {
            MODIFIERS = Field.class.getDeclaredField("modifiers");
            MODIFIERS.setAccessible(true);
        }
        catch (Exception e)
        {
            Logger.getLogger(SingletonUsurper.class).error("failed",e);
        }
    }

    private final Class<?> singletonClass;
    private Field instanceField;
    private final Object usurper;
    private Object original;

    /**
     * @param class1
     */
    public SingletonUsurper(Class<?> clazz, Object usurper)
    {
        this.singletonClass = clazz;
        this.usurper = usurper;
        usurp();
    }

    public void usurp()
    {
        try
        {
            instanceField = findInstanceField(singletonClass);

            if ( instanceField == null )
            {
                instanceField = findInstanceInInternalClasses(singletonClass);
            }

            if ( instanceField == null )
            {
                LOG.warn("no instance field found");
            }
            else
            {
                setInstance();
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("failed",e);
        }
    }

    /**
     * see if the given class has an instance field
     * @param clazz
     */
    private Field findInstanceField(Class<?> clazz)
    {
        Field field = null;
        
        for ( Field f : clazz.getDeclaredFields() )
        {
            if ( "instance".equals(f.getName().toLowerCase()) )
            {
                field = f;
                break;
            }
        }
        
        return field;
    }

    /**
     * look in any internal classes for the instance 
     * @param clazz TODO
     */
    private Field findInstanceInInternalClasses(Class<?> clazz)
    {
        Field field = null;
        
        for ( Class<?> c : clazz.getDeclaredClasses())
        {
            field = findInstanceField(c);
            
            if (field != null) break;
        }
        
        return field;
    }

    /**
     * @throws IllegalAccessException
     */
    private void setInstance() throws IllegalAccessException
    {
        instanceField.setAccessible(true);

        MODIFIERS.setInt(instanceField,instanceField.getModifiers() & ~Modifier.FINAL);

        original = instanceField.get(null);
        instanceField.set(null,usurper);
    }

    public void restore()
    {
        try
        {
            if ( instanceField != null )
            {
                instanceField.set(null,original);
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("failed",e);
        }
    }

}
