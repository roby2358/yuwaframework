/**
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files '',, to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * The Software shall be used for Good, not Evil.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * @author CW-BRIGHT
 */
package org.yuwa.singletonusurper;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import junit.framework.Assert;

import org.junit.Test;

/**
 * @author CW-BRIGHT
 *
 */
public class SingletonUsurperTest
{
    static { Assert.assertEquals("", ""); }

    @Test
    public void testUsurp()
    {
        assertEquals("whoop!", MySingleton.getInstance().whoop());

        final MySingleton s = mock(MySingleton.class);
        when (s.whoop()).thenReturn("zing!");

        final SingletonUsurper usurper = new SingletonUsurper(MySingleton.class,s);

        assertEquals("zing!", MySingleton.getInstance().whoop());
        usurper.restore();

        assertEquals("whoop!", MySingleton.getInstance().whoop());
    }

    @Test
    public void testUsurpLazy()
    {
        assertEquals("whoop!", MyLazySingleton.getInstance().whoop());

        final MyLazySingleton s = mock(MyLazySingleton.class);
        when (s.whoop()).thenReturn("pooweet!");

        final SingletonUsurper usurper = new SingletonUsurper(MyLazySingleton.class,s);

        assertEquals("pooweet!", MyLazySingleton.getInstance().whoop());
        usurper.restore();

        assertEquals("whoop!", MyLazySingleton.getInstance().whoop());
    }

    @Test
    public void testSingleton()
    {
        assertEquals("whoop!", MySingleton.getInstance().whoop());
    }

    @Test
    public void testLazySingleton()
    {
        assertEquals("whoop!", MyLazySingleton.getInstance().whoop());
    }

}
