/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ViewSpace.java $
 * created by Owner Sep 11, 2006
 */
package org.yuwa.util.space.view;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

import org.yuwa.util.space.SpaceDisplay;
import org.yuwa.util.space.ViewSpace;





/**
 * The viewspace provides a global fog level.
 *  
 * @author Owner
 *
 */
public class ViewSpaceFogger implements ViewSpace
{
    private ViewSpace next = ViewSpace.NO_VIEWSPACE;

    private String id;

    
    public void clear()
    {
        this.next.clear();
    }
    
    /**
     * @see org.yuwa.util.space.ViewSpace#init(javax.media.opengl.GLAutoDrawable, javax.media.opengl.GL)
     */
    public void init(GLAutoDrawable drawable, GL gl) { this.next.init(drawable, gl); }

    /**
     * @see org.yuwa.util.space.ViewSpace#display(javax.media.opengl.GL)
     */
    public void display(GL gl)
    {
        gl.glEnable(GL.GL_FOG);
        float[] fogColor = {0.5f, 0.5f, 0.5f, 1.0f};

        gl.glFogi(GL.GL_FOG_MODE, GL.GL_EXP2);
        gl.glFogfv(GL.GL_FOG_COLOR, fogColor,1);
        gl.glFogf(GL.GL_FOG_DENSITY, 0.01f);
        gl.glHint(GL.GL_FOG_HINT, GL.GL_DONT_CARE);
        
        gl.glPushMatrix();

        this.next.display(gl);

        gl.glPopMatrix();

        gl.glDisable(GL.GL_FOG);
        
        gl.glFlush();
    }


    /**
     * @see org.yuwa.util.space.ViewSpace#add(Object, org.yuwa.util.space.SpaceDisplay)
     */
    public void add(final Object o, final SpaceDisplay display) { this.next.add(o,display); }


    /**
     * @see org.yuwa.util.space.ViewSpace#add(org.yuwa.util.space.SpaceDisplay)
     */
    public void add(final SpaceDisplay o) { this.next.add(o); }


    /**
     * @see org.yuwa.util.space.ViewSpace#remove(Object)
     */
    public void remove(final Object w) { this.next.remove(w); }
    
    /**
     * remove a display object
     */
    public void remove(final SpaceDisplay d) { this.next.remove(d); }

    /**
     * @see org.yuwa.util.space.ViewSpace#setNext(org.yuwa.util.space.ViewSpace)
     */
    public void setNext(ViewSpace next) { this.next = next; }

    /**
     * @return the id
     */
    public String getId() { return this.id; }

    /**
     * @param id the id to set
     */
    public void setId(String id) { this.id = id; }

}
