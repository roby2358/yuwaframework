/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ClasspathTextureHolder.java $
 * created by Owner Dec 19, 2006
 */
package org.yuwa.util.space.texture;

import java.io.IOException;
import java.io.InputStream;

import org.yuwa.util.space.Debug;

import com.sun.opengl.util.texture.Texture;
import com.sun.opengl.util.texture.TextureIO;

/**
 * This class represents a texture holder that 
 * loads an image as a resource on the classpath
 * 
 * @author Owner
 */
public class ClasspathTextureHolder implements TextureHolder
{
    private String id;

    private String path;

    private String extension = ".png";

    private Texture texture;

    private boolean enabled = false;

    private int countX = 1;

    private int countY = 1;

    
    /**
     * load the image if we haven't already
     */
    public void load()
    {
        this.initIfNeeded();
    }
    
    /**
     * @see org.yuwa.util.space.texture.TextureHolder#get(int)
     */
    public TextureBounds get(int i)
    {
        this.initIfNeeded();

        int x = i % this.countX;
        int y = i / this.countX;
        TextureBounds bounds = new TextureBounds(this);

        bounds.x0 = (float)x / this.countX;
        bounds.x1 = bounds.x0 + 1f / this.countX;
        bounds.y1 = 1f - (float)y / this.countY;
        bounds.y0 = bounds.y1 - 1f / this.countY;

        if ( ! this.enabled )
        {
            texture.enable();
            texture.bind();
        }

        return bounds;
    }

    /**
     * @see org.yuwa.util.space.texture.TextureHolder#getFull()
     */
    public TextureBounds getFull()
    {
        this.initIfNeeded();
        texture.enable();
        texture.bind();

        return new TextureBounds(this);
    }

    /**
     * @see org.yuwa.util.space.texture.TextureHolder#release()
     */
    public void release()
    {
        if ( this.texture != null ) texture.disable();
        this.enabled = false;
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // utility methods
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * if we haven't already, load the image & create the texture
     */
    private void initIfNeeded()
    {
        if ( this.texture == null )
            try {
                InputStream textureStream = this.getClass().getResourceAsStream(this.path);
                this.texture = TextureIO.newTexture(textureStream, true, this.extension);
                Debug.debug("...loaded " + this.path);
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    /**
     * @return the id
     */
    public String getId() { return this.id; }

    /**
     * @param id the id to set
     */
    public void setId(String id) { this.id = id; }

    /**
     * @return the texture
     */
    public Texture getTexture()
    {
        this.initIfNeeded();
        return this.texture;
    }

    /**
     * @param texture the texture to set
     */
    public void setTexture(Texture texture) { this.texture = texture; }

    /**
     * @param countX the countX to set
     */
    public void setCountX(int countX) { this.countX = countX; }

    /**
     * @param countY the countY to set
     */
    public void setCountY(int countY) { this.countY = countY; }

    /**
     * @param path the path to set
     */
    public void setPath(String path) { this.path = path; }

    /**
     * @param extension the extension to set
     */
    public void setExtension(String extension) { this.extension = extension; }

}
