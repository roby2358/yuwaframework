/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: Marker.java $
 * created by Owner Nov 2, 2006
 */
package org.yuwa.util.space.display;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

import org.yuwa.util.space.SpaceDisplay;
import org.yuwa.util.space.SimpleSpacePosition;
import org.yuwa.util.space.SpacePosition;



/**
 * @author Owner
 *
 */
public class Marker implements SpaceDisplay
{
    private SpacePosition position = new SimpleSpacePosition();
    
    private int displayType;
    
    private int displayId;

    
    public void init(GLAutoDrawable drawable, GL gl)
    {
    }
    
    
    public void display(GL gl)
    {
        gl.glLoadName(this.displayType);
        gl.glPushName(this.displayId);
        
        gl.glDisable(GL.GL_LIGHTING);
        gl.glBegin(GL.GL_LINES);
        gl.glColor4f(0f,0f,0f,1f);
        gl.glVertex3f(this.position.getOffX()+1f,this.position.getOffY(),this.position.getOffZ());
        gl.glVertex3f(this.position.getOffX()-1f,this.position.getOffY(),this.position.getOffZ());
        gl.glVertex3f(this.position.getOffX(),this.position.getOffY(),this.position.getOffZ()+1f);
        gl.glVertex3f(this.position.getOffX(),this.position.getOffY(),this.position.getOffZ()-1f);
        gl.glVertex3f(this.position.getOffX(),this.position.getOffY()+1f,this.position.getOffZ());
        gl.glVertex3f(this.position.getOffX(),this.position.getOffY()-1f,this.position.getOffZ());
        gl.glEnd();
        gl.glEnable(GL.GL_LIGHTING);
        
        gl.glPopName();
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    /**
     * @param position the position to set
     */
    public void set(SpacePosition position) { this.position = position; }


    /**
     * @return the displayId
     */
    public int getDisplayId() { return this.displayId; }


    /**
     * @param displayId the displayId to set
     */
    public void setDisplayId(int displayId) { this.displayId = displayId; }


    /**
     * @return the displayType
     */
    public int getDisplayType() { return this.displayType; }


    /**
     * @param displayType the displayType to set
     */
    public void setDisplayType(int displayType) { this.displayType = displayType; }

}
