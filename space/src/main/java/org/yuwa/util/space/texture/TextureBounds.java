/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: TextureBounds.java $
 * created by Owner Dec 19, 2006
 */
package org.yuwa.util.space.texture;


/**
 * contains the rectangle that defines a subimage of
 * the currently enabled and bound image.
 * 
 * Make sure you call release with done, so the texture
 * can be disabled.
 * 
 * @author Owner
 */
public class TextureBounds
{
    private TextureHolder holder;
    
    public float x0 = 0f;
    public float y0 = 0f;
    public float x1 = 1f;
    public float y1 = 1f;
    
    public TextureBounds(TextureHolder holder)
    {
        this.holder = holder;
    }
    
    /**
     * tell the holder to release the texture
     *
     */
    public void release() { this.holder.release(); }
    
}
