/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ViewPoint.java $
 * created by Owner Sep 26, 2006
 */
package org.yuwa.util.space.view;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

import org.yuwa.util.space.Debug;
import org.yuwa.util.space.SpaceDisplay;
import org.yuwa.util.space.SimpleSpacePosition;
import org.yuwa.util.space.SpacePosition;
import org.yuwa.util.space.ViewSpace;

/**
 * This is a viewpoint within the space. When drawn, it positions
 * the space relative to the current position.
 * 
 * @author Owner
 *
 */
public class FrustumViewpoint implements ViewSpace
{
    private String id;

    private SpacePosition position = new SimpleSpacePosition();
    private float orientRotX;
    private float orientRotY;
    private float orientRotZ;
    private float left;
    private float right;
    private float bottom;
    private float top;
    private float near;
    private float far;
    private float width = -1;

    private ViewSpace next = ViewSpace.NO_VIEWSPACE;

    
    /**
     * clear the frustum, which really doesn't do anything
     */
    public void clear()
    {
        this.next.clear();
    }

    /**
     * @see org.yuwa.warmage.view.space.ViewPoint#init(javax.media.opengl.GLAutoDrawable, javax.media.opengl.GL)
     */
    public void init(GLAutoDrawable drawable, GL gl)
    {
        // Get nice perspective calculations. 
        gl.glDepthFunc(GL.GL_LEQUAL);
        gl.glHint(GL.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);

        if ( this.width > 0 ) this.assignWidth(drawable, this.width);

        Debug.debug("init " + this);
        
        this.next.init(drawable, gl);
    }


    /**
     * @see org.yuwa.warmage.view.space.ViewPoint#display(javax.media.opengl.GL)
     */
    public void display(GL gl)
    {
//        Debug.debug("display " + this);
        
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glFrustum(left,right, bottom,top, near, far);

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();

        // face
        gl.glRotatef(-this.position.getRotX(),1,0,0);
        gl.glRotatef(-this.position.getRotY(),0,1,0);
        gl.glRotatef(-this.position.getRotZ(),0,0,1);

        // position
        gl.glTranslatef( -this.position.getOffX(), -this.position.getOffY(), -this.position.getOffZ() );

        // orient
        gl.glRotatef(-orientRotX,1,0,0);
        gl.glRotatef(-orientRotY,0,1,0);
        gl.glRotatef(-orientRotZ,0,0,1);

        gl.glPushMatrix();
        
        this.next.display(gl);
        
        gl.glPopMatrix();
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // utility methods
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * @see org.yuwa.warmage.view.space.ViewPoint#assignWidth(javax.media.opengl.GLAutoDrawable, float)
     */
    public void assignWidth(GLAutoDrawable drawable, float width)
    {
        float height = width * (float)drawable.getHeight() / (float)drawable.getWidth();

        this.left = -width/2f;
        this.right = width/2f;
        this.bottom = -height/2f;
        this.top = height/2f;
    }


    public String toString()
    {
        StringBuilder builder = new StringBuilder("[ FrustumViewpoint ");

        builder.append(" id=");
        builder.append(id);

        builder.append(" left=");
        builder.append(left);

        builder.append(" right=");
        builder.append(right);

        builder.append(" bottom=");
        builder.append(bottom);

        builder.append(" top=");
        builder.append(top);

        builder.append(" rotX=");
        builder.append(orientRotX);

        builder.append(" rotY=");
        builder.append(orientRotY);

        builder.append(" rotZ=");
        builder.append(orientRotZ);

        builder.append(" near=");
        builder.append(near);

        builder.append(" far=");
        builder.append(far);

        builder.append(" width=");
        builder.append(width);

        if( this.position != null )
        {
            builder.append(" facingX=");
            builder.append(this.position.getRotX());

            builder.append(" facingY=");
            builder.append(this.position.getRotY());

            builder.append(" facingZ=");
            builder.append(this.position.getRotZ());

            builder.append(" x=");
            builder.append(this.position.getOffX());

            builder.append(" y=");
            builder.append(this.position.getOffY());

            builder.append(" z=");
            builder.append(this.position.getOffZ());
        }

        builder.append(" ]");

        return builder.toString();
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    /**
     * @return the id
     */
    public String getId() { return this.id; }


    /**
     * @param id the id to set
     */
    public void setId(String id) { this.id = id; }
    

    /**
     * @see org.yuwa.warmage.view.space.ViewPoint#setProtoPlayer(org.yuwa.util.space.SimpleSpacePosition)
     */
    public void set(SpacePosition p) { this.position = p; }


    /**
     * @param bottom the bottom to set
     */
    public void setBottom(float bottom) { this.bottom = bottom; }


    /**
     * @param far the far to set
     */
    public void setFar(float far) { this.far = far; }


    /**
     * @param left the left to set
     */
    public void setLeft(float left) { this.left = left; }


    /**
     * @param near the near to set
     */
    public void setNear(float near) { this.near = near; }


    /**
     * @param right the right to set
     */
    public void setRight(float right) { this.right = right; }


    /**
     * @param top the top to set
     */
    public void setTop(float top) { this.top = top; }


    /**
     * @return the bottom
     */
    public float getBottom() { return this.bottom; }


    /**
     * @return the far
     */
    public float getFar() { return this.far; }


    /**
     * @return the left
     */
    public float getLeft() { return this.left; }


    /**
     * @return the near
     */
    public float getNear() { return this.near; }


    /**
     * @return the right
     */
    public float getRight() { return this.right; }


    /**
     * @return the top
     */
    public float getTop() { return this.top; }


    /**
     * @return the width
     */
    public float getWidth() { return this.width; }


    /**
     * @param width the width to set
     */
    public void setWidth(float width) { this.width = width; }


    /**
     * @see org.yuwa.warmage.view.space.ViewPoint#getPosition()
     */
    public SpacePosition getPosition() { return this.position; }

    /**
     * @return the orientRotX
     */
    public float getOrientRotX() { return this.orientRotX; }

    /**
     * @param orientRotX the orientRotX to set
     */
    public void setOrientRotX(float orientRotX) { this.orientRotX = orientRotX; }

    /**
     * @return the orientRotY
     */
    public float getOrientRotY() { return this.orientRotY; }


    /**
     * @param orientRotY the orientRotY to set
     */
    public void setOrientRotY(float orientRotY) { this.orientRotY = orientRotY; }


    /**
     * @return the orientRotZ
     */
    public float getOrientRotZ() { return this.orientRotZ; }


    /**
     * @param orientRotZ the orientRotZ to set
     */
    public void setOrientRotZ(float orientRotZ) { this.orientRotZ = orientRotZ; }

    
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // delegate methods
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * @return
     * @see org.yuwa.util.space.SimpleSpacePosition#getOffX()
     */
    public float getOffX() { return this.position.getOffX(); }


    /**
     * @return
     * @see org.yuwa.util.space.SimpleSpacePosition#getOffY()
     */
    public float getOffY() { return this.position.getOffY(); }


    /**
     * @return
     * @see org.yuwa.util.space.SimpleSpacePosition#getOffZ()
     */
    public float getOffZ() { return this.position.getOffZ(); }


    /**
     * @return
     * @see org.yuwa.util.space.SimpleSpacePosition#getRotX()
     */
    public float getRotX() { return this.position.getRotX(); }


    /**
     * @return
     * @see org.yuwa.util.space.SimpleSpacePosition#getRotY()
     */
    public float getRotY() { return this.position.getRotY(); }


    /**
     * @return
     * @see org.yuwa.util.space.SimpleSpacePosition#getRotZ()
     */
    public float getRotZ() { return this.position.getRotZ(); }


    /**
     * @param offX
     * @see org.yuwa.util.space.SimpleSpacePosition#setOffX(float)
     */
    public void setOffX(float offX) { this.position.setOffX(offX); }


    /**
     * @param offY
     * @see org.yuwa.util.space.SimpleSpacePosition#setOffY(float)
     */
    public void setOffY(float offY) { this.position.setOffY(offY); }


    /**
     * @param offZ
     * @see org.yuwa.util.space.SimpleSpacePosition#setOffZ(float)
     */
    public void setOffZ(float offZ) { this.position.setOffZ(offZ); }


    /**
     * @param rotX
     * @see org.yuwa.util.space.SimpleSpacePosition#setRotX(float)
     */
    public void setRotX(float rotX) { this.position.setRotX(rotX); }


    /**
     * @param rotY
     * @see org.yuwa.util.space.SimpleSpacePosition#setRotY(float)
     */
    public void setRotY(float rotY) { this.position.setRotY(rotY); }


    /**
     * @param rotZ
     * @see org.yuwa.util.space.SimpleSpacePosition#setRotZ(float)
     */
    public void setRotZ(float rotZ) { this.position.setRotZ(rotZ); }

    /**
     * @see org.yuwa.util.space.ViewSpace#setNext(org.yuwa.util.space.ViewSpace)
     */
    public void setNext(ViewSpace next) { this.next = next; }
    
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // delegate methods
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * @param o
     * @param display
     * @see org.yuwa.util.space.ViewSpace#add(Object, org.yuwa.util.space.SpaceDisplay)
     */
    public void add(Object o, SpaceDisplay display) { this.next.add(o, display); }


    /**
     * @param o
     * @see org.yuwa.util.space.ViewSpace#add(org.yuwa.util.space.SpaceDisplay)
     */
    public void add(SpaceDisplay o) { this.next.add(o); }


    /**
     * @param w
     * @see org.yuwa.util.space.ViewSpace#remove(Object)
     */
    public void remove(Object w) { this.next.remove(w); }
    
    /**
     * remove a display object
     */
    public void remove(final SpaceDisplay d) { this.next.remove(d); }

}
