/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: TextureHolder.java $
 * created by Owner Dec 19, 2006
 */
package org.yuwa.util.space.texture;


/**
 * @author Owner
 *
 */
public interface TextureHolder
{

    /**
     * @return the id of this object
     */
    String getId();
    
    /**
     * tells the texture to load -- note that the texture
     * will load automatically if this isn't called
     */
    void load();
    
    /**
     * get bounds that represents the i-th sub-image
     * 
     * @param i
     * @return
     */
    TextureBounds get(int i);
    
    /**
     * get a bounds object that represents the whole texture
     * @return
     */
    TextureBounds getFull();
    
    /**
     * release the texture that is currently enabled
     *
     */
    void release();

}
