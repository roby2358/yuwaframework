/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: SpacePosition.java $
 * created by Owner Feb 1, 2007
 */
package org.yuwa.util.space;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;


/**
 * @author Owner
 *
 */
public interface SpacePosition
{

    /**
     * @param that
     */
    void set(SpacePosition that);


    SpacePosition create();


    void init(GLAutoDrawable drawable, GL gl);


    void display(GL gl);


    void toArray(float[] array);


    /**
     * render this object as a string for diagnostics
     */
    String toString();


    /**
     * @return Returns the offX.
     */
    float getOffX();


    /**
     * @param offX The offX to set.
     */
    void setOffX(float offX);


    /**
     * @return Returns the offY.
     */
    float getOffY();


    /**
     * @param offY The offY to set.
     */
    void setOffY(float offY);


    /**
     * @return Returns the offZ.
     */
    float getOffZ();


    /**
     * @param offZ The offZ to set.
     */
    void setOffZ(float offZ);


    /**
     * @return Returns the rotX.
     */
    float getRotX();


    /**
     * @param rotX The rotX to set.
     */
    void setRotX(float rotX);


    /**
     * @return Returns the rotY.
     */
    float getRotY();


    /**
     * @param rotY The rotY to set.
     */
    void setRotY(float rotY);


    /**
     * @return Returns the rotZ.
     */
    float getRotZ();


    /**
     * @param rotZ The rotZ to set.
     */
    void setRotZ(float rotZ);

    SpacePosition NO_POSITION = new SpacePosition() {

        public SpacePosition create() { return this; }

        public void display(GL gl) { }

        public float getOffX() { return 0f; }

        public float getOffY() { return 0f; }

        public float getOffZ() { return 0f; }

        public float getRotX() { return 0f; }

        public float getRotY() { return 0f; }

        public float getRotZ() { return 0f; }

        public void init(GLAutoDrawable drawable, GL gl) { }

        public void set(SpacePosition that) { }

        public void setOffX(float offX) { }

        public void setOffY(float offY) { }

        public void setOffZ(float offZ) { }

        public void setRotX(float rotX) { }

        public void setRotY(float rotY) { }

        public void setRotZ(float rotZ) { }

        public void toArray(float[] array)
        {
            array[0] = 0f;
            array[1] = 0f;
            array[2] = 0f;
        } 
    };
}
