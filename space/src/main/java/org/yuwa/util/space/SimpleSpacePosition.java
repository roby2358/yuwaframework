/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: PositionedObject.java $
 * created by Owner Aug 30, 2006
 */
package org.yuwa.util.space;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

/**
 * Represents a position in the game space.
 * 
 * Note that Position implements the methods of Ticker, but isn't itself a ticker. The
 * object that owns the position is responsible for ticking it -- to avoid
 * double-ticking.
 * 
 * @author Owner
 *
 */
public class SimpleSpacePosition implements SpacePosition
{
    private float rotX;
    private float rotY;
    private float rotZ;
    
    private float offX;
    private float offY;
    private float offZ;
    
    
    public SimpleSpacePosition()
    {
        // nop
    }
    
    
    public SimpleSpacePosition(SimpleSpacePosition that)
    {
        this.set(that);
    }


    /**
     * @see org.yuwa.util.space.SpacePosition#set(org.yuwa.util.space.SimpleSpacePosition)
     */
    public void set(SpacePosition that)
    {
        this.rotX = that.getRotX();
        this.rotY = that.getRotY();
        this.rotZ = that.getRotZ();
        
        this.offX = that.getOffX();
        this.offY = that.getOffY();
        this.offZ = that.getOffZ();
    }
    
    
    /**
     * @see org.yuwa.util.space.SpacePosition#create()
     */
    public SimpleSpacePosition create()
    {
        return new SimpleSpacePosition(this);
    }
    
    
    /**
     * @see org.yuwa.util.space.SpacePosition#init(javax.media.opengl.GLAutoDrawable, javax.media.opengl.GL)
     */
    public void init(GLAutoDrawable drawable, GL gl)
    {
    }

    
    /**
     * @see org.yuwa.util.space.SpacePosition#display(javax.media.opengl.GL)
     */
    public void display(GL gl)
    {
        gl.glTranslatef(this.offX,this.offY,this.offZ);

        gl.glRotatef(this.rotX,1f,0f,0f);
        gl.glRotatef(this.rotY,0f,1f,0f);
        gl.glRotatef(this.rotZ,0f,0f,1f);
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // utility methods
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    
    /**
     * @see org.yuwa.util.space.SpacePosition#toArray(float[])
     */
    public void toArray(float[] array)
    {
        array[0] = this.offX;
        array[1] = this.offY;
        if ( array.length > 2 ) array[2] = this.offZ;
        if ( array.length > 3 ) array[3] = 1f;
    }
    
    
    /**
     * @see org.yuwa.util.space.SpacePosition#toString()
     */
    public String toString()
    {
        StringBuilder builder = new StringBuilder("[ Position ");
        
        builder.append(" x=");
        builder.append(this.offX);
        
        builder.append(" y=");
        builder.append(this.offY);
        
        builder.append(" z=");
        builder.append(this.offZ);
        
        builder.append(" rx=");
        builder.append(this.rotX);
        
        builder.append(" ry=");
        builder.append(this.rotY);
        
        builder.append(" rz=");
        builder.append(this.rotZ);
        
        builder.append(" ]");
        
        return builder.toString();
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * @see org.yuwa.util.space.SpacePosition#getOffX()
     */
    public float getOffX() { return this.offX; }

    /**
     * @see org.yuwa.util.space.SpacePosition#setOffX(float)
     */
    public void setOffX(float offX) { this.offX = offX; }

    /**
     * @see org.yuwa.util.space.SpacePosition#getOffY()
     */
    public float getOffY() { return this.offY; }

    /**
     * @see org.yuwa.util.space.SpacePosition#setOffY(float)
     */
    public void setOffY(float offY) { this.offY = offY; }

    /**
     * @see org.yuwa.util.space.SpacePosition#getOffZ()
     */
    public float getOffZ() { return this.offZ; }

    /**
     * @see org.yuwa.util.space.SpacePosition#setOffZ(float)
     */
    public void setOffZ(float offZ) { this.offZ = offZ; }

    /**
     * @see org.yuwa.util.space.SpacePosition#getRotX()
     */
    public float getRotX() { return this.rotX; }

    /**
     * @see org.yuwa.util.space.SpacePosition#setRotX(float)
     */
    public void setRotX(float rotX) { this.rotX = rotX; }

    /**
     * @see org.yuwa.util.space.SpacePosition#getRotY()
     */
    public float getRotY() { return this.rotY; }

    /**
     * @see org.yuwa.util.space.SpacePosition#setRotY(float)
     */
    public void setRotY(float rotY) { this.rotY = rotY; }

    /**
     * @see org.yuwa.util.space.SpacePosition#getRotZ()
     */
    public float getRotZ() { return this.rotZ; }

    /**
     * @see org.yuwa.util.space.SpacePosition#setRotZ(float)
     */
    public void setRotZ(float rotZ) { this.rotZ = rotZ; }
    
}
