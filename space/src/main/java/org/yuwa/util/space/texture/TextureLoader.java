/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: TextureLoader.java $
 * created by Owner Dec 26, 2006
 */
package org.yuwa.util.space.texture;

import java.util.HashMap;
import java.util.Map;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

import org.yuwa.util.space.Debug;
import org.yuwa.util.space.SpaceDisplay;

/**
 * This class just takes a bunch of textures and loads
 * them when asked
 * 
 * @author Owner
 *
 */
public class TextureLoader implements SpaceDisplay
{
    private Map<String,TextureHolder> texturesById = new HashMap<String,TextureHolder>();
    private int displayId;
    
    /**
     * add another texture
     * 
     * @param t
     */
    public void add(TextureHolder t)
    {
        this.texturesById.put(t.getId(), t);
    }
    
    
    /**
     * load all the textures we have
     *
     */
    public void load()
    {
    	Debug.debug("...load");
        for ( TextureHolder t : this.texturesById.values() )
        {
            t.load();
        }
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // SpaceDisplay implementation
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * @see org.yuwa.util.space.SpaceDisplay#display(javax.media.opengl.GL)
     */
    public void display(GL gl) { }


    /**
     * @see org.yuwa.util.space.SpaceDisplay#getDisplayId()
     */
    public int getDisplayId() { return this.displayId; }


    /**
     * @see org.yuwa.util.space.SpaceDisplay#getDisplayType()
     */
    public int getDisplayType() { return SpaceDisplay.Type.NON.ordinal(); }


    /**
     * @see org.yuwa.util.space.SpaceDisplay#init(javax.media.opengl.GLAutoDrawable, javax.media.opengl.GL)
     */
    public void init(GLAutoDrawable drawable, GL gl) { this.load(); }


    /**
     * @see org.yuwa.util.space.SpaceDisplay#setDisplayId(int)
     */
    public void setDisplayId(int i) { this.displayId = i; }
}
