/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ViewSpace.java $
 * created by Owner Sep 11, 2006
 */
package org.yuwa.util.space.view;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

import org.yuwa.util.space.SpaceDisplay;
import org.yuwa.util.space.ViewSpace;


/**
 * The viewspace holds a list of space objects, and defines
 * the coordinate system (i.e. the center and scale) of the space.
 * 
 * It also provides a global fog level.
 * 
 * This class also takes care of initializing new objects
 * that come in.
 *  
 * @author Owner
 *
 */
public class ViewSpaceInitializer implements ViewSpace
{
	private String id;

	private GLAutoDrawable drawable;

	public List<SpaceDisplay> objects = new ArrayList<SpaceDisplay>();

	private ViewSpace next = ViewSpace.NO_VIEWSPACE;


	/**
	 * clear out out list
	 */
	public void clear()
	{
		this.objects.clear();
		this.next.clear();
	}

	/**
	 * @see org.yuwa.util.space.ViewSpace#init(javax.media.opengl.GLAutoDrawable, javax.media.opengl.GL)
	 */
	public void init(GLAutoDrawable drawable, GL gl)
	{
		this.drawable = drawable;
		this.next.init(drawable, gl);
	}

	/**
	 * @see org.yuwa.util.space.ViewSpace#display(javax.media.opengl.GL)
	 */
	public void display(GL gl)
	{
		for ( SpaceDisplay o : this.objects )
		{
			o.init(this.drawable,gl);
		}
		this.objects.clear();

		this.next.display(gl);
	}


	/**
	 * @see org.yuwa.util.space.ViewSpace#add(Object, org.yuwa.util.space.SpaceDisplay)
	 */
	public void add(final Object o, final SpaceDisplay display)
	{
		this.objects.add(display);
		this.next.add(o, display);
	}


	/**
	 * @see org.yuwa.util.space.ViewSpace#add(org.yuwa.util.space.SpaceDisplay)
	 */
	public void add(final SpaceDisplay display)
	{
		this.objects.add(display);
		this.next.add(display);
	}


	/**
	 * @see org.yuwa.util.space.ViewSpace#remove(Object)
	 */
	public void remove(final Object w)
	{
		this.objects.remove(w);
		this.next.remove(w);
	}

	/**
	 * remove a display object
	 */
	public void remove(final SpaceDisplay d)
	{
		this.next.remove(d);
	}

	/**
	 * @see org.yuwa.util.space.ViewSpace#setNext(org.yuwa.util.space.ViewSpace)
	 */
	public void setNext(ViewSpace next) { this.next = next; }


	/**
	 * @return the id
	 */
	public String getId() { return this.id; }


	/**
	 * @param id the id to set
	 */
	public void setId(String id) { this.id = id; }

}
