/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ViewSpace.java $
 * created by Owner Sep 11, 2006
 */
package org.yuwa.util.space.view;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

import org.yuwa.util.space.Debug;
import org.yuwa.util.space.SpaceDisplay;
import org.yuwa.util.space.ViewSpace;
import org.yuwa.util.space.display.Marker;


/**
 * The viewspace holds a list of space objects, and defines
 * the coordinate system (i.e. the scale) of the space.
 * 
 * It also provides a global fog level.
 * 
 * This class also takes care of initializing new objects
 * that come in.
 *  
 * @author Owner
 *
 */
public class ViewSpaceCoords implements ViewSpace
{
    private String id;

    private float x;
    private float y;
    private float z;

    private float scale = 1;

    private Marker marker = new Marker();

    private ViewSpace next = ViewSpace.NO_VIEWSPACE;

    /**
     * clear doens't do anything, since the coords stay the same
     *
     */
    public void clear()
    {
        this.next.clear();
    }

    /**
     * @see org.yuwa.util.space.ViewSpace#init(javax.media.opengl.GLAutoDrawable, javax.media.opengl.GL)
     */
    public void init(GLAutoDrawable drawable, GL gl)
    {
        Debug.debug("init");

        this.marker.init(drawable, gl);

        this.next.init(drawable, gl);
    }

    /**
     * @see org.yuwa.util.space.ViewSpace#display(javax.media.opengl.GL)
     */
    public void display(GL gl)
    {
//      Debug.debug("display " + this.scale);

        // set the coordinate system
        float s = 1f/this.scale;
        gl.glScalef(s,s,s);
        gl.glTranslatef(this.x,this.y,this.z);

        gl.glPushMatrix();
        this.marker.display(gl);
        gl.glPopMatrix();

        this.next.display(gl);
    }

    /**
     * @see org.yuwa.util.space.ViewSpace#add(Object, org.yuwa.util.space.SpaceDisplay)
     */
    public void add(final Object o, final SpaceDisplay display) { this.next.add(o,display); }

    /**
     * @see org.yuwa.util.space.ViewSpace#add(org.yuwa.util.space.SpaceDisplay)
     */
    public void add(final SpaceDisplay o) { this.next.add(o); }

    /**
     * @see org.yuwa.util.space.ViewSpace#remove(Object)
     */
    public void remove(final Object w) { this.next.remove(w); }
    
    /**
     * remove a display object
     */
    public void remove(final SpaceDisplay d) { this.next.remove(d); }

    
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    /**
     * @return the id
     */
    public String getId() { return this.id; }


    /**
     * @param id the id to set
     */
    public void setId(String id) { this.id = id; }

    /**
     * @see org.yuwa.util.space.ViewSpace#setNext(org.yuwa.util.space.ViewSpace)
     */
    public void setNext(ViewSpace next) { this.next = next; }

    /**
     * @param scale the scale to set
     */
    public void setScale(float scale) { this.scale = scale; }

    /**
     * @param x the x to set
     */
    public void setX(float x) { this.x = x; }

    /**
     * @param y the y to set
     */
    public void setY(float y) { this.y = y; }

    /**
     * @param z the z to set
     */
    public void setZ(float z) { this.z = z; }

}
