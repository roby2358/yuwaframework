/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ViewSpaceSelector.java $
 * created by Owner Dec 22, 2006
 */
package org.yuwa.util.space.view;

import java.util.HashMap;
import java.util.Map;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

import org.yuwa.util.space.Debug;
import org.yuwa.util.space.SpaceDisplay;
import org.yuwa.util.space.ViewSpace;

/**
 * @author Owner
 *
 */
public class ViewSpaceSelector implements ViewSpace
{
    private ViewSpace next = ViewSpace.NO_VIEWSPACE;

    private ViewSpace current = ViewSpace.NO_VIEWSPACE;

    private Map<String,ViewSpace> viewsById = new HashMap<String,ViewSpace>();

    private String id;

    
    /**
     * clear everything out
     */
    public void clear()
    {
        for ( ViewSpace v : viewsById.values() ) v.clear();
    }
    
    /**
     * @see org.yuwa.util.space.ViewSpace#add(java.lang.Object, org.yuwa.util.space.SpaceDisplay)
     */
    public void add(Object o, SpaceDisplay display)
    {
        for ( ViewSpace v : this.viewsById.values() ) { v.add(o,display); }
    }


    /**
     * @see org.yuwa.util.space.ViewSpace#add(org.yuwa.util.space.SpaceDisplay)
     */
    public void add(SpaceDisplay o)
    {
        for ( ViewSpace v : this.viewsById.values() ) { v.add(o); }
    }


    /**
     * @see org.yuwa.util.space.ViewSpace#init(javax.media.opengl.GLAutoDrawable, javax.media.opengl.GL)
     */
    public void init(GLAutoDrawable drawable, GL gl)
    {
        this.current.init(drawable, gl);

        for ( ViewSpace v : this.viewsById.values() ) { v.init(drawable, gl); }
    }


    /**
     * @see org.yuwa.util.space.ViewSpace#display(javax.media.opengl.GL)
     */
    public void display(GL gl)
    {
        this.current.display(gl);
    }


    /**
     * @see org.yuwa.util.space.ViewSpace#remove(java.lang.Object)
     */
    public void remove(Object w)
    {
        for ( ViewSpace v : this.viewsById.values() ) { v.remove(w); }
    }


    /**
     * @see org.yuwa.util.space.ViewSpace#remove(java.lang.Object)
     */
    public void remove(SpaceDisplay w)
    {
        for ( ViewSpace v : this.viewsById.values() ) { v.remove(w); }
    }


    /**
     * @see org.yuwa.util.space.ViewSpace#setNext(org.yuwa.util.space.ViewSpace)
     */
    public void setNext(ViewSpace next)
    {
        Debug.debug("*** next " + next);
        this.next = next;
        this.current.setNext(next);
    }

    
    public void select(ViewSpace viewspace)
    {
        this.current = viewspace;

        for ( ViewSpace v : this.viewsById.values() ) v.setNext(ViewSpace.NO_VIEWSPACE);
        
        this.current.setNext( this.next );

        Debug.debug("*** select " + this.current);
    }
    
    
    public void select(String id)
    {
        this.select( this.viewsById.get(id) );
    }


    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    public void add(ViewSpace v)
    {
        Debug.debug("*** add " + v);
        this.viewsById.put(v.getId(),v);
        if ( this.current == ViewSpace.NO_VIEWSPACE ) this.select(v);
    }


    /**
     * @return the id
     */
    public String getId() { return this.id; }


    /**
     * @param id the id to set
     */
    public void setId(String id) { this.id = id; }

    
    /**
     * return the viewspace named by the id
     * 
     * @param id
     * @return the viewspace named by the id
     */
    public ViewSpace getView(String id) { return this.viewsById.get(id); }
}
