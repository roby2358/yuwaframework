/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ViewSpace.java $
 * created by Owner Dec 13, 2006
 */
package org.yuwa.util.space;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;





/**
 * @author Owner
 *
 */
public interface ViewSpace
{

    /**
     * @return
     */
    String getId();
    
    /**
     * clear (reset) the view space
     *
     */
    void clear();

    /**
     * initizlize the space
     * 
     * @param drawable
     * @param gl
     */
    void init(final GLAutoDrawable drawable, final GL gl);


    /**
     * display the space
     * 
     * @param gl
     */
    void display(final GL gl);


    /**
     * add a world object to the display
     * 
     * @param o
     */
    void add(final Object o, final SpaceDisplay display);


    /**
     * add a space display object
     * 
     * @param o the space display object to add
     */
    void add(final SpaceDisplay o);


    /**
     * remove a world object from the display
     * 
     * @param w
     */
    void remove(final Object w);


    /**
     * remove a display object
     * 
     * @param w
     */
    void remove(final SpaceDisplay display);
    
    /**
     * set the next viewspace in the chain
     * 
     * @param next
     */
    void setNext(ViewSpace next);

    /**
     * a null object to use as a placeholder
     */
    ViewSpace NO_VIEWSPACE = new ViewSpace() {

        public String getId() { return "NO_VIEWSPACE"; }

        public void clear() { }
        
        public void add(Object o, SpaceDisplay display) { }

        public void add(SpaceDisplay o) { }

        public void init(GLAutoDrawable drawable, GL gl) { }

        public void display(GL gl) { }

        public void remove(Object w) { }

        public void setNext(ViewSpace next) { }

        public void remove(SpaceDisplay display) { }
        
    };
}
