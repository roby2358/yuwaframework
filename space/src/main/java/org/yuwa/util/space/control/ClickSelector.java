/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ClickSelector.java $
 * created by Owner Dec 24, 2006
 */
package org.yuwa.util.space.control;

import java.awt.event.MouseEvent;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

import org.yuwa.util.space.Debug;
import org.yuwa.util.space.view.FrustumViewpoint;
import org.yuwa.util.space.view.ViewSpaceSelector;

import com.sun.opengl.util.BufferUtil;

/**
 * @author Owner
 *
 */
public class ClickSelector
{
    private static final int SELECT_BUFER_SIZE = 100;

    private String selectorDisplayName = "viewpoint";
    private String selectorSelectName = "selectViewpoint";

    private ViewSpaceSelector viewSelector;

    private FrustumViewpoint displayView;

    private FrustumViewpoint selectView;

    private float pixelWidth = .01f;

    private List<SpaceClick> hits = new ArrayList<SpaceClick>();

    private IntBuffer intBuffer;


    /**
     * constructor does initialization
     *
     */
    public ClickSelector()
    {
        this.intBuffer = BufferUtil.newIntBuffer(SELECT_BUFER_SIZE);
    }


    /**
     * copy constructor
     * @param that
     */
    public ClickSelector(ClickSelector that)
    {
        super();
        this.selectorDisplayName = "viewpoint";
        this.selectorSelectName = "selectViewpoint";
        this.viewSelector = that.viewSelector;
        this.displayView = that.displayView;
        this.selectView = that.selectView;
        this.pixelWidth = .01f;
    }

    public ClickSelector copy()
    {
        return new ClickSelector(this);
    }
    
    private void clear()
    {
        this.hits = new ArrayList<SpaceClick>();
        this.intBuffer.clear();
    }

    /**
     * @param gl
     * @param drawable
     */
    public void findHits(GL gl, GLAutoDrawable drawable, MouseEvent mouseEvent)
    {
        this.clear();
        
        this.displayView = (FrustumViewpoint) this.viewSelector.getView(this.selectorDisplayName);
        this.selectView = (FrustumViewpoint) this.viewSelector.getView(this.selectorSelectName);

        float pixelX = this.pixelWidth / drawable.getWidth();
        float pixelY = this.pixelWidth / drawable.getHeight();
        float fx = ( this.displayView.getRight() - this.displayView.getLeft() ) * ( (float)mouseEvent.getX() / (float)drawable.getWidth() );
        float fy =( this.displayView.getTop() - this.displayView.getBottom() ) * ( (float)mouseEvent.getY() / (float)drawable.getHeight() );
        float xx = this.displayView.getLeft() + fx;
        float yy = this.displayView.getTop() - fy;

        Debug.debug("left " + this.displayView.getLeft() + " + " + fx);
        Debug.debug("bottom " + this.displayView.getTop() + " - " + fy);

        this.selectView.setWidth(-1);
        this.selectView.setLeft( xx - pixelX );
        this.selectView.setRight( xx + pixelX );
        this.selectView.setBottom( yy - pixelY );
        this.selectView.setTop( yy + pixelY );

        Debug.debug("use click selectView " + this.selectView);

        gl.glSelectBuffer(intBuffer.capacity(),intBuffer);
        gl.glRenderMode(GL.GL_SELECT);
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glInitNames();
        this.viewSelector.select(selectorSelectName);
        this.viewSelector.display(gl);

        int count = gl.glRenderMode(GL.GL_RENDER);

        for ( int i = 0 ; i < count ; i++ )
        {
            SpaceClick click = new SpaceClick();
            click.take(intBuffer);
            this.hits.add(click);
        }

        this.viewSelector.select(selectorDisplayName);
    }


    /**
     * 
     * @param hits
     * @param intBuffer
     */
    public void showHits()
    {
        Debug.debug("hits = " + hits.size());
        for (SpaceClick click : this.hits)
        {
            Debug.debug("click " + click);
        }
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * @param viewSpaceSelector
     */
    public void setViewSpaceSelector(ViewSpaceSelector selector) { this.viewSelector = selector; }


    /**
     * @param selectorDisplayName the selectorDisplayName to set
     */
    public void setSelectorDisplayName(String selectorDisplayName) { this.selectorDisplayName = selectorDisplayName; }


    /**
     * @param selectorSelectName the selectorSelectName to set
     */
    public void setSelectorSelectName(String selectorSelectName) { this.selectorSelectName = selectorSelectName; }


    /**
     * @param pixelWidth the pixelWidth to set
     */
    public void setPixelWidth(float pixelWidth) { this.pixelWidth = pixelWidth; }

    /**
     * @return the list of hits for the click
     */
    public List<SpaceClick> getHits() { return this.hits; }

}
