/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: SpaceClick.java $
 * created by Owner Jan 1, 2007
 */
package org.yuwa.util.space.control;

import java.nio.IntBuffer;

/**
 * @author Owner
 *
 */
public class SpaceClick
{
    private float near;

    private float far;

    private int[] values = new int[0];

    public void take(IntBuffer buffer)
    {
        // get the number of names in the stack
        int names = buffer.get();
        this.values = new int[names];

        // get the near & far Z values
        this.near = this.asFloat(buffer.get());
        this.far = this.asFloat(buffer.get());

        // copy the stack values
        for ( int i = 0 ; i < names ; i++ )
        {
            this.values[i] = buffer.get();
        }
    }

    /**
     * 
     * @param i
     * @return the integer normalized against the max Int value
     */
    private float asFloat(int i)
    {
        return (float)i / (float)Integer.MAX_VALUE;
    }

    /**
     * render this object as a string for diagnostics
     * @return this object as a string
     */
    public String toString()
    {
        StringBuilder builder = new StringBuilder("[ SpaceClick ");

        builder.append(" near=");
        builder.append(this.near);

        builder.append(" far=");
        builder.append(this.far);

        builder.append(" values=");
        builder.append(this.values.length);

        builder.append(" (");
        for ( int i = 0 ; i < this.values.length ; i++ )
        {
            builder.append(" ");
            builder.append(this.values[i]);
        }

        builder.append(" ) ]");

        return builder.toString();
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * @return the far
     */
    public float getFar() { return this.far; }

    /**
     * @return the near
     */
    public float getNear() { return this.near; }

    /**
     * @return the values
     */
    public int[] getValues() { return this.values; }

}
