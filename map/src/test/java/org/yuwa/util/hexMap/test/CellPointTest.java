/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/*
 * Created on Feb 16, 2005
 *
 */
package org.yuwa.util.hexMap.test;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.yuwa.util.hexMap.CellPoint;

/**
 * @author Owner
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CellPointTest extends TestCase
{
    CellPoint a;
    CellPoint b;
    CellPoint c;
    CellPoint d;
    
    public static Test suite()
    {
        return new TestSuite(CellPointTest.class);
    }
    
    protected void setUp()
    {
        this.a = new CellPoint(12,34);
        this.b = new CellPoint(12,34);
        this.c = new CellPoint(12,12);
        this.d = new CellPoint(56,34);
    }
    
    
    public void testEquals()
    {
        Assert.assertEquals(a,a);
        Assert.assertEquals(a,b);
        Assert.assertEquals(a,new TestCellPoint(a));
        Assert.assertTrue(! a.equals(c));
        Assert.assertTrue(! a.equals(d));
        Assert.assertTrue(! a.equals(null));
        Assert.assertTrue(! a.equals("abc"));
    }
    
    
    public void testHash()
    {
        Assert.assertEquals( a.hashCode(),b.hashCode() );
        Assert.assertTrue( a.hashCode() != c.hashCode() );
        Assert.assertTrue( a.hashCode() != d.hashCode() );
    }
    
    
    public void testCopy()
    {
        CellPoint x = a.copy();
        Assert.assertEquals(a,x);
        Assert.assertEquals(a.hashCode(),x.hashCode());
    }
    
    
    public void testNoPoint()
    {
        CellPoint p = new CellPoint(-1,-1);
        Assert.assertEquals(p,CellPoint.NO_POINT);
        CellPoint.NO_POINT.setX(0);
        CellPoint.NO_POINT.setY(0);
        Assert.assertEquals(p,CellPoint.NO_POINT);
    }
    
    private static class TestCellPoint extends CellPoint
    {
        TestCellPoint(CellPoint a) { super(a.getX(),a.getY()); }
    }
}
