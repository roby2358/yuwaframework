/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/* Generated by Together */

package org.yuwa.util.hexMap;

import java.util.HashSet;
import java.util.Set;


/**
 * This class represents a hex grid, implemented as a regular x,y grid
 * with every other row offset. It encapsulates calculation methods on the
 * grid, but does not include storage of anything in the grid.
 */
public class HexMap {

//   4   2
//    \ /
// 5 - o - 0
//    / \
//   3   1

    private static final int[][][] ADJACENCY = new int[][][]
    {
    { {1,0}, {1,1}, {1,-1}, {0,1}, {0,-1}, {-1,0} },
    { {1,0}, {0,1}, {0,-1}, {-1,1}, {-1,-1}, {-1,0} },

    { {1,1}, {0,1}, {1,0}, {-1,0}, {1,-1}, {0,-1} },
    { {0,1}, {-1,1}, {1,0}, {-1,0}, {0,-1}, {-1,-1} },

    { {1,-1}, {1,0}, {0,-1}, {1,1}, {-1,0}, {0,1} },
    { {0,-1}, {1,0}, {-1,-1}, {0,1}, {-1,0}, {-1,1} },

    { {0,1}, {-1,0}, {1,1}, {0,-1}, {1,0}, {1,-1} },
    { {-1,1}, {-1,0}, {0,1}, {-1,-1}, {1,0}, {0,-1} },

    { {0,-1}, {-1,-1}, {1,-1}, {0,-1}, {1,0}, {1,-1} },
    { {-1,-1}, {0,-1}, {-1,0}, {1,0}, {-1,1}, {0,1} },

    { {-1,0}, {0,-1}, {0,1}, {1,-1}, {1,1}, {1,0} },
    { {-1,0}, {-1,-1}, {-1,1}, {0,-1}, {0,1}, {1,0} }
    };




    private final int width;
    private final int height;

    /** constructor; initialize the map with width and height */
    public HexMap(int w, int h) {
        this.width = w;
        this.height = h;
    }


    public CellPoint[] step(CellPoint p1, CellPoint p2)
    {
        CellPoint[] result = null;

        int x1 = this.calcBigX(p1);
        int x2 = this.calcBigX(p2);
        int dx = Math.abs(x2 - x1);
        int dy = Math.abs(p2.getY() - p1.getY());

        boolean flat = ( dx > dy );
        boolean up = ( p2.getY() < p1.getY() );
        boolean forwards = ( x2 > x1 );

        int r = 0;

        if ( flat && forwards ) r = 0;
        else if ( flat ) r = 5;
        else if ( up && forwards ) r = 2;
        else if ( up ) r = 4;
        else if ( forwards ) r = 1;
        else r = 3;

        int[][] a = ADJACENCY[r * 2 + (p1.getY() & 1) ];

        result = new CellPoint[6];
        for ( int i = 0 ; i < 6 ; i++ )
        {
            result[i] = this.newPoint( p1.getX() + a[i][0], p1.getY() + a[i][1] );
        }

        return result;
    }


    /** find the range from one point to another */
    public int range(CellPoint p1, CellPoint p2) {
        int x1 = this.calcBigX(p1);
        int x2 = this.calcBigX(p2);
        int dy = Math.abs(p2.getY() - p1.getY());
        int dx = Math.abs(x2 - x1);

        int range = dy;
        if (dx > dy) range += (dx - dy) / 2;
        return range;
    }


    /**
     * determine if two points are adjacent
     * @param p1 point 1
     * @param p2 point 2
     * @return true if adjacent
     */
    public boolean adjacent(CellPoint p1, CellPoint p2)
    {
        return range(p1,p2) == 1;
    }

    public interface RangeComparer {
        public boolean atRange(int r1, int r2);
    }


    private static final RangeComparer WITHIN_RANGE_COMPARER = new RangeComparer() {
            public boolean atRange(int r1, int r2) {
                return r1 >= r2;
            }
        };

    private static final RangeComparer OUTSIDE_RANGE_COMPARER = new RangeComparer() {
            public boolean atRange(int r1, int r2) {
                return r1 < r2;
            }
        };

    private static final RangeComparer AT_RANGE_COMPARER = new RangeComparer() {
            public boolean atRange(int r1, int r2) {
                return r1 == r2;
            }
        };

    /** get the set of all cells in range of a given cell and range */
    public Set inRange(CellPoint p1, int r) {
        return this.atRange(p1, r, WITHIN_RANGE_COMPARER);
    }


    /** get the set of all cells in range of a given cell and range */
    public Set outsideRange(CellPoint p1, int r) {
        return this.atRange(p1, r, OUTSIDE_RANGE_COMPARER);
    }


    /** get the set of all cells in range of a given cell and range */
    public Set atRange(CellPoint p1, int r) {
        return this.atRange(p1, r, AT_RANGE_COMPARER);
    }


    /**
     *  internal implementation of atRange, takes a range comparer
     */
    protected Set atRange(CellPoint p1, int r, RangeComparer comp) {
        Set<CellPoint> result = new HashSet<CellPoint>();
        for (int j = this.getWidth() - 1; j >= 0; j--) {
            for (int i = this.getWidth() - 1; i >= 0; i--) {
                CellPoint p2 = new CellPoint(i, j);
                if (comp.atRange(r, this.range(p1, p2))) {
                    result.add(p2);
                }
            }
        }
        return result;
    }


    /** find the line of sight from one point to another */
    public CellPath getLos(CellPoint p1, CellPoint p2) {
        CellPath result = new CellPath();
        result.add(p1);

        int x1 = calcBigX(p1);
        int x2 = calcBigX(p2);
        int y2 = p2.getY();
        int yy = p1.getY();
        int xx = x1;
        int dy = p2.getY() - p1.getY();
        int dx = x2 - x1;

        Stepper stepper1 = new Stepper();
        Stepper stepper2 = new Stepper();
        int dd           = (Math.abs(dx) - Math.abs(dy)) / 2;

        if (dd >= 0) {
            stepper2.dd = Math.abs(dy);
            stepper2.dy = Stepper.sgn(dy);
            stepper2.dx = Stepper.sgn(dx);

            stepper1.dd = dd;
            stepper1.dx = stepper2.dx * 2;
        }
        else {
            // have to split the x change between 1 & 2
            stepper1.dy = Stepper.sgn(dy);
            stepper2.dy = stepper1.dy;
            stepper1.dd = (Math.abs(dx) + Math.abs(dy) + 1) / 2;
            stepper1.dx = Stepper.sgn(dx);
            stepper2.dd = Math.abs(dy) - stepper1.dd;
            stepper2.dx = -stepper1.dx;
        }

        int m = Math.max(stepper1.dd, stepper2.dd);
        stepper1.setM(m);
        stepper2.setM(m);
        int die = m;

        while (die-- > 0 && (xx != x2 || yy != y2)) {

            if (stepper1.step()) {
                yy += stepper1.dy;
                xx += stepper1.dx;
                result.add(this.fix(xx, yy));
            }

            if (stepper2.step()) {
                yy += stepper2.dy;
                xx += stepper2.dx;
                result.add(this.fix(xx, yy));
            }

        }
        return result;
    }

    /** calculate the "big X" -- X on a doubled grid, offset depending on Y */
    private int calcBigX(CellPoint p) {
        return p.getX() * 2 + 1 - (p.getY() & 1);
    }

    /** this method takes a doubled x, rounds it to the correct hex & builds a point */
    private CellPoint fix(int x, int y) {
        int xx = x - 1 + (y & 1);
        return new CellPoint(xx / 2, y);
    }


    /** get the width of the map */
    public int getWidth() { return width; }


    /** get the height of the map */
    public int getHeight() { return height; }


    /**
     * returns a cell point object if valid, otherwise returns NO_POINT
     * @param x
     * @param y
     * @return a valid CellPoint object, or NO_POINT
     */
    public CellPoint newPoint(int x, int y)
    {
        final CellPoint result;

        if ( x < 0 || x >= this.getWidth() )
        {
            result = CellPoint.NO_POINT;
        }
        else if ( y < 0 || y >= this.getHeight() )
        {
            result = CellPoint.NO_POINT;
        }
        else
        {
            result = new CellPoint(x,y);
        }

        return result;
    }
}
