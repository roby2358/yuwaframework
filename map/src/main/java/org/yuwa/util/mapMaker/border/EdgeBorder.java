/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/* Generated by Together */

package org.yuwa.util.mapMaker.border;

import org.yuwa.util.mapMaker.ByteGrid;
import org.yuwa.util.mapMaker.GridBuilder;
import org.yuwa.util.mapMaker.util.BuilderHelper;

public class EdgeBorder extends BuilderHelper implements GridBuilder
{
    private GridBuilder next = NO_BUILDER;


    public EdgeBorder() { super(); }


    public EdgeBorder(BuilderHelper b) { super(b); }


    public void accept(ByteGrid grid)
    {
        for ( int x = 0; x < grid.getWidth(); x++ )
        {
            this.setRandom( grid, x, 0 );
            this.setRandom( grid, x, grid.getHeight() - 1 );
        }

        for ( int y = 0; y < grid.getHeight(); y++ )
        {
            this.setRandom( grid, 0, y );
            this.setRandom( grid, grid.getWidth() - 1, y );
        }
        
        this.next.accept(grid);
    }

    
    public void setNext(GridBuilder next)
    {
        this.next = next;
    }
}

