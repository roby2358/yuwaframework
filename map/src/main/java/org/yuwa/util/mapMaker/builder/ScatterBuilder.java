/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


/**
 * $Id: ScatterWorldBuilder.java $
 * created by Owner Sep 1, 2006
 */
package org.yuwa.util.mapMaker.builder;

import java.util.Random;
import java.util.TreeSet;

import org.yuwa.util.mapMaker.ByteGrid;
import org.yuwa.util.mapMaker.GridBuilder;



/**
 * This builder works by scattering seeds and then doing a random
 * traversal of the map extending those seeds 
 * 
 * @author Owner
 *
 */
public class ScatterBuilder implements GridBuilder
{
    private ByteGrid map;
    private TreeSet<Seed> stack = new TreeSet<Seed>();
    private Random random = new Random();
    private int background = -1;
    private int maxTerrain = 16;
    private int seedCount = 5;
    private int clusterSize = 2;

    private GridBuilder next = GridBuilder.NO_BUILDER;
    
    private final ScatterBuilder THIS = this;

    /**
     * @see org.yuwa.util.mapMaker.GridBuilder#accept(org.yuwa.util.mapMaker.ByteGrid)
     */
    public void accept(ByteGrid g)
    {
        this.map = g;
        this.build();
        this.next.accept(this.map);
    }

    /**
     * place the seeds and scatter them around to fill the map
     * @return the completed map
     */
    private void build()
    {
        // plant a bunch of seeds
        this.chooseTerrain();

        // start spreading the seeds
        while ( this.stack.size() > 0 )
        {
            // pop the next seed
            Seed next = this.stack.first();
            this.stack.remove( next );
            
            next.scatter();
        }

    }


    /**
     * 
     */
    private void chooseTerrain()
    {
        int clusters = this.maxTerrain / this.clusterSize + 1;
        int remaining = this.seedCount;

        for ( int i = 0 ; remaining > 0 && i < clusters ; i++ )
        {
            if ( this.random.nextInt(clusters - i) < remaining )
            {
                remaining--;
                
                Seed seed = new Seed();
                
                do
                {
                    seed.x = this.random.nextInt(this.map.getWidth() / 3) * 3 + 1;
                    seed.y = this.random.nextInt(this.map.getHeight() / 3) * 3 + 1;

                    if ( seed.x >= this.map.getWidth() ) seed.x = this.map.getWidth();
                    if ( seed.y >= this.map.getHeight() ) seed.y = this.map.getHeight();

                    seed.value = i * this.clusterSize;
                }
                while ( this.map.cell(seed.x, seed.y) != this.background );

                seed.claim();
                seed.scatter();
            }
        }
    }


    /**
     * assign a weight to a seed based on the given seed
     * @param seed
     */
    private int assignWeight(Seed seed)
    {
        int priority = seed.weight + 1 + this.random.nextInt(10);
//        int priority = ( seed.weight / 10 + 1 ) * 10 + this.random.nextInt(10);
//      int priority = ++this.counter;
        return priority;
    }


    /**
     * @param next
     * @return
     */
    private byte cluster(Seed next)
    {
        return (byte)( (next.value / clusterSize) * clusterSize + this.random.nextInt(clusterSize) );
    }


    /**
     * this inner class represents a cell that's been
     * planted & is waiting to get popped off the stack
     * 
     * @author Owner
     *
     */
    class Seed implements Comparable<Seed>
    {
        public int x;
        public int y;
        public int value;
        public int weight = 0;

        
        /**
         * utility method -- claim the current space
         */
        void claim() { this.claim(0,0); }

        /**
         * @param value
         * @param THIS TODO
         * @param cells TODO
         * @param i
         * @param j
         */
        void claim(int i, int j)
        {
            int xx = x + i;
            int yy = y + j;

            if ( xx < 0 || xx >= THIS.map.getWidth() )
            {
                // nop
            }
            else if ( yy < 0 || yy >= THIS.map.getHeight() )
            {
                // nop
            }
            else if ( THIS.map.cell(xx,yy) == THIS.background )
            {
                THIS.map.getGrid()[xx][yy] = THIS.cluster(this);

                int weight = THIS.assignWeight(this);

                if ( weight >= 0 )
                {
                    Seed newSeed = new Seed();
                    newSeed.x = xx;
                    newSeed.y = yy;
                    newSeed.value = value;
                    newSeed.weight = weight;

                    THIS.stack.add(newSeed);
                }
            }
        }

        /**
         * @param builder TODO
         */
        void scatter()
        {
            claim(-1,0);
            claim(+1,0);

            claim(0,-1);
            claim(0,+1);

            if ( (y & 1) == 0 )
            {
                claim(+1,-1);
                claim(+1,+1);
            }
            else
            {
                claim(-1,-1);
                claim(-1,+1);
            }
        }

        public String toString()
        {
            return("[ seed " + this.x + "," + this.y + " " + this.weight + " " + this.value + "]");
        }

        /**
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        public int compareTo(Seed that)
        {
            int compare = this.weight - that.weight;
            
            if ( compare == 0 ) compare = this.y - that.y;
            
            if ( compare == 0 ) compare = this.x - that.x;
            
            return compare;
        }

        public boolean equals(Object that)
        {
            boolean result = false;

            if ( that == null )
            {
                result = false;
            }
            else if ( that == this )
            {
                result = true;
            }
            else if ( that instanceof Seed )
            {
                result = this.equals( (Seed)that );
            }

            return result;
        }

        public boolean equals(Seed that)
        {
            boolean result = false;

            if ( that == null )
            {
                result = false;
            }
            else if ( that == this )
            {
                result = true;
            }
            else
            {
                result = (this.compareTo(that) == 0);
            }

            return result;
        }

        public int hashcode()
        {
            return this.weight;
        }

    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    /**
     * @see org.yuwa.util.mapMaker.GridBuilder#setNext(org.yuwa.util.mapMaker.GridBuilder)
     */
    public void setNext(GridBuilder n) { this.next = n; }

    /**
     * @param background The background to set.
     */
    public void setBackground(int background) { this.background = background; }


    /**
     * set the number of seeds to place
     * 
     * @param seedCount the seedCount to set
     */
    public void setSeedCount(int seedCount) { this.seedCount = seedCount; }


    /**
     * @param maxTerrain The maxTerrain to set.
     */
    public void setMaxTerrain(int maxTerrain) { this.maxTerrain = maxTerrain; }


    /**
     * @param clusterSize the clusterSize to set
     */
    public void setClusterSize(int clusterSize) { this.clusterSize = clusterSize; }

}
