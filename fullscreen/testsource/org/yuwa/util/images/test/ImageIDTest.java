/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.images.test;

import junit.framework.TestCase;

import org.yuwa.util.images.ImageID;
import org.yuwa.util.images.ImageIDBuilder;


public class ImageIDTest extends TestCase
{
    String group = "group";
    int pic = 0;
    ImageID id;

    public void setUp()
    {
        ImageIDBuilder builder = new ImageIDBuilder();
        builder.setGroup(group);
        builder.setPic(pic);
        this.id = builder.imageID();
    }
    
    public void testNoId()
    {
        assertTrue( !ImageID.NO_ID.isSet() );
    }
    
    public void testIsSet()
    {
        assertTrue(this.id.isSet());
    }
    
    public void testCopy()
    {
        ImageID that = this.id.copy();
        assertEquals(this.id.toString(), that.toString() );
    }

    /*
     * Class under test for String toString()
     */
    public void testToString()
    {
        assertEquals("[ image group #0 ]",this.id.toString());
    }
    
    public void testNoImageIDisSet()
    {
        assertFalse(ImageID.NO_ID.isSet());
    }
    
    public void testNoImageIDtoString()
    {
        assertEquals("(no image)",ImageID.NO_ID.toString());
    }

}
