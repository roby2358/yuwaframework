/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.images.test;

import java.awt.Image;

import junit.framework.TestCase;

import org.yuwa.util.images.ImageGroup;
import org.yuwa.util.images.ImageIDBuilder;

public class ImageGroupTest extends TestCase
{
    public static String ID = "group";

    public static String FILE_PATH = "/terrain.gif";

    public static int COUNT = 12;

    public static int HEIGHT = 64;

    public static int WIDTH = 64;

    public ImageGroup group = new ImageGroup();

    public void setUp()
    {
        this.group.setId(ID);
        this.group.setFilePath(FILE_PATH);
        this.group.setHeight(HEIGHT);
        this.group.setWidth(WIDTH);
    }

    public void testGetId()
    {
        assertEquals(ID, this.group.getId());
    }

    public void testSetId()
    {
        // done in setUp
    }

    public void testGetFilePath()
    {
        assertEquals(FILE_PATH, this.group.getFilePath());
    }

    public void testSetFilePath()
    {
        // done in setUp
    }

    public void testGetWidth()
    {
        assertEquals(WIDTH, this.group.getWidth());
    }

    public void testSetWidth()
    {
        // done in setUp
    }

    public void testGetHeight()
    {
        assertEquals(HEIGHT, this.group.getHeight());
    }

    public void testSetHeight()
    {
        // done in setUp
    }

    public void testGetCount()
    {
        this.group.setCount(COUNT);
        assertEquals(COUNT, this.group.getCount());
    }

    public void testSetCount()
    {
        // done in setUp
    }

    public void testItem()
    {
        this.group.load();
        Image image = this.group.item(12);
        assertEquals(64, this.group.getCount());
        assertNotNull(image);
    }

    public void testItemTooBig()
    {
        boolean ok = false;
        try
        {
            this.group.setCount(4);
            this.group.load();
            Image image = this.group.item(6);
            assertEquals(64, this.group.getCount());
            assertNotNull(image);
        }
        catch (IllegalArgumentException e)
        {
            ok = true;
        }

        assertTrue(ok);
    }

    public void testItemNegative()
    {
        boolean ok = false;
        try
        {
            this.group.setCount(4);
            this.group.load();
            Image image = this.group.item(-5);
            assertEquals(64, this.group.getCount());
            assertNotNull(image);
        }
        catch (IllegalArgumentException e)
        {
            ok = true;
        }

        assertTrue(ok);
    }

    public void testLoad()
    {
        this.group.setCount(COUNT);
        this.group.load();
        assertEquals(COUNT, this.group.getCount());
        ImageIDBuilder builder = new ImageIDBuilder(ID, 4);
    }

    /*
     * Class under test for String toString()
     */
    public void testToString()
    {
        this.group.setCount(COUNT);

        StringBuilder expected = new StringBuilder();
        expected.append("[ id = ");
        expected.append(ID);
        expected.append(" fileName = ");
        expected.append(FILE_PATH);
        expected.append(" size = ");
        expected.append(WIDTH);
        expected.append("x");
        expected.append(HEIGHT);
        expected.append(" ");
        expected.append(COUNT);
        expected.append(" images ]");

        assertEquals(expected.toString(), this.group.toString());
    }

}
