/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/


package org.yuwa.util.images.test;

import junit.framework.TestCase;

import org.yuwa.util.images.ImageIDBuilder;

public class ImageIDBuilderTest extends TestCase
{

    public void testSetGroup()
    {
        // done in testImageID
    }

    public void testSetPic()
    {
        // done in testImageID
    }

    public void testImageID()
    {
        ImageIDBuilder builder = new ImageIDBuilder();
        builder.setGroup("booga");
        builder.setPic(1001);
        assertEquals("[ image booga #1001 ]", builder.imageID().toString());
    }

    public void testImageIDRaw()
    {
        boolean ok = false;
        try
        {
            ImageIDBuilder builder = new ImageIDBuilder();
            assertEquals("[ image booga #1001 ]", builder.imageID().toString());
        }
        catch (IllegalArgumentException e)
        {
            ok = true;
        }

        assertTrue(ok);
    }

    public void testImageIDBuilder()
    {
        ImageIDBuilder builder = new ImageIDBuilder("booga", 1001);
        assertEquals("[ image booga #1001 ]", builder.imageID().toString());
    }

    public void testImageIDBuilderNullGroup()
    {
        boolean ok = false;
        try
        {
            ImageIDBuilder builder = new ImageIDBuilder(null, 1001);
            assertEquals("[ image booga #1001 ]", builder.imageID().toString());
        }
        catch (IllegalArgumentException e)
        {
            ok = true;
        }

        assertTrue(ok);
    }

    public void testImageIDBuilderNegativePic()
    {
        boolean ok = false;
        try
        {
            ImageIDBuilder builder = new ImageIDBuilder("booga", -10);
            assertEquals("[ image booga #1001 ]", builder.imageID().toString());
        }
        catch (IllegalArgumentException e)
        {
            ok = true;
        }

        assertTrue(ok);
    }

    public void testImageIDNullGroup()
    {
        boolean ok = false;
        try
        {
            ImageIDBuilder builder = new ImageIDBuilder();
            builder.setGroup(null);
            builder.setPic(1001);
            assertEquals("[ image null #1001 ]", builder.imageID().toString());
        }
        catch (IllegalArgumentException e)
        {
            ok = true;
        }

        assertTrue(ok);
    }

    public void testImageIDNegativePic()
    {
        boolean ok = false;

        try
        {
            ImageIDBuilder builder = new ImageIDBuilder();
            builder.setGroup("booga");
            builder.setPic(-1);
            assertEquals("[ image booga #-1 ]", builder.imageID().toString());
        }
        catch (IllegalArgumentException e)
        {
            ok = true;
        }

        assertTrue(ok);
    }

}
