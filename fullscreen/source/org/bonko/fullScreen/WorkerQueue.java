/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.fullScreen;

import java.util.LinkedList;


public class WorkerQueue implements Runnable
{
//  private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass());
    private LinkedList<Runnable> toDo = new LinkedList<Runnable>();
    private boolean halt = false;
    
    
    private static final Runnable DO_NOTHING = new Runnable() {
        public void run() { }  
    };
    
    
    public void run()
    {
        while ( ! halt && ! Thread.currentThread().isInterrupted() )
        {
            Runnable r = this.waitForSomethingToDo(); 
            this.doSomething(r);
        }
    }
    
    
    public void add(Runnable r)
    {
        synchronized ( this.toDo )
        {
            // if ( logger.isDebugEnabled() ) logger.debug("add task (" + this.toDo.size() + "+1) " + r);
            this.toDo.add(r);
            this.toDo.notifyAll();
        }
    }
    
    
    private Runnable waitForSomethingToDo()
    {
        Runnable r = DO_NOTHING;
        
        synchronized ( this.toDo )
        {
            try
            {
                // if ( logger.isDebugEnabled() ) logger.debug("wait for next task (" + this.toDo.size() + ")");
                if ( this.toDo.size() == 0 ) this.toDo.wait();
                r = this.toDo.removeFirst();
            }
            catch (InterruptedException e)
            {
                this.halt = true;
            }
        }
        
        return r;
        
    }
    
    
    private void doSomething(final Runnable r)
    {
        // if ( logger.isDebugEnabled() ) logger.debug("run next task " + r );
        r.run();
        
        if ( ( r instanceof Repeater ) && ( ((Repeater)r).repeat() ) ) this.add(r); 
    }
    
}
