/**
 * Copyright (c) 2011 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 */
package org.yuwa.framework.config;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

/**
 * uses reflection to invoke the setters and populate an object from config
 * 
 * @author ryoung
 */
public class Randomizer<T>
{
    // as an optimization, each instance is tied to a class so
    // we don't have to rediscover the setters each time
    private final Map<String, Method> setters;

    private static final char[] ALPHABET = "01234567890ABCDEFGHIJKLMNOPQRSTUVWYZabcedfghijklmnopqrstuvwxyz"
            .toCharArray();
    private static final int STRING_LENGTH = 10; // (?)

    // provide a common seed so the values come out the same each time. Set to
    // null to really randomize.
    private Long commonSeed = 1432595018543L;

    private Random random = startOver();


    public Randomizer(Class<T> clazz)
    {
        setters = findSetters(clazz);
    }


    private Randomizer(Randomizer<T> that)
    {
        setters = that.setters;
        commonSeed = that.commonSeed;
        random = startOver();
    }


    public Randomizer<T> copy()
    {
        return new Randomizer<T>(this);
    }


    public void setRandom(Random r)
    {
        this.random = r;
    }


    public void setCommonSeed(Long s)
    {
        this.commonSeed = s;
        random = startOver();
    }


    public Random startOver()
    {
        return random = (commonSeed == null)
                ? new Random()
                : new Random(commonSeed);
    }


    /**
     * ramdomize the object by invoking the setters with random values
     * 
     * @param object
     * @param config
     * @param keyPrefix
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public void populate(final T object) throws IllegalArgumentException,
            IllegalAccessException, InvocationTargetException
    {
        for (final Entry<String, Method> entry : setters.entrySet())
        {
            final Class<?> parameterType = entry.getValue().getParameterTypes()[0];

            if (boolean.class.equals(parameterType))
            {
                final boolean b = random.nextBoolean();
                entry.getValue().invoke(object, b);
            }
            else if (int.class.equals(parameterType))
            {
                final int i = random.nextInt();
                entry.getValue().invoke(object, i);
            }
            else if (long.class.equals(parameterType))
            {
                final long l = random.nextLong();
                entry.getValue().invoke(object, l);
            }
            else if (Boolean.class.equals(parameterType))
            {
                final boolean bb = new Boolean(random.nextBoolean());
                entry.getValue().invoke(object, bb);
            }
            else if (Integer.class.equals(parameterType))
            {
                final int ii = random.nextInt();
                entry.getValue().invoke(object, ii);
            }
            else if (Long.class.equals(parameterType))
            {
                final long ll = random.nextLong();
                entry.getValue().invoke(object, ll);
            }
            else if (String.class.equals(parameterType))
            {
                // lol how long a string?
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < STRING_LENGTH; i++)
                {
                    int at = random.nextInt(ALPHABET.length);
                    char c = ALPHABET[at];
                    builder.append(c);
                }
                entry.getValue().invoke(object, builder.toString());
            }
            // TODO: what to do about dates?
            // ...else ignore
        }
    }


    public Map<String, Method> findSetters(final Class<?> clazz)
    {
        final Map<String, Method> setters = new HashMap<String, Method>();

        for (Method m : clazz.getMethods())
        {
            if (!m.getName().startsWith("set"))
            {
                // nope must be set...()
            }
            else if (m.getName().length() < 4)
            {
                // nope can't be set()
            }
            else if (m.getParameterTypes().length != 1)
            {
                // nope can only have 1 parameter
            }
            else
            {
                final String name = m.getName().substring(3).toLowerCase();
                setters.put(name, m);
            }
        }

        return setters;
    }

}
