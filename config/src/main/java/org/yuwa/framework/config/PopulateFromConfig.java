/**
 * Copyright (c) 2011 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 */
package org.yuwa.framework.config;


import static org.apache.commons.lang.StringUtils.isBlank;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * uses reflection to invoke the setters and populate an object from config
 * 
 * @author ryoung
 */
public class PopulateFromConfig<T>
{

    /** our logger */
    protected static final Logger logger = Logger
            .getLogger(PopulateFromConfig.class);

    /** placeholder symbols for empty string and null */
    public enum ConfigSymbols
    {
        _EMPTY_, _NULL_
    };

    /** these are the types we currently support */
    @SuppressWarnings("serial")
    private static final Set<Class<?>> SUPPORTED_TYPES = new HashSet<Class<?>>() {
        {
            add(boolean.class);
            add(int.class);
            add(long.class);
            add(Boolean.class);
            add(Integer.class);
            add(Long.class);
            add(String.class);
        }
    };

    // as an optimization, each instance is tied to a class so
    // we don't have to rediscover the setters each time
    private final Map<String, Method> m_setters;

    /** the target class */
    private final Class<?> m_class;

    /** optionally allow setting the configuration up front */
    private final Configuration m_configuration;

    /** optionally allow setting the prefixes up front */
    private final String[] m_prefixes;


    /**
     * constructor based only on class
     * 
     * @param clazz
     */
    public PopulateFromConfig(final Class<T> clazz)
    {
        m_class = clazz;
        m_setters = this.findSetters(clazz);
        m_configuration = null;
        m_prefixes = new String[0];
    }


    /**
     * constructor based only on class
     * 
     * @param clazz
     */
    public PopulateFromConfig(final Class<T> clazz,
            final Configuration configuration,
            final String... prefixes)
    {
        m_class = clazz;
        m_setters = this.findSetters(clazz);
        m_configuration = configuration;
        m_prefixes = prefixes;
    }


    /**
     * populate the object using the configuration and prefixes set at
     * construction time
     * 
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public void populate(final T object) throws IllegalArgumentException,
    IllegalAccessException, InvocationTargetException
    {
        this.populate(object, m_configuration, m_prefixes);
    }


    /**
     * act as a factory to create and then populate the object
     * 
     * @return a new populated instance of the class
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     */
    @SuppressWarnings("unchecked")
    public T create() throws InstantiationException, IllegalAccessException,
    IllegalArgumentException, InvocationTargetException
    {
        final T object = (T) m_class.newInstance();
        this.populate(object, m_configuration, m_prefixes);

        return object;
    }


    /**
     * act as a factory to create and then populate the object
     * 
     * @return a new populated instance of the class
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     */
    @SuppressWarnings("unchecked")
    public T create(final Configuration config, final String... keyPrefix)
            throws InstantiationException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException
            {
        final T object = (T) m_class.newInstance();
        this.populate(object, config, keyPrefix);

        return object;
            }


    /**
     * populate the object by invoking the setters using the properties of the
     * same name from the config using the given prefixes, in order
     * 
     * @param object
     * @param config
     * @param keyPrefix
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public void populate(final T object,
            final Configuration config,
            final String... keyPrefix)
                    throws IllegalArgumentException,
                    IllegalAccessException, InvocationTargetException
                    {
        if (logger.isDebugEnabled())
            logger.debug("populate " + object.getClass().getName() + " as "
                    + Arrays.asList(keyPrefix));

        for (final Entry<String, Method> entry : this.m_setters.entrySet())
        {

            final Method method = entry.getValue();
            final Class<?> parameterType = method.getParameterTypes()[0];
            final String value = this.findValue(config, keyPrefix,
                    entry.getKey());

            if (logger.isDebugEnabled())
                logger.debug("invoke " + method.getName() + "=" + value + " ("
                        + parameterType + ")");

            if (value == null)
            {
                // skip
            }
            else if (boolean.class.equals(parameterType))
            {
                method.invoke(object, this.asBoolean(value));
            }
            else if (int.class.equals(parameterType))
            {
                method.invoke(object, this.asInt(value));
            }
            else if (long.class.equals(parameterType))
            {
                method.invoke(object, this.asLong(value));
            }
            else if (Boolean.class.equals(parameterType))
            {
                method.invoke(object, this.asBooleanObject(value));
            }
            else if (Integer.class.equals(parameterType))
            {
                method.invoke(object, this.asIntegerObject(value));
            }
            else if (Long.class.equals(parameterType))
            {
                method.invoke(object, this.asLongObject(value));
            }
            else
            {
                method.invoke(object, this.asString(value));
            }
        }
                    }


    /**
     * @param config
     * @param keyPrefix
     * @param entry
     * @param configKey
     *            TODO
     * @return
     */
    private String findValue(final Configuration config,
            final String[] keyPrefix, final String configKey)
    {
        String value = null;

        for (int i = keyPrefix.length - 1; value == null && i >= 0; i--)
        {

            final String prefix = this.fixPrefix(keyPrefix[i]);

            final String key = prefix + configKey;

            value = config.getString(key);

            if (logger.isDebugEnabled())
                logger.trace("found " + key + "=" + value);
        }
        return value;
    }


    /**
     * @param prefix
     * @return the prefix with a . if needed
     */
    private String fixPrefix(String prefix)
    {
        return (prefix.isEmpty() || prefix.endsWith(".")) ? prefix
                : (prefix + ".");
    }


    /**
     * find the setters that we'll need to invoke to populate objects of this
     * class
     * 
     * @param clazz
     * @return the available setters for the class
     */
    public Map<String, Method> findSetters(final Class<?> clazz)
    {
        if (logger.isDebugEnabled())
            logger.debug("findSetters for " + clazz.getName());

        final Map<String, Method> setters = new HashMap<String, Method>();

        for (Method m : clazz.getMethods())
        {

            if (!m.getName().startsWith("set"))
            {
                // nope must be set...()
            }
            else if (m.getName().length() < 4)
            {
                logger.trace(m.getName() + " nope can't be set()");
            }
            else if (m.getParameterTypes().length != 1)
            {
                logger.trace(m.getName() + " nope can only have 1 parameter");
            }
            else if (!SUPPORTED_TYPES.contains(m.getParameterTypes()[0]))
            {
                logger.trace(m.getName()
                        + " nope ignore things we don't understand: "
                        + m.getParameterTypes()[0].getName());
            }
            else
            {
                final String name = m.getName().substring(3).toLowerCase();

                if (logger.isDebugEnabled())
                    logger.debug("found " + m.getName() + " as " + name);

                setters.put(name, m);
            }
        }

        return setters;
    }


    /**
     * converts placeholder symbols _EMPTY_ or _NULL_ to the right value of
     * empty or null
     * 
     * @param value
     * @return
     */
    public String fromSymbol(String value)
    {
        final String result;
        if (value == null)
            result = null;
        else if (value.isEmpty())
            result = StringUtils.EMPTY;
        else if (ConfigSymbols._EMPTY_.name().equals(value))
            result = StringUtils.EMPTY;
        else if (ConfigSymbols._NULL_.name().equals(value))
            result = null;
        else
            result = value;
        return result;
    }


    /**
     * @param value
     * @return the value as a string, substituting empty and blank literals
     */
    private String asString(final String value)
    {
        return fromSymbol(value);
    }


    /**
     * @param value
     * @return the value as a Boolean object, substituting empty and blank
     *         literals
     */
    private Boolean asBooleanObject(final String value)
    {
        final String string = fromSymbol(value);
        return (isBlank(string)) ? null : new Boolean(string);
    }


    /**
     * @param value
     * @return the value as an Integer object, substituting empty and blank
     *         literals
     */
    private Integer asIntegerObject(final String value)
    {
        final String string = fromSymbol(value);
        return (isBlank(string)) ? null : new Integer(string);
    }


    /**
     * @param value
     * @return the value as Long object, substituting empty and blank literals
     */
    private Long asLongObject(final String value)
    {
        final String string = fromSymbol(value);
        return (isBlank(string)) ? null : new Long(string);
    }


    /**
     * @param value
     * @return the value as a boolean primative, substituting empty and blank
     *         literals
     */
    private boolean asBoolean(final String value)
    {
        final String string = fromSymbol(value);
        return (isBlank(string)) ? false : Boolean.parseBoolean(string);
    }


    /**
     * @param value
     * @return the value as an int primitive, substituting empty and blank
     *         literals
     */
    private int asInt(final String value)
    {
        final String string = fromSymbol(value);
        return (isBlank(string)) ? 0 : Integer.parseInt(string);
    }


    /**
     * @param value
     * @return the value as long primitive, substituting empty and blank
     *         literals
     */
    private long asLong(final String value)
    {
        final String string = fromSymbol(value);
        return (isBlank(string)) ? 0L : Long.parseLong(string);
    }

}
