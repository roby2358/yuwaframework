/**
 * Copyright (c) 2011 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 */
package org.yuwa.framework.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Random;

import org.junit.Test;

/**
 * @author ryoung
 * 
 */
public class RandomizerTest
{

    public static class Woog
    {
        public int a;
        public String b;
        public boolean c = true;
        public long d;
        public Boolean e;
        public Integer f;
        public Long g;
        public String nope = "nope";
        public String yep = "yep";

        public void setA(int a) { this.a = a; }
        public void setB(String b) { this.b = b; }
        public void setC(boolean c) { this.c = c; }
        public void setD(long d) { this.d = d; }
        public void setE(Boolean e) { this.e = e; }
        public void setF(Integer f) { this.f = f; }
        public void setG(Long g) { this.g = g; }
        public void setYep(String yep) { this.yep = yep; }
    }

    public Randomizer<Woog> randomizer = new Randomizer<Woog>(Woog.class);

    public Woog t = new Woog();
    public Woog u = new Woog();
    public Woog v = new Woog();
    public Woog w = new Woog();


    @Test
    public void testRandomizer() throws Exception
    {
        randomizer.populate(w);
        assertEquals(-404242627, w.a);
        assertEquals("9mvVflRjrj", w.b);
        assertEquals(false, w.c);
        assertEquals(5318600777234478724L, w.d);
        assertEquals(Boolean.FALSE, w.e);
        assertEquals(new Integer(571446188), w.f);
        assertEquals(new Long(894679514977334791L), w.g);
        assertEquals("nope", w.nope);
        assertEquals("iVLyENieA0", w.yep);

        randomizer.startOver();
        randomizer.populate(v);
        assertEquals(-404242627, w.a);
        assertEquals("9mvVflRjrj", w.b);
        assertEquals(false, w.c);
        assertEquals(5318600777234478724L, w.d);
        assertEquals(Boolean.FALSE, w.e);
        assertEquals(new Integer(571446188), w.f);
        assertEquals(new Long(894679514977334791L), w.g);
        assertEquals("nope", w.nope);
        assertEquals("iVLyENieA0", w.yep);
    }


    @Test
    public void testSetCommonSeed() throws Exception
    {
        randomizer.setCommonSeed(1L);
        randomizer.populate(w);
        assertEquals(-582126989, w.a);
        assertEquals("8i0KuYLjPz", w.b);
        assertEquals(true, w.c);
        assertEquals(7515937759503895804L, w.d);
        assertEquals(Boolean.TRUE, w.e);
        assertEquals(new Integer(-1155869325), w.f);
        assertEquals(new Long(1853403699951111791L), w.g);
        assertEquals("nope", w.nope);
        assertEquals("64Hd89xWQg", w.yep);

        randomizer.setCommonSeed(1L);
        randomizer.populate(v);
        assertEquals(-582126989, w.a);
        assertEquals("8i0KuYLjPz", w.b);
        assertEquals(true, w.c);
        assertEquals(7515937759503895804L, w.d);
        assertEquals(Boolean.TRUE, w.e);
        assertEquals(new Integer(-1155869325), w.f);
        assertEquals(new Long(1853403699951111791L), w.g);
        assertEquals("nope", w.nope);
        assertEquals("64Hd89xWQg", w.yep);
    }

    @Test
    public void testFindSetters()
    {
        Map<String, Method> setters = randomizer.findSetters(Woog.class);

        assertEquals(8, setters.size());
        assertEquals("setA", setters.get("a").getName());
        assertEquals("setB", setters.get("b").getName());
        assertEquals("setC", setters.get("c").getName());
        assertEquals("setD", setters.get("d").getName());
        assertEquals("setE", setters.get("e").getName());
        assertEquals("setF", setters.get("f").getName());
        assertEquals("setG", setters.get("g").getName());
        assertEquals("setYep", setters.get("yep").getName());
    }

    @Test
    public void testRandomizerNullCommonSeed() throws Exception
    {
        randomizer.setCommonSeed(null);
        randomizer.populate(w);
        assertTrue(0 != w.a);
        assertTrue(null != w.b);
        // assertTrue(false,w.c);
        assertTrue(0L != w.d);
        // assertTrue(Boolean.FALSE,w.e);
        assertTrue(null != w.f);
        assertTrue(null != w.g);
        assertEquals("nope", w.nope);
        assertTrue(null != w.yep);
    }

    @Test
    public void testRandomizerSetRandom() throws Exception
    {
        randomizer.setRandom(new Random());
        randomizer.populate(w);
        assertTrue(0 != w.a);
        assertTrue(null != w.b);
        // assertTrue(false,w.c);
        assertTrue(0L != w.d);
        // assertTrue(Boolean.FALSE,w.e);
        assertTrue(null != w.f);
        assertTrue(null != w.g);
        assertEquals("nope", w.nope);
        assertTrue(null != w.yep);
    }
}
