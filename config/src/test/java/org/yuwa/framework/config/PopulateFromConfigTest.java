/**
 * Copyright (c) 2011 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 */
package org.yuwa.framework.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Method;
import java.util.GregorianCalendar;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Test;

/**
 * @author ryoung
 * 
 */
public class PopulateFromConfigTest
{

    public final Configuration config = new PropertiesConfiguration();
    public final PopulateFromConfig<Object> pop = new PopulateFromConfig<Object>(
            Object.class);


    @Test
    public void testFromSymbolNull()
    {
        assertEquals(null, pop.fromSymbol(null));
    }


    @Test
    public void testFromSymbolEmpty()
    {
        assertEquals("", pop.fromSymbol(""));
    }


    @Test
    public void testFromSymbolNullSymbol()
    {
        assertEquals(null, pop.fromSymbol("_NULL_"));
    }


    @Test
    public void testFromSymbolEmptySymbol()
    {
        assertEquals("", pop.fromSymbol("_EMPTY_"));
    }


    @Test
    public void testFindSettersZero()
    {
        @SuppressWarnings("unused")
        final Object o = new Object() {
            private void setPrivate(String one) { };
            public void xsetA(String one) { };
            public void woog(String one) { };
            public void set(String one) { };
            public void setZero() { };
            public void setTwo(String one, String two) { };
        };

        Map<String, Method> setters = pop.findSetters(o.getClass());

        assertEquals(0, setters.size());
    }


    @Test
    public void testFindSettersOne()
    {
        @SuppressWarnings("unused")
        final Object o = new Object() {
            private void setPrivate(String one) { };
            public void xsetA(String one) { };
            public void woog(String one) { };
            public void set(String one) { };
            public void setZero() { };
            public void setTwo(String one, String two) { };
            public void setA(String one) { };
        };

        Map<String, Method> setters = pop.findSetters(o.getClass());

        assertEquals(1, setters.size());
        assertEquals("setA", setters.get("a").getName());
    }


    @Test
    public void testFindSettersTwo()
    {
        @SuppressWarnings("unused")
        final Object o = new Object() {
            private void setPrivate(String one) { };
            public void xsetA(String one) { };
            public void woog(String one) { };
            public void set(String one) { };
            public void setZero() { };
            public void setTwo(String one, String two) { };
            public void setA(String one) { };
            public void setB(String one) { };
        };

        Map<String, Method> setters = pop.findSetters(o.getClass());

        assertEquals(2, setters.size());
        assertEquals("setA", setters.get("a").getName());
        assertEquals("setB", setters.get("b").getName());
    }


    @Test
    public void testFindSettersNames()
    {
        @SuppressWarnings("unused")
        final Object o = new Object() {
            public void set(String one) { };
            public void setA(String one) { };
            public void setBongo(String one) { };
            public void setCorkOlicious(String one) { };
            public void setDingDongDilly(String one) { };
        };

        Map<String, Method> setters = pop.findSetters(o.getClass());

        assertEquals(4, setters.size());
        assertEquals("setA", setters.get("a").getName());
        assertEquals("setBongo", setters.get("bongo").getName());
        assertEquals("setCorkOlicious", setters.get("corkolicious").getName());
        assertEquals("setDingDongDilly", setters.get("dingdongdilly").getName());
    }

    @SuppressWarnings("unused")
    public static class SomeClass
    {
        private String a;
        private String one = "ok";
        private String two = "ok";

        private void setPrivate(String one) { };
        public void xsetA(String one) { };
        public void woog(String one) { };
        public void set(String one) { };
        public void setZero() { };
        public void setTwo(String one, String two) { };
        public void setA(String a) { this.a = a; };
        public void setDont(GregorianCalendar nope) { throw new RuntimeException("nope"); }
        public String getA() { return a; }
        public String getOne() { return one; }
        public String getTwo() { return two; }
    }


    @Test
    public void testFindSettersSomeClass() throws Exception
    {
        final SomeClass o = new SomeClass();
        final Map<String, Method> setters = pop.findSetters(o.getClass());

        assertEquals(1, setters.size());

        // try it ;)
        final Method m = setters.get("a");
        m.invoke(o, "zing");
        assertEquals("zing", o.getA());
    }


    @Test
    public void testPopulateOne() throws Exception
    {
        config.setProperty("a", "this is a");
        config.setProperty("one", "I am one!");
        config.setProperty("huh?", "what?");

        final PopulateFromConfig<SomeClass> pop = new PopulateFromConfig<SomeClass>(
                SomeClass.class);
        final SomeClass o = new SomeClass();
        pop.populate(o, config, "");

        assertEquals("this is a", o.getA());
        assertEquals("ok", o.getOne());
    }


    @Test
    public void testPopulateOnePrefix() throws Exception
    {
        config.setProperty("zoop.a", "this is a");
        config.setProperty("zoop.one", "I am one!");
        config.setProperty("zoop.huh?", "what?");

        final PopulateFromConfig<SomeClass> pop = new PopulateFromConfig<SomeClass>(
                SomeClass.class);
        final SomeClass o = new SomeClass();
        pop.populate(o, config, "zoop.");

        assertEquals("this is a", o.getA());
        assertEquals("ok", o.getOne());
    }


    public static class SomeOtherClass
    {
        private String pString = "default";
        private int pInt = 11111;
        private long pLong = 22222L;
        private boolean pBoolean = false;
        private Integer oInteger = 11111;
        private Long oLong = 22222L;
        private Boolean oBoolean;

        public String getString() { return pString; }
        public void setString(String s) { pString = s; }
        public int getInteger() { return pInt; }
        public void setInteger(int i) { pInt = i; }
        public long getLong() { return pLong; }
        public void setLong(long l) { pLong = l; }
        public boolean getBoolean() { return pBoolean; }
        public void setBoolean(boolean b) { pBoolean = b; }
        public Integer getuInteger() { return oInteger; }
        public void setuInteger(Integer i) { this.oInteger = i; }
        public Long getuLong() { return oLong; }
        public void setuLong(Long l) { this.oLong = l; }
        public Boolean getuBoolean() { return oBoolean; }
        public void setuBoolean(Boolean b) { this.oBoolean = b; }
    }


    @Test
    public void testPopulateSomeOtherClass() throws Exception
    {
        config.setProperty("string", "a string");
        config.setProperty("integer", "12345");
        config.setProperty("long", "9999999999");
        config.setProperty("boolean", "true");
        config.setProperty("uinteger", "8765");
        config.setProperty("ulong", "111111111111");
        config.setProperty("uboolean", "true");

        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class);
        final SomeOtherClass o = new SomeOtherClass();
        pop.populate(o, config, "");

        assertEquals(true, o.getBoolean());
        assertEquals(12345, o.getInteger());
        assertEquals(9999999999L, o.getLong());
        assertEquals("a string", o.getString());
        assertEquals(new Boolean(true), o.getuBoolean());
        assertEquals(new Integer(8765), o.getuInteger());
        assertEquals(new Long(111111111111L), o.getuLong());
    }


    @Test
    public void testPopulateSomeOtherClassNulls() throws Exception
    {
        config.setProperty("string", "_NULL_");
        config.setProperty("integer", "_NULL_");
        config.setProperty("long", "_NULL_");
        config.setProperty("boolean", "_NULL_");
        config.setProperty("uinteger", "_NULL_");
        config.setProperty("ulong", "_NULL_");
        config.setProperty("uboolean", "_NULL_");

        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class);
        final SomeOtherClass o = new SomeOtherClass();
        pop.populate(o, config, "");

        assertEquals(false, o.getBoolean());
        assertEquals(0, o.getInteger());
        assertEquals(0L, o.getLong());
        assertNull(o.getString());
        assertNull(o.getuBoolean());
        assertNull(o.getuInteger());
        assertNull(o.getuLong());
    }


    @Test
    public void testPopulateSomeOtherClassMoreNulls() throws Exception
    {
        config.setProperty("string", "_EMPTY_");
        config.setProperty("integer", "_EMPTY_");
        config.setProperty("long", "_EMPTY_");
        config.setProperty("boolean", "_EMPTY_");
        config.setProperty("uinteger", "_EMPTY_");
        config.setProperty("ulong", "_EMPTY_");
        config.setProperty("uboolean", "_EMPTY_");

        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class);
        final SomeOtherClass o = new SomeOtherClass();
        pop.populate(o, config, "");

        assertEquals(false, o.getBoolean());
        assertEquals(0, o.getInteger());
        assertEquals(0L, o.getLong());
        assertEquals("", o.getString());
        assertNull(o.getuBoolean());
        assertNull(o.getuInteger());
        assertNull(o.getuLong());
    }


    @Test
    public void testPopulateSomeOtherClassMulti() throws Exception
    {
        config.setProperty("default.string", "one");

        config.setProperty("default.integer", "1");
        config.setProperty("zoop.integer", "2");

        config.setProperty("default.long", "111");
        config.setProperty("zoop.long", "222");
        config.setProperty("zing.long", "333");

        config.setProperty("zoop.dont", "no");

        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class);
        final SomeOtherClass o = new SomeOtherClass();
        pop.populate(o, config, "default.", "zoop.", "zing.");

        assertEquals("one", o.getString());
        assertEquals(2, o.getInteger());
        assertEquals(333L, o.getLong());
    }


    @Test
    public void testPopulateSomeOtherClassMultiWithoutDots() throws Exception
    {
        config.setProperty("string", "one");

        config.setProperty("integer", "1");
        config.setProperty("zoop.integer", "2");

        config.setProperty("long", "111");
        config.setProperty("zoop.long", "222");
        config.setProperty("zing.long", "333");

        config.setProperty("dont", "no");

        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class);
        final SomeOtherClass o = new SomeOtherClass();
        pop.populate(o, config, "", "zoop", "zing");

        assertEquals("one", o.getString());
        assertEquals(2, o.getInteger());
        assertEquals(333L, o.getLong());
    }


    @Test
    public void testPopulateSomeOtherClassMultiOnlyLastOne() throws Exception
    {
        config.setProperty("string", "one");

        config.setProperty("zoop.integer", "2");

        config.setProperty("zing.long", "333");

        config.setProperty("dont", "no");

        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class);
        final SomeOtherClass o = new SomeOtherClass();
        pop.populate(o, config, "", "zoop", "zing");

        assertEquals("one", o.getString());
        assertEquals(2, o.getInteger());
        assertEquals(333L, o.getLong());
    }


    @Test
    public void testPopulateSomeOtherClassMultiOtherConstructor()
            throws Exception
    {
        final Configuration config = new PropertiesConfiguration();
        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class, config, "default.", "zoop.", "zing.");

        config.setProperty("default.string", "one");

        config.setProperty("default.integer", "1");
        config.setProperty("zoop.integer", "2");

        config.setProperty("default.long", "111");
        config.setProperty("zoop.long", "222");
        config.setProperty("zing.long", "333");

        config.setProperty("zoop.dont", "no");

        final SomeOtherClass o = new SomeOtherClass();
        pop.populate(o);

        assertEquals("one", o.getString());
        assertEquals(2, o.getInteger());
        assertEquals(333L, o.getLong());
    }


    @Test
    public void testPopulateSomeOtherClassMultiOnlyLastOneOtherConstructor()
            throws Exception
    {
        final Configuration config = new PropertiesConfiguration();
        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class, config, "", "zoop", "zing");

        config.setProperty("string", "one");

        config.setProperty("zoop.integer", "2");

        config.setProperty("zing.long", "333");

        config.setProperty("dont", "no");

        final SomeOtherClass o = new SomeOtherClass();
        pop.populate(o);

        assertEquals("one", o.getString());
        assertEquals(2, o.getInteger());
        assertEquals(333L, o.getLong());
    }


    @Test
    public void testPopulateSomeOtherClassMultiCreate() throws Exception
    {
        final Configuration config = new PropertiesConfiguration();
        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class, config, "default.", "zoop.", "zing.");

        config.setProperty("default.string", "one");

        config.setProperty("default.integer", "1");
        config.setProperty("zoop.integer", "2");

        config.setProperty("default.long", "111");
        config.setProperty("zoop.long", "222");
        config.setProperty("zing.long", "333");

        config.setProperty("zoop.dont", "no");

        final SomeOtherClass o = pop.create();

        assertEquals("one", o.getString());
        assertEquals(2, o.getInteger());
        assertEquals(333L, o.getLong());
    }


    @Test
    public void testPopulateSomeOtherClassMultiOnlyLastOneCreate()
            throws Exception
    {
        final Configuration config = new PropertiesConfiguration();
        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class, config, "", "zoop", "zing");

        config.setProperty("string", "one");

        config.setProperty("zoop.integer", "2");

        config.setProperty("zing.long", "333");

        config.setProperty("dont", "no");

        final SomeOtherClass o = pop.create();

        assertEquals("one", o.getString());
        assertEquals(2, o.getInteger());
        assertEquals(333L, o.getLong());
    }


    @Test
    public void testPopulateSomeOtherClassMultiOtherCreate() throws Exception
    {
        final Configuration config = new PropertiesConfiguration();
        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class);

        config.setProperty("default.string", "one");

        config.setProperty("default.integer", "1");
        config.setProperty("zoop.integer", "2");

        config.setProperty("default.long", "111");
        config.setProperty("zoop.long", "222");
        config.setProperty("zing.long", "333");

        config.setProperty("zoop.dont", "no");

        final SomeOtherClass o = pop.create(config, "default.", "zoop.",
                "zing.");

        assertEquals("one", o.getString());
        assertEquals(2, o.getInteger());
        assertEquals(333L, o.getLong());
    }


    @Test
    public void testPopulateSomeOtherClassMultiOnlyLastOneOtherCreate()
            throws Exception
    {
        final Configuration config = new PropertiesConfiguration();
        final PopulateFromConfig<SomeOtherClass> pop = new PopulateFromConfig<SomeOtherClass>(
                SomeOtherClass.class);

        config.setProperty("string", "one");

        config.setProperty("zoop.integer", "2");

        config.setProperty("zing.long", "333");

        config.setProperty("dont", "no");

        final SomeOtherClass o = pop.create(config, "", "zoop", "zing");

        assertEquals("one", o.getString());
        assertEquals(2, o.getInteger());
        assertEquals(333L, o.getLong());
    }
}