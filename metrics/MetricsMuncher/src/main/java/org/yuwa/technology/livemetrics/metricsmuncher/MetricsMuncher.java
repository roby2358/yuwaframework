/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Sep 11, 2007 $
 */
package org.yuwa.technology.livemetrics.metricsmuncher;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringWriter;
import java.net.BindException;
import java.util.List;
import java.util.Map;

import org.yuwa.technology.livemetrics.json.JsonBinder;

/**
 * brings in a JSON metrics file and outputs it in a CSV format
 *
 * <p>
 * Sep 11, 2007
 *
 * @author royoung
 */
public class MetricsMuncher
implements Runnable
{
	private String fileName;

	private String timeunit = "minute";

	private PrintStream out;

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		if ( args.length < 1 )
			throw new RuntimeException("USAGE: metricsmuncher [timeunit] filename");

		MetricsMuncher main = new MetricsMuncher();

		int i = 0;
		if ( args.length > 1 )
			main.timeunit = args[i++];
		main.fileName = args[i++];
		main.out = System.out;
		
		main.run();
	}

	/**
	 * run the muncher
	 *
	 */
	public void run()
	{
		try
		{
			Map<String, Object> info = this.parse();

			this.out.println( this.asValue(info.get("name") ) );
			this.printComponents(info, timeunit);
		}
		catch (Exception e)
		{
			this.out.println("failed: " + e);
			e.printStackTrace();
		}
	}

	/**
	 * print all the components
	 *
	 * @param info
	 * @param timeunit TODO
	 */
	private void printComponents(Map<String, Object> info, String timeunit)
	{
		List<Object> components = this.asList( info.get("components") );

		for ( Object c : components )
		{
			printComponent( this.asObject(c), timeunit );
		}
	}

	/**
	 * print a single component
	 * @param timeunit TODO
	 * @param s
	 */
	private void printComponent(Map<String,Object> c, String timeunit)
	{
		this.out.println(c.get("name"));

		this.printInfo( this.asObject( c.get(timeunit) ) );
	}



	/**
	 * @param object
	 */
	private void printInfo(Map<String,Object> c)
	{
		List<Object> count = this.asList( c.get("count") );
		List<Object> total = this.asList( c.get("total") );
		List<Object> average = this.asList( c.get("average") );
		List<Object> min = this.asList( c.get("min") );
		List<Object> max = this.asList( c.get("max") );

		this.out.println("count, total, average, min, max");

		for ( int i = count.size() - 1 ; i >= 0  ; i-- )
		{
			StringBuilder builder = new StringBuilder();

			builder.append( this.asValue(count.get(i) ) );
			builder.append( ", " );
			builder.append( this.asValue(total.get(i) ) );
			builder.append( ", " );
			builder.append( this.asValue(average.get(i) ) );
			builder.append( ", " );
			builder.append( this.asValue(min.get(i) ) );
			builder.append( ", " );
			builder.append( this.asValue(max.get(i) ) );

			this.out.println( builder.toString() );
		}

	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// json object helper methods
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * parse the input file into an Object
	 *
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws BindException
	 */
	private Map<String, Object> parse()
	throws FileNotFoundException, IOException, BindException
	{
		FileReader reader = new FileReader(this.fileName);
		StringWriter writer = new StringWriter();

		int n;
		while ( (n = reader.read()) > -1 )
		{
			writer.write(n);
		}

		JsonBinder binder = new JsonBinder(writer.toString());
		Map<String,Object> info = binder.parse();
		return info;
	}

	/**
	 * @param info
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<Object> asList(Object o)
	{
		return (List<Object>)o;
	}

	/**
	 * @param o
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Map<String, Object> asObject(Object o)
	{
		return (Map<String,Object>)o;
	}

	/**
	 * @param o
	 * @return
	 */
	private String asValue(Object o)
	{
		return (o == null) ? "" : o.toString();
	}

}
