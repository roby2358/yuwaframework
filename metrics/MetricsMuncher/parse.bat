@echo off

set CLASSES=.\target\lib
set CP=%CP%;%CLASSES%\log4j-1.2.15.jar
set CP=%CP%;target\metricsmuncher-0.0.1.jar

java -cp %CP% org.yuwa.technology.livemetrics.metricsmuncher.MetricsMuncher %1