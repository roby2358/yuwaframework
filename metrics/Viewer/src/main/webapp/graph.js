// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
// an object to display a graph of provided data
// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

function Graph()
{
// public

this.show = show
this.reset = reset

this.currentComponent = 'total'
this.currentUnit = 'minute'
this.currentStat = 'minmax'

// private

var THIS = this

reset()

// externals referenced
// minuteLabels
// hourLabels
// dayLabels
// minValues
// averageValues
// maxValues

// implementation

function reset()
{
THIS.high = 1000000000
THIS.low = 0
}

function show(data)
{
    var history
    var i = findComponent(data)

    // decide what time unit to use
    if ( THIS.currentUnit != 'minute' ) minuteLabels.style.display = 'none'
    else
    {
        minuteLabels.style.display = 'block'
        history = data.components[i].minute
    }

    if ( THIS.currentUnit != 'hour' ) hourLabels.style.display = 'none'
    else
    {
        hourLabels.style.display = 'block'
        history = data.components[i].hour
    }

    if ( THIS.currentUnit != 'day' ) dayLabels.style.display = 'none'
    else
    {
        dayLabels.style.display = 'block'
        history = data.components[i].day
    }

    // decide what stat to use
    if ( THIS.currentStat != 'minmax' )
    {
        minValues.style.display = 'none'
        averageValues.style.display = 'none'
        maxValues.style.display = 'none'
    }
    else
    {
        minValues.style.display = 'block'
        averageValues.style.display = 'block'
        maxValues.style.display = 'block'

        findMaxValue(history.max)

        minValues.children[0].v = buildMinPath(history.min)
        averageValues.children[0].v = buildMidPath(history.average)
        maxValues.children[0].v = buildMaxPath(history.max)
    }

    if ( THIS.currentStat != 'count' ) countValues.style.display = 'none'
    else
    {
        countValues.style.display = 'block'

        findMaxValue(history.count)

        countValues.children[0].v = buildMinPath(history.count)
    }

    if ( THIS.currentStat != 'total' ) totalValues.style.display = 'none'
    else
    {
        totalValues.style.display = 'block'

        findMaxValue(history.total)

        totalValues.children[0].v = buildMinPath(history.total)
    }

}

// find the current component
function findComponent(data)
{
    var i = 0

    for ( var j = 0 ; j < data.components.length && i == 0 ; j++ )
    {
    	if ( data.components[j].name == THIS.currentComponent ) i = j
    }
    
    return i
}

// get the value for a point in a list
function value(points,i)
{
    v = 1 * points[i]
    if ( THIS.high && v > THIS.high ) v = THIS.high
    if ( THIS.low && v < THIS.low ) v = THIS.low
    return v
}

// find the maximum value and set the value labels
function findMaxValue(points)
{
    maxValue = value(points,0)
    for ( var i = 1 ; i < points.length ; i++ )
    {
    	var v = value(points,i)
    	if ( maxValue < v ) maxValue = v  
    }

    // fill in the y labels
    var scale = 100
    var displayScale = 1
    var m = maxValue
    while ( m > 1000 )
    {
        m /= 1000
        scale *= 1000
        displayScale += ',000'
    }

    value10.value = 10 * maxValue / 100
    value30.value = 30 * maxValue / 100
    value50.value = 50 * maxValue / 100
    value70.value = 70 * maxValue / 100
    value90.value = 90 * maxValue / 100
    
	value10.innerHTML = 10 * maxValue / scale 
	value30.innerHTML = 30 * maxValue / scale
	value50.innerHTML = 50 * maxValue / scale
	value70.innerHTML = 70 * maxValue / scale
	value90.innerHTML = 90 * maxValue / scale
	
	value100.innerHTML = 'max ' + maxValue + ' scale=' + displayScale 
}

// build a minimum path
function buildMinPath(points)
{
var path = buildpath(points)
path += ' 100,100 0,100 xe'
return path
}

// build a mid path
function buildMidPath(points)
{
var path = buildpath(points)
path += ' nf e'
return path
}

// build a max path
function buildMaxPath(points)
{
var path = buildpath(points)
path += ' 100,0 0,0 xe'
return path
}

// generic build path
function buildpath(points)
{
    var path = 'm'
    
    var x = 0
    var y = 100 - Math.floor(100 * value(points,0) / maxValue)
    path += ' ' + x + ',' + y + ' l '

    if ( points.length == 1 )
    {
        // hm... just one point... make it go all the way across
        x = 100
        path += ' ' + x + ',' + y
    }
    else
    {
        for ( var i = 1 ; i < points.length ; i++ )
        {
            x = Math.floor(100 * i / (points.length - 1))
            y = 100 - Math.floor(100 * value(points,i) / maxValue)
            path += ' ' + x + ',' + y
        }
    }

    return path
}

}
