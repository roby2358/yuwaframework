
// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
// initialize/change the target URL
var url

// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
// set up some global variables
var currentDisplay = 'graph'

var interval = 30 * 1000
var intervaId

var minValue = 0
var redlineValue = 90

var graph = new Graph()

// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
// AJAX methods

// start is called onload
function start()
{
    loadData()
    intervalId = setInterval("loadData()",interval)
}


// stop is called onunload
function stop()
{
    clearInterval(intervaId)
}


// change the data source to a new URL
function changeUrl(u)
{
url = document.location + 'proxy?u=' + u
graph.reset()
loadData()
}


// change the URL to the initial url
changeUrl( 'http://localhost:9192/' )


// begin the request to get the data
function loadData()
{
    urlDisplay.innerText = url
    
    var httpRequest = new XMLHttpRequest();
    httpRequest.open("GET", url, true);
    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                rawDataDisplay.innerText = httpRequest.responseText
                showData()
            } else {
                error("There was a problem with the URL.");
            }
            httpRequest = null;
        }
    };
    httpRequest.send(null);
}


// show the data
function showData()
{
    var s = rawDataDisplay.innerText
    var data = null
    
	try
	{
    data = eval("(" + s + ")");
    this.errorDisplay.innerText = 'OK'
    }
    catch (s)
    {
    this.errorDisplay.innerText = '' + s
    }

    if ( data == null )
    {
        // nop
    }
    else
    {
        showComponents(data)
        if ( currentDisplay == 'graph' ) graph.show(data)
    }
}


// show all the components associated with the data
function showComponents(data)
{
    var s = ''
    var sorted = data.components.sort(compareComponents)
    
    for ( var i = 0 ; i < sorted.length ; i++ )
    {
        if ( s != '' ) { s += ' | ' }
        s += '<span class="'
        s += (sorted[i].name == graph.currentComponent) ? 'shade2' : 'shade1'
        s += ' clicker" onclick="selectComponent(this)" value="minute">'
        s += sorted[i].name
        s += '</span>'
    }

    componentDiv.innerHTML = s    
}

// compare the components, return -1 if a < b, 0 if a == b, 1 if a > b
function compareComponents(a,b)
{
if ( a.name < b.name ) { return -1 }
else if ( a.name > b.name ) { return 1 }
else { return 0 }
}

// show error text
function error(s)
{
    errorDisplay.innerText = s
}

// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
// dynamic HTML methods

function selectDisplay(s)
{
    var d = document.getElementById(s.value)

    if ( d != null )
    {
        currentDisplay = s.value
        
        for ( var i = 0 ; i < displayButtons.children.length ; i++ ) displayButtons.children[i].className='shade1 clicker'
        s.className='shade2 clicker'

        for ( var i = 0 ; i < displays.children.length ; i++ ) displays.children[i].style.display='none'
	    d.style.display = 'block'

        graph.reset()
        showData()
    }
}

// select the unit of time
function selectUnit(s)
{
	flip(unitButtons,s)
    
    graph.currentUnit = s.value
    graph.reset()
    showData()
}

// select the component to show
function selectStat(s)
{
	flip(statButtons,s)

    graph.currentStat = s.value
    graph.reset()
    showData()
}

// flip all the buttons to the right style
function flip(buttons,button)
{
    for ( var i = 0 ; i < buttons.children.length ; i++ )
        buttons.children[i].className='shade1 clicker'
    button.className='shade2 clicker'
}

// select the component to show
function selectComponent(s)
{
    graph.currentComponent = s.innerText
    graph.reset()
    showData()
}

// set the ceiling for the graph
function selectCeiling(s)
{
    graph.high = 2 * s.value
    showData()
}


// hide or show the raw data
function hideShowData()
{
if ( urlDisplayDiv.style.display != 'none' )
{
    urlDisplayDiv.style.display = 'none'
    rawDataDisplayDiv.style.display = 'none'
}
else
{
    urlDisplayDiv.style.display = 'block'
    rawDataDisplayDiv.style.display = 'block'
}

}
