/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 */

package org.yuwa.technology.livemetrics.viewer;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.BasicConfigurator;
import org.yuwa.technology.livemetrics.ServiceTracker;
import org.yuwa.technology.livemetrics.ServiceTrackerBuilder;
import org.yuwa.technology.livemetrics.metrics.JsonMetricsVisitor;
import org.yuwa.technology.livemetrics.util.SimpleThreadFactory;



public class RandomServiceServlet
extends HttpServlet
{
	private static final String SERVER_NAME = "aServer";
	@SuppressWarnings("unused")
	private static final String HOST_NAME = "localhost";
	private static final String CHANNEL_NAME_0 = "apples";
	private static final String CHANNEL_NAME_1 = "oranges";
	private static final String CHANNEL_NAME_2 = "banannas";
	private static final String CHANNEL_NAME_3 = "cantelope";

	private static final int DEFAULT_LISTENER_PORT = 9192;

	private static final long INTERVAL = 5000L;

	/** default serial version ID */
	private static final long serialVersionUID = 1L;

	private ServiceTracker tracker;

	@SuppressWarnings("unused")
	private ThreadFactory threadFactory = new SimpleThreadFactory();

	@SuppressWarnings("unused")
	private int listenerPort = DEFAULT_LISTENER_PORT;

	private Thread thread;

	private Random random = new Random();

	private final RandomServiceServlet THIS = this;


	/**
	 * @see javax.servlet.GenericServlet#init()
	 */
	@Override
	public void init() throws ServletException
	{
		super.init();

		BasicConfigurator.configure();

		List<String> components = new ArrayList<String>();
		components.add(CHANNEL_NAME_0);
		components.add(CHANNEL_NAME_1);
		components.add(CHANNEL_NAME_2);
		components.add(CHANNEL_NAME_3);
		
		ServiceTrackerBuilder builder = new ServiceTrackerBuilder();
		builder.setPort(DEFAULT_LISTENER_PORT);
		builder.setName(SERVER_NAME);
		builder.setUseListener(true);
		builder.setNewlines(true);
		builder.setComponentList(components);
		this.tracker = builder.build();

		Runnable r = new Runnable(){
			public void run()
			{
				try
				{
					while (true)
					{
						THIS.add(CHANNEL_NAME_0);
						THIS.add(CHANNEL_NAME_1);
						THIS.add(CHANNEL_NAME_2);
						THIS.add(CHANNEL_NAME_3);
						Thread.sleep(INTERVAL);
					}
				}
				catch (InterruptedException e)
				{
					// done
				}
			}
		};

		thread = new SimpleThreadFactory().newThread(r);
		thread.start();
	}


	/**
	 * add a random value to a channel
	 * 
	 * @param channelName
	 */
	private void add(String channelName)
	{
		int value = (int) this.randomValue();
		synchronized(this.tracker) { this.tracker.add(channelName,value); }
	}


	/**
	 * generate a random value that has an interesting distribution
	 * 
	 * @return a random value
	 */
	private float randomValue()
	{
		return (float)(25 + THIS.random.nextInt(26)) * ( THIS.random.nextFloat() + THIS.random.nextFloat() );
	}


	/**
	 * service the request
	 * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		PrintWriter writer = response.getWriter();

		JsonMetricsVisitor builder = new JsonMetricsVisitor();
		synchronized( this.tracker.getHistory() )
		{
			// TODO: check this
			this.tracker.getHistory().accept(builder);
		}

		response.setHeader("Cache-control","no-cache");
		response.setHeader("Cache-control","no-store");
		response.setHeader("Pragma","no-cache");
		response.setHeader("Expires","0");

		writer.println(builder.toString());
		writer.flush();
		writer.close();
	}

}
