/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package org.yuwa.technology.livemetrics.viewer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * the dynamic page needs to poll a variety of servers, but the XmlHttpRequest
 * only allows communication with the original server. This servlet acts as a
 * proxy to route the request.
 * 
 * @author royoung
 */
public class ProxyServlet
extends HttpServlet
{
	/** default serial version id */
	private static final long serialVersionUID = 1L;

	/** our logger */
	protected Logger log = Logger.getLogger(getClass());

	/** the parameter that holds the URL */
	private static final String PARAMETER_URL = "u";

	/**
	 * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try
		{
			String url = request.getParameter(PARAMETER_URL);

			response.setHeader("Cache-control","no-cache");
			response.setHeader("Cache-control","no-store");
			response.setHeader("Pragma","no-cache");
			response.setHeader("Expires","0");
			
			ProxyGetRequest proxyRequest = new ProxyGetRequest();
			proxyRequest.setUrl(url);
			proxyRequest.setOutput(response.getOutputStream());
			proxyRequest.run();
			
			if ( proxyRequest.isException() )
				throw proxyRequest.getException();
		}
		catch (Throwable t)
		{
			throw new ServletException("Proxy request failed",t);
		}
	}

}
