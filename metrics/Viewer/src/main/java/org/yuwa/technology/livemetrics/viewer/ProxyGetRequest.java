/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package org.yuwa.technology.livemetrics.viewer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;


/**
 * make a connection to a remote url and stream our input and output to it 
 * 
 * @author royoung
 *
 */
public class ProxyGetRequest
implements Runnable
{
    /** our logger */
    protected Logger log = Logger.getLogger(getClass());

    /** the default timeout value */
    public static final int DEFAULT_CONNECTION_TIMEOUT = 2000;

    /** the default value for the socket timeout while reading data */
    private static final int DEFAULT_SOCKET_TIMEOUT = 10000;

    /** a non-error exception to use as a placeholder */
    public static final Exception NO_EXCEPTION = new Exception("Success");

    /** the source URL */
    private String url;

    /** the connection timeout value to use */
    private int connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;

    /** the socket timeout value while reading data */
    private int socketTimeout = DEFAULT_SOCKET_TIMEOUT;

    /** the output stream to write the bits to */
    private OutputStream output;
    
    /** flips to true when finished, for async processing */
    private boolean done = false;

    /** any exceptions that happened */
    private Exception exception = NO_EXCEPTION;

    /**
     * run the method with the properties we have, and save the resulting byte stream
     */
    public void run()
    {
        try
        {
        	long start = System.nanoTime();
        	
            HttpClient client = new HttpClient(new MultiThreadedHttpConnectionManager());
            client.getHttpConnectionManager()
            .getParams()
            .setConnectionTimeout(this.connectionTimeout);

            GetMethod get = new GetMethod(this.url.toString());
            get.getParams().setSoTimeout(this.socketTimeout);
            
            client.executeMethod(get);

            if ( get.getStatusCode() != HttpServletResponse.SC_OK )
            {
                this.exception = new RuntimeException("error status " + get.getStatusCode() + " " + get.getStatusText() );
            }
            else
            {
            	this.pullOutput(get.getResponseBodyAsStream());
            }
            
            this.done = true;
            
            if ( log.isDebugEnabled() ) {
            	float time = (float)( System.nanoTime() - start ) / 1000000.0f;
            	log.debug("get in " + time + " ms");
            }
        }
        catch (Exception e)
        {
            this.exception = e;
        }
    }

	/**
	 * pull the output from the remote server
	 * 
	 * @param in
	 * @param bufferedOutputStream
	 * @throws IOException
	 */
	private void pullOutput(InputStream in)
	throws IOException
	{
		byte[] buffer = new byte[32768];
		int n;
		while ( (n = in.read(buffer)) != -1 ) {
			this.output.write(buffer,0,n);
		}
	}
	
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * the URL of the image to fetch
     */
    public String getUrl()
    {
        return this.url;
    }

    /**
     * set the url of the image to fetch
     * 
     * @param url
     */
    public void setUrl(String url)
    {
        this.url = url;
    }

    /**
     * the connection timeout value 
     * 
     * @return the timeout value
     */
    public int getConnectionTimeout()
    {
        return this.connectionTimeout;
    }

    /**
     * the connection timeout value to set
     * 
     * @param connectionTimeout
     */
    public void setConnectionTimeout(int connectionTimeout)
    {
        this.connectionTimeout = connectionTimeout;
    }

    /**
     * true if there was an exception
     * 
     * @return true if there was an exception
     */
    public boolean isException()
    {
        return this.exception != NO_EXCEPTION;
    }

    /**
     * the resulting exception, or null if none
     * 
     * @return exception
     */
    public Exception getException()
    {
        return this.exception;
    }

    /**
     * true when we've finished processing the request
     * 
     * @return true if we're done
     */
    public boolean isDone()
    {
        return this.done;
    }

    /**
     * how long to wait before timing out on a socket connection
     * 
     * @return socket timeout
     */
    public int getSocketTimeout()
    {
        return this.socketTimeout;
    }

    /**
     * how long to wait before timing out on a socket connection
     * 
     * @param socketTimeout
     */
    public void setSocketTimeout(int socketTimeout)
    {
        this.socketTimeout = socketTimeout;
    }

	/**
	 * @param output the output to set
	 */
	public void setOutput(OutputStream output)
	{
		this.output = output;
	}

}
