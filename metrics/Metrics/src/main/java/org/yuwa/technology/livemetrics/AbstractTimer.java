/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Mar 17, 2008 $
 */
package org.yuwa.technology.livemetrics;


/**
 * A convenience class that just tracks elapsed time in milliseconds
 *
 * <p>
 * Mar 17, 2008
 *
 * @author royoung
 */
public abstract class AbstractTimer
implements Timer
{
	/** the tracker to update */
	private final ServiceTracker tracker;

	/** the name of the component to accumulate to */
	private final String name;

	/** the start time */
	private long start = this.getTime();

	private static final TimerMemory MEMORY = new TimerMemory();


	/**
	 * constructor takes a tracker to report to
	 * 
	 * @param tracker
	 */
	public AbstractTimer(ServiceTracker tracker, String name)
	{
		this.tracker = tracker;
		this.name = name;
	}

	/**
	 * add the elapsed milliseconds to our tracker
	 * 
	 * @see org.yuwa.technology.livemetrics.Timer#addMe()
	 */
	public void addMe()
	{
		long elapsed = this.elapsed();
		this.tracker.add(this.name, elapsed);
	}

	/**
	 * get the time one way or another -- defined by subclass
	 * 
	 * @return
	 */
	public abstract long getTime();

	/**
	 * @return the start time
	 */
	public long start()
	{
		return this.start;
	}

	/**
	 * @return the elapsed time
	 */
	public long elapsed()
	{
		long elapsed = this.getTime() - start;
		elapsed = MEMORY.checkAgainstLastValue(this.name, elapsed);
		return elapsed;
	}

}
