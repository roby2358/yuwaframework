/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Apr 7, 2008 $
 */
package org.yuwa.technology.livemetrics;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * A thread-safe (mostly) object to remember the last value of elapsed time for
 * a metric, and to supply that value if the elapsed time shows up negative due
 * to squirrley clock values.
 * 
 * <p>
 * Mostly thread-safe because there are some minor race conditions that don't
 * matter, and clear() may act weird in a multi-threaded context. But it won't
 * throw errors, and will behave as expected in a concurrent environment.
 * 
 * <p>
 * Apr 7, 2008
 * 
 * @author royoung
 */
public class TimerMemory
{
	/** a place to keep track of previous values to adjut for clock weirdness */
	private static final ConcurrentHashMap<String,AtomicLong> PREVIOUS_BY_NAME = new ConcurrentHashMap<String,AtomicLong>();

	/** this is the threshold below which we don't trust the elapsed time */
	private long dontTrust = 0L;

	/**
	 * Check against the last value returned, to screen out negative elapsed
	 * times due to clock weirdness
	 * 
	 * <p>
	 * Note that there are some race conditions here where two threads can
	 * instantiate and put a value in at the same time. I'm not too worried
	 * about that, though, since the last value is just arbitrary.
	 * 
	 * @param elapsed
	 * @return
	 */
	public long checkAgainstLastValue(String name, long elapsed)
	{
		final long newValue;
		AtomicLong previous = PREVIOUS_BY_NAME.get(name);

		if ( previous == null && elapsed >= this.dontTrust )
		{
			newValue = this.add(name, elapsed);
		}
		else if ( previous == null )
		{
			newValue = this.add(name, this.dontTrust);
		}
		else if ( elapsed < this.dontTrust )
		{
			newValue = previous.get();
		}
		else
		{
			previous.set(elapsed);
			newValue = elapsed;
		}
		return newValue;
	}

	/**
	 * @param name
	 * @param elapsed
	 * @return
	 */
	private long add(String name, long elapsed)
	{
		final long newValue = elapsed;
		final AtomicLong previous = new AtomicLong(newValue);
		PREVIOUS_BY_NAME.put(name, previous);
		return newValue;
	}

	/**
	 * a not particularly thread-safe way to clear the memory; use this for
	 * junit testing
	 */
	public void clear()
	{
		PREVIOUS_BY_NAME.clear();
	}

	/**
	 * Sets the threshold for elapsed times that we are willing to trust.
	 * Elapsed times below this value will use the previous value for that
	 * metric instead.
	 * 
	 * @param dontTrust the dontTrust to set
	 */
	public void setDontTrust(long dontTrust)
	{
		this.dontTrust = dontTrust;
	}

}

