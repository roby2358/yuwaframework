/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jan 28, 2008 $
 */
package org.yuwa.technology.livemetrics.microserver;

import java.io.IOException;
import java.net.Socket;

/**
 * The response object encapsulates information that we send out in an HTTP
 * response.
 * 
 * <p>
 * Contains methods to build the header and stream the response out to a socket.
 * 
 * <p>
 * Jan 28, 2008
 * 
 * @author royoung
 */
public interface MicroServerHttpResponse
{

	/**
	 * send all the information we've bundled up
	 */
	void send(Socket socket)
	throws IOException;
	
	/**
	 * @return the defeatCaching
	 */
	boolean isDefeatCaching();

	/**
	 * set true to add headers to defeat browser caching (i.e. force reload each time)
	 * 
	 * <p>
	 * defaults to true
	 * 
	 * @param defeatCaching the defeatCaching to set
	 */
	void setDefeatCaching(boolean defeatCaching);

	/**
	 * @return the bytes
	 */
	byte[] getBytes();

	/**
	 * set the bytes to send in the response
	 * 
	 * <p>
	 * defaults to an empty array of 0 bytes
	 * 
	 * @param bytes the bytes to set
	 */
	void setBytes(byte[] bytes);

	/**
	 * @return the code
	 */
	int getHttpCode();

	/**
	 * set the HTTP code to send
	 * 
	 * <p>
	 * defaults to 200
	 * 
	 * @param code the code to set
	 */
	void setHttpCode(int code);

	/**
	 * @return the message
	 */
	String getHttpMessage();

	/**
	 * set the HTTP message to send
	 * 
	 * <p>
	 * defaults to OK, as in "200 OK"
	 * 
	 * @param message the message to set
	 */
	void setHttpMessage(String message);

	/**
	 * @return the encoding
	 */
	String getEncoding();

	/**
	 * set the encoding to use when building strings
	 * 
	 * <p>
	 * defaults to UTF-8
	 * 
	 * @param encoding the encoding to set
	 */
	void setEncoding(String encoding);

	/**
	 * @return the encodingHeader
	 */
	String getEncodingHeader();

	/**
	 * set the encoding value to send in the header
	 * 
	 * <p>
	 * defaults to UNICODE-1-1-UTF-8
	 * 
	 * @param encodingHeader the encodingHeader to set
	 */
	void setEncodingHeader(String encodingHeader);

	/**
	 * @return the contentType
	 */
	String getContentType();

	/**
	 * set the encoding type in the header
	 * 
	 * <p>
	 * defaults to text/plain
	 * 
	 * @param contentType the contentType to set
	 */
	void setContentType(String contentType);

	/**
	 * @return the pauseLength
	 */
	long getPauseLength();

	/**
	 * set the length of the pause after sending content before closing the
	 * connection
	 * 
	 * <p>
	 * defaults to 300 milliseconds
	 * 
	 * @param pauseLength the pauseLength to set
	 */
	void setPauseLength(long pauseLength);

}
