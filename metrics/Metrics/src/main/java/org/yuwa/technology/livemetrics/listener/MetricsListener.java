/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Sep 24, 2007 $
 */
package org.yuwa.technology.livemetrics.listener;


import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ThreadFactory;

import org.yuwa.technology.livemetrics.ServiceTracker;
import org.yuwa.technology.livemetrics.metrics.JsonMetricsVisitor;
import org.yuwa.technology.livemetrics.microserver.MicroServer;
import org.yuwa.technology.livemetrics.microserver.MicroServerHandler;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpRequest;


/**
 * the metrics listener listens on a port for HTTP requests, and bundles up
 * metrics information in response
 * 
 * <p>
 * It just rolls up a micro server and metics handler, and presents a
 * consolidated interface to keep the client simple.
 * 
 * <p>
 * The HTTP server logic is extremely bare bones. The listener just ignores the
 * input request and any request parameters, but formats the output as an HTTP
 * response.
 * 
 * @author royoung
 * 
 */
public class MetricsListener
implements MicroServerHandler
{
	/** the microserver to run */
	private MicroServer server = new MicroServer();
	
	/** handle metrics with the metrics handler */
	private MetricsHandler handler = new MetricsHandler();

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// implementation
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * start the listener running
	 *
	 */
	public void start()
	{
		this.server.setHandler(this);
		this.server.start();
	}

	/**
	 * stop the listener from running
	 *
	 */
	public void stop()
	{
		this.server.stop();
	}

	/**
	 * handle a request by creating an "empty" request and passing it down
	 * 
	 * @see org.yuwa.technology.livemetrics.microserver.MicroServerHandler#handle(java.net.Socket)
	 */
	public void handle(Socket socket)
	throws IOException
	{
		this.handler.handle(socket, MicroServerHttpRequest.NO_REQUEST);
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors / delegates
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @param t
	 * @see org.yuwa.technology.livemetrics.listener.MetricsHandler#setMetrics(org.yuwa.technology.livemetrics.ServiceTracker)
	 */
	public void setMetrics(ServiceTracker t)
	{
		this.handler.setMetrics(t);
	}

	/**
	 * @param builder
	 * @see org.yuwa.technology.livemetrics.listener.MetricsHandler#setProtoBuilder(org.yuwa.technology.livemetrics.metrics.JsonMetricsVisitor)
	 */
	public void setProtoBuilder(JsonMetricsVisitor builder)
	{
		this.handler.setProtoBuilder(builder);
	}

	/**
	 * @param initLog4j
	 * @see org.yuwa.technology.livemetrics.microserver.MicroServer#setInitLog4j(boolean)
	 */
	public void setInitLog4j(boolean initLog4j)
	{
		this.server.setInitLog4j(initLog4j);
	}

	/**
	 * set the port to listen to
	 * 
	 * @param port
	 * @see org.yuwa.technology.livemetrics.microserver.MicroServer#setPort(int)
	 */
	public void setPort(int port)
	{
		this.server.setPort(port);
	}

	/**
	 * @param threadFactory
	 * @see org.yuwa.technology.livemetrics.microserver.MicroServer#setThreadFactory(java.util.concurrent.ThreadFactory)
	 */
	public void setThreadFactory(ThreadFactory threadFactory)
	{
		this.server.setThreadFactory(threadFactory);
	}

}
