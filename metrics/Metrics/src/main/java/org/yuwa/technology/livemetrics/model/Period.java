/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */


package org.yuwa.technology.livemetrics.model;

public class Period
implements Metric
{
	/** default serial version ID */
	private static final long serialVersionUID = 1L;
	
	private long periodLength;
	private long period;
	private long min;
	private long max;
	private long total;
	private long count;
	private long average;
	private long lastTime;
	private float[] percentiles = new float[0];
	private long[] percentileValues = new long[0];
	private Histogram histogram = Histogram.NO_HISTOGRAM;

	/**
	 * constructor takes a period length
	 * 
	 * @param length
	 */
	public Period(long time, long length)
	{
		this.periodLength = length;
		this.period = time / this.periodLength;
		this.lastTime = time;
	}
	
	/**
	 * copy constructor
	 * 
	 * @param that
	 */
	public Period(Period that)
	{
		this.set(that);
	}
	
	/**
	 * copy the properties of another period object
	 * 
	 * @param that
	 */
	public void set(Period that)
	{
		this.periodLength = that.periodLength;
		this.period = that.period;
		this.min = that.min;
		this.max = that.max;
		this.total = that.total;
		this.count = that.count;
		this.average = that.average;
		this.lastTime = that.lastTime;
		this.histogram = that.histogram;
		this.percentiles = that.percentiles;
		this.percentileValues = that.percentileValues;
	}
	
	
	/**
	 * create a copy of this period
	 * 
	 * @return this period
	 */
	public Period copy()
	{
	    return new Period(this);
	}


	/**
	 * @param period the period to update
	 * @param histogram the histogram to update from
	 * @return
	 */
	public void updatePercentileValues()
	{
		// we don't always keep the histogram, so if we approximate percentiles,
		// keep a copy of everything
		this.percentiles = histogram.getPercentiles();
		this.percentileValues = histogram.approximatePercentileValues();
	}

	
	/**
	 * @return the next period of this length
	 */
	public Period next()
	{
		Period that = new Period(0,this.periodLength);
		that.period = this.period + 1;
		that.lastTime = this.period * this.periodLength + 1;
		that.histogram = this.histogram.startFresh();
		that.percentiles = that.histogram.getPercentiles();
		that.percentileValues = new long[ that.histogram.getPercentiles().length ];
		return that;
	}

	/**
	 * add another period's values to our own
	 * 
	 * <p>
	 * Note that it must be the same period, or else we do nothing
	 * @param that
	 */
	public void add(Period that)
	{
		if ( this.periodLength != that.periodLength )
		{
			// apples & oranges; ignore
		}
		else if ( this.period > that.period )
		{
			// we are more recent; ignore
		}
		else if ( this.period < that.period )
		{
			// they are more recent, just use them
			this.set(that);
		}
		else
		{
			this.addValues(that);
		}
	}

	/**
	 * add all the values from that into this
	 * 
	 * @param that
	 */
	public void addValues(Period that)
	{
		if ( this.count == 0 || this.min > that.min ) this.min = that.min;
		if ( this.count == 0 || this.max < that.max ) this.max = that.max;
		this.total += that.total;
		this.count += that.count;
		if ( this.count != 0 ) this.average = this.total / this.count;
		else this.average = 0;
		this.lastTime = (this.lastTime > that.lastTime) ? this.lastTime : that.lastTime;
	}
	
	/**
	 * add a single value to the current metric
	 *
	 */
	public void addOne(long now, long value)
	{
		if ( count == 0 || value < min ) min = value;
		if ( count == 0 || value > max ) max = value;
		total += value;
		count++;
		average = total / count;
		this.lastTime = now;
		
		this.histogram.add(value);
	}

	/**
	 * return true if this period is old
	 * 
	 * @return true if the period is old
	 */
	public boolean isOld(long time)
	{
		long now = time / this.periodLength;
        return ( now > this.period );
	}

	public void accept(MetricVisitor visitor) { visitor.visit(this); }
	
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @return the average
	 */
	public long getAverage()
	{
		return average;
	}

	/**
	 * @param average the average to set
	 */
	public Period setAverage(long average)
	{
		this.average = average;
		return this;
	}

	/**
	 * @return the count
	 */
	public long getCount()
	{
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public Period setCount(long count)
	{
		this.count = count;
		return this;
	}

	/**
	 * @return the max
	 */
	public long getMax()
	{
		return max;
	}

	/**
	 * @param max the max to set
	 */
	public Period setMax(long max)
	{
		this.max = max;
		return this;
	}

	/**
	 * @return the min
	 */
	public long getMin()
	{
		return min;
	}

	/**
	 * @param min the min to set
	 */
	public Period setMin(long min)
	{
		this.min = min;
		return this;
	}

	/**
	 * @return the total
	 */
	public long getTotal()
	{
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public Period setTotal(long total)
	{
		this.total = total;
		return this;
	}

	/**
	 * @return the periodLength
	 */
	public long getPeriodLength()
	{
		return this.periodLength;
	}

	/**
	 * @param periodLength the periodLength to set
	 */
	public Period setPeriodLength(long periodLength)
	{
		this.periodLength = periodLength;
		return this;
	}

	/**
	 * @return the percentiles
	 */
	public float[] getPercentiles()
	{
		return this.percentiles;
	}

	/**
	 * @return the percentileValues
	 */
	public long[] getPercentileValues()
	{
		return this.percentileValues;
	}

	/**
	 * @param percentileValues the percentileValues to set
	 */
	public Period setPercentileValues(long[] percentileValues)
	{
		this.percentileValues = percentileValues;
		return this;
	}

	/**
	 * @return the histogram
	 */
	public Histogram getHistogram()
	{
		return this.histogram;
	}

	/**
	 * @param histogram the histogram to set
	 */
	public Period setHistogram(Histogram histogram)
	{
		this.histogram = histogram;
		return this;
	}
	
}
