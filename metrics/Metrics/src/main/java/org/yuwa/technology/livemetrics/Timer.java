/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Mar 17, 2008 $
 */
package org.yuwa.technology.livemetrics;


/**
 * An interface for the various timer interfaces
 *
 * <p>
 * Mar 17, 2008
 *
 * @author royoung
 */
public interface Timer
{
	/**
	 * add the elapsed time to the underlying metrics
	 */
	void addMe();
	
	/**
	 * @return the start time for this timer
	 */
	long start();
	
	/**
	 * @return the elapsed time from the start
	 */
	long elapsed();

	/**
	 * the timer also acts as a prototype, so you can create one just like it
	 * 
	 * @param service
	 * @param componentName
	 * @return
	 */
	Timer create(ServiceTracker tracker, String componentName);
	
	/**
	 * a do-nothing implementation to use as a placeholder
	 */
	Timer NO_TIMER = new Timer()
	{
		public void addMe()
		{
			// nop
		}

		public Timer create(ServiceTracker tracker, String componentName)
		{
			return this;
		}

		public long elapsed()
		{
			return 0;
		}

		public long start()
		{
			return 0;
		}
	};
}
