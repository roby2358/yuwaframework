/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jan 29, 2008 $
 */
package org.yuwa.technology.livemetrics.listener;

import java.io.IOException;
import java.net.Socket;

import org.yuwa.technology.livemetrics.microserver.MicroServer;
import org.yuwa.technology.livemetrics.microserver.MicroServerHandler;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpRequest;


/**
 * This is a utility class that rolls up a server with a heartbeat handler
 *
 * <p>
 * Jan 29, 2008
 *
 * @author royoung
 */
public class HeartbeatListener implements MicroServerHandler
{
	/** the microserver to run */
	private MicroServer server = new MicroServer();
	
	/** handle metrics with the metrics handler */
	private HeartbeatHandler handler = new HeartbeatHandler();

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// implementation
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * start the listener running
	 *
	 */
	public void start()
	{
		this.server.setHandler(this);
		this.server.start();
	}

	/**
	 * stop the listener from running
	 *
	 */
	public void stop()
	{
		this.server.stop();
	}

	/**
	 * handle a request by creating an "empty" request and passing it down
	 * 
	 * @see org.yuwa.technology.livemetrics.microserver.MicroServerHandler#handle(java.net.Socket)
	 */
	public void handle(Socket socket)
	throws IOException
	{
		this.handler.handle(socket, MicroServerHttpRequest.NO_REQUEST);
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors / delegates
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    
    /**
     * set the port we'll listen to
     */
    public void setPort(int port)
    {
        this.server.setPort(port);
    }

	/**
	 * @param handler the handler to set
	 */
	public void setHandler(HeartbeatHandler handler)
	{
		this.handler = handler;
	}

}
