/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 */

package org.yuwa.technology.livemetrics.metrics;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.json.JsonBuilder;
import org.yuwa.technology.livemetrics.model.Component;
import org.yuwa.technology.livemetrics.model.Histogram;
import org.yuwa.technology.livemetrics.model.MetricVisitor;
import org.yuwa.technology.livemetrics.model.Period;
import org.yuwa.technology.livemetrics.model.Service;


/**
 * this visitor walks through the components, rendering the structure as JSON
 * 
 * <p>
 * Sep 24, 2007
 * 
 * @author royoung
 */
public class JsonMetricsVisitor
implements MetricVisitor
{
	private Logger log = Logger.getLogger(getClass());

	private JsonBuilder builder = new JsonBuilder();

	private Date now = new Date();

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS Z");

	private DecimalFormat format = new DecimalFormat();

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// constructor & get results
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	public JsonMetricsVisitor()
	{
	}

	/**
	 * return a instance that's a copy of this class
	 */
	public JsonMetricsVisitor copy()
	{
		JsonMetricsVisitor that = new JsonMetricsVisitor();
		that.builder = this.builder.copy();
		return that;
	}

	/**
	 * render this visitor as a string
	 */
	public String toString()
	{
		return this.builder.toString();
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// implementation methods
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * gather information from the Server
	 */
	public void visit(Service service)
	{
		try
		{
			this.builder.put("name",service.getName());
			this.builder.put("host",service.getHostName());
			this.builder.put("legend",service.getLegend());

			// add a date stamp, too
			String date = this.dateFormat.format(this.now);
			this.builder.put("date",date);

			String startDate = this.dateFormat.format(service.getStartDate());
			this.builder.put("start",startDate);

			this.builder.addEmptyList("components");

			// make sure "total" is first
			Component total = service.getComponentsByName().get("total");
			if ( total != null )
			{
				total.accept(this);
			}

			// show all the rest of the components if there are any besides total
			if ( total == null || service.getComponentsByName().size() > 1 )
			{
				for ( Component c : service.getComponentsByName().values() )
				{
					if ( ! "total".equalsIgnoreCase(c.getName()) )
					{
						c.accept(this);
					}
				}
			}
		}
		catch (Throwable t)
		{
			this.log.error("failed to serialize " + t);
		}
	}

	/**
	 * gather information from the component
	 */
	public void visit(Component component)
	{
		/** start a new builder to hold this as an object */
		JsonBuilder componentBuilder = this.builder.copy();

		componentBuilder.put("name",component.getName());
		componentBuilder.put("lo",component.getLowerLimit());
		componentBuilder.put("hi",component.getUpperLimit());
		componentBuilder.put("legend",component.getLegend());

		if ( component.isReportPercentiles() )
		{
			this.addPercentiles(component.getMinuteHistogram().getPercentiles(), componentBuilder);
		}

		JsonBuilder minutes = this.builder.copy();
		JsonBuilder hours = this.builder.copy();
		JsonBuilder days = this.builder.copy();

		this.addPeriods(component.getMinutes(), minutes);
		this.addPeriods(component.getHours(), hours);
		this.addPeriods(component.getDays(), days);

		if ( component.isReportPercentiles() )
		{
			this.addPercentileValues(component.getMinutes(), minutes);
			this.addPercentileValues(component.getHours(), hours);
			this.addPercentileValues(component.getDays(), days);
		}

		if ( component.isReportHistogram() )
		{
			this.addHistogram(component.getMinutes(), minutes);
			this.addHistogram(component.getHours(), hours);
			this.addHistogram(component.getDays(), days);
		}

		componentBuilder.put("minute",minutes);
		componentBuilder.put("hour",hours);
		componentBuilder.put("day",days);

		this.builder.add("components",componentBuilder);
	}

	/**
	 * Gather information from the Period
	 * 
	 * <p>
	 * Note that we don't actually call this usually,
	 * but rather accumulate by component as lists of periods
	 */
	public void visit(Period period)
	{
		this.builder.put("min",period.getMin());
		this.builder.put("max",period.getMax());
		this.builder.put("average",period.getAverage());
		this.builder.put("count",period.getCount());
		this.builder.put("total",period.getTotal());
	}

	/**
	 * render the histogram as JSON
	 * 
	 * <p>
	 * Note that we don't actually call this usually,
	 * but rather accumulate by component as lists of histogram values
	 * 
	 * @see org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Histogram)
	 */
	public void visit(Histogram bars)
	{
		this.builder.put("min",bars.getMin());
		this.builder.put("max",bars.getMax());
		this.builder.put("buckets",bars.getBuckets());

		this.builder.addEmptyList("values");

		for ( long v : bars.getValues() )
		{
			this.builder.add("values",v);
		}

		DecimalFormat format = new DecimalFormat();
		float[] percentiles = bars.getPercentiles();
		long[] percentileValues = bars.approximatePercentileValues();

		for ( float f : percentiles )
		{
			this.builder.add("p", format.format(f * 100f) );
		}

		for ( long l : percentileValues )
		{
			this.builder.add("pv",l);
		}
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// helpers
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * treat a list of periods as a single object with lists of values
	 * 
	 * @param periods
	 * @param jsonBuilder TODO
	 */
	private void addPeriods(LinkedList<Period> periods, JsonBuilder jsonBuilder)
	{
		jsonBuilder.addEmptyList("total");
		jsonBuilder.addEmptyList("count");
		jsonBuilder.addEmptyList("average");
		jsonBuilder.addEmptyList("min");
		jsonBuilder.addEmptyList("max");

		for ( Period p : periods )
		{
			jsonBuilder.add("total",p.getTotal());
			jsonBuilder.add("count",p.getCount());
			jsonBuilder.add("average",p.getAverage());
			jsonBuilder.add("min",p.getMin());
			jsonBuilder.add("max",p.getMax());
		}
	}

	/**
	 * @param f
	 * @return
	 */
	private String formatPercentile(float f)
	{
		return format.format( f * 100f );
	}

	/**
	 * @param first
	 */
	private void addPercentiles(float[] percentiles, JsonBuilder builder)
	{
		for ( float f : percentiles )
		{
			builder.add("percentiles", this.formatPercentile(f) );
		}
	}

	/**
	 * add in the percentile values for the period
	 * 
	 * @param periods
	 * @param jsonBuilder
	 */
	private void addPercentileValues(LinkedList<Period> periods, JsonBuilder builder)
	{
		float[] percentiles = periods.getFirst().getPercentiles();

		for ( int i = 0 ; i < percentiles.length ; i++ )
		{
			String name = "\"" + this.formatPercentile(percentiles[i]) + "\"";

			for ( Period p : periods )
			{
				long[] values = p.getPercentileValues();
				if ( i < values.length )
				{
					builder.add( name, "" + p.getPercentileValues()[i]);
				}
				else
				{
					builder.add( name, "0");
				}
			}
		}
	}

	/**
	 * add in the histogram values for the period
	 * 
	 * @param periods
	 * @param jsonBuilder
	 */
	private void addHistogram(LinkedList<Period> periods, JsonBuilder builder)
	{
		JsonBuilder valuesBuilder = builder.copy();
		
		valuesBuilder.put("min",periods.getFirst().getHistogram().getMin());
		valuesBuilder.put("max",periods.getFirst().getHistogram().getMax());
		valuesBuilder.put("buckets",periods.getFirst().getHistogram().getBuckets());
		
		for ( Period p : periods )
		{
			long[] values = p.getHistogram().getValues();
			for ( int i = 0 ; i < values.length ; i++ )
			{
				String name = "\"" + i + "\"";
				valuesBuilder.add( name, "" + values[i] );
			}
		}
		
		builder.put("histogram",valuesBuilder);
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @param builder the builder to set
	 */
	public void setJsonBuilder(JsonBuilder builder)
	{
		this.builder = builder;
	}

}
