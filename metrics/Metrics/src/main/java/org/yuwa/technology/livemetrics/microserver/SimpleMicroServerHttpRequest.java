/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jan 28, 2008 $
 */
package org.yuwa.technology.livemetrics.microserver;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * This implementation includes simple parsing logic to read in the request
 * 
 * <p>
 * Jan 28, 2008
 *
 * @author royoung
 */
public class SimpleMicroServerHttpRequest
implements MicroServerHttpRequest
{
	/** our logger */
	protected Logger log = Logger.getLogger(getClass());
	
	/** the size of the internal buffer for reading the bytes */
	private static final int BUFFER_SIZE = 65536;
	
	/** the actual bits in the request */
	private byte[] requestBits = new byte[0];

	/** the position where we are in the buffer */
	private int position;

	/** a builder for accumulating strings */
	private StringBuilder builder = new StringBuilder();

	/** the method */
	private String method = "ERROR";
	
	/** the full path in the request */
	private String fullPath = "";
	
	/** the path broken up by / */
	private List<String> path = new ArrayList<String>();
	
	/** a map of headers by name */
	private Map<String,String> headersByName = new HashMap<String,String>();
	
	/** the body */
	private byte[] body = new byte[0];
	
	
	/**
	 * default constructor ignores the input and returns an "empty" request
	 */
	public SimpleMicroServerHttpRequest()
	{
		// nop
	}
	
	/**
	 * parse the given socket's input stream and populate this object
	 * accordingly
	 * 
	 * @param socket
	 * @throws IOException 
	 */
	public SimpleMicroServerHttpRequest(Socket socket)
	throws IOException
	{
		if ( log.isDebugEnabled() )
			log.debug("begin SimpleMicroServerHttpRequest");
		
		this.requestBits = this.readBytes(socket);
		log.debug("got " + this.requestBits.length + " bytes");
		this.position = 0;
		if ( this.requestBits != null && this.requestBits.length > 0)
			this.parse();
	}
	
	/**
	 * A utility constructor, mostly for testing, in case we already have the
	 * bytes as an array
	 * 
	 * @param socket
	 */
	public SimpleMicroServerHttpRequest(byte[] input)
	throws IOException
	{
		if ( log.isDebugEnabled() )
			log.debug("begin SimpleMicroServerHttpRequest");
				
		this.requestBits = input;
		this.position = 0;
		this.parse();
	}
	
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// utility
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	private byte[] readBytes(Socket socket)
	throws IOException
	{
		final byte[] bits;
		
		InputStream in = socket.getInputStream();
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		byte[] buffer = new byte[BUFFER_SIZE];
		int n;
		
		// TODO: fix this
		if ( (n = in.read(buffer) ) > -1 )
		{
			log.debug("read " + n);
			out.write(buffer,0,n);
		}
		
		log.debug("finished reading " + n);
		
		bits = out.toByteArray();
		
		return bits;
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// parsing methods
	//
	// public for testing, but not exposed in the interface
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/*
	 * http://www.w3.org/Protocols/HTTP/Request.html
	 * 
	 * Request           =     SimpleRequest | FullRequest
	 * SimpleRequest     =        GET <uri> CrLf
	 * FullRequest       =        Method URI ProtocolVersion CrLf
	 * [*<HTRQ Header>]
	 * [<CrLf> <data>]
	 * <Method>          =     <InitialAlpha>
	 * ProtocolVersion   =        HTTP/1.0
	 * uri               =        <as defined in URL spec>
	 * <HTRQ Header>     =     <Fieldname> : <Value> <CrLf>
	 * <data>            =      MIME-conforming-message     
    */
	
	public void parse()
	throws IOException
	{
		if ( ! this.parseMethod() ) throw new IOException("method not found");
		
		if ( ! this.parseUri() ) throw new IOException("uri not found");
		
		this.parseVersion();
		
		while ( ! this.parseEmptyLine() ) this.parseHeader();
		
		this.parseBody();
	}
	
	public boolean parseMethod()
	{
		final boolean ok;
		
		this.parseRun(" \r\n");
		
		this.parseTo(" \r\n");
		
		if ( this.builder.length() == 0 )
		{
			ok = false;
		}
		else
		{
			this.method = builder.toString().toUpperCase();
			ok = true;
			this.parseRun(" ");
		}

		
		return ok;
	}
	
	public boolean parseUri()
	{
		final boolean ok;

		this.clear();

		this.parseTo(" \r\n");

		if ( this.builder.length() == 0 )
		{
			ok = false;
		}
		else
		{
			ok = true;
			
			// grab the path and the path elements
			this.fullPath = this.builder.toString();
			String[] pieces = this.fullPath.split("/");

			// TODO: parse with URI class? we really only care about the path
			// for now
			
			for ( String s : pieces )
			{
				if ( s != null && s.length() != 0)
				{
					try
					{
						this.path.add(URLDecoder.decode(s,"UTF-8"));
					}
					catch (UnsupportedEncodingException e)
					{
						// bah... ignore
					}
				}
			}
		}

		return ok;
	}

	public boolean parseVersion()
	{
		// ignore... just burn to end of line
		this.parseTo("\r");
		this.parseRun("\r\n");
		
		return true;
	}
	
	public boolean parseHeader()
	{
		final boolean ok;

		// TODO: check for empty strings

		if ( this.done(1) || this.next() == '\r' )
		{
			ok = false;
		}
		else
		{
			ok = true;

			this.parseTo(" :\r\n");
			String name = this.builder.toString();

			this.parseRun(" :");

			this.parseTo("\r\n");
			String value = this.builder.toString();

			this.parseChar('\r');
			this.parseChar('\n');

			this.headersByName.put(name, value);
			
			if ( log.isDebugEnabled() )
				log.debug("got header ["+name+"] ["+value+"]");
		}

		return ok;
	}

	public boolean parseEmptyLine()
	{
		final boolean ok;
		if ( ! this.done(2) && this.next() == '\r' )
		{
			// TODO: handle failures
			this.parseChar('\r');
			this.parseChar('\n');
			ok = true;
		}
		else
		{
			ok = false;
		}
		return ok;
	}
	
	public boolean parseBody()
	{
		this.clear();

		if ( this.requestBits.length - this.position > 0 )
		{
			this.body = new byte[this.requestBits.length - this.position];
			int j = 0;
			for ( int i = this.position ; i < this.requestBits.length ; i++ )
				this.body[j++] = this.requestBits[i];
		}
		
		return true;
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// utility
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    
	/**
	 * munch some number of characters
	 */
	public void munch(int n)
	{
		this.position += n;
	}
	
	/**
	 * @return the next character
	 */
	public char next()
	{
		return (char)this.requestBits[this.position];
	}
	
	/**
	 * true if n is past the input
	 * @param n
	 * @return
	 */
	public boolean done(int n)
	{
		return this.position + n > this.requestBits.length;
	}

	/**
	 * wipe the builder clean
	 */
	private void clear()
	{
		if ( this.builder.length() > 0 )
			this.builder.delete(0, this.builder.length());
	}

	/**
	 * true if we parse out the given char
	 * @param c
	 * @return true if we munched the given char
	 */
	public boolean parseChar(char c)
	{
		final boolean ok;
		
		if ( ! this.done(1) && this.next() == c )
		{
			this.munch(1);
			ok = true;
		}
		else
		{
			ok = false;
		}
		return ok;
	}
	
	/**
	 * parse to one of some terminating character
	 * 
	 * @param chars
	 * @return
	 */
	public StringBuilder parseTo(String chars)
	{
		this.clear();
		while ( ! this.done(1) && chars.indexOf(this.next()) == -1 )
		{
			this.builder.append(this.next());
			this.munch(1);
		}
		return this.builder;
	}
	/**
	 * true if we parsed a run of 1 or more of the given char
	 * 
	 * @param c
	 * @return true if we found the given char
	 */
	public boolean parseRun(String chars)
	{
		boolean ok = false;
		
		while ( ! this.done(1) && chars.indexOf( this.next() ) > -1 )
		{
			this.munch(1);
			ok = true;
		}

		return ok;
	}
	
	/**
	 * @return
	 */
	@SuppressWarnings("unused")
	private boolean nextIsWhitespace()
	{
		char c = this.next();
		return c == ' ' || c == '\r' || c == '\n';
	}

	/**
	 * used for testing, but not exposed in the interface
	 * @throws UnsupportedEncodingException 
	 */
	public void set(String input, int position)
	throws UnsupportedEncodingException
	{
		this.requestBits = input.getBytes("UTF-8");
		this.position = position;
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @return the method
	 */
	public String getMethod()
	{
		return this.method;
	}

	/**
	 * @return the fullPath
	 */
	public String getFullPath()
	{
		return this.fullPath;
	}

	/**
	 * @return the path
	 */
	public List<String> getPath()
	{
		return this.path;
	}

	/**
	 * @return the headersByName
	 */
	public Map<String, String> getHeadersByName()
	{
		return this.headersByName;
	}

	/**
	 * @return the body
	 */
	public byte[] getBody()
	{
		return this.body;
	}
	
}
