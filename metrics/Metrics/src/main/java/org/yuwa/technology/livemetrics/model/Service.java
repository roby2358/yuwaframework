/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 */

package org.yuwa.technology.livemetrics.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.yuwa.technology.livemetrics.listener.MetricsListener;

/**
 * bundles up a number of components, and provides methods for adding values to
 * those components by name
 * 
 * <p>
 * Also maintains a "total" component, which represents the sum of all the other
 * components
 * 
 * <p>
 * Note that a service tracker interface has separate accessors for "service"
 * and "history", allowing for a more lightweight service object than this one,
 * which only tracks the current period and maintains history in another object
 * 
 * <p>
 * This class is not thread-safe; the client should provide appropriate
 * synchronization, perhaps by using the ServiceWrapper class.
 * 
 * @author royoung
 * 
 */
public class Service
implements Metric
{
    /** default serial version ID */
    private static final long serialVersionUID = 1L;

    private String hostName;
    private String name;
    private String legend;
    private Date date = new Date();
    private Map<String,Component> componentsByName = new HashMap<String,Component>();
    private Component total = new Component();
    private long now = System.currentTimeMillis();
    private Date startDate = new Date(now);
    private MetricsListener listener;

    /**
     * Constructor sets up the total counter
     *
     */
    public Service()
    {
        this.total.setName("total");
        this.add(this.total);
    }

    /**
     * let a metric visitor in
     */
    public void accept(MetricVisitor visitor) { visitor.visit(this); }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // public methods
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


    /**
     * tell this component to start histogram tracking, but just report
     * percentiles
     * 
     * @return the component
     */
    public Service startPercentiles(String componentName, long min, long max, int buckets)
    {
        this.getComponent(componentName).startPercentiles(min, max, buckets);
        return this;
    }


    /**
     * tell this component to start histogram tracking, and report percentile
     * and histogram data, too
     * 
     * <p>
     * NOTE: turning this on for a component will drastically increase the
     * amount of data that it tracks, especially if you use a lot of buckets.
     * This will have an impact on the memory footprint, and the time needed to
     * render and transmit the component. You may even want to split the
     * component in two, one with histogram and one without.
     * 
     * @return the component
     */
    public Service startHistogram(String componentName, long min, long max, int buckets)
    {
        this.getComponent(componentName).startHistogram(min, max, buckets);
        return this;
    }

    /**
     * check all the periods to see if they're still current; if not, roll them
     * over
     */
    public void roll()
    {
        this.before();

        for ( Component c : this.componentsByName.values() )
        {
            c.roll(this.now);
        }

        this.after();
    }

    /**
     * add a value for the given component & to the total
     * 
     * @param componentName
     * @param value
     */
    public void add(String componentName, long value)
    {
        this.before();

        Component component = this.getComponent(componentName);
        component.add(this.now, value);
        this.total.add(this.now, value);

        this.after();
    }


    /**
     * add one to the count for this second; if the second
     * has passed, hand the value down 
     * 
     * <p>
     * in most cases, the value will just be 1
     * 
     * @param componentName
     */
    public void addToSecond(String componentName, long value)
    {
        this.before();

        Component component = this.getComponent(componentName);
        component.addToSecond(this.now, value);
        this.total.addToSecond(this.now, value);

        this.after();
    }

    /**
     * fire any events to happen before we make a change
     */
    public void before()
    {
        // Note: this could be problematic on a multi-CPU system (a little anyways)
        this.now = System.currentTimeMillis();
    }

    /**
     * fire any events to happen after we make a change
     */
    public void after()
    {
        // nop
    }


    /**
     * stop anything that we need to stop
     */
    public void stop()
    {
        if (this.listener != null)
            this.listener.stop();
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * add a single component
     * 
     * @param c
     */
    public void add(Component c)
    {
        this.componentsByName.put(c.getName(), c);
    }

    /**
     * a name to display this service
     * 
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public Service setName(String name)
    {
        this.name = name;
        return this;
    }

    /**
     * get a component by its name
     * 
     * <p>
     * if not found, creates it and adds it to the map
     * 
     * @return the component
     */
    public Component getComponent(String name)
    {
        Component component = this.componentsByName.get(name);

        if ( component == null )
        {
            component = new Component();
            component.setName(name);
            this.add(component);
        }

        return component;
    }

    /**
     * get the components map
     * 
     * @return the map of components, keyed by name
     */
    public Map<String,Component> getComponentsByName()
    {
        return this.componentsByName;
    }

    /**
     * the component maps holds all the components, hashed by their name
     * 
     * @param componentsByName the componentsByName to set
     */
    public Service setComponentsByName(Map<String, Component> componentsByName)
    {
        this.componentsByName = componentsByName;
        return this;
    }

    /**
     * a host name to display for this service
     * 
     * @return the host
     */
    public String getHostName()
    {
        return hostName;
    }

    /**
     * @param host the host to set
     */
    public Service setHostName(String host)
    {
        this.hostName = host;
        return this;
    }

    /**
     * URL to the legend for this service
     * 
     * @return the legend
     */
    public String getLegend()
    {
        return this.legend;
    }

    /**
     * URL to the legend for this service
     * 
     * @param legend the legend to set
     */
    public Service setLegend(String legend)
    {
        this.legend = legend;
        return this;
    }

    /**
     * get the time the service considers to be "now" -- to handle multiple
     * events in (virtually) the same time
     * 
     * @return the now
     */
    public long getNow()
    {
        return this.now;
    }

    /**
     * @return the date
     */
    public Date getDate()
    {
        return this.date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date)
    {
        this.date = date;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate()
    {
        return this.startDate;
    }

    /**
     * we need to set it when unmarshalling
     * 
     * @param startDate
     */
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    /**
     * @param listener
     */
    public void setListener(MetricsListener listener)
    {
        this.listener = listener;
    }
}
