/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jan 29, 2008 $
 */
package org.yuwa.technology.livemetrics.listener;

import java.io.IOException;
import java.net.Socket;

import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpHandler;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpRequest;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpResponse;
import org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpResponse;


/**
 *
 * <p>
 * Jan 29, 2008
 *
 * @author royoung
 */
public class HeartbeatHandler
implements MicroServerHttpHandler
{
	/** our logger */
	protected Logger log = Logger.getLogger(getClass());

	/** the default time to panic */
	private static final long DEFAULT_PANIC_TIME = 60L * 60L * 1000L;

	/** the last time our heart beat */
	private long lastHeartbeat = System.currentTimeMillis();

	/** how long until we're worried */
	private long panicTimeMillis = DEFAULT_PANIC_TIME;

	/**
	 * handle the request by checking the heartbeat time and reponding if we're OK
	 * 
	 * @see org.yuwa.technology.livemetrics.microserver.MicroServerHandler#handle(java.net.Socket)
	 */
	public void handle(Socket socket, MicroServerHttpRequest request)
	throws IOException
	{
		if ( log.isDebugEnabled() )
			log.debug("begin handle");

		final String message;

		long elapsed = System.currentTimeMillis() - this.lastHeartbeat;
		if ( elapsed > this.panicTimeMillis )
		{
			message = "ERROR elapsed time since heartbeat exceeds " + this.panicTimeMillis;
		}
		else
		{
			message = "OK";
		}

		if ( log.isDebugEnabled() )
			log.debug("elapsed=" + elapsed + " " + message);

		// encode the message and send it
		MicroServerHttpResponse response = new SimpleMicroServerHttpResponse();
		response.setBytes(message.getBytes(response.getEncoding()));
		response.send(socket);
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// implementation
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	public void beat()
	{
		// note: don't log the heartbeat... fill the log with "thumps" ;)
		
		this.lastHeartbeat = System.currentTimeMillis();
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @param panicTimeMillis the panicTimeMillis to set
	 */
	public void setPanicTimeMillis(long panicTimeMillis)
	{
		this.panicTimeMillis = panicTimeMillis;
	}

	/**
	 * @return the panicTimeMillis
	 */
	public long getPanicTimeMillis()
	{
		return this.panicTimeMillis;
	}
}
