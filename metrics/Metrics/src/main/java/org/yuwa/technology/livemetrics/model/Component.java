/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package org.yuwa.technology.livemetrics.model;

import java.util.LinkedList;

/**
 * this object gathers the metrics for a single component within the service
 * 
 * <p>
 * For example, it might measure one path in a messaging gateway.
 * 
 * <p>
 * As of 1.7.0, synchronization is done here, so this object is thread-safe
 * 
 * @author royoung
 * 
 */
public class Component implements Metric
{
	/** default serial version ID */
	private static final long serialVersionUID = 1L;

	private int maxMinutes = 60;
	private int maxHours = 24;
	private int maxDays = 14;
	private long lowerLimit = 0;
	private long upperLimit = 100;

	private String name;
	private String legend;
	private LinkedList<Period> minutes = new LinkedList<Period>();
	private LinkedList<Period> hours = new LinkedList<Period>();
	private LinkedList<Period> days = new LinkedList<Period>();
	private long lastSecond;
	private long lastSecondCount;
	private boolean reportHistogram = false;
	private boolean reportPercentiles = false;


	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// public
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * constructor seeds the different period lists
	 * 
	 * <p>
	 * Start each period the max ago, so it will roll to the current period &
	 * fill the array
	 * 
	 */
	public Component(long now)
	{
		long firstMinute = now - Metric.ONE_MINUTE * this.maxMinutes;
		long firstHour = now - Metric.ONE_HOUR * this.maxHours;
		long firstDay = now - Metric.ONE_DAY * this.maxDays;

		Period minute = new Period(firstMinute, Metric.ONE_MINUTE);
		Period hour = new Period(firstHour, Metric.ONE_HOUR);
		Period day = new Period(firstDay, Metric.ONE_DAY);

		this.minutes.add(minute);
		this.hours.add(hour);
		this.days.add(day);

		this.getCurrentPeriod(this.minutes, maxMinutes, now);
		this.getCurrentPeriod(this.hours, maxHours, now);
		this.getCurrentPeriod(this.days, maxDays, now);
	}


	/**
	 * default constructor uses currentTimeMillis ;)
	 */
	public Component()
	{
		this(System.currentTimeMillis());
	}


	/**
	 * allow a metric visitor to visit us
	 */
	public synchronized void accept(MetricVisitor visitor)
	{
		visitor.visit(this);
	}


	/**
	 * add a value to all our periods, rolling them to current if we're behind
	 * 
	 * @param value
	 */
	public synchronized void add(long time, long value)
	{
		this.checkToFlushSecond(time);
		this.checkForRoll(time);

		Period minute = this.getCurrentPeriod(this.minutes, maxMinutes, time);
		Period hour = this.getCurrentPeriod(this.hours, maxHours, time);
		Period day = this.getCurrentPeriod(this.days, maxDays, time);

		minute.addOne(time, value);
		hour.addOne(time, value);
		day.addOne(time, value);
	}


	/**
	 * add one to the count for this second; if the second has passed, hand the
	 * value down
	 * 
	 * <p>
	 * in most cases, the value will just be 1
	 * 
	 * @param componentName
	 */
	public synchronized void addToSecond(long now, long value)
	{
		this.checkToFlushSecond(now);
		this.roll(now);
		this.lastSecondCount += value;
	}


	/**
	 * call roll using the current system time
	 * 
	 * @param now
	 *            TODO
	 */
	public synchronized void roll(long now)
	{
		this.checkToFlushSecond(now);
		this.checkForRoll(now);

		this.getCurrentPeriod(this.minutes, this.maxMinutes, now);
		this.getCurrentPeriod(this.hours, this.maxHours, now);
		this.getCurrentPeriod(this.days, this.maxDays, now);
	}


	/**
	 * tell this component to start histogram tracking, but just report
	 * percentiles
	 * 
	 * @return the component
	 */
	public synchronized Component startPercentiles(long min, long max, int buckets)
	{
		Histogram newHistogram = new SimpleHistogram()
		.setMin(min)
		.setMax(max)
		.setBuckets(buckets);
		
		this.minutes.getFirst().setHistogram(newHistogram);
		this.hours.getFirst().setHistogram(newHistogram.startFresh());
		this.days.getFirst().setHistogram(newHistogram.startFresh());

		this.reportPercentiles = true;

		return this;
	}


	/**
	 * tell this component to start histogram tracking, and report percentile
	 * and histogram data, too
	 * 
	 * <p>
	 * NOTE: turning this on for a component will drastically increase the
	 * amount of data that it tracks, especially if you use a lot of buckets.
	 * This will have an impact on the memory footprint, and the time needed to
	 * render and transmit the component. You may even want to split the
	 * component in two, one with histogram and one without.
	 * 
	 * @return the component
	 */
	public synchronized Component startHistogram(long min, long max, int buckets)
	{
		this.startPercentiles(min, max, buckets);
		this.reportHistogram = true;
		return this;
	}


	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// utility / implementation
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * true if any of the components are old
	 * 
	 * <p>
	 * Just check minutes, because everything will roll on the minute, and we
	 * don't want to get issues with microsecond differences causing multiple
	 * rolls
	 *
	 * <p>
	 * exposed for junit testing
	 * 
	 * @param time
	 * @return true if any of the components are old
	 */
	boolean isOld(long time)
	{
		boolean oldMinutes = this.minutes.getFirst().isOld(time);
		return oldMinutes;
	}


	/**
	 * flush anything we've gathered in the last second
	 * 
	 * <p>
	 * Note that the component adds the accumulated value to the *current*
	 * periods -- this method does *not* cause the periods to roll
	 * 
	 * @param time
	 *            the current time in millis
	 * @param value
	 */
	private void checkToFlushSecond(long time)
	{
		long second = time / ONE_SECOND;
		if (second != this.lastSecond && this.lastSecondCount > 0)
		{
			this.addToAll(time, this.lastSecondCount);

			// start a new second & add the old one
			this.lastSecond = second;
			this.lastSecondCount = 0;
		}
	}


	/**
	 * check to see if we're about to roll the history
	 * 
	 * @param time
	 */
	private void checkForRoll(long time)
	{
		if (this.isOld(time))
		{
			Period firstMinute = this.minutes.getFirst();
			Period firstHour = this.hours.getFirst();
			Period firstDay = this.days.getFirst();

			firstMinute.updatePercentileValues();
			firstHour.updatePercentileValues();
			firstDay.updatePercentileValues();
		}
	}


	/**
	 * add the value to all the current periods
	 * 
	 * @param value
	 */
	private void addToAll(long time, long value)
	{
		this.minutes.getFirst().addOne(time, value);
		this.hours.getFirst().addOne(time, value);
		this.days.getFirst().addOne(time, value);
	}


	/**
	 * returns the current period, rolling the list as long as we're behind
	 * 
	 * @param periodList
	 *            the linked list of periods
	 * @param millis
	 *            the milliseconds in one period
	 * @param maxSize
	 *            the max size of the list
	 * @return the current minute
	 */
	private Period getCurrentPeriod(LinkedList<Period> periodList, int maxSize,
			long time)
	{
		Period p = periodList.getFirst();
		while (p.isOld(time))
		{
			Period previous = p;
			p = previous.next();
			periodList.addFirst(p);
			if (periodList.size() > maxSize)
				periodList.removeLast();
			
			if ( ! this.reportHistogram )
				previous.setHistogram(Histogram.NO_HISTOGRAM);
		}
		return p;
	}


	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}


	/**
	 * @param name
	 *            the name to set
	 */
	public Component setName(String name)
	{
		this.name = name;
		return this;
	}


	/**
	 * @return the day
	 */
	public LinkedList<Period> getDays()
	{
		return days;
	}


	/**
	 * @param day
	 *            the day to set
	 */
	public synchronized Component setDays(LinkedList<Period> day)
	{
		this.days = day;
		return this;
	}


	/**
	 * @return the hour
	 */
	public LinkedList<Period> getHours()
	{
		return hours;
	}


	/**
	 * @param hour
	 *            the hour to set
	 */
	public synchronized Component setHours(LinkedList<Period> hour)
	{
		this.hours = hour;
		return this;
	}


	/**
	 * @return the minute
	 */
	public LinkedList<Period> getMinutes()
	{
		return minutes;
	}


	/**
	 * @param minute
	 *            the minute to set
	 */
	public synchronized Component setMinutes(LinkedList<Period> minute)
	{
		this.minutes = minute;
		return this;
	}


	/**
	 * @return the lowerLimit
	 */
	public long getLowerLimit()
	{
		return lowerLimit;
	}


	/**
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public Component setLowerLimit(int lowerLimit)
	{
		this.lowerLimit = lowerLimit;
		return this;
	}


	/**
	 * @return the upperLimit
	 */
	public long getUpperLimit()
	{
		return upperLimit;
	}


	/**
	 * @param upperLimit
	 *            the upperLimit to set
	 */
	public Component setUpperLimit(int upperLimit)
	{
		this.upperLimit = upperLimit;
		return this;
	}


	/**
	 * URL to the legend for this component
	 * 
	 * @return the legend
	 */
	public String getLegend()
	{
		return this.legend;
	}


	/**
	 * URL to the legend for this component
	 * 
	 * @param legend
	 *            the legend to set
	 */
	public Component setLegend(String legend)
	{
		this.legend = legend;
		return this;
	}


	/**
	 * @return the reportPercentiles
	 */
	public boolean isReportPercentiles()
	{
		return this.reportPercentiles;
	}


	/**
	 * @return the reportHistogram
	 */
	public boolean isReportHistogram()
	{
		return this.reportHistogram;
	}


	/**
	 * @return the minuteHistogram
	 */
	public Histogram getMinuteHistogram()
	{
		return this.minutes.getFirst().getHistogram();
	}


	/**
	 * @return the hourHistogram
	 */
	public Histogram getHourHistogram()
	{
		return this.hours.getFirst().getHistogram();
	}


	/**
	 * @return the dayHistogram
	 */
	public Histogram getDayHistogram()
	{
		return this.days.getFirst().getHistogram();
	}

}
