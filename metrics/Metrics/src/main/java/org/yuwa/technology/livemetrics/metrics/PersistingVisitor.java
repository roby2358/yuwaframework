/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jul 7, 2008 $
 */
package org.yuwa.technology.livemetrics.metrics;

import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ThreadFactory;

import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.json.JsonBuilder;
import org.yuwa.technology.livemetrics.model.Component;
import org.yuwa.technology.livemetrics.model.Histogram;
import org.yuwa.technology.livemetrics.model.MetricVisitor;
import org.yuwa.technology.livemetrics.model.Period;
import org.yuwa.technology.livemetrics.model.Service;
import org.yuwa.technology.livemetrics.util.SimpleThreadFactory;


/**
 * The persisting visitor periodically captures the history and writes it to disk
 * 
 * <p>
 * Jul 7, 2008
 *
 * @author royoung
 */
public class PersistingVisitor
implements MetricVisitor
{
	/** our logger */
	protected Logger log = Logger.getLogger(getClass());

	/** a metrics visitor to render the history */
	private JsonMetricsVisitor jsonVisitor = new JsonMetricsVisitor();

	/** the last time we ran */
	private long last = System.currentTimeMillis();

	/** period between persists (millis) */
	private long period = 60L * 1000L;

	/** path to persistence file */
	private String path;

	/** the number of consequtive errors */
	private int errorCount;

	/** the number of consequtive errors we'll report */
	private int errorTolerance = 5;

	/** the next one, if you want to chain them */
	private MetricVisitor next = MetricVisitor.NO_VISITOR;

	private ThreadFactory threadFactory = new SimpleThreadFactory();

	private Thread thread;

	/** public for junit testing, but not exposed through interface */
	protected MyWriter myWriter = new MyWriter();

	private final PersistingVisitor THIS = this;


	public void start()
	{
		this.thread = this.threadFactory.newThread(this.myWriter);
		this.thread.start();
	}

	public void stop()
	{
		synchronized ( this.myWriter )
		{
			this.myWriter.running = false;
			this.thread.interrupt();
		}
	}


	/**
	 * exposed for junit testing
	 */
	public boolean isStale(long now)
	{
		final boolean stale = (now - this.last) > this.period;
		return stale;
	}

	/**
	 * @see org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Service)
	 */
	public void visit(Service service)
	{
		JsonMetricsVisitor visitor = null;
		long now = 0;
		boolean go = false;

		try
		{
			synchronized ( service )
			{
				now = service.getNow();
				if ( this.isStale(now) )
				{
					this.log.debug("stale, persist metrics");

					visitor = this.jsonVisitor.copy();
					service.accept(visitor);
					go = true;

					// TODO: is this the right place? It will stop other threads
					// from writing, but if the write fails, we lose data
					this.last = now;
				}
			}
		}
		catch (Throwable t)
		{
			if ( this.errorTolerance > this.errorCount++ )
				log.error("unable to save metrics to [" + this.path + "] : " + t.getMessage());
		}

		if ( go )
		{
			synchronized ( this.myWriter )
			{
				this.myWriter.go = true;
				this.myWriter.visitor = visitor;
				this.myWriter.notifyAll();
			}
		}

		this.next.visit(service);
	}

	/**
	 * a inner class that does the actual file writing in a thread
	 * <p>
	 * Jul 8, 2008
	 *
	 * @author royoung
	 */
	private class MyWriter
	implements Runnable
	{
		boolean running = true;
		boolean go = false;
		JsonMetricsVisitor visitor = null;
		FileWriter out = null;

		public void run()
		{
			synchronized ( this )
			{
				THIS.log.info("starting");

				while ( running )
				{
					if ( go )
					{
						try
						{
							if ( THIS.log.isDebugEnabled() )
								THIS.log.debug("save metrics to " + THIS.path);

							final String metrics = visitor.toString();

							out = new FileWriter(THIS.path,false);
							out.write(metrics);
							out.close();

							THIS.errorCount = 0;
						}
						catch (Throwable t)
						{
							if ( THIS.errorTolerance > THIS.errorCount++ )
								log.error("unable to save metrics to [" + THIS.path + "] : " + t.getMessage());

							THIS.forceClose(out);
						}
					}

					this.go = false;

					// wait for something to happen
					try
					{
						Thread.sleep(10L);
						this.wait();
					}
					catch (InterruptedException e)
					{
						// ok wake up
					}
				}
			}

			THIS.log.info("stopping");
		}

	}

	/**
	 * force the writer to close
	 * 
	 * @param out
	 */
	private void forceClose(FileWriter out)
	{
		try
		{
			if ( out != null ) out.close();
		}
		catch (IOException e)
		{
			// suppress
		}
	}


	/**
	 * @see org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Component)
	 */
	public void visit(Component component)
	{
		// ignore & forward

		this.next.visit(component);
	}


	/**
	 * @see org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Period)
	 */
	public void visit(Period period)
	{
		// ignore & forward

		this.next.visit(period);
	}


	public void visit(Histogram bars)
	{
		// ignore & forward
		this.next.visit(bars);
	}


//	~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
//	accessors
//	~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * the builder to use to generate JSON ... otherwise use default builder
	 * 
	 * @param builder
	 */
	public void setJsonBuilder(JsonBuilder builder)
	{
		this.jsonVisitor.setJsonBuilder(builder);
	}


	/**
	 * the period between persists, in millis
	 * 
	 * @param period the period to set
	 */
	public void setPeriod(long period)
	{
		this.period = period;
	}

	/**
	 * the file path to the persistence file
	 * 
	 * @param path the path to set
	 */
	public void setPath(String path)
	{
		this.path = path;
	}

	/**
	 * the number of consequtive errors to log before we give up
	 * 
	 * @param errorTolerance the errorTolerance to set
	 */
	public void setErrorTolerance(int errorTolerance)
	{
		this.errorTolerance = errorTolerance;
	}

	/**
	 * the next one, if you want to chain them
	 * 
	 * @param next the next to set
	 */
	public void setNext(MetricVisitor next)
	{
		this.next = next;
	}

	/**
	 * exposed for junit testing
	 * 
	 * @param last the last to set
	 */
	public void setLast(long last)
	{
		this.last = last;
	}

	/**
	 * @param threadFactory the threadFactory to set
	 */
	public void setThreadFactory(ThreadFactory threadFactory)
	{
		this.threadFactory = threadFactory;
	}

}
