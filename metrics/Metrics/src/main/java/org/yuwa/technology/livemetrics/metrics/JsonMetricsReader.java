/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Nov 10, 2008 $
 */
package org.yuwa.technology.livemetrics.metrics;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.BindException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.json.GenericJsonHelper;
import org.yuwa.technology.livemetrics.json.JsonBinder;
import org.yuwa.technology.livemetrics.model.Component;
import org.yuwa.technology.livemetrics.model.Period;
import org.yuwa.technology.livemetrics.model.Service;


/**
 * The reader is the flip-side of the parser; it reads in JSON and produces
 * model objects
 * 
 * <p>
 * Nov 10, 2008
 * 
 * @author royoung
 */
public class JsonMetricsReader
{
	/** our logger */
	protected Logger log = Logger.getLogger(getClass());

	/** the source charset */
	private static final String SOURCE_CHARSET = "US-ASCII";

	/** the source string */
	private String source;

	/** the resulting service */
	private Service service;

	/** an internal representation of the data */
	private Map<String,Object> info = new HashMap<String,Object>();

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS zzzz");

	private GenericJsonHelper helper = new GenericJsonHelper();
	
	/**
	 * a reader that takes it if you already have a string
	 * 
	 * @param string
	 * @throws BindException 
	 */
	public void read(String string)
	throws IOException
	{
		this.source = string;
		this.parse();
	}

	/**
	 * take an input stream and parse it into a service object
	 * 
	 * @param from
	 */
	public void read(InputStream from)
	throws IOException
	{
		BufferedInputStream buffered = new BufferedInputStream(from);
		InputStreamReader reader = new InputStreamReader(buffered,SOURCE_CHARSET);
		StringWriter writer = new StringWriter();

		int n;
		while ( (n = reader.read() ) > -1 )
		{
			writer.write(n);
		}
		this.source = writer.toString();
		this.parse();
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// implementatation
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * parse the input stream
	 * @throws BindException 
	 */
	private void parse()
	throws BindException
	{
		JsonBinder binder = new JsonBinder(this.source);
		this.info = binder.parse();

		this.service = this.parseService( this.info );
	}

	/**
	 * parse out a service
	 * 
	 * <p>
	 * exposed for junit testing
	 * 
	 * @return the service
	 * @throws ParseException 
	 */
	private Service parseService(Map<String,Object> o)
	{
		Service service = new Service();

		service.setName( this.helper.getString( info, "name" ) );
		service.setHostName( this.helper.getString( info, "host" ) );
		service.setDate( this.helper.getDate( info, "date", this.dateFormat ) );
		service.setStartDate( this.helper.getDate( info, "start", this.dateFormat ) );

		// I've never really used this, but still...
		service.setLegend( this.helper.getString( info, "legend" ) );
		
		this.service = service;

		List<Object> components = this.helper.getList( o, "components" );

		for ( Object c : components )
		{
			Component component = this.parseComponent( this.helper.asMap(c), service.getDate() );
			this.service.add(component);
		}

		return service;
	}

	/**
	 * parse out a component
	 * 
	 * <p>
	 * exposed for junit testing
	 * 
	 * @return the component
	 */
	private Component parseComponent(Map<String,Object> o, Date now)
	{
		Component component = new Component();

		component.setName( this.helper.getString( o, "name") );

		// I've really used these, but still...
		component.setLegend( this.helper.getString( o, "legend") );
		component.setLowerLimit( (int)this.helper.getLong( o, "lo") );
		component.setUpperLimit( (int)this.helper.getLong( o, "hi") );

		// parse out the period data
		
		LinkedList<Period> minutes = parsePeriods(o,
				"minute",
				new Period(now.getTime(),Period.ONE_MINUTE),
				now);
		component.setMinutes(minutes);

		LinkedList<Period> hours = parsePeriods(o,
				"hour",
				new Period(now.getTime(),Period.ONE_HOUR),
				now);
		component.setHours(hours);

		LinkedList<Period> days = parsePeriods(o,
				"day",
				new Period(now.getTime(),Period.ONE_DAY),
				now);
		component.setDays(days);

		return component;
	}

	/**
	 * @param o
	 * @param periodName TODO
	 * @param protoPeriod TODO
	 * @param now
	 * @param component
	 */
	private LinkedList<Period> parsePeriods(Map<String, Object> o,
			String periodName,
			Period protoPeriod,
			Date now)
	{
		LinkedList<Period> periods = new LinkedList<Period>();
			
		Map<String,Object> minutes = this.helper.getMap( o, periodName );
		
		List<Object> totals = this.helper.getList( minutes, "total" );
		List<Object> counts = this.helper.getList( minutes, "count" );
		List<Object> averages = this.helper.getList( minutes, "average" );
		List<Object> mins = this.helper.getList( minutes, "min" );
		List<Object> maxs = this.helper.getList( minutes, "max" );

		for ( int i = 0 ; i < totals.size() ; i++ )
		{
			Period p = protoPeriod.copy();
			p.setTotal( this.helper.asLong( totals.get(i) ) );
			p.setCount( this.helper.asLong( counts.get(i) ) );
			p.setMax( this.helper.asLong( maxs.get(i) ) );
			p.setMin( this.helper.asLong( mins.get(i) ) );
			p.setAverage( this.helper.asLong( averages.get(i) ) );
			
			periods.add(p);
		}
		
		return periods;
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @return the source
	 */
	public String getSource()
	{
		return this.source;
	}

	/**
	 * @return the service
	 */
	public Service getService()
	{
		return this.service;
	}
}
