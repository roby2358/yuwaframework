/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package org.yuwa.technology.livemetrics;

import org.yuwa.technology.livemetrics.model.Service;


/**
 * the interface clients will use to track metrics
 * 
 * @author royoung
 */
public interface ServiceTracker
{
	/**
	 * check all the periods to see if they're still current; if not, roll them
	 * over
	 */
	void roll();

	/**
	 * add a value for the given component
	 * 
	 * @param componentName
	 * @param value
	 */
	void add(String componentName, long value);

	/**
	 * add a value for the given component, tallied by second
	 * 
	 * <p>
	 * the accumulated values are stored when the next second is encountered
	 * 
	 * @param componentName
	 * @param value
	 */
	void addToSecond(String componentName, long value);

	/**
	 * get the underlying service object
	 * 
	 * @return the service
	 */
	Service getService();


	/**
	 * get the history associated with the service
	 * 
	 * <p>
	 * May be the service itself, or may be a holder object that only has
	 * history values
	 * 
	 * @return the service history
	 */
	Service getHistory();
	
	/**
	 * the tracker can also act as a factory for timer objects 
	 * 
	 * @param componentName
	 * @return a new timer object for this tracker
	 */
	Timer startTimer(String componentName);


	/**
	 * tell this component to start histogram tracking for the named component
	 * but just report percentiles
	 * 
	 * @return the component
	 */
	ServiceTracker startPercentiles(String componentName, long min, long max, int buckets);


	/**
	 * tell this component to start histogram tracking for the named component
	 * and report percentile and histogram data, too
	 * 
	 * <p>
	 * NOTE: turning this on for a component will drastically increase the
	 * amount of data that it tracks, especially if you use a lot of buckets.
	 * This will have an impact on the memory footprint, and the time needed to
	 * render and transmit the component. You may even want to split the
	 * component in two, one with histogram and one without.
	 * 
	 * @return the component
	 */
	ServiceTracker startHistogram(String componentName, long min, long max, int buckets);

	
	/**
	 * If the implementation of the service tracker is doing something that needs
	 * to be stopped, this method will stop it.
	 */
	void stop();
	
	
    /**
	 * a do-nothing implementation to use as a placeholder
	 */
	ServiceTracker NO_TRACKER = new ServiceTracker()
	{
		public void add(String componentName, long value) { /* nop */ }

		public void addToSecond(String componentName, long value) { /* nop */ }

		public Service getHistory()
		{
			throw new UnsupportedOperationException("not implemented");	
		}

		public Service getService()
		{
			throw new UnsupportedOperationException("not implemented");	
		}

        public void roll() { /* nop */ }

		public Timer startTimer(String componentName)
		{
			return Timer.NO_TIMER;	
		}

		public ServiceTracker startHistogram(String componentName, long min,
				long max, int buckets)
		{
			return this;
		}

		public ServiceTracker startPercentiles(String componentName, long min,
				long max, int buckets)
		{
			return this;
		}

        public void stop() { /* nop */ }
	};
}