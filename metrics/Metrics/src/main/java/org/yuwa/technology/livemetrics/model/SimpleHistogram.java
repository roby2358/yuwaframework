/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Oct 27, 2008 $
 */
package org.yuwa.technology.livemetrics.model;

import org.apache.log4j.Logger;


/**
 * This is a simple implementation of Histogram that just holds the values in a big array.
 * 
 * <p>
 * Oct 27, 2008
 *
 * @author royoung
 */
public class SimpleHistogram
implements Histogram
{
	/** our logger */
	protected Logger log = Logger.getLogger(getClass());
	
	/** default serial version UID */
	private static final long serialVersionUID = 1L;
	
	private long min;
	private long max;
	private long[] values = new long[0];
	private float[] percentiles = DEFAULT_PERCENTILES;

	/**
	 * @param visitor
	 */
	public void accept(MetricVisitor visitor)
	{
		visitor.visit(this);
	}
	
	/**
	 * @return a new histogram with the same properties to start over with
	 */
	public SimpleHistogram startFresh()
	{
		SimpleHistogram that = new SimpleHistogram();
		that.max = this.max;
		that.min = this.min;
		that.percentiles = this.percentiles;
		that.values = new long[this.values.length];
		return that;
	}
	
	/**
	 * add a value to the right bucket
	 */
	public void add(long value)
	{
		if ( value <= this.min )
		{
			this.values[0]++;
		}
		else if ( value >= this.max )
		{
			this.values[ this.values.length - 1 ]++;
		}
		else
		{
			long adjusted = ( value - this.min ) * this.values.length / (this.max - this.min);

			int bucket = this.values.length - 1;
			if ( adjusted < 0 )
			{
				bucket = 0;
			}
			else if ( adjusted < bucket )
			{
				bucket = (int)adjusted;
			}

			this.values[bucket]++;
		}
	}
	
	/**
	 * @param bar
	 * @return the end value for a given bar
	 */
	public long getEndValue(int bar)
	{
		return ( ( bar + 1 ) * ( this.max - this.min ) ) / this.values.length + this.min;
	}

	/**
	 * approximates the percentile scores using the histogram values
	 * 
	 * @return a list of percentiles 10-90, 99.9 and 99.99
	 */
	public long[] approximatePercentileValues()
	{
		long[] p = new long[this.percentiles.length];

		long total = 0;
		for ( long l : this.values )
		{
			total += l;
		}

		log.debug("~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-");
		
		int current = 0;
		int bucket = 0;
		long soFar = 0;
		for ( long l : this.values )
		{
			soFar += l;
			
			while ( ( current < this.percentiles.length )
					&& ( soFar >= ( total * this.percentiles[current] ) ) )
			{
				if ( log.isDebugEnabled() )
				{
				log.debug( "sofar "
						+ current
						+ " < "
						+ this.percentiles.length
						+ " && "
						+ bucket
						+ " = "
						+ soFar
						+ " >= "
						+ ( total * this.percentiles[current] )
						+ " -> "
						+ this.getEndValue(bucket) );
				}
				
				p[current] = this.getEndValue(bucket);
				current++;
			}
			
			bucket++;
		}
			
		return p;
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @return the min
	 */
	public long getMin()
	{
		return this.min;
	}
	
	/**
	 * @param min the min to set
	 */
	public SimpleHistogram setMin(long min)
	{
		this.min = min;
		return this;
	}
	
	/**
	 * @return the max
	 */
	public long getMax()
	{
		return this.max;
	}
	
	/**
	 * @param max the max to set
	 */
	public SimpleHistogram setMax(long max)
	{
		this.max = max;
		return this;
	}
	
	/**
	 * @return the buckets
	 */
	public int getBuckets()
	{
		return this.values.length;
	}
	
	/**
	 * @param bars the buckets to set
	 */
	public SimpleHistogram setBuckets(int bars)
	{
		this.values = new long[bars];
		return this;
	}
	
	/**
	 * @return the values
	 */
	public long[] getValues()
	{
		return this.values;
	}

	/**
	 * @return the percentiles
	 */
	public float[] getPercentiles()
	{
		return this.percentiles;
	}

	/**
	 * @param percentiles the percentiles to set
	 */
	public SimpleHistogram setPercentiles(float[] percentiles)
	{
		this.percentiles = percentiles;
		return this;
	}
	
}
