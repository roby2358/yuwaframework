/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Aug 25, 2008 $
 */
package org.yuwa.technology.livemetrics;

import java.io.UnsupportedEncodingException;

import org.yuwa.technology.livemetrics.json.JsonBuilder;
import org.yuwa.technology.livemetrics.metrics.JsonMetricsVisitor;


/**
 * the metrics reporter encapsulates the steps of spinning off a visitor and
 * serializing the history of a given metrics object
 * 
 * <p>
 * Mar 24, 2008
 * 
 * @author royoung
 */
public class MetricsReporter
{
	/** set this to true for nicely formatted JSON */
	private boolean includeNewlines = true;

	/** a prototype for spinning off copies of the builder */
	private JsonMetricsVisitor mProtoBuilder = new JsonMetricsVisitor();

	/**
	 * default constructor
	 * 
	 */
	public MetricsReporter()
	{
		this.init();
	}


	/**
	 * optionally call init if you've changed some properties
	 */
	public void init()
	{
		JsonBuilder jsonBuilder = new JsonBuilder();
		jsonBuilder.setNewlines(includeNewlines);

		this.mProtoBuilder.setJsonBuilder(jsonBuilder);
	}


	/**
	 * take a tracker and build history for it
	 * 
	 * @param tracker
	 * @return
	 */
	public byte[] buildHistory(ServiceTracker tracker)
	{
		JsonMetricsVisitor builder = this.mProtoBuilder.copy();
		tracker.roll();
		tracker.getHistory().accept(builder);

		byte[] history;

		try
		{
			history = builder.toString().getBytes("UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			history = new byte[0];
		}

		return history;
	}


	/**
	 * @return the includdeNewlines
	 */
	public boolean isIncluddeNewlines()
	{
		return this.includeNewlines;
	}


	/**
	 * @param includdeNewlines the includdeNewlines to set
	 */
	public void setIncludeNewlines(boolean includeNewlines)
	{
		this.includeNewlines = includeNewlines;
	}

}