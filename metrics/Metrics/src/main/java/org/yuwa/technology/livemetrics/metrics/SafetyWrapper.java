/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */


package org.yuwa.technology.livemetrics.metrics;

import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.ServiceTracker;
import org.yuwa.technology.livemetrics.Timer;
import org.yuwa.technology.livemetrics.model.Service;


/**
 * this class acts as a wrapper for the public methods of a service to provide
 * exception-safety. It suppresses all errors from talking to a service object.
 * 
 * <p>
 * At runtime, the objects interact like this:
 * 
 * <p>
 * ServiceTracker (interface) -> SafetyWrapper -> Service
 * 
 * <p>
 * The client should never be aware of the SafetyWrapper class, since it is
 * just a safety wrapper around Service
 * 
 * <p>
 * Note that as of 1.7.0, events are synchronized at the Component level,
 * so there is no longer synchronization in this class.
 * 
 * @author royoung
 */
public class SafetyWrapper
implements ServiceTracker
{
	protected Logger log = Logger.getLogger(getClass());

	private Service service = new Service();

	private Timer protoTimer = Timer.NO_TIMER;

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// delegate to Service
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @see org.yuwa.technology.livemetrics.ServiceTracker#startHistogram(java.lang.String, long, long, int)
	 */
	public ServiceTracker startHistogram(String componentName,
			long min,
			long max,
			int buckets)
	{
		try
		{
			this.service.startHistogram(componentName, min, max, buckets);
		}
		catch (Throwable t)
		{
			// suppress errors
			log.warn("startHistogram failed for ["+componentName+"] ["+min+"] ["+max+"] ["+buckets+"]",t);
		}

		return this;
	}

	/**
	 * @see org.yuwa.technology.livemetrics.ServiceTracker#startPercentiles(java.lang.String, long, long, int)
	 */
	public ServiceTracker startPercentiles(String componentName,
			long min,
			long max,
			int buckets)
	{
		try
		{
			this.service.startPercentiles(componentName, min, max, buckets);
		}
		catch (Throwable t)
		{
			// suppress errors
			log.warn("startPercentiles failed for ["+componentName+"] ["+min+"] ["+max+"] ["+buckets+"]",t);
		}

		return this;
	}

	/**
	 * check all the periods to see if they're still current; if not, roll them
	 * over
	 */
	public void roll()
	{
		try
		{
			service.roll();
		}
		catch (Throwable t)
		{
			// suppress errors
			log.info("roll failed : " + t.getClass().getName() + " " + t.getMessage() );
		}
	}

	/**
	 * an exception-safe wrapper
	 */
	public void add(String componentName, long value)
	{
		try
		{
			service.add(componentName, value);
		}
		catch (Throwable t)
		{
			// suppress errors
			log.info("add failed [" + componentName + "] [" + value + "]: " + t.getClass().getName() + " " + t.getMessage() );
		}
	}

	/**
	 * an exception-safe wrapper
	 */
	public void addToSecond(String componentName, long value)
	{
		try
		{
			service.addToSecond(componentName, value);
		}
		catch (Throwable t)
		{
			// suppress errors
			log.info("add failed [" + componentName + "] [" + value + "]: " + t.getClass().getName() + " " + t.getMessage() );
		}
	}

    /**
     * an exception-safe wrapper
     */
    public void stop()
    {
        try
        {
            service.stop();
        }
        catch (Throwable t)
        {
            // suppress errors
            log.info("stop failed " + t.getMessage() );
        }
    }

	/**
	 * create a new timer based on our prototype
	 * 
	 * @see org.yuwa.technology.livemetrics.ServiceTracker#startTimer(java.lang.String)
	 */
	public Timer startTimer(String componentName)
	{
		return this.protoTimer.create(this, componentName);
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	public Service getService()
	{
		return service;
	}

	/**
	 * @param service the service to set
	 */
	public void setService(Service service)
	{
		this.service = service;
	}

	/**
	 * This service holds its own history
	 */
	public Service getHistory()
	{
		return this.service;
	}

	/**
	 * set the prototype for timers to create
	 * 
	 * @param protoTimer the protoTimer to set
	 */
	public void setProtoTimer(Timer protoTimer)
	{
		this.protoTimer = protoTimer;
	}

}
