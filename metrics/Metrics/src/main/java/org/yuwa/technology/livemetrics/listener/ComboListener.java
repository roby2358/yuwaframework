/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jan 29, 2008 $
 */
package org.yuwa.technology.livemetrics.listener;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.microserver.MicroServer;
import org.yuwa.technology.livemetrics.microserver.MicroServerHandler;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpHandler;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpRequest;
import org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest;


/**
 * The combo listener rolls up a number of listeners, and actually pays
 * attention to the http request to route it appropriately.
 * 
 * <p>
 * The logic is to take the last element of the input path, and match that
 * against a map of handlers. Pretty simple.
 * 
 * <p>
 * Jan 29, 2008
 * 
 * @author royoung
 */
public class ComboListener
implements MicroServerHandler
{
	/** our logger */
	private Logger log = Logger.getLogger(getClass());
		
	/** the microserver to run */
	private MicroServer server = new MicroServer();
	
	/** the map of handlers */
	private Map<String,MicroServerHttpHandler> handlersByName = new HashMap<String,MicroServerHttpHandler>();
	
	/** pass unknown requests to this one */
	private MicroServerHttpHandler unknownHandler = MicroServerHttpHandler.NO_HANDLER;

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// implementation
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * start the listener running
	 *
	 */
	public void start()
	{
		this.server.setHandler(this);
		this.server.start();
	}

	/**
	 * stop the listener from running
	 *
	 */
	public void stop()
	{
		this.server.stop();
	}
	
	/**
	 * parse the http request and decide who to send it to
	 * 
	 * @see org.yuwa.technology.livemetrics.microserver.MicroServerHandler#handle(java.net.Socket)
	 */
	public void handle(Socket socket)
	throws IOException
	{
		if ( log.isDebugEnabled() )
			log.debug("begin handle");
		
		// parse the request
		MicroServerHttpRequest request = new SimpleMicroServerHttpRequest(socket);
		
		// hand it to the right handler
		if ( request.getPath().size() == 0 )
		{
			log.warn("huh? no path " + request.getFullPath() );
			this.unknownHandler.handle(socket, request);
		}
		else
		{
			int lastIndex = request.getPath().size() - 1;
			String name = request.getPath().get(lastIndex);

			if ( log.isDebugEnabled() )
				log.debug("use [" + request.getFullPath() + "] ["  + lastIndex + "] [" + name + "]");
			
			MicroServerHttpHandler handler = this.handlersByName.get(name);
			if (handler == null ) handler = this.unknownHandler;
			handler.handle(socket, request);
		}
	}
	
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	
	/**
	 * set the port we'll listen to
	 */
	public void setPort(int port)
	{
	    this.server.setPort(port);
	}
	
	/**
	 * add a new handler to our map, by name
	 * 
	 * @param name
	 * @param handler
	 */
	public void add(String name, MicroServerHttpHandler handler)
	{
		this.handlersByName.put(name,handler);
	}

	/**
	 * set the map of handlers by name
	 * 
	 * @param handlersByName the handlersByName to set
	 */
	public void setHandlersByName(Map<String, MicroServerHttpHandler> handlersByName)
	{
		this.handlersByName = handlersByName;
	}

	/**
	 * @param unknownHandler the unknownHandler to set
	 */
	public void setUnknownHandler(MicroServerHttpHandler unknownHandler)
	{
		this.unknownHandler = unknownHandler;
	}

}
