/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Sep 24, 2007 $
 */
package org.yuwa.technology.livemetrics;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.json.JsonBuilder;
import org.yuwa.technology.livemetrics.listener.MetricsListener;
import org.yuwa.technology.livemetrics.metrics.JsonMetricsVisitor;
import org.yuwa.technology.livemetrics.metrics.SafetyWrapper;


/**
 * This is a convenience class because while all this could be specified in XML
 * config, it's a hassle
 * 
 * <p>
 * Sep 24, 2007
 * 
 * @author royoung
 */
public class ServiceTrackerBuilder
{
	private static final String HISTOGRAM_REGEXP = "\\s*([^\\s]+)\\s*:\\s*([0-9]+)\\s*-\\s*([0-9]+)\\s*/\\s*([0-9]+)\\s*";

	protected Logger log = Logger.getLogger(this.getClass());
	
	private SafetyWrapper tracker = new SafetyWrapper();
	private MetricsListener listener = new MetricsListener();
	private JsonMetricsVisitor visitor = new JsonMetricsVisitor();
	private JsonBuilder builder = new JsonBuilder();
	private boolean useListener = false;
	private Timer protoTimer = new MicrosecondMetricTimer(this.tracker,"elapsed");
	private List<String> histogramComponents = new ArrayList<String>();
	private List<String> percentileComponents = new ArrayList<String>();
	
	/**
	 * return everything we've built
	 * @return the tracker we built
	 */
	public ServiceTracker build()
	{
	    this.tracker.setProtoTimer(this.protoTimer);
	    
		// make sure we have a host name
		if ( this.tracker.getService().getHostName() == null )
			this.tryToFindHostName();
		
		this.visitor.setJsonBuilder(this.builder);
		
		if ( this.useListener )
		{
	        // build & start the listener now... just run until the end of time
			this.listener.setProtoBuilder(this.visitor);
			this.listener.setMetrics(this.tracker);
			this.listener.start();
			
			this.tracker.getService().setListener(this.listener);
		}
		
		for ( String config : this.histogramComponents )
		{
			this.parseHistogramConfig(config,true);
		}
		
		for ( String config : this.percentileComponents )
		{
			this.parseHistogramConfig(config,false);
		}
		
		return this.tracker;
	}
	

	/**
	 * @param s
	 * @return the pieces of the config line
	 */
	private void parseHistogramConfig(String s, boolean histogram)
	{
		try
		{
			Matcher m = Pattern.compile(HISTOGRAM_REGEXP).matcher(s);
			if ( ! m.matches() )
			{
				log.warn("bad histogram/percentile string [" + s + "] should be " + HISTOGRAM_REGEXP.replaceAll("\\\\s\\*", " "));
			}
			else
			{
				String component = m.group(1);
				long min = Long.parseLong(m.group(2));
				long max = Long.parseLong(m.group(3));
				int buckets = Integer.parseInt(m.group(4));

				if ( histogram )
					this.tracker.startHistogram(component, min, max, buckets);
				else
					this.tracker.startPercentiles(component, min, max, buckets);
			}
		}
		catch (Exception e)
		{
			log.warn("error parsing histogram/percentile string [" + s + "]  should be " + HISTOGRAM_REGEXP.replaceAll("\\\\s\\*", " ") + " (" + e.getClass().getName() + " : " + e.getMessage() + ")" );
		}
	}

	/**
	 * 
	 */
	private void tryToFindHostName()
	{
		try
		{
			this.tracker.getService().setHostName(InetAddress.getLocalHost().getHostName());
		}
		catch (UnknownHostException uhe)
		{
			try
			{
				this.tracker.getService().setHostName(InetAddress.getLocalHost().getHostAddress());
			}
			catch (UnknownHostException uhe2)
			{
				this.tracker.getService().setHostName("unknown");
			}
		}
	}
	
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// threading delegates
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	
	/**
	 * @param threadFactory
	 */
	public void setThreadFactory(ThreadFactory threadFactory)
	{
		this.listener.setThreadFactory(threadFactory);
	}
	
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors / delegates
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * set the display name of the service
	 */
	public void setName(String name)
	{
		this.tracker.getService().setName(name);
	}

	/**
	 * set the display name of the host -- if not specified, the tracker will
	 * try to determine the host name using Java classes.
	 */
	public void setHostName(String name)
	{
		this.tracker.getService().setHostName(name);
	}
	
	/**
	 * for adding names up components up front, rather than adding them on the
	 * fly
	 * 
	 * @param name
	 */
	public void setComponentList(List<String> names)
	{
		for ( String name : names )
			this.tracker.getService().getComponent(name);
	}

	/**
	 * set the legend URL for the service
	 * 
	 * @param url
	 */
	public void setLegend(String url)
	{
		this.tracker.getService().setLegend(url);
	}
	

	/**
	 * set the legend URL for a component
	 * 
	 * @param componentName
	 * @param url
	 */
	public void setLegend(String componentName, String url)
	{
		this.tracker.getService().getComponent(componentName).setLegend(url);
	}
	
	/**
	 * for systems that don't configure log4j already, set this to true to
	 * initialize log4j from log4j.properties in the root of the classpath
	 * 
	 * @param initLog4j
	 */
	public void setInitLog4j(boolean initLog4j)
	{
		this.listener.setInitLog4j(initLog4j);
	}

	/**
	 * set the port for the listenter to listen to. Defaults to 9199
	 * 
	 * @param port
	 */
	public void setPort(int port)
	{
		this.listener.setPort(port);
	}

	/**
	 * set this to true to add newlines to the JSON for readability
	 * 
	 * @param newlines
	 */
	public void setNewlines(boolean newlines)
	{
		this.builder.setNewlines(newlines);
	}

	/**
	 * set this to true to create and start a listener; false to skip it.
	 * 
	 * <p>
	 * defaults to true
	 * 
	 * @param useListener the useListener to set
	 */
	public void setUseListener(boolean useListener)
	{
		this.useListener = useListener;
	}

    /**
     * @return the listener, so the client can start and stop it
     */
    public MetricsListener getListener()
    {
        return this.listener;
    }
	
	
	/**
	 * returns the tracker only, without building anything
	 * 
	 * @return the tracker that we've built
	 */
	public ServiceTracker getTracker()
	{
		return this.tracker;
	}

	/**
	 * get the visitor object that we built; can be used as a prototype for
	 * serializing stats in an app
	 * 
	 * @return the visitor
	 */
	public JsonMetricsVisitor getVisitor()
	{
		return this.visitor;
	}

	/**
	 * set the prototype timer to use
	 * @param protoTimer the protoTimer to set
	 */
	public void setProtoTimer(Timer protoTimer)
	{
		this.protoTimer = protoTimer;
	}


	/**
	 * set the components we want to have histograms
	 * 
	 * <p>
	 * format is component : min - max / buckets
	 * 
	 * @param histogramComponents
	 */
	public void setHistogramComponents(List<String> histogramComponents)
	{
		this.histogramComponents = histogramComponents;
	}


	/**
	 * set the components we want to have histograms
	 * 
	 * <p>
	 * format is component : min - max / buckets
	 *
	 * @param percentileComponents
	 */
	public void setPercentileComponents(List<String> percentileComponents)
	{
		this.percentileComponents = percentileComponents;
	}
	
}
