/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Sep 24, 2007 $
 */
package org.yuwa.technology.livemetrics.listener;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.ServiceTracker;
import org.yuwa.technology.livemetrics.metrics.JsonMetricsVisitor;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpHandler;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpRequest;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpResponse;
import org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpResponse;


/**
 * the metrics listener listens on a port for HTTP requests, and bundles up
 * metrics information in response
 * 
 * <p>
 * The HTTP server logic is extremely bare bones. The listener just ignores the
 * input request and any request parameters, but formats the output as an HTTP
 * response.
 * 
 * @author royoung
 * 
 */
public class MetricsHandler
implements MicroServerHttpHandler
{
	/** our logger */
	protected Logger log = Logger.getLogger(getClass());

	/** default encoding */
	private static final String CHARACTER_ENCODING = "UTF-8";

	/** a prototype for spinning off copies of the builder */
	private JsonMetricsVisitor protoBuilder = new JsonMetricsVisitor();
	
	/** the tracker to get information from */
	private ServiceTracker tracker;
	

	/**
	 * handle the request that came in
	 * 
	 * @param socket
	 * @throws IOException
	 */
	public void handle(Socket socket, MicroServerHttpRequest request)
	throws IOException
	{
		if ( log.isDebugEnabled() ) log.debug("got request from " + socket.getRemoteSocketAddress());
		
		if ( log.isDebugEnabled() ) log.debug("build response");
		
		JsonMetricsVisitor builder = this.protoBuilder.copy();

		// TODO: ignore the request for now
		// TODO: grab a component name from the request (i.e. as path) and limit
		// the builder to that
		
		long nanos = System.nanoTime();

		byte[] bytes = this.buildHistory(builder);
		
		if ( log.isInfoEnabled() ) log.info( "build time: " + ( System.nanoTime() - nanos ) );

		MicroServerHttpResponse response = new SimpleMicroServerHttpResponse();
		response.setEncoding(CHARACTER_ENCODING);
		response.setBytes(bytes);
		
		response.send(socket);
	}

	/**
	 * This does the work of taking the tracking object and building the JSON
	 * representation
	 * 
	 * @param builder
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private byte[] buildHistory(JsonMetricsVisitor builder)
			throws UnsupportedEncodingException
	{
		this.tracker.roll();
		
		synchronized( this.tracker.getHistory() )
		{
			this.tracker.getHistory().accept(builder);
		}
		
		final byte[] bytes = builder.toString().getBytes(CHARACTER_ENCODING);
		
		return bytes;
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @param builder the builder to set
	 */
	public void setProtoBuilder(JsonMetricsVisitor builder)
	{
		this.protoBuilder = builder;
	}

	/**
	 * @param the tracker to set
	 */
	public void setMetrics(ServiceTracker t)
	{
		this.tracker = t;
	}

}
