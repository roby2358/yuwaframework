/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Apr 8, 2008 $
 */
package org.yuwa.technology.livemetrics.util;

/**
 * The system clock Java uses for System.currentTimeMillis may have a
 * granularity of 10-15 milliseconds, making it a poor timer for measuring
 * elapsed time. However, because of the implementation of Java threading, you
 * can force the granularity down to 1 millisecond by starting a thread and
 * putting it to sleep with a granularity that is not a multiple of the system
 * clock granularity. This class does that.
 * 
 * <p>
 * An object of this type just contains a single thread which goes to sleep,
 * well, forever. Or for almost as long as is possible. However, in doing so it
 * forces the Java system clock to run with a granularity of 1 ms.
 * 
 * <p>
 * Instantiate this object at the start of your application somewhere, and hold
 * it in your servlet or whatnot. You don't need to reference it ever.
 * 
 * <p>
 * Apr 8, 2008
 * 
 * @author royoung
 */
public class TimerGranularityFix
implements Runnable
{
	private Thread myThread = new Thread(this);

	public TimerGranularityFix()
	{
		myThread.start();
	}

	public void run()
	{
		try
		{
			Thread.sleep(Integer.MAX_VALUE);
		}
		catch (InterruptedException ie)
		{
			// fall out
			System.err.println("INTERRUPTED");
		}
	}
}
