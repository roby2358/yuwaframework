/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
 */

package org.yuwa.technology.livemetrics.json;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


/**
 * A utility class for converting Java object structures to JSON for
 * transmitting to an AJAX-driven web page
 * 
 * <p>
 * for information on JSON, see http://www.json.org/
 * 
 * <p>
 * This class is intended to bundle up a single object in the form of name/value
 * or name/list pairs. Each value is just a string, created with the object's
 * toString method.
 * 
 * <p>
 * To have finer control of the value, the client can generate it's own string
 * and put that in with the name. Also, if the value is a JsonBuilder itself,
 * then its ToString method will encode the data as a string and use that for a
 * value. So to get nested JSON heirarchies, just use nested instances of
 * JsonBuilders.
 * 
 * <p>
 * Aug 13, 2007
 * 
 * @author royoung
 */
@SuppressWarnings("unchecked")
public class JsonBuilder
{
	/** our logger */
//	protected Logger log = Logger.getLogger(getClass());

	/** a list of names -- can't be a hash, because we want to preserve order (tho I'm not sure that matters) */
	private LinkedHashMap<String,Object> objectsByName = new LinkedHashMap<String,Object>();

	/** a default array of types we consider to be scalar */
	private String[] defaultScalars = new String[] {
			"java.lang.String",
			"java.lang.Integer",
			"java.lang.Boolean",
			"java.lang.Long",
			"java.lang.Double",
			"java.lang.Float",
			"int",
			"boolean",
			"long",
			"double",
			"float"
	};

	/** a set of types that we consider scalar */
	private Set<String> scalars = new HashSet<String>(Arrays.asList(defaultScalars));

	/** a switch to include public fields */
	private boolean includeFields = false;

	/** add newlines to the output for readability? */
	private boolean newlines = true;

	/** include null values? */
	private boolean includeNulls = false;

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// constructors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * default, no-arg constructor
	 */
	public JsonBuilder()
	{
	}


	/**
	 * for convenience, a builder that encodes the public properties of o
	 * 
	 * @param o
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public JsonBuilder(Object o)
	throws IllegalAccessException, InvocationTargetException
	{
		this.put(o);
	}

	/**
	 * prototype method treats this as the original and returns a copy with the
	 * same initial values
	 * 
	 * @return a copy of this
	 */
	public JsonBuilder copy()
	{
		JsonBuilder that = new JsonBuilder();
		that.includeFields = this.includeFields;
		that.newlines = this.newlines;
		return that;
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// implementation
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * put a name/value pair
	 *
	 * @param name
	 * @param value
	 */
	public void put(String name, Object value)
	{
		this.objectsByName.put(name,value);
	}

	/**
	 * add the given object to a named list
	 * 
	 * @param name
	 * @param o
	 */
	public void add(String name, Object o)
	{
		Object old = this.objectsByName.get(name);
		if ( old != null && old instanceof List )
		{
			((List<Object>) old).add(o);
		}
		else if ( old == null )
		{
			List<Object> list = new ArrayList<Object>();
			list.add(o);
			this.objectsByName.put(name,list);
		}
		else {
			List<Object> list = new ArrayList<Object>();
			list.add(old);
			list.add(o);
			this.objectsByName.put(name,list);
		}
	}

	/**
	 * add an empty list; not required, but handy if the list may be empty
	 * 
	 * @param name
	 */
	public void addEmptyList(String name)
	{
		Object oldList = this.objectsByName.get(name);
		if ( oldList == null )
		{
			List<Object> newList = new ArrayList<Object>();
			this.objectsByName.put(name,newList);
		}
	}


	/**
	 * uses reflection to grab properties from the object and add them
	 * 
	 * @param o
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public void put(Object o)
	throws IllegalAccessException, InvocationTargetException
	{
		WorkingObjectObject wo = new WorkingObjectObject(o);

		Set<String> properties = wo.getClassMaps().getTypesByPropertyName().keySet();
		TreeSet<String> sortedProperties = new TreeSet<String>(properties);

		for ( String property : sortedProperties )
		{
			String type = wo.getClassMaps().getTypesByPropertyName().get(property).getName();
			Object value = wo.get(property);

			// TODO: allow maps as well, treat as Object
			// TODO: flag to traverse object structure?

			if ( value == null && ! this.includeNulls )
			{
				// nop
			}
			else if ( ! this.includeFields
					&& ! wo.getClassMaps().getGettersByName().containsKey(property) )
			{
				// nop
			}
			else if ( value instanceof List )
			{
				for ( Object oo : (List<Object>)value )
				{
					// TODO: won't handle lists of objects
					this.add(property, oo.toString());
				}
			}
//			TODO: we know it's an array, but how do we iterate over it generically?
//			else if ( value.getClass().isArray() )
//			{
//			Array a = new Array(value);

//			for ( Object op : Arrays.asList(value) )
//			{
//			// TODO: won't handle arrays of objects
//			log.debug(p + " ... " + op.toString());
//			this.add(p, op.toString());
//			}
//			}
			else if ( ! this.scalars.contains( type ) )
			{
				// TODO: add traverse flag?
				// nop
			}
			else
			{
				this.put( property, wo.get(property) );
			}

		}
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// render as JSON string
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * render this object as a JSON string
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		StringBuilder builder = new StringBuilder( (this.newlines) ? "\n{" : "{" );

		int count = 0;
		for ( String name : this.objectsByName.keySet() )
		{
			Object value = this.objectsByName.get(name);

//			if ( value instanceof Map )
//			{
//			// TODO: treat a Map as an object
//			}
//			else
			if ( value instanceof List )
			{
				this.buildArray(builder,name, (List<Object>)value,count>0);
			}
			else
			{
				this.buildNameValue(builder,name,value,count>0);
			}
			count++;
		}

		builder.append("}");

		return builder.toString();
	}

	/**
	 * this method for creating JSON that is intended to be embeded in a page at
	 * load time. Apostrophes and quotes need extra layers of escaping so
	 * javascript doesn't get confused.
	 * 
	 * <p>
	 * Apostrophes and alreadhy-escaped quotes are rendered as \\\' and \\\".
	 * It's possible to do this using JSTL tags, but it's pretty hideous. This
	 * method does that work in advance.
	 * 
	 * <p>
	 * The return value is an "eval" statement that can be embedded directly
	 * into javascript, such as var s = <c:out value='${json.embedded}' escapeXml='false' />
	 * 
	 * @return a json string encoded so it can be embeded directly in a page's
	 *         javascript
	 */
	public String getEmbedded()
	{
		StringBuilder eval = new StringBuilder("eval('(")
		.append( this.toString()
				.replaceAll("'","\\\\\\\\\\\\'")
				.replaceAll("\\\\\"","\\\\\\\\\\\\\"") )
				.append(")')");

		return eval.toString();
	}

	/**
	 * build the JSON for a name/value pair
	 * @param builder
	 * @param name
	 * @param value
	 */
	private void buildNameValue(StringBuilder builder, String name, Object value, boolean addComma)
	{
		if ( addComma ) builder.append( this.comma() );
		builder.append(name);
		builder.append(":");
		this.buildValue(builder, value);
	}


	/**
	 * @param builder
	 * @param value
	 */
	private void buildValue(StringBuilder builder, Object value) {

		if ( value == null )
		{
			builder.append("null");
		}
		else if ( ! this.scalars.contains(value.getClass().getName()) )
		{
			builder.append( value.toString() );
		}
		else
		{
			builder.append("\"");
			builder.append(value.toString().replaceAll("\"","\\\\\""));
			builder.append("\"");
		}
	}


	/**
	 * build the JSON for an array of items
	 * 
	 * @param builder
	 * @param name
	 * @param value
	 */
	private void buildArray(StringBuilder builder, String name, List<Object> value, boolean addComma)
	{
		if ( addComma ) builder.append( this.comma());

		builder.append(name);
		builder.append(":[");
		for ( int i = 0 ; i < value.size() ; i++ )
		{
			if ( i > 0 ) builder.append(",");

			Object object = value.get(i);

			// TODO: awrk... lists of lists

			this.buildValue(builder, object);
		}
		builder.append("]");
	}


	/**
	 * @return
	 */
	private String comma() {

		return (this.newlines) ? ",\n" : ",";
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


	/**
	 * @return the scalars
	 */
	public Set<String> getScalars() { return this.scalars; }

	/**
	 * @param includeFields the includeFields to set
	 */
	public JsonBuilder setIncludeFields(boolean includeFields)
	{
		this.includeFields = includeFields;
		return this;
	}

	/**
	 * @param newlines the newlines to set
	 */
	public JsonBuilder setNewlines(boolean newlines)
	{
		this.newlines = newlines;
		return this;
	}


	/**
	 * true if null values should be included
	 * 
	 * @param includeNulls the includeNulls to set
	 */
	public JsonBuilder setIncludeNulls(boolean includeNulls)
	{
		this.includeNulls = includeNulls;
		return this;
	}

}
