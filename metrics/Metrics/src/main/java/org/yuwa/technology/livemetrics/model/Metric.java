/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 */

package org.yuwa.technology.livemetrics.model;

import java.io.Serializable;

/**
 * The metric interface ties together all the classes in the livemetrics model
 * so that a MetricsVisitor can visit them
 * 
 * <p>
 * Oct 15, 2007
 * 
 * @author royoung
 */
public interface Metric
extends Serializable
{
	/**
	 * take a metric visitor and process ourselves with it
	 * 
	 * @param visitor
	 */
	void accept(MetricVisitor visitor);

	long ONE_SECOND = 1000L;
	long ONE_MINUTE = 60L * 1000L;
	long ONE_HOUR = 60L * ONE_MINUTE;
	long ONE_DAY = 24L * ONE_HOUR;
}
