/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Mar 17, 2008 $
 */
package org.yuwa.technology.livemetrics;



/**
 * Measure the elapsed time in microseconds, using System.nanoTime.
 * 
 * <p>
 * Note that nanoTime is a much slower operation than currentTimeMillis, so only
 * use this class if you really need granularity down to microseconds.
 * 
 * <p>
 * Mar 17, 2008
 * 
 * @author royoung
 */
public class MicrosecondMetricTimer
extends AbstractTimer
{
	/**
	 * @param tracker
	 * @param name
	 */
	public MicrosecondMetricTimer(ServiceTracker tracker, String name)
	{
		super(tracker, name);
	}

	/**
	 * factory method allows timers to act as a prototype object
	 * 
	 * @param tracker
	 * @param name
	 * @return
	 */
	public Timer create(ServiceTracker tracker, String name)
	{
		return new MicrosecondMetricTimer(tracker,name);
	}

	/**
	 * use currentTimeMillis to get the time, with granularity of 1 ms
	 * 
	 * @see org.yuwa.technology.livemetrics.AbstractTimer#getTime()
	 */
	@Override
	public long getTime()
	{
		return System.nanoTime() / 1000L;
	}
	
}
