/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Oct 27, 2008 $
 */
package org.yuwa.technology.livemetrics.model;



/**
 * 
 * <p>
 * Oct 27, 2008
 *
 * @author royoung
 */
public interface Histogram
extends Metric
{
	/**
	 * the default percentile ranks to track
	 */
	float[] DEFAULT_PERCENTILES = new float[] { 0.10f, 0.20f, 0.30f, 0.40f, 0.50f, 0.60f, 0.70f, 0.80f, 0.90f, 0.99f, 0.999f, 0.9999f };
	
	/**
	 * @return a new histogram with the same properties to start over with
	 */
	Histogram startFresh();
	
	/**
	 * add a value to the right bucket
	 */
	void add(long value);
	
	/**
	 * @param bar
	 * @return the end value for a given bar
	 */
	long getEndValue(int bar);

	/**
	 * approximates the percentile scores using the histogram values
	 * 
	 * @return a list of percentiles 10-90, 99.9 and 99.99
	 */
	long[] approximatePercentileValues();

	/**
	 * @return the array of percentile values that we report
	 */
	float[] getPercentiles();
	
	/**
	 * @return the minumum value represented by this histogram
	 */
	long getMin();
	
	/**
	 * @return the maximum value represented by this histogram
	 */
	long getMax();
	
	/**
	 * @return the number of bars in this histogram
	 */
	int getBuckets();
	
	/**
	 * @return the values in each bar of the histogram
	 */
	long[] getValues();
	
	Histogram NO_HISTOGRAM = new Histogram() {

		private static final long serialVersionUID = 1L;

		public void add(long value) { /* nop */ }

		public long[] approximatePercentileValues() { return new long[0]; }

		public float[] getPercentiles() { return new float[0]; }

		public int getBuckets() { return 0; }

		public long getEndValue(int bar) { return 0; }

		public long getMax() { return 0; }

		public long getMin() { return 0; }

		public long[] getValues() { return new long[0]; }

		public Histogram startFresh() { return this; }

		public void accept(MetricVisitor visitor)
		{
			visitor.visit(this);
		}
		
	};
}
