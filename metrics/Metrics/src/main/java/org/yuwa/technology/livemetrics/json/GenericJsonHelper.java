/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Nov 10, 2008 $
 */
package org.yuwa.technology.livemetrics.json;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * This is a helper class that carries helper methods for dealing with maps of string, object
 * 
 * <p>
 * Nov 10, 2008
 *
 * @author royoung
 */
public class GenericJsonHelper
{
	/** our logger */
	protected Logger log = Logger.getLogger(getClass());

	/**
	 * @param o
	 * @param name
	 * @return true if the map has the name as a key
	 */
	public boolean has(Map<String, Object> o, String name)
	{
		return o.get(name) != null;
	}
	
	/**
	 * OK this one is kind of silly... but for consistency...
	 * 
	 * @param o
	 * @return the object as a string, or "" if null
	 */
	public String asString(Object o)
	{
		return (o == null) ? "" : o.toString();
	}

	/**
	 * @param o
	 * @return the object as a string; empty if null
	 */
	public String getString(Map<String, Object> o, String name)
	{
		return  asString( o.get(name) );
	}

	/**
	 * @param o
	 * @return the object as a list, or null
	 */
	@SuppressWarnings("unchecked")
	public List<Object> asList(Object o)
	{
		return (List<Object>)o;
	}

	/**
	 * @param info
	 * @return the object as a list, or null
	 */
	public List<Object> getList(Map<String, Object> o, String name)
	{
		Object list = o.get(name);
		return asList(list);
	}

	/**
	 * @param c
	 * @return the object as a map, or null
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> asMap(Object c)
	{
		return (Map<String, Object>) c;
	}

	/**
	 * 
	 * 
	 * @param o
	 * @return the object as a map or null
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getMap(Map<String, Object> o, String name)
	{
		return (Map<String,Object>)o.get(name);
	}

	/**
	 * @param o
	 * @return the object as a long; 0 if null or not parseable
	 */
	public long asLong(Object o)
	{
		long i = 0L;
		if ( o != null && ! "0".equals(o) )
		{
			try
			{
				i = Long.parseLong(o.toString());
			}
			catch (NumberFormatException e)
			{
				// log and suppress
				log.warn("bad number string, using 0 : [" + o + "]");
				i = 0L;
			}
		}
		return i;
	}

	/**
	 * @param o
	 * @return the object as a long; 0 if null or not parseable
	 */
	public long getLong(Map<String, Object> o, String name)
	{
		return this.asLong(o.get(name));
	}

	
	/**
	 * @param values
	 * @return an array of long or null if no list
	 */
	public long[] asLongArray(List<Object> values)
	{
		long[] array = null;
		
		if ( values != null && values.size() > 0)
		{
			array = new long[values.size()];
			for ( int i = 0 ; i < array.length ; i++ )
			{
				array[i] = this.asLong( values.get(i) );
			}
		}
		return array;
	}

	/**
	 * convert the object to a float... hopefully, it's a string that's a float
	 * @param object
	 * @return the object as a float
	 */
	public float asFloat(Object o)
	{
		float f = 0.0f;
		if ( o != null && ! "0".equals(o) && ! "0.0".equals(o) )
		{
			try
			{
				f = Float.parseFloat(o.toString());
			}
			catch (NumberFormatException e)
			{
				// log and suppress
				log.warn("bad float string, using 0.0 : [" + o + "]");
				f = 0.0f;
			}
		}
		return f;
	}

	/**
	 * @param o
	 * @return the object as a long; 0 if null or not parseable
	 */
	public float getFloat(Map<String, Object> o, String name)
	{
		return this.asFloat(o.get(name));
	}

	
	/**
	 * @param values
	 * @return an array of percentiles or null if no list
	 */
	public float[] asFloatArray(List<Object> values)
	{
		float[] array = null;
		
		if ( values != null && values.size() > 0)
		{
			array = new float[values.size()];
			for ( int i = 0 ; i < array.length ; i++ )
			{
				array[i] = this.asFloat( values.get(i) );
			}
		}
		return array;
	}

	/**
	 * @param dateFormat
	 * @param dateObject
	 * @return the object as a date; null if not found or not parseable
	 */
	public Date asDate(SimpleDateFormat dateFormat, Object dateObject)
	{
		Date date = null;
		if ( dateObject != null )
		{
			try
			{
				date = dateFormat.parse(dateObject.toString());
			}
			catch (ParseException e)
			{
				// log and suppress
				log.warn("bad date string, ignoring: [" + dateObject + "]");
			}
		}
		return date;
	}

	/**
	 * @param o TODO
	 * @param service
	 * @return the object as a date; null if not found or not parseable
	 */
	public Date getDate(Map<String, Object> o, String name, SimpleDateFormat dateFormat)
	{
		Object dateObject = o.get(name);
		
		Date date = this.asDate(dateFormat, dateObject);

		return date;
	}
}
