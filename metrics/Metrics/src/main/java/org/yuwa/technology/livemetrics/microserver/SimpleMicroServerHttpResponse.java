/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jan 28, 2008 $
 */
package org.yuwa.technology.livemetrics.microserver;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import org.apache.log4j.Logger;

/**
 * The response object encapsulates information that we send out in an HTTP
 * response.
 * 
 * <p>
 * Contains methods to build the header and stream the response out to a socket.
 * 
 * <p>
 * Jan 28, 2008
 * 
 * @author royoung
 */
public class SimpleMicroServerHttpResponse
implements MicroServerHttpResponse
{
	/** our logger */
	protected Logger log = Logger.getLogger(getClass());

	/** pause length to let the last bits get out*/
	private static final int PAUSE_LENGTH = 300;

	/** true if we should send headers to defeat caching */
	private boolean defeatCaching = true;

	/** the bytes we should send as a response */
	private byte[] bytes = new byte[0];

	/** the http response code */
	private int code = 200;

	/** the http response method */
	private String message = "OK";

	/** the encoding to use when handling Java strings */
	private String encoding = "UTF-8";

	/** the encoding string to include in the header */
	private String encodingHeader = "UNICODE-1-1-UTF-8";

	/** the encoding type for the header */
	private String contentType = "text/plain";

	/** how long to pause before closing the connection */
	private long pauseLength = PAUSE_LENGTH;

	
	/**
	 * send all the information we've bundled up
	 */
	public void send(Socket socket)
	throws IOException
	{
		if ( log.isDebugEnabled() ) log.debug("send response " + ( (bytes != null) ? bytes.length : 0 ) );

		long nanos = System.nanoTime();

		OutputStream out = null;
		
		try
		{
			byte[] bytesToSend = this.getBytes();
			String header = this.buildHeader(bytesToSend);

			if ( log.isDebugEnabled() )
				log.debug("header:\n" + header);

			out = socket.getOutputStream();
			out.write(header.getBytes(this.getEncoding()));
			out.write(bytesToSend);
//			out.write("\r\n".getBytes(this.getEncoding()));

			if ( log.isInfoEnabled() ) log.info( "transmit time: " + ( System.nanoTime() - nanos ) );
		}
		catch ( Throwable t )
		{
			log.error("failed",t);
			t.printStackTrace();

			if ( log.isInfoEnabled() ) log.info( "failed time: " + ( System.nanoTime() - nanos ) );
		}
		finally
		{
			if ( log.isDebugEnabled() ) log.debug("close");

			if ( out != null )
			{
				out.flush();
				this.pause();
				out.close();
			}
		}
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// utility
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * exposed for junit testing, but you probably won't need to call it ever
	 * @param bytes TODO
	 * 
	 * @return the header as a string
	 */
	public String buildHeader(byte[] bytes)
	{
		StringBuilder header = new StringBuilder();

		header.append("HTTP/1.0 ")
		.append(this.getHttpCode())
		.append(" ")
		.append(this.getHttpMessage())
		.append("\r\n")
		.append("Content-Encoding: ")
		.append(this.getEncodingHeader())
		.append("\r\n")
		.append("Content-Type: ")
		.append(this.getContentType())
		.append("\r\n");

		if ( bytes != null )
		{
			header.append("Content-Length: ")
			.append(bytes.length+2);
			header.append("\r\n");
		}

		if ( this.defeatCaching )
		{
			header.append("Cache-control: no-cache\r\n")
			.append("Cache-control: no-store\r\n")
			.append("Pragma: no-cache\r\n")
			.append("Expires: 0\r\n");
		}

		header.append("\r\n");

		return header.toString();
	}

	/**
	 * add a little delay to see if it fixes the connection resets
	 */
	private void pause()
	{
		try
		{
			Thread.sleep(this.pauseLength);
		}
		catch (InterruptedException e)
		{
			// ignore
		}
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @return the defeatCaching
	 */
	public boolean isDefeatCaching()
	{
		return this.defeatCaching;
	}

	/**
	 * set true to add headers to defeat browser caching (i.e. force reload each time)
	 * 
	 * <p>
	 * defaults to true
	 * 
	 * @param defeatCaching the defeatCaching to set
	 */
	public void setDefeatCaching(boolean defeatCaching)
	{
		this.defeatCaching = defeatCaching;
	}

	/**
	 * @return the bytes
	 */
	public byte[] getBytes()
	{
		return this.bytes;
	}

	/**
	 * set the bytes to send in the response
	 * 
	 * <p>
	 * defaults to an empty array of 0 bytes
	 * 
	 * @param bytes the bytes to set
	 */
	public void setBytes(byte[] bytes)
	{
		this.bytes = bytes;
	}

	/**
	 * @return the code
	 */
	public int getHttpCode()
	{
		return this.code;
	}

	/**
	 * set the HTTP code to send
	 * 
	 * <p>
	 * defaults to 200
	 * 
	 * @param code the code to set
	 */
	public void setHttpCode(int code)
	{
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getHttpMessage()
	{
		return this.message;
	}

	/**
	 * set the HTTP message to send
	 * 
	 * <p>
	 * defaults to OK, as in "200 OK"
	 * 
	 * @param message the message to set
	 */
	public void setHttpMessage(String message)
	{
		this.message = message;
	}

	/**
	 * @return the encoding
	 */
	public String getEncoding()
	{
		return this.encoding;
	}

	/**
	 * set the encoding to use when building strings
	 * 
	 * <p>
	 * defaults to UTF-8
	 * 
	 * @param encoding the encoding to set
	 */
	public void setEncoding(String encoding)
	{
		this.encoding = encoding;
	}

	/**
	 * @return the encodingHeader
	 */
	public String getEncodingHeader()
	{
		return this.encodingHeader;
	}

	/**
	 * set the encoding value to send in the header
	 * 
	 * <p>
	 * defaults to UNICODE-1-1-UTF-8
	 * 
	 * @param encodingHeader the encodingHeader to set
	 */
	public void setEncodingHeader(String encodingHeader)
	{
		this.encodingHeader = encodingHeader;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType()
	{
		return this.contentType;
	}

	/**
	 * set the encoding type in the header
	 * 
	 * <p>
	 * defaults to text/plain
	 * 
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType)
	{
		this.contentType = contentType;
	}

	/**
	 * @return the pauseLength
	 */
	public long getPauseLength()
	{
		return this.pauseLength;
	}

	/**
	 * set the length of the pause after sending content before closing the
	 * connection
	 * 
	 * <p>
	 * defaults to 300 milliseconds
	 * 
	 * @param pauseLength the pauseLength to set
	 */
	public void setPauseLength(long pauseLength)
	{
		this.pauseLength = pauseLength;
	}

}
