/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Sep 24, 2007 $
 */
package org.yuwa.technology.livemetrics.microserver;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ThreadFactory;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.util.SimpleThreadFactory;


/**
 * The mico-server is intended as a bare-bones implementation of an HTTP server
 * that listens on a socket, accepts HTTP requests, and issues HTTP responses.
 * 
 * <p>
 * It's intended for super-super lightweight applications like reporting metrics
 * or heartbeats, where including a full-blown Servlet container would be too
 * much.
 * 
 * <p>
 * Each request is handed to a handler object, which may or may not parse the
 * input and produces output. Strictly speaking, this class doesn't care about
 * the protocol, so if the handler uses something other than HTTP, that's OK
 * too.
 * 
 * @author royoung
 * 
 */
public class MicroServer
implements Runnable
{
    /** our logger */
    protected Logger log = Logger.getLogger(getClass());

    /** pause length to let the last bits get out*/
    private static final int PAUSE_LENGTH = 300;

    /** default port to listen to */
    private static final int DEFAULT_PORT = 9199;

    private ThreadFactory threadFactory = new SimpleThreadFactory();
    private Thread thread;
    private boolean initLog4j = false;
    private boolean running = true;

    private int port = DEFAULT_PORT;

    private ServerSocket listener;
    private MicroServerHandler handler = MicroServerHandler.NO_HANDLER;

    /**
     * start the listener running
     */
    public void start()
    {
        if ( this.initLog4j ) BasicConfigurator.configure();

        this.thread = this.threadFactory.newThread(this);
        this.thread.start();
    }

    /**
     * stop the listener from running
     */
    public void stop()
    {
        log.info("stop");
        this.running = false;
        try
        {
            this.listener.close();
            this.listener = null;
        }
        catch (IOException e)
        {
            // nop
        }
    }

    /**
     * run the listener
     */
    public void run()
    {
        try
        {
            this.listener = new ServerSocket(this.port);
            this.listen(listener);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            log.error("Could not start listener",e);
        }
    }

    /**
     * listen for socket requests as long as we're running
     * 
     * @param listener
     * @throws IOException
     */
    private void listen(ServerSocket listener) throws IOException
    {
        try
        {
            log.info("listening on port " + this.port);

            while ( this.running )
            {
                Socket socket = this.listener.accept();

                try
                {
                    this.handler.handle(socket);
                }
                catch ( Throwable ie )
                {
                    // suppress errors for this socket
                }
                finally
                {
                    if ( socket != null ) {
                        this.pause();
                        socket.close();
                    }
                    log.info("done listening");
                }
            }
        }
        catch (SocketException se) {
            log.info("listener stopped");
        }
        finally
        {
            log.info("listener closing (" + this.port + ")");
            this.pause();
            if (this.listener != null )
                this.listener.close();
        }
    }

    /**
     * add a little delay to see if it fixes the connection resets
     */
    private void pause()
    {
        try
        {
            Thread.sleep(PAUSE_LENGTH);
        }
        catch (InterruptedException e)
        {
            Thread.interrupted();
        }
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    /**
     * @param threadFactory the threadFactory to set
     */
    public void setThreadFactory(ThreadFactory threadFactory)
    {
        this.threadFactory = threadFactory;
    }

    /**
     * @return the port
     */
    public int getPort()
    {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(int port)
    {
        this.port = port;
    }

    /**
     * @param initLog4j the initLog4j to set
     */
    public void setInitLog4j(boolean initLog4j)
    {
        this.initLog4j = initLog4j;
    }

    /**
     * @param handler the handler to set
     */
    public void setHandler(MicroServerHandler handler)
    {
        this.handler = handler;
    }

}
