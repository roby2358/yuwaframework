/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jul 7, 2008 $
 */
package org.yuwa.technology.livemetrics.model;

import org.yuwa.technology.livemetrics.model.Component;
import org.yuwa.technology.livemetrics.model.Histogram;
import org.yuwa.technology.livemetrics.model.MetricVisitor;
import org.yuwa.technology.livemetrics.model.Period;
import org.yuwa.technology.livemetrics.model.Service;
import org.yuwa.technology.livemetrics.model.SimpleHistogram;

import junit.framework.TestCase;

/**
 *
 * <p>
 * Jul 7, 2008
 *
 * @author royoung
 */

public class MetricVisitorTest extends TestCase
{


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Service)}.
	 */
	public void testVisitService()
	{
		Service server = new Service();
		MetricVisitor.NO_VISITOR.visit(server);
		// how to verify it did nothing?
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Component)}.
	 */
	public void testVisitComponent()
	{
		Component component = new Component();
		MetricVisitor.NO_VISITOR.visit(component);
		// how to verify it did nothing?
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Period)}.
	 */
	public void testVisitPeriod()
	{
		Period period = new Period(System.currentTimeMillis(),Period.ONE_SECOND);
		MetricVisitor.NO_VISITOR.visit(period);
		// how to verify it did nothing?
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Period)}.
	 */
	public void testVisitHistogram()
	{
		Histogram bars = new SimpleHistogram();
		MetricVisitor.NO_VISITOR.visit(bars);
		// how to verify it did nothing?
	}
}
