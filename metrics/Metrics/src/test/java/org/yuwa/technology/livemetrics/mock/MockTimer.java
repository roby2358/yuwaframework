/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Aug 25, 2008 $
 */
package org.yuwa.technology.livemetrics.mock;

import org.yuwa.technology.livemetrics.AbstractTimer;
import org.yuwa.technology.livemetrics.ServiceTracker;
import org.yuwa.technology.livemetrics.Timer;
import org.yuwa.technology.livemetrics.testtools.Woo;


/**
 * A mock class that gives us full control over the values
 * 
 * <p>
 * Aug 25, 2008
 *
 * @author royoung
 */
public class MockTimer extends AbstractTimer
{
	public long startTime = getTime();
	
	/**
	 * @param tracker
	 * @param componentName
	 */
	public MockTimer(ServiceTracker tracker, String componentName)
	{
		super(tracker, componentName);
	}


	/**
	 * @see org.yuwa.technology.livemetrics.Timer#create(org.yuwa.technology.livemetrics.ServiceTracker, java.lang.String)
	 */
	public Timer create(ServiceTracker tracker, String componentName)
	{
		return new MockTimer(tracker,componentName);
	}


	/**
	 * @see org.yuwa.technology.livemetrics.AbstractTimer#getTime()
	 */
	@Override
	public long getTime()
	{
		return ( Woo.get() * 100 ) % 1000000000L;
	}
	
}
