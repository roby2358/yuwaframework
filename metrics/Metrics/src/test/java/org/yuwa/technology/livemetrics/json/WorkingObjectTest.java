/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 */

package org.yuwa.technology.livemetrics.json;

import java.lang.reflect.InvocationTargetException;

import junit.framework.TestCase;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.json.WorkingObject;
import org.yuwa.technology.livemetrics.json.WorkingObjectObject;

public class WorkingObjectTest extends TestCase
{
    protected Logger log = Logger.getLogger(getClass());

    public void testWorkingObjectString()
    {
        
    }


    public void testWorkingObjectObject()
    {
        
    }


    public void testSet()
    {
        
    }


    public void testToString()
    {
        
    }


    public void testGetObject()
    {
        
    }


    public void testGetId()
    {
        
    }


    public void testGetPPages()
    {
        
    }


    public void testGetAltId()
    {
        
    }


    public void testSetAltId()
    {
        
    }

    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // test classes
    
    public static class TestRoundTrip
    {
        private int a;
        public String b;
        public String c;
        public String d;
        public String e;
        
        private static int count = 0;
        private int f = count++;
        
        public int getA() { return a; }
        public void setA(int a) { this.a = a; }
        public String getC() { return c; }
        public void setC(String c) { this.c = c; }
        public String getD() { return d; }
        public void setE(String e) { this.e = e; }
        public int getF() { return f; }
    }
    
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

    public void testRoundTrip()
    throws IllegalAccessException, InvocationTargetException
    {
        TestRoundTrip trt0 = new TestRoundTrip();
        WorkingObject wo0 = new WorkingObjectObject(trt0);
        
        trt0.setA(1);
        trt0.b = "b!";
        trt0.setC("c!");
        trt0.d = "d!";
        trt0.setE("e!");
        
        log.debug(ToStringBuilder.reflectionToString(trt0));
        
        WorkingObjectObject wo1 = new WorkingObjectObject(TestRoundTrip.class.getName());
        for ( String property : wo1.getClassMaps().getTypesByPropertyName().keySet() )
        {
            log.debug("set " + property);
            wo1.set(property, wo0.get(property) );
        }
        
        for ( String property : wo1.getClassMaps().getTypesByPropertyName().keySet() )
        {
            log.debug("got " + property + " : " + wo0.get(property) + " = " + wo1.get(property) );
        }

        TestRoundTrip trt1 = (TestRoundTrip) wo1.getObject();
        
        assertEquals(trt0.getA(),trt1.getA());
        assertEquals(trt0.b,trt1.b);
        assertEquals(trt0.getC(),trt1.getC());
        assertEquals(trt0.getD(),trt1.getD());
        assertEquals(trt0.e,trt1.e);

        log.debug("count " + trt0.getF() + " != " + trt1.getF() );
        
        assertFalse(trt0.getF() == trt1.getF());
    }

}
