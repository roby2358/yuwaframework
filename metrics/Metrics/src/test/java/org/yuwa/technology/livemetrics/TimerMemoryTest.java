/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Apr 7, 2008 $
 */
package org.yuwa.technology.livemetrics;

import org.yuwa.technology.livemetrics.TimerMemory;

import junit.framework.TestCase;

/**
 *
 * <p>
 * Apr 7, 2008
 *
 * @author royoung
 */
public class TimerMemoryTest extends TestCase
{
	public String name0 = "zebra0";
	public String name1 = "yak1";
	public String name2 = "xilo2";
	
	public TimerMemory memory = new TimerMemory();
	

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception
	{
		super.setUp();
		this.memory.clear();
	}
	
	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.TimerMemory#checkAgainstLastValue(java.lang.String, long)}.
	 */
	public void testCheckAgainstLastValue()
	{
		long elapsed = this.memory.checkAgainstLastValue(this.name0, 0);
		assertEquals(0,elapsed);
	}
	
	public void testCheckPosPosPos()
	{
		long elapsed = this.memory.checkAgainstLastValue(this.name0, 100);
		assertEquals(100,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, 200);
		assertEquals(200,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, 300);
		assertEquals(300,elapsed);
	}
	
	public void testCheckPosPosNeg()
	{
		long elapsed = this.memory.checkAgainstLastValue(this.name0, 100);
		assertEquals(100,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, 200);
		assertEquals(200,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, -300);
		assertEquals(200,elapsed);
	}
	
	public void testCheckPosNegPos()
	{
		long elapsed = this.memory.checkAgainstLastValue(this.name0, 100);
		assertEquals(100,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, -200);
		assertEquals(100,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, 300);
		assertEquals(300,elapsed);
	}
	
	public void testCheckPosNegNeg()
	{
		long elapsed = this.memory.checkAgainstLastValue(this.name0, 100);
		assertEquals(100,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, -200);
		assertEquals(100,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, -300);
		assertEquals(100,elapsed);
	}
	
	public void testCheckNegPosPos()
	throws Exception
	{
		String name = "testCheckNegPosPos";
		long elapsed = this.memory.checkAgainstLastValue(name, -100);
		assertEquals(0,elapsed);
		elapsed = this.memory.checkAgainstLastValue(name, 200);
		assertEquals(200,elapsed);
		elapsed = this.memory.checkAgainstLastValue(name, 300);
		assertEquals(300,elapsed);
	}
	
	public void testCheckNegPosNeg()
	{
		long elapsed = this.memory.checkAgainstLastValue(this.name0, -100);
		assertEquals(0,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, 200);
		assertEquals(200,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, -300);
		assertEquals(200,elapsed);
	}
	
	public void testCheckNegNegPos()
	{
		long elapsed = this.memory.checkAgainstLastValue(this.name0, -100);
		assertEquals(0,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, -200);
		assertEquals(0,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, 300);
		assertEquals(300,elapsed);
	}
	
	public void testCheckNegNegNeg()
	{
		long elapsed = this.memory.checkAgainstLastValue(this.name0, -100);
		assertEquals(0,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, -200);
		assertEquals(0,elapsed);
		elapsed = this.memory.checkAgainstLastValue(this.name0, -300);
		assertEquals(0,elapsed);
	}
	
	public void testCheckMultiple()
	{
		this.memory.checkAgainstLastValue(this.name0, 1);
		this.memory.checkAgainstLastValue(this.name1, 200);
		this.memory.checkAgainstLastValue(this.name2, 3);

		long elapsed0 = this.memory.checkAgainstLastValue(this.name0, 100);
		long elapsed1 = this.memory.checkAgainstLastValue(this.name1, -200);
		long elapsed2 = this.memory.checkAgainstLastValue(this.name2, 300);

		assertEquals(100,elapsed0);
		assertEquals(200,elapsed1);
		assertEquals(300,elapsed2);
	}
}
