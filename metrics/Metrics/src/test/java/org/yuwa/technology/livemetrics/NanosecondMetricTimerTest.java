/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Mar 17, 2008 $
 */
package org.yuwa.technology.livemetrics;

import junit.framework.TestCase;

import org.yuwa.technology.livemetrics.model.Component;
import org.yuwa.technology.livemetrics.testtools.Woo;


/**
 *
 * <p>
 * Mar 17, 2008
 *
 * @author royoung
 */

public class NanosecondMetricTimerTest extends TestCase
{
	public NanosecondMetricTimer timer;
	public ServiceTracker tracker;
	public String name = "component" + Woo.get();
	
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception
	{
		super.setUp();
		
		ServiceTrackerBuilder builder = new ServiceTrackerBuilder();
		builder.setName("test");
		builder.setUseListener(false);
		this.tracker = builder.build();
		
		this.timer = new NanosecondMetricTimer(this.tracker,this.name);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.NanosecondMetricTimer#getTime()}.
	 */
	public void testGetTime()
	throws Exception
	{
		long time = System.nanoTime();
		Thread.sleep(100L);
		long now = this.timer.getTime();
		long then = time + 80000000L;
		assertTrue( now + " >= " + then + "(" + (now-then) + ")", now >= then );
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.NanosecondMetricTimer#NanosecondMetricTimer(org.yuwa.technology.livemetrics.ServiceTracker, java.lang.String)}.
	 */
	public void testNanosecondMetricTimer()
	throws Exception
	{
		Thread.sleep(200L);
		this.timer.addMe();
		
		Component component = this.tracker.getHistory().getComponent(this.name);
		long value = component.getMinutes().get(0).getTotal();
		System.err.println(value);
		assertTrue(value + " >= " + 180000000L + "(" + (value - 180000000L) + ")", value >= 190000000L);
		assertTrue(value + " < " + 420000000L + "(" + (value - 420000000L) + ")", value < 420000000L);
	}
	
	public void testCreate()
	{
		Timer timer = this.timer.create(tracker, "zoom");
		assertNotNull(timer);
		assertEquals(this.timer.getClass().getName(),timer.getClass().getName());
		timer.addMe();
		
		assertEquals(1,this.tracker.getHistory().getComponent("zoom").getMinutes().get(0).getCount());
		assertEquals(0,this.tracker.getHistory().getComponent(this.name).getMinutes().get(0).getCount());
	}

}
