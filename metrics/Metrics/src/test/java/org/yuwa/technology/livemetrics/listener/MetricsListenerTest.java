/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jan 30, 2008 $
 */
package org.yuwa.technology.livemetrics.listener;

import java.util.Map;

import junit.framework.TestCase;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.ServiceTracker;
import org.yuwa.technology.livemetrics.ServiceTrackerBuilder;
import org.yuwa.technology.livemetrics.json.JsonBinder;
import org.yuwa.technology.livemetrics.testtools.Woo;



/**
 * 
 * <p>
 * Jan 30, 2008
 * 
 * @author royoung
 */
public class MetricsListenerTest
extends TestCase
{
	public Logger log = Logger.getLogger(getClass());
	public MetricsListener listener = new MetricsListener();
	public ServiceTracker tracker;
	public int port = 9000 + (int)Woo.small() % 1000;
	
	static
	{
		BasicConfigurator.configure();
	}


	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception
	{
		super.setUp();
		
		ServiceTrackerBuilder builder = new ServiceTrackerBuilder();
		builder.setHostName("woo");
		builder.setLegend("http://woo/woo");
		builder.setName("testtracker");
		builder.setNewlines(false);
		builder.setUseListener(false);
		this.tracker = builder.build();

		this.listener.setMetrics(this.tracker);
		
		// sleep
		Thread.sleep(500L);

		listener.setPort(this.port);
		listener.start();
	}


	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	public void testOk()
	{
	    // ok
	}

	/**
	 * test a round-trip request
	 */
    // TODO: fix	
	public void xRequestOk() throws Exception
	{
		HttpClient client = this.buildHttpClient();
		GetMethod get = new GetMethod("http://localhost:" + port);
		client.executeMethod(get);

		// sleep
		Thread.sleep(500L);

		listener.stop();

		String responseBodyAsString = get.getResponseBodyAsString();

//		System.err.println(responseBodyAsString.length());
//		System.err.println(responseBodyAsString);

		assertEquals(2310,responseBodyAsString.length());
		
		this.log.debug(responseBodyAsString);
		
		JsonBinder binder = new JsonBinder(responseBodyAsString);
		Map<String,Object> map = binder.parse();
		
		assertEquals("testtracker",map.get("name"));
		assertEquals("http://woo/woo",map.get("legend"));
		assertEquals("woo",map.get("host"));
	}


	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// utility
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * Just run it as a main program
	 * 
	 * @param args
	 */
	public static void main(String args[])
	{
		HeartbeatListener listener = new HeartbeatListener();
		HeartbeatHandler handler = new HeartbeatHandler();

		listener.setHandler(handler);

		listener.start();

	}

	/**
	 * @return
	 */
	public HttpClient buildHttpClient()
	{
		HttpClient client = new HttpClient(
				new MultiThreadedHttpConnectionManager());
		client.getHttpConnectionManager()
		.getParams()
		.setConnectionTimeout(10000);
		return client;
	}

}
