/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Nov 10, 2008 $
 */
package org.yuwa.technology.livemetrics.metrics;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.Map;

import org.yuwa.technology.livemetrics.metrics.JsonMetricsReader;
import org.yuwa.technology.livemetrics.model.Component;
import org.yuwa.technology.livemetrics.model.Period;
import org.yuwa.technology.livemetrics.model.Service;

import junit.framework.TestCase;


/**
 *
 * <p>
 * Nov 10, 2008
 *
 * @author royoung
 */
public class JsonMetricsReaderTest extends TestCase
{
	public JsonMetricsReader reader = new JsonMetricsReader();
	
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception
	{
		super.setUp();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// helpers
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	public static final long DATE_LONG = 1226365399676L;
	public static final long START_DATE_LONG = 1178490139676L;

	public static final String input0 = "{name:\"login\",\n"
		+ "host:\"qsegrapi01\",\n"
		+ "date:\"2008/11/10 17:03:19.676 -0800\",\n"
		+ "start:\"2007/05/06 14:22:19.676 -0800\",\n"
		+ "components:[\n"
		+ "{name:\"total\",\n"
		+ "lo:\"0\",\n"
		+ "hi:\"100\",\n"
		+ "minute:\n"
		+ "{total:[\"86473\",\"0\",\"67091\",\"205928\",\"295531\",\"198489\",\"21971\",\"651703\",\"657042\",\"0\",\"246603\",\"0\",\"18833\",\"26035\",\"137364\",\"21468\",\"502399\",\"24801\",\"210288\",\"24223\",\"21369\",\"128893\",\"298108\",\"46734\",\"30655\",\"0\",\"21018\",\"260835\",\"23896\",\"0\",\"19708\",\"21305\",\"253696\",\"322\",\"37731\",\"0\",\"536005\",\"3795\",\"4408340\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "count:[\"6\",\"0\",\"2\",\"10\",\"6\",\"4\",\"2\",\"14\",\"12\",\"0\",\"2\",\"0\",\"2\",\"4\",\"6\",\"2\",\"6\",\"4\",\"12\",\"2\",\"2\",\"4\",\"6\",\"4\",\"2\",\"0\",\"2\",\"4\",\"2\",\"0\",\"2\",\"2\",\"2\",\"2\",\"2\",\"0\",\"12\",\"2\",\"2\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "average:[\"14412\",\"0\",\"33545\",\"20592\",\"49255\",\"49622\",\"10985\",\"46550\",\"54753\",\"0\",\"123301\",\"0\",\"9416\",\"6508\",\"22894\",\"10734\",\"83733\",\"6200\",\"17524\",\"12111\",\"10684\",\"32223\",\"49684\",\"11683\",\"15327\",\"0\",\"10509\",\"65208\",\"11948\",\"0\",\"9854\",\"10652\",\"126848\",\"161\",\"18865\",\"0\",\"44667\",\"1897\",\"2204170\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "min:[\"13842\",\"0\",\"33521\",\"120\",\"4930\",\"49133\",\"10441\",\"121\",\"17771\",\"0\",\"123272\",\"0\",\"9390\",\"171\",\"11688\",\"10707\",\"10013\",\"154\",\"9473\",\"12083\",\"10261\",\"9527\",\"10235\",\"131\",\"15301\",\"0\",\"10481\",\"151\",\"11921\",\"0\",\"9823\",\"10624\",\"126330\",\"139\",\"18864\",\"0\",\"32567\",\"1547\",\"2204043\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "max:[\"15025\",\"0\",\"33570\",\"37685\",\"133587\",\"50094\",\"11530\",\"139659\",\"111726\",\"0\",\"123331\",\"0\",\"9443\",\"12852\",\"39562\",\"10761\",\"133551\",\"12246\",\"45591\",\"12140\",\"11108\",\"54915\",\"126368\",\"23240\",\"15354\",\"0\",\"10537\",\"130272\",\"11975\",\"0\",\"9885\",\"10681\",\"127366\",\"183\",\"18867\",\"0\",\"56844\",\"2248\",\"2204297\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]},\n"
		+ "hour:\n"
		+ "{total:[\"359492\",\"9149160\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "count:[\"18\",\"130\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "average:[\"19971\",\"70378\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "min:[\"120\",\"121\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "max:[\"37685\",\"2204297\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]},\n"
		+ "day:\n"
		+ "{total:[\"9508652\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "count:[\"148\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "average:[\"64247\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "min:[\"120\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "max:[\"2204297\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]}},\n"
		+ "{name:\"login-elapsed\",\n"
		+ "lo:\"0\",\n"
		+ "hi:\"100\",\n"
		+ "minute:\n"
		+ "{total:[\"43741\",\"0\",\"33570\",\"103048\",\"147828\",\"99279\",\"11530\",\"325956\",\"328593\",\"0\",\"123331\",\"0\",\"9443\",\"13064\",\"68756\",\"10761\",\"251264\",\"12441\",\"105760\",\"12140\",\"11108\",\"64497\",\"149568\",\"23413\",\"15354\",\"0\",\"10537\",\"130469\",\"11975\",\"0\",\"9885\",\"10681\",\"127366\",\"183\",\"18867\",\"0\",\"268139\",\"2248\",\"2204297\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "count:[\"3\",\"0\",\"1\",\"5\",\"3\",\"2\",\"1\",\"7\",\"6\",\"0\",\"1\",\"0\",\"1\",\"2\",\"3\",\"1\",\"3\",\"2\",\"6\",\"1\",\"1\",\"2\",\"3\",\"2\",\"1\",\"0\",\"1\",\"2\",\"1\",\"0\",\"1\",\"1\",\"1\",\"1\",\"1\",\"0\",\"6\",\"1\",\"1\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "average:[\"14580\",\"0\",\"33570\",\"20609\",\"49276\",\"49639\",\"11530\",\"46565\",\"54765\",\"0\",\"123331\",\"0\",\"9443\",\"6532\",\"22918\",\"10761\",\"83754\",\"6220\",\"17626\",\"12140\",\"11108\",\"32248\",\"49856\",\"11706\",\"15354\",\"0\",\"10537\",\"65234\",\"11975\",\"0\",\"9885\",\"10681\",\"127366\",\"183\",\"18867\",\"0\",\"44689\",\"2248\",\"2204297\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "min:[\"13898\",\"0\",\"33570\",\"166\",\"4964\",\"49185\",\"11530\",\"150\",\"17808\",\"0\",\"123331\",\"0\",\"9443\",\"212\",\"11737\",\"10761\",\"10053\",\"195\",\"9514\",\"12140\",\"11108\",\"9582\",\"11102\",\"173\",\"15354\",\"0\",\"10537\",\"197\",\"11975\",\"0\",\"9885\",\"10681\",\"127366\",\"183\",\"18867\",\"0\",\"32613\",\"2248\",\"2204297\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "max:[\"15025\",\"0\",\"33570\",\"37685\",\"133587\",\"50094\",\"11530\",\"139659\",\"111726\",\"0\",\"123331\",\"0\",\"9443\",\"12852\",\"39562\",\"10761\",\"133551\",\"12246\",\"45591\",\"12140\",\"11108\",\"54915\",\"126368\",\"23240\",\"15354\",\"0\",\"10537\",\"130272\",\"11975\",\"0\",\"9885\",\"10681\",\"127366\",\"183\",\"18867\",\"0\",\"56844\",\"2248\",\"2204297\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]},\n"
		+ "hour:\n"
		+ "{total:[\"180359\",\"4578733\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "count:[\"9\",\"65\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "average:[\"20039\",\"70442\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "min:[\"166\",\"150\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "max:[\"37685\",\"2204297\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]},\n"
		+ "day:\n"
		+ "{total:[\"4759092\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "count:[\"74\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "average:[\"64312\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "min:[\"150\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\n"
		+ "max:[\"2204297\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]}}]}";

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// tests
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.metrics.JsonMetricsReader#JsonMetricsReader(java.lang.String)}.
	 */
	public void testJsonMetricsReaderString()
	throws Exception
	{
		this.reader.read(input0);
		assertEquals(input0,this.reader.getSource());
		
		Service service = reader.getService();
		assertOK(service);
	}

	/**
	 * @param service
	 */
	private void assertOK(Service service)
	{
		assertNotNull(service);
		assertEquals(input0,this.reader.getSource());

		assertEquals("login",service.getName());
		assertEquals("qsegrapi01",service.getHostName());
		assertEquals(new Date(DATE_LONG),service.getDate());
		assertEquals(new Date(START_DATE_LONG),service.getStartDate());

		Map<String, Component> componentsByName = service.getComponentsByName();
		assertEquals(2,componentsByName.size());
		
		Component total = componentsByName.get("total");
		assertNotNull(total);
		assertEquals("total",total.getName());
		assertEquals(0,total.getLowerLimit());
		assertEquals(100,total.getUpperLimit());
		
		assertEquals(60,total.getMinutes().size());
		assertEquals(24,total.getHours().size());
		assertEquals(14,total.getDays().size());
		
		Period totalFirstMinute = total.getMinutes().getFirst();
		assertEquals(86473L,totalFirstMinute.getTotal());
		assertEquals(6L,totalFirstMinute.getCount());
		assertEquals(14412L,totalFirstMinute.getAverage());
		assertEquals(13842L,totalFirstMinute.getMin());
		assertEquals(15025L,totalFirstMinute.getMax());
		
		Period totalFirstHour = total.getHours().getFirst();
		assertEquals(359492L,totalFirstHour.getTotal());
		assertEquals(18L,totalFirstHour.getCount());
		assertEquals(19971L,totalFirstHour.getAverage());
		assertEquals(120L,totalFirstHour.getMin());
		assertEquals(37685L,totalFirstHour.getMax());
		
		Period totalFirstDay = total.getDays().getFirst();
		assertEquals(9508652L,totalFirstDay.getTotal());
		assertEquals(148L,totalFirstDay.getCount());
		assertEquals(64247L,totalFirstDay.getAverage());
		assertEquals(120L,totalFirstDay.getMin());
		assertEquals(2204297L,totalFirstDay.getMax());
		
		Component login = componentsByName.get("login-elapsed");
		assertNotNull(total);
		assertEquals("login-elapsed",login.getName());
		assertEquals(0,total.getLowerLimit());
		assertEquals(100,total.getUpperLimit());
		
		assertEquals(60,login.getMinutes().size());
		assertEquals(24,login.getHours().size());
		assertEquals(14,login.getDays().size());
		
		Period loginFirstMinute = login.getMinutes().getFirst();
		assertEquals(43741L,loginFirstMinute.getTotal());
		assertEquals(3L,loginFirstMinute.getCount());
		assertEquals(14580L,loginFirstMinute.getAverage());
		assertEquals(13898L,loginFirstMinute.getMin());
		assertEquals(15025L,loginFirstMinute.getMax());
		
		Period loginFirstHour = login.getHours().getFirst();
		assertEquals(180359L,loginFirstHour.getTotal());
		assertEquals(9L,loginFirstHour.getCount());
		assertEquals(20039L,loginFirstHour.getAverage());
		assertEquals(166L,loginFirstHour.getMin());
		assertEquals(37685L,loginFirstHour.getMax());
		
		Period loginFirstDay = login.getDays().getFirst();
		assertEquals(4759092L,loginFirstDay.getTotal());
		assertEquals(74L,loginFirstDay.getCount());
		assertEquals(64312L,loginFirstDay.getAverage());
		assertEquals(150L,loginFirstDay.getMin());
		assertEquals(2204297L,loginFirstDay.getMax());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.metrics.JsonMetricsReader#JsonMetricsReader(java.io.InputStream)}.
	 */
	public void testJsonMetricsReaderInputStream()
	throws Exception
	{
		ByteArrayInputStream in = new ByteArrayInputStream(input0.getBytes("US-ASCII"));
		this.reader.read(in);
		
		Service service = reader.getService();
		assertOK(service);
	}
}
