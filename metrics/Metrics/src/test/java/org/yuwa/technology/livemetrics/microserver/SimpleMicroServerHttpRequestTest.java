/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jan 30, 2008 $
 */
package org.yuwa.technology.livemetrics.microserver;

import junit.framework.TestCase;

import org.apache.log4j.BasicConfigurator;
import org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest;

/**
 *
 * <p>
 * Jan 30, 2008
 *
 * @author royoung
 */
public class SimpleMicroServerHttpRequestTest
extends TestCase
{
	public SimpleMicroServerHttpRequest request = new SimpleMicroServerHttpRequest();
	
	// feh
	static
	{
		BasicConfigurator.configure();
	}
	
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() 
	throws Exception
	{
		super.setUp();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() 
	throws Exception
	{
		super.tearDown();
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#SimpleMicroServerHttpRequest(byte[])}.
	 */
	public void testSimpleMicroServerHttpRequestByteArray()
	throws Exception
	{
		String input = "POST http://server:8080/woo+wonga/waa%3Ewongo/hooya\r\n"
			+ "zing:zap\r\n"
			+ "woo : waa\r\n"
			+ "ping : p o p\r\n"
			+ "\r\n"
			+ "bodybodybodybodybodybodybodybodybodybodybody";

		this.request = new SimpleMicroServerHttpRequest(input.getBytes("UTF-8"));
		
		assertEquals("POST",this.request.getMethod());
		assertEquals("http://server:8080/woo+wonga/waa%3Ewongo/hooya",this.request.getFullPath());
		assertEquals(5,this.request.getPath().size());
		assertEquals("http:",this.request.getPath().get(0));
		assertEquals("server:8080",this.request.getPath().get(1));
		assertEquals("woo wonga",this.request.getPath().get(2));
		assertEquals("waa>wongo",this.request.getPath().get(3));
		assertEquals("hooya",this.request.getPath().get(4));
		assertEquals("zap",this.request.getHeadersByName().get("zing"));
		assertEquals("waa",this.request.getHeadersByName().get("woo"));
		assertEquals("p o p",this.request.getHeadersByName().get("ping"));
		assertEquals("bodybodybodybodybodybodybodybodybodybodybody",new String(this.request.getBody(),"UTF-8"));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parse(byte[])}.
	 */
	public void testParsePost()
	throws Exception
	{
		String input = "POST http://server:8080/woo+wonga/waa%3Ewongo/hooya\r\n"
			+ "zing:zap\r\n"
			+ "woo : waa\r\n"
			+ "ping : p o p\r\n"
			+ "\r\n"
			+ "bodybodybodybodybodybodybodybodybodybodybody";
		
		this.request.set(input, 0);
		this.request.parse();
		
		assertEquals("POST",this.request.getMethod());
		assertEquals("http://server:8080/woo+wonga/waa%3Ewongo/hooya",this.request.getFullPath());
		assertEquals(5,this.request.getPath().size());
		assertEquals("http:",this.request.getPath().get(0));
		assertEquals("server:8080",this.request.getPath().get(1));
		assertEquals("woo wonga",this.request.getPath().get(2));
		assertEquals("waa>wongo",this.request.getPath().get(3));
		assertEquals("hooya",this.request.getPath().get(4));
		assertEquals("zap",this.request.getHeadersByName().get("zing"));
		assertEquals("waa",this.request.getHeadersByName().get("woo"));
		assertEquals("p o p",this.request.getHeadersByName().get("ping"));
		assertEquals("bodybodybodybodybodybodybodybodybodybodybody",new String(this.request.getBody(),"UTF-8"));
	}

	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parse(byte[])}.
	 */
	public void testParseGet()
	throws Exception
	{
		String input = "GET http://server:8080/woo+wonga/waa%3Ewongo/hooya\r\n"
			+ "zing:zap\r\n"
			+ "woo : waa\r\n"
			+ "ping : p o p\r\n"
			+ "\r\n";
		
		this.request.set(input, 0);
		this.request.parse();
		
		assertEquals("GET",this.request.getMethod());
		assertEquals("http://server:8080/woo+wonga/waa%3Ewongo/hooya",this.request.getFullPath());
		assertEquals(5,this.request.getPath().size());
		assertEquals("http:",this.request.getPath().get(0));
		assertEquals("server:8080",this.request.getPath().get(1));
		assertEquals("woo wonga",this.request.getPath().get(2));
		assertEquals("waa>wongo",this.request.getPath().get(3));
		assertEquals("hooya",this.request.getPath().get(4));
		assertEquals("zap",this.request.getHeadersByName().get("zing"));
		assertEquals("waa",this.request.getHeadersByName().get("woo"));
		assertEquals("p o p",this.request.getHeadersByName().get("ping"));
		assertEquals("",new String(this.request.getBody(),"UTF-8"));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseMethod()}.
	 */
	public void testParseMethodGet()
	throws Exception
	{
		this.request.set("GET woo woo woo\r\n",0);
		boolean parse = this.request.parseMethod();
		
		assertTrue(parse);
		assertEquals("GET",this.request.getMethod());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseMethod()}.
	 */
	public void testParseMethodGeT()
	throws Exception
	{
		this.request.set("gEt woo woo woo\r\n",0);
		boolean parse = this.request.parseMethod();
		
		assertTrue(parse);
		assertEquals("GET",this.request.getMethod());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseUri()}.
	 */
	public void testParseUri()
	throws Exception
	{
		String uri = "http://server.blah:8080/woo1/woo2/woo3/woo4/";
		this.request.set(uri,0);
		boolean parse = this.request.parseUri();
		
		assertTrue(parse);
		assertEquals(uri,request.getFullPath());
		assertNotNull(request.getPath());
		assertEquals(6,request.getPath().size());
		assertEquals("http:",request.getPath().get(0));
		assertEquals("server.blah:8080",request.getPath().get(1));
		assertEquals("woo1",request.getPath().get(2));
		assertEquals("woo2",request.getPath().get(3));
		assertEquals("woo3",request.getPath().get(4));
		assertEquals("woo4",request.getPath().get(5));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseUri()}.
	 */
	public void testParseUriTrailing()
	throws Exception
	{
		String uri = "http://server.blah:8080/woo1/woo2/woo3/woo4/";
		this.request.set(uri + "    ",0);
		boolean parse = this.request.parseUri();
		
		assertTrue(parse);
		assertEquals(uri,request.getFullPath());
		assertNotNull(request.getPath());
		assertEquals(6,request.getPath().size());
		assertEquals("http:",request.getPath().get(0));
		assertEquals("server.blah:8080",request.getPath().get(1));
		assertEquals("woo1",request.getPath().get(2));
		assertEquals("woo2",request.getPath().get(3));
		assertEquals("woo3",request.getPath().get(4));
		assertEquals("woo4",request.getPath().get(5));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseUri()}.
	 */
	public void testParseUriEncoded()
	throws Exception
	{
		String uri = "http://server.blah:8080/woo%3E1/woo+2/woo%613/woo%7D4/";
		this.request.set(uri,0);
		boolean parse = this.request.parseUri();
		
		assertTrue(parse);
		assertEquals(uri,request.getFullPath());
		assertNotNull(request.getPath());
		assertEquals(6,request.getPath().size());
		assertEquals("http:",request.getPath().get(0));
		assertEquals("server.blah:8080",request.getPath().get(1));
		assertEquals("woo>1",request.getPath().get(2));
		assertEquals("woo 2",request.getPath().get(3));
		assertEquals("wooa3",request.getPath().get(4));
		assertEquals("woo}4",request.getPath().get(5));
	}

	/**
	 * faile
	 * 
	 * @throws Exception
	 */
	public void testParseUriNo()
	throws Exception
	{
		String uri = " http://woo/woo";
		this.request.set(uri, 0);
		boolean parse = this.request.parseUri();
		
		assertFalse(parse);
	}

	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseVersion()}.
	 */
	public void testParseVersion()
	throws Exception
	{
		String uri = "12345 zing\r\nwongo\r\n";
		this.request.set(uri, 0);
		boolean parse0 = this.request.parseVersion();
		StringBuilder builder = this.request.parseTo("\r");
		assertTrue(parse0);
		assertEquals("wongo",builder.toString());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseHeader()}.
	 */
	public void testParseHeader0()
	throws Exception
	{
		String input = "name:value";
		this.request.set(input,0);
		this.request.parseHeader();
		assertEquals(1,this.request.getHeadersByName().size());
		assertEquals("value",this.request.getHeadersByName().get("name"));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseHeader()}.
	 */
	public void testParseHeader1()
	throws Exception
	{
		String input = "name:value\r\n";
		this.request.set(input,0);
		this.request.parseHeader();
		assertEquals(1,this.request.getHeadersByName().size());
		assertEquals("value",this.request.getHeadersByName().get("name"));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseHeader()}.
	 */
	public void testParseHeader2()
	throws Exception
	{
		String input = "name   :      value\r\n";
		this.request.set(input,0);
		this.request.parseHeader();
		assertEquals(1,this.request.getHeadersByName().size());
		assertEquals("value",this.request.getHeadersByName().get("name"));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseHeader()}.
	 */
	public void testParseHeader3()
	throws Exception
	{
		String input = "name: va lue";
		this.request.set(input,0);
		this.request.parseHeader();
		assertEquals(1,this.request.getHeadersByName().size());
		assertEquals("va lue",this.request.getHeadersByName().get("name"));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseHeader()}.
	 */
	public void testParseHeaderMulti()
	throws Exception
	{
		String input = "name0 : value0\r\nname1:value1\r\nname2 : value2\r\nname3 :value3";
		this.request.set(input,0);
		while ( this.request.parseHeader() ) ;
		
		assertEquals(4,this.request.getHeadersByName().size());
		assertEquals("value0",this.request.getHeadersByName().get("name0"));
		assertEquals("value1",this.request.getHeadersByName().get("name1"));
		assertEquals("value2",this.request.getHeadersByName().get("name2"));
		assertEquals("value3",this.request.getHeadersByName().get("name3"));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseEmptyLine()}.
	 */
	public void testParseEmptyLine()
	throws Exception
	{
		String in = "\r\n\r\n\r\nxxxxx\r\n\r\n";
		this.request.set(in, 0);
		int i = 0;
		while ( this.request.parseEmptyLine() ) i++;
		
		assertEquals(3,i);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseBody()}.
	 */
	public void testParseBody()
	throws Exception
	{
		String in = "0123456789";
		this.request.set(in, 0);
		boolean parse = this.request.parseBody();

		assertTrue(parse);

		String out = new String(this.request.getBody(),"UTF-8");

		assertEquals(10,out.length());
		assertEquals(in,out);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseBody()}.
	 */
	public void testParseBodyEmpty()
	throws Exception
	{
		String in = "";
		this.request.set(in, 0);
		boolean parse = this.request.parseBody();

		assertTrue(parse);

		String out = new String(this.request.getBody(),"UTF-8");
		
		assertEquals(0,out.length());
		assertEquals(in,out);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpRequest#parseBody()}.
	 */
	public void testParseBodyEmptyAfterCRLF()
	throws Exception
	{
		String in = "\r\n";
		this.request.set(in, 2);
		boolean parse = this.request.parseBody();

		assertTrue(parse);

		String out = new String(this.request.getBody(),"UTF-8");
		
		assertEquals(0,out.length());
		assertEquals("",out);
	}
}
