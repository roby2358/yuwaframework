/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jan 30, 2008 $
 */
package org.yuwa.technology.livemetrics.listener;

import java.io.IOException;
import java.net.Socket;

import junit.framework.TestCase;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.BasicConfigurator;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpHandler;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpRequest;
import org.yuwa.technology.livemetrics.microserver.MicroServerHttpResponse;
import org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpResponse;
import org.yuwa.technology.livemetrics.testtools.Woo;



/**
 * 
 * <p>
 * Jan 30, 2008
 * 
 * @author royoung
 */
public class ComboListenerTest
extends TestCase
{
	public ComboListener listener = new ComboListener();
	public int port = 10000 + (int)Woo.small() % 10000;

	static
	{
		BasicConfigurator.configure();
	}


	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception
	{
		super.setUp();

		listener.add("aaa", AAA);
		listener.add("bbb", BBB);
		listener.setUnknownHandler(UNKNOWN);

		// sleep
		Thread.sleep(500L);

		listener.setPort(this.port);
		listener.start();
	}


	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}


	/**
	 * test a round-trip request
	 */
	public void testRequestAaa() throws Exception
	{
		HttpClient client = this.buildHttpClient();
		GetMethod get = new GetMethod("http://localhost:" + port + "/woo/bing/aaa");
		client.executeMethod(get);

		// sleep
		Thread.sleep(500L);

		listener.stop();

		assertEquals("aaaaaaaa",get.getResponseBodyAsString());
	}


	/**
	 * test a round-trip request
	 */
	public void testRequestBbb() throws Exception
	{
		HttpClient client = this.buildHttpClient();
		GetMethod get = new GetMethod("http://localhost:" + port + "/woo/bing/bbb");
		client.executeMethod(get);

		// sleep
		Thread.sleep(500L);

		listener.stop();

		assertEquals("bbbbbbbb",get.getResponseBodyAsString());
	}


	/**
	 * test a round-trip request
	 */
	public void testRequestUnknown() throws Exception
	{
		HttpClient client = this.buildHttpClient();
		GetMethod get = new GetMethod("http://localhost:" + port + "/woo/bing/zing");
		client.executeMethod(get);

		// sleep
		Thread.sleep(500L);
		
		listener.stop();

		assertEquals("unknown request",get.getResponseBodyAsString());
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// test objects
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	public static final MicroServerHttpHandler AAA = new MicroServerHttpHandler() {

		public void handle(Socket socket, MicroServerHttpRequest request)
		throws IOException
		{
			MicroServerHttpResponse response = new SimpleMicroServerHttpResponse();
			response.setBytes("aaaaaaaa".getBytes("UTF-8"));
			response.send(socket);
		}

	};

	public static final MicroServerHttpHandler BBB = new MicroServerHttpHandler() {

		public void handle(Socket socket, MicroServerHttpRequest request)
		throws IOException
		{
			MicroServerHttpResponse response = new SimpleMicroServerHttpResponse();
			response.setBytes("bbbbbbbb".getBytes("UTF-8"));
			response.send(socket);
		}

	};

	public static final MicroServerHttpHandler UNKNOWN = new MicroServerHttpHandler() {

		public void handle(Socket socket, MicroServerHttpRequest request)
		throws IOException
		{
			MicroServerHttpResponse response = new SimpleMicroServerHttpResponse();
			response.setBytes("unknown request".getBytes("UTF-8"));
			response.send(socket);
		}

	};


	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// utility
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * Just run it as a main program
	 * 
	 * @param args
	 */
	public static void main(String args[])
	{
		ComboListener listener = new ComboListener();

		listener.add("aaa", AAA);
		listener.add("bbb", BBB);
		listener.setUnknownHandler(UNKNOWN);

		listener.start();

	}

	/**
	 * @return
	 */
	public HttpClient buildHttpClient()
	{
		HttpClient client = new HttpClient(
				new MultiThreadedHttpConnectionManager());
		client.getHttpConnectionManager()
		.getParams()
		.setConnectionTimeout(10000);
		return client;
	}

}
