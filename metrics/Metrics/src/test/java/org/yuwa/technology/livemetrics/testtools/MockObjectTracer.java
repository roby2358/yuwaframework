/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: Rob Y
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: 2007/04/12 $
 */
package org.yuwa.technology.livemetrics.testtools;

import java.util.ArrayList;
import java.util.List;

/**
 * the mock tracer just keeps a list of operations that
 * have been called in a mock object so we can check
 * it in the unit test
 * 
 * <p>
 * Apr 12, 2007
 *
 * @author royoung
 */
public class MockObjectTracer {

	private List<String> trace = new ArrayList<String>();

	/**
	 * render this object as a string
	 */
	public String toString()
	{
		StringBuilder builder = new StringBuilder();

		for ( String t : trace ) {
			if ( builder.length() > 0 )
				builder.append("\n");
			
			builder.append(t);
		}
		return builder.toString();
	}
	
	/**
	 * generate a hashCode for this object
	 */
	public int hashCode()
	{
		int prime = 1009;
		int result = 0;
		
		for ( String t : this.trace ) {
			result = result * prime + t.hashCode();
		}
		
		return result;
	}
	

    /**
     * @return true if this is logically equal to the other object
     */
    public boolean equals(Object that) {
    	
        final boolean result;

        if (this == that) {
            result = true;
        }
        else if (that == null) {
            result = false;
        }
        else if (this.getClass() != that.getClass()) {
            result = false;
        }
        else {
            result = this.equals((MockObjectTracer) that);
        }

        return result;
    }
    
    /**
     * returns true if the supplied RatingCategory is logicall equal to this one
     * 
     * <p>
     * Based on isoCode text applicationId
     * 
     * @return true if this is logically equal to the other RatingCategory
     */
    public boolean equals(MockObjectTracer that) {
    	
        final boolean result;
        
        if (this == that) {
            result = true;
        }
        else if (that == null) {
            result = false;
        }
        else if ( this.trace == null && that.trace != null ) {
        	result = false;
        }
        else if ( this.trace.size() != that.trace.size() ) {
        	result = false;
        }
        else {
        	boolean ok = true;

        	// Hm... I could probably just do list.equals(list)
        	for ( int i = 0 ; ok && i < this.trace.size() ; i++ )
        	{
        		ok = this.trace.get(i).equals(that.trace.get(i));
        	}
        	
        	result = ok;
        }
        
        return result;
    }
    
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    // accessors
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    
    /**
     * add a string to our trace
     * 
     * @param s
     */
    public void add(String s) {
    	this.trace.add(s);
    }
    
	/**
	 * @return the trace
	 */
	public List<String> getTrace() {
		return trace;
	}
	
}
