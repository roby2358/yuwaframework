/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package org.yuwa.technology.livemetrics.json;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.json.ClassMaps;

public class ClassMapsTest
extends TestCase
{
	public Logger log = Logger.getLogger(getClass());

	public void setUp()
	throws Exception
	{
		super.setUp();
	}

	public class TestClassMaps {
		// empty
	};

	public void testClassMaps()
	throws Exception
	{
		// just check the constructor works
		ClassMaps maps = new ClassMaps(TestClassMaps.class);

		log.debug(maps.toString());

		assertTrue("made it",true);
	}

	public void testNo()
	throws Exception
	{
		// this shouldn't do anything
		ClassMaps.no();

		// yes... but how do we know it did nothing? Hm!
		assertTrue("did nothing",true);
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// test classes

	public class TestToString0 {
		public int field = 1;
	};

	public class TestToString1 {
		public String getId() { return "tostring1"; }
		public String getPPages() { return ""; }
	};

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	// TODO: since these iterate over a map, need to order the keys before I can
	// do this comparison

	public void testToString0()
	throws Exception
	{
		ClassMaps maps = new ClassMaps(TestToString0.class);

		String string = maps.toString();

		log.debug(string);

		assertTrue(0 == string.indexOf("[ ClassMaps ") );
		assertTrue(-1 != string.indexOf("\n   idMethod=no") );
		assertTrue(-1 != string.indexOf("\n   ppagesMethod=no") );
		assertTrue(-1 != string.indexOf("\n   field : field=int") );
		assertTrue(-1 != string.indexOf("\n   getter : class=java.lang.Class") );
		assertTrue( string.length() - 2 == string.indexOf(" ]") );
	}

	public void testToString1()
	throws Exception
	{
		ClassMaps maps = new ClassMaps(TestToString1.class);

		String string = maps.toString();

		log.debug(string);

		assertTrue(0 == string.indexOf("[ ClassMaps ") );
		assertTrue(-1 != string.indexOf("\n   idMethod=getId") );
		assertTrue(-1 != string.indexOf("\n   ppagesMethod=getId") );
		assertTrue(-1 != string.indexOf("\n   getter : class=java.lang.Class") );
		assertTrue(-1 != string.indexOf("\n   getter : ppages=java.lang.String") );
		assertTrue(-1 != string.indexOf("\n   getter : id=java.lang.String") );
		assertTrue( string.length() - 2 == string.indexOf(" ]") );
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// test classes

	public static class AdderClass
	{
		public void add(String s) { }

		public void add(Integer i) { }

		public void add(int i) { }

		public void add(boolean b) { }

		public void add(float f) { }

		public void addSomething(String s) { }

		public void notAddSomething(String s) { }
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	public void testGetAddersByClass()
	throws Exception
	{
		ClassMaps m = new ClassMaps(AdderClass.class);

		log.debug(m.toString());

		assertNotNull(m.getAddersByClass().get(Class.forName("java.lang.String")));
//		assertNotNull(m.getAddersByClass().get(Class.forName("int")));
//		assertNotNull(m.getAddersByClass().get(Class.forName("boolean")));
		assertNotNull(m.getAddersByClass().get(Class.forName("java.lang.Integer")));
//		assertNotNull(m.getAddersByClass().get(Class.forName("float")));

		// addSomething should show up as a setter
		assertNotNull(m.getSettersByName().get("something"));
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// test classes

	public static class IdPpages0
	{
//		public String getId() { return ""; }

//		public String getPpages() { return ""; }
	}

	public static class IdPpages1
	{
		public void getId() { }

		public void getPpages() { }
	}

	public static class IdPpages2
	{
		public String getId() { return ""; }

		public String getPpages() { return ""; }
	}

	public static class IdPpages3
	{
		public String GetiD() { return ""; }

		public String GETPPAGES() { return ""; }
	}

	public static class IdPpages4
	{
//		public String getId() { return ""; }

		public String getPpages() { return ""; }
	}

	public static class IdPpages5
	{
		public String getId() { return ""; }

//		public String getPpages() { return ""; }
	}

	public static class IdPpages6
	{
		public String getId(String a) { return ""; }

		public String getPpages(String a) { return ""; }
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	public void testGetIdMethod()
	throws Exception
	{
		ClassMaps m0 = new ClassMaps(IdPpages0.class);
		assertTrue(ClassMaps.NO_METHOD == m0.getIdMethod());

		// TODO: hm...
		ClassMaps m1 = new ClassMaps(IdPpages1.class);
		assertFalse(ClassMaps.NO_METHOD == m1.getIdMethod());

		ClassMaps m2 = new ClassMaps(IdPpages2.class);
		assertFalse(ClassMaps.NO_METHOD == m2.getIdMethod());

		ClassMaps m3 = new ClassMaps(IdPpages3.class);
		assertFalse(ClassMaps.NO_METHOD == m3.getIdMethod());

		ClassMaps m4 = new ClassMaps(IdPpages4.class);
		assertEquals(ClassMaps.NO_METHOD,m4.getIdMethod());

		ClassMaps m5 = new ClassMaps(IdPpages5.class);
		assertFalse(ClassMaps.NO_METHOD == m5.getIdMethod());

		ClassMaps m6 = new ClassMaps(IdPpages6.class);
		assertEquals(ClassMaps.NO_METHOD,m6.getIdMethod());
	}

	public void testGetPropertyPagesMethod()
	throws Exception
	{
		ClassMaps m0 = new ClassMaps(IdPpages0.class);
		assertTrue(ClassMaps.NO_METHOD == m0.getPropertyPagesMethod());

		// TODO: hm...
		ClassMaps m1 = new ClassMaps(IdPpages1.class);
		assertFalse(ClassMaps.NO_METHOD == m1.getPropertyPagesMethod());

		ClassMaps m2 = new ClassMaps(IdPpages2.class);
		assertFalse(ClassMaps.NO_METHOD == m2.getPropertyPagesMethod());

		ClassMaps m3 = new ClassMaps(IdPpages3.class);
		assertFalse(ClassMaps.NO_METHOD == m3.getPropertyPagesMethod());

		ClassMaps m4 = new ClassMaps(IdPpages4.class);
		assertFalse(ClassMaps.NO_METHOD == m4.getPropertyPagesMethod());

		ClassMaps m5 = new ClassMaps(IdPpages5.class);
		assertEquals(ClassMaps.NO_METHOD,m5.getPropertyPagesMethod());

		ClassMaps m6 = new ClassMaps(IdPpages6.class);
		assertEquals(ClassMaps.NO_METHOD,m6.getPropertyPagesMethod());
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// test classes

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	public void testGetFieldsByName()
	throws Exception
	{

	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// test classes

	public static class TestProperties
	{
		public String seen;
		@SuppressWarnings("unused")
		private String unseen;
		
		public String getPropA() { return ""; }
		public void setPropA(String a) { }

		public Integer getPrOpB() { return new Integer(0); }
		public void setPRopB(Integer b) { }

		public String gEtPrOpBx() { return ""; }
		public void setPRopBx(String b) { }

		public String getPrOpBy() { return ""; }
		public void sEtPRopBy(String b) { }

		public String getPrOpC() { return ""; }
		public void setPRopC(int c) { }

		public String getPropD() { return ""; }

		public void setPropE(String a) { }

		public String get() { return ""; }
		public void set(String s) { }
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	public void testGetTypesByPropertyName()
	{
		ClassMaps m = new ClassMaps(TestProperties.class);

		log.debug(m.toString());

		assertEquals(String.class,m.getTypesByPropertyName().get("propa"));
		assertEquals(Integer.class,m.getTypesByPropertyName().get("propb"));
	}

	public void testGetSettersByName()
	throws Exception
	{
		ClassMaps m = new ClassMaps(TestProperties.class);

		assertEquals(5, m.getSettersByName().size());

		assertNotNull(m.getSettersByName().get("propb"));
		assertNotNull(m.getSettersByName().get("propbx"));
		assertNotNull(m.getSettersByName().get("propa"));
		assertNotNull(m.getSettersByName().get("prope"));
		assertNotNull(m.getSettersByName().get("propc"));

		assertNull(m.getSettersByName().get("propby"));
		assertNull(m.getSettersByName().get("propd"));
	}

	public void testGetGettersByName()
	throws Exception
	{
		ClassMaps m = new ClassMaps(TestProperties.class);

		assertEquals(6, m.getGettersByName().size());

		assertEquals(Integer.class,m.getGettersByName().get("propb").getReturnType());
		assertEquals(String.class,m.getGettersByName().get("propd").getReturnType());
		assertEquals(Class.class,m.getGettersByName().get("class").getReturnType());
		assertEquals(String.class,m.getGettersByName().get("propa").getReturnType());
		assertEquals(String.class,m.getGettersByName().get("propc").getReturnType());
		assertEquals(String.class,m.getGettersByName().get("propby").getReturnType());

		assertNull(m.getGettersByName().get("propbx"));
		assertNull(m.getGettersByName().get("prope"));
	}

	public void testCheckProperties()
	{
		ClassMaps m = new ClassMaps(TestProperties.class);

		assertEquals(3, m.getTypesByPropertyName().size());
		
		assertEquals(String.class,m.getTypesByPropertyName().get("propa"));
		assertEquals(Integer.class,m.getTypesByPropertyName().get("propb"));
		assertEquals(String.class,m.getTypesByPropertyName().get("seen"));

		assertNull(m.getTypesByPropertyName().get("unseen"));
	}
	
	public void testInnerClasses()
	{
		StringBuilder builder = new StringBuilder();
		Class<?> c = getClass();

		for ( Class<?> cc : c.getClasses() )
		{
			builder.append("\n   subclass : " + cc.getName());
		}

		log.debug(builder.toString());
	}
}
