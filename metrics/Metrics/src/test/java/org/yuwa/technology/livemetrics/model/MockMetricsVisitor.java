/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jul 7, 2008 $
 */
package org.yuwa.technology.livemetrics.model;

import org.yuwa.technology.livemetrics.testtools.MockObjectTracer;


/**
 *
 * <p>
 * Jul 7, 2008
 *
 * @author royoung
 */
public class MockMetricsVisitor
implements MetricVisitor
{
	public final MockObjectTracer tracer;
	
	/**
	 * constructor takes a tracer
	 */
	public MockMetricsVisitor(MockObjectTracer tracer)
	{
		this.tracer = tracer;
	}
	
	/**
	 * @see org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Service)
	 */
	public void visit(Service server)
	{
		this.tracer.add("visit server " + server.getName());
	}


	/**
	 * @see org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Component)
	 */
	public void visit(Component component)
	{
		this.tracer.add("visit component " + component.getName());
	}


	/**
	 * @see org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Period)
	 */
	public void visit(Period period)
	{
		this.tracer.add("visit period " + period.getPeriodLength());
	}

	/**
	 * @see org.yuwa.technology.livemetrics.model.MetricVisitor#visit(org.yuwa.technology.livemetrics.model.Histogram)
	 */
	public void visit(Histogram bars)
	{
		this.tracer.add("visit histogram " + bars.getBuckets());
	}
	
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
}
