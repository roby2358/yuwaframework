/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Oct 30, 2008 $
 */
package org.yuwa.technology.livemetrics.model;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.technology.livemetrics.testtools.Woo;


/**
 *
 * <p>
 * Oct 30, 2008
 *
 * @author royoung
 */
public class ComponentTest extends TestCase
{
	public long now = 1225387230000L;
	public Component component = new Component(now);
	public final ComponentTest THIS = this;

	protected void setUp() throws Exception
	{
		super.setUp();

		this.component.setName("mycomponent");
	}


	protected void tearDown() throws Exception
	{
		super.tearDown();
	}


	/**
	 * an inner class to see what we got
	 * <p>
	 * Oct 30, 2008
	 *
	 * @author royoung
	 */
	public static class Got
	{
		public boolean hit = false;
		public String info = "";
	}


	/**
	 * @param p
	 */
	private void assertZero(Period p)
	{
		Assert.assertEquals(0,p.getAverage());
		Assert.assertEquals(0,p.getCount());
		Assert.assertEquals(0,p.getMax());
		Assert.assertEquals(0,p.getMin());
		Assert.assertEquals(0,p.getTotal());
	}


	/**
	 * @param value
	 * @param p
	 */
	private void assertEqual(long value, Period p)
	{
		Assert.assertEquals(value,p.getAverage());
		Assert.assertEquals(1,p.getCount());
		Assert.assertEquals(value,p.getMax());
		Assert.assertEquals(value,p.getMin());
		Assert.assertEquals(value,p.getTotal());
	}

	/**
	 * bundle up histogram data for easy checking
	 */
	public String bundleHistogram(Period p)
	{
		StringBuilder builder = new StringBuilder("");
		builder.append(" h=");
		builder.append( p.getHistogram().getValues().length );

		builder.append(" p=");
		builder.append( p.getPercentileValues().length );

		for ( long v : p.getPercentileValues() )
		{
			builder.append(" ");
			builder.append( v );
		}

		return builder.toString();
	}

	/**
	 * bundle up histogram data for easy checking
	 */
	public String bundleHistogram(Histogram h)
	{
		StringBuilder builder = new StringBuilder("");
		builder.append(" h=");
		builder.append( h.getValues().length );

		long[] approximatePercentileValues = h.approximatePercentileValues();

		builder.append(" p=");
		builder.append( approximatePercentileValues.length );

		for ( long v : approximatePercentileValues )
		{
			builder.append(" ");
			builder.append( v );
		}

		return builder.toString();
	}

	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#Component()}.
	 */
	public void testComponent()
	{
		// default values
		Component component = new Component();
		Assert.assertEquals(0,component.getLowerLimit());
		Assert.assertEquals(100,component.getUpperLimit());
		Assert.assertNotNull(component.getDays());
		Assert.assertNotNull(component.getHours());
		Assert.assertNull(component.getLegend());
		Assert.assertNotNull(component.getMinutes());
		Assert.assertNull(component.getName());
	}


	/**
	 * @param minute
	 */
	private void populateWithoutHistogram(Period minute)
	{
		minute.setAverage(Woo.small());
		minute.setCount(Woo.small());
		minute.setMax(Woo.small());
		minute.setMin(Woo.small());
		minute.setTotal(Woo.small());
//		minute.setHistogram(histogram);
//		minute.setPercentiles(percentiles);
//		minute.setPercentileValues(percentileValues);
	}


	/**
	 * @param minute
	 * @param m
	 */
	private void assertEqual(Period minute, Period m)
	{
		Assert.assertEquals(minute.getAverage(),m.getAverage());
		Assert.assertEquals(minute.getCount(),m.getCount());
		Assert.assertEquals(minute.getMax(),m.getMax());
		Assert.assertEquals(minute.getMin(),m.getMin());
		Assert.assertEquals(minute.getTotal(),m.getTotal());
	}

	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#accept(org.yuwa.technology.livemetrics.model.MetricVisitor)}.
	 */
	public void testAccept()
	{
		final Got got = new Got();

		MetricVisitor visitor = new MetricVisitor() {

			public void visit(Service server)
			{
				throw new UnsupportedOperationException("not a service");
			}

			public void visit(Component component)
			{
				got.hit = true;
				got.info = THIS.component.getName();
			}

			public void visit(Period period)
			{
				throw new UnsupportedOperationException("not a period");
			}

			public void visit(Histogram bars)
			{
				throw new UnsupportedOperationException("not a histogram");
			}

		};

		this.component.accept(visitor);

		Assert.assertTrue(got.hit);
		Assert.assertEquals(this.component.getName(),got.info);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#add(long, long)}.
	 */
	public void testAdd()
	{
		long value = Woo.small();
		this.component.add(now, value);
		Period m = this.component.getMinutes().getFirst();
		Period h = this.component.getHours().getFirst();
		Period d = this.component.getDays().getFirst();

		assertEqual(value, m);
		assertEqual(value, h);
		assertEqual(value, d);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#addToSecond(long, long)}.
	 */
	public void testAddToSecond()
	{
		long value = Woo.small();
		this.component.addToSecond(now, value);

		Period m1 = this.component.getMinutes().getFirst();
		Period h1 = this.component.getHours().getFirst();
		Period d1 = this.component.getDays().getFirst();

		assertZero(m1);
		assertZero(h1);
		assertZero(d1);

		this.component.addToSecond(now + 1000L, 1);

		Period m2 = this.component.getMinutes().getFirst();
		Period h2 = this.component.getHours().getFirst();
		Period d2 = this.component.getDays().getFirst();

		assertEqual(value, m2);
		assertEqual(value, h2);
		assertEqual(value, d2);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#roll(long)}.
	 */
	public void testRollMinute()
	{
		this.component.roll(now);

		Period m1 = this.component.getMinutes().getFirst();
		Period h1 = this.component.getHours().getFirst();
		Period d1 = this.component.getDays().getFirst();

		populateWithoutHistogram(m1);
		populateWithoutHistogram(h1);
		populateWithoutHistogram(d1);

		this.component.roll(now + Metric.ONE_MINUTE * 5);

		Period m2 = this.component.getMinutes().getFirst();
		Period h2 = this.component.getHours().getFirst();
		Period d2 = this.component.getDays().getFirst();

		assertZero(m2);
		assertSame(h1,h2);
		assertEqual(h1,h2);
		assertSame(d1,d2);
		assertEqual(d1,d2);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#roll(long)}.
	 */
	public void testRollHour()
	{
		this.component.roll(now);

		Period m1 = this.component.getMinutes().getFirst();
		Period h1 = this.component.getHours().getFirst();
		Period d1 = this.component.getDays().getFirst();

		populateWithoutHistogram(m1);
		populateWithoutHistogram(h1);
		populateWithoutHistogram(d1);

		this.component.roll(now + Metric.ONE_HOUR * 1);

		Period m2 = this.component.getMinutes().getFirst();
		Period h2 = this.component.getHours().getFirst();
		Period d2 = this.component.getDays().getFirst();

		assertZero(m2);
		assertZero(h2);
		assertSame(d1,d2);
		assertEqual(d1,d2);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#roll(long)}.
	 */
	public void testRollDay()
	{
		this.component.roll(now);

		Period m1 = this.component.getMinutes().getFirst();
		Period h1 = this.component.getHours().getFirst();
		Period d1 = this.component.getDays().getFirst();

		populateWithoutHistogram(m1);
		populateWithoutHistogram(h1);
		populateWithoutHistogram(d1);

		this.component.roll(now + Metric.ONE_DAY * 5);

		Period m2 = this.component.getMinutes().getFirst();
		Period h2 = this.component.getHours().getFirst();
		Period d2 = this.component.getDays().getFirst();

		assertZero(m2);
		assertZero(h2);
		assertZero(d2);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#isOld(long)}.
	 */
	public void testIsOld()
	{
		assertFalse(this.component.isOld(now));
		assertFalse(this.component.isOld(now + 1000));
		assertFalse(this.component.isOld(now + 2000));
		assertFalse(this.component.isOld(now + 5000));
		assertFalse(this.component.isOld(now + 10000));
		assertFalse(this.component.isOld(now + 29000));
		assertTrue(this.component.isOld(now + 30000));
	}

	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#startPercentiles(long, long, int)}.
	 */
	public void testStartPercentiles()
	{
		this.component.startPercentiles(0L, 100L, 1000);

		for ( int i = 0 ; i < 10000 ; i++ )
			this.component.add(now, i / 500);

		for ( int i = 0 ; i < 8000 ; i++ )
			this.component.add(now, i / 200 + 20);

		for ( int i = 0 ; i < 2000 ; i++ )
			this.component.add(now, i / 50 + 60);

		String expected0 = " h=1000 p=12 3 7 11 15 19 29 39 49 59 95 99 99";
		String expected1 = " h=0 p=12 3 7 11 15 19 29 39 49 59 95 99 99";
		String expectedFresh = " h=1000 p=12 0 0 0 0 0 0 0 0 0 0 0 0";

		Assert.assertNotNull(this.component.getMinuteHistogram());
		Assert.assertEquals(12,this.component.getMinuteHistogram().approximatePercentileValues().length);
		Assert.assertEquals(expected0,this.bundleHistogram( this.component.getMinuteHistogram() ));

		Assert.assertNotNull(this.component.getHourHistogram());
		Assert.assertEquals(12,this.component.getHourHistogram().approximatePercentileValues().length);
		Assert.assertEquals(expected0,this.bundleHistogram( this.component.getHourHistogram() ));

		Assert.assertNotNull(this.component.getDayHistogram());
		Assert.assertEquals(12,this.component.getDayHistogram().approximatePercentileValues().length);
		Assert.assertEquals(expected0,this.bundleHistogram( this.component.getDayHistogram() ));

		this.component.add(now + Metric.ONE_MINUTE, 1);

		Assert.assertEquals(expected1,this.bundleHistogram( this.component.getMinutes().get(1) ));

		Assert.assertEquals(expectedFresh,this.bundleHistogram( this.component.getMinutes().getFirst() ));
		Assert.assertEquals(expected0,this.bundleHistogram( this.component.getHours().getFirst() ));
		Assert.assertEquals(expected0,this.bundleHistogram( this.component.getDays().getFirst() ));

		this.component.add(now + Metric.ONE_HOUR, 1);

		Assert.assertEquals(expected1,this.bundleHistogram( this.component.getHours().get(1) ));

		Assert.assertEquals(expectedFresh,this.bundleHistogram( this.component.getMinutes().getFirst() ));
		Assert.assertEquals(expectedFresh,this.bundleHistogram( this.component.getHours().getFirst() ));
		Assert.assertEquals(expected0,this.bundleHistogram( this.component.getDays().getFirst() ));

		this.component.add(now + Metric.ONE_DAY, 1);

		Assert.assertEquals(expected1,this.bundleHistogram( this.component.getDays().get(1) ));

		Assert.assertEquals(expectedFresh,this.bundleHistogram( this.component.getMinutes().getFirst() ));
		Assert.assertEquals(expectedFresh,this.bundleHistogram( this.component.getHours().getFirst() ));
		Assert.assertEquals(expectedFresh,this.bundleHistogram( this.component.getDays().getFirst() ));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#startHistogram(long, long, int)}.
	 */
	public void testStartHistogram()
	{
		@SuppressWarnings("unused")
        Got got = new Got();

		this.component.startHistogram(0L, 100L, 1000);

		for ( int i = 0 ; i < 10000 ; i++ )
			this.component.add(now, i / 500);

		for ( int i = 0 ; i < 8000 ; i++ )
			this.component.add(now, i / 200 + 20);

		for ( int i = 0 ; i < 2000 ; i++ )
			this.component.add(now, i / 50 + 60);

		String expected0 = " h=1000 p=12 3 7 11 15 19 29 39 49 59 95 99 99";

		Assert.assertNotNull(this.component.getMinuteHistogram());
		Assert.assertEquals(12,this.component.getMinuteHistogram().approximatePercentileValues().length);
		Assert.assertEquals(expected0,this.bundleHistogram( this.component.getMinuteHistogram() ));

		Assert.assertNotNull(this.component.getHourHistogram());
		Assert.assertEquals(12,this.component.getHourHistogram().approximatePercentileValues().length);
		Assert.assertEquals(expected0,this.bundleHistogram( this.component.getMinuteHistogram() ));

		Assert.assertNotNull(this.component.getDayHistogram());
		Assert.assertEquals(12,this.component.getDayHistogram().approximatePercentileValues().length);
		Assert.assertEquals(expected0,this.bundleHistogram( this.component.getMinuteHistogram() ));


		String expected1 = " h=1000 p=12 3 7 11 15 19 29 39 49 59 95 99 99";

		this.component.add(now + Metric.ONE_MINUTE, 1);

		Assert.assertEquals(" h=1000 p=12 0 0 0 0 0 0 0 0 0 0 0 0",this.bundleHistogram( this.component.getMinutes().getFirst() ));
		Assert.assertEquals(expected1,this.bundleHistogram( this.component.getMinutes().get(1) ));
		Assert.assertEquals(expected1,this.bundleHistogram( this.component.getHours().getFirst() ));
		Assert.assertEquals(expected1,this.bundleHistogram( this.component.getDays().getFirst() ));

		this.component.add(now + Metric.ONE_HOUR, 1);

		Assert.assertEquals(" h=1000 p=12 0 0 0 0 0 0 0 0 0 0 0 0",this.bundleHistogram( this.component.getHours().getFirst() ));
		Assert.assertEquals(expected1,this.bundleHistogram( this.component.getHours().get(1) ));
		Assert.assertEquals(expected1,this.bundleHistogram( this.component.getDays().getFirst() ));

		this.component.add(now + Metric.ONE_DAY, 1);

		Assert.assertEquals(" h=1000 p=12 0 0 0 0 0 0 0 0 0 0 0 0",this.bundleHistogram( this.component.getDays().getFirst() ));
		Assert.assertEquals(expected1,this.bundleHistogram( this.component.getDays().get(1) ));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#getName()}.
	 */
	public void testGetName()
	{
		Assert.assertEquals("mycomponent",this.component.getName());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#setName(java.lang.String)}.
	 */
	public void testSetName()
	{
		String name = "woohahahaha" + Woo.get();
		this.component.setName(name);
		Assert.assertEquals(name,this.component.getName());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#getDays()}.
	 */
	public void testGetDays()
	{
		Assert.assertNotNull(this.component.getDays());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#setDays(java.util.LinkedList)}.
	 */
	public void testSetDays()
	{
		// TODO: hm... why do I allow this?
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#getHours()}.
	 */
	public void testGetHours()
	{
		Assert.assertNotNull(this.component.getHours());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#setHours(java.util.LinkedList)}.
	 */
	public void testSetHours()
	{
		// TODO: hm... why do I allow this?
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#getMinutes()}.
	 */
	public void testGetMinutes()
	{
		Assert.assertNotNull(this.component.getMinutes());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#setMinutes(java.util.LinkedList)}.
	 */
	public void testSetMinutes()
	{
		// TODO: hm... why do I allow this?
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#getLowerLimit()}.
	 */
	public void testGetLowerLimit()
	{
		Assert.assertEquals(0, this.component.getLowerLimit());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#setLowerLimit(int)}.
	 */
	public void testSetLowerLimit()
	{
		this.component.setLowerLimit(1010);
		Assert.assertEquals(1010, this.component.getLowerLimit());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#getUpperLimit()}.
	 */
	public void testGetUpperLimit()
	{
		Assert.assertEquals(0, this.component.getLowerLimit());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#setUpperLimit(int)}.
	 */
	public void testSetUpperLimit()
	{
		this.component.setLowerLimit(2030);
		Assert.assertEquals(2030, this.component.getLowerLimit());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#getLegend()}.
	 */
	public void testGetLegend()
	{
		Assert.assertNull(this.component.getLegend());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#setLegend(java.lang.String)}.
	 */
	public void testSetLegend()
	{
		String legend = "woo" + Woo.get();
		this.component.setLegend(legend);
		Assert.assertEquals(legend,this.component.getLegend());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#isReportHistogram()}.
	 */
	public void testIsReportHistogramNo()
	{
		Assert.assertFalse(this.component.isReportHistogram());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#isReportHistogram()}.
	 */
	public void testIsReportHistogramNoAgain()
	{
		this.component.startPercentiles(0L, 100L, 100);
		Assert.assertFalse(this.component.isReportHistogram());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Component#isReportHistogram()}.
	 */
	public void testIsReportHistogramYes()
	{
		this.component.startHistogram(0L, 100L, 100);
		Assert.assertTrue(this.component.isReportHistogram());
	}

}
