/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jul 7, 2008 $
 */
package org.yuwa.technology.livemetrics.model;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.yuwa.technology.livemetrics.testtools.MockObjectTracer;
import org.yuwa.technology.livemetrics.testtools.Woo;


/**
 *
 * <p>
 * Jul 7, 2008
 *
 * @author royoung
 */

public class ServiceTest extends TestCase
{
	public MockObjectTracer tracer = new MockObjectTracer();
	public MockMetricsVisitor visitor = new MockMetricsVisitor(tracer);
	public Service service = new Service();
	public String name = "myname" + Woo.get();
	public String componentName = "wooga" + Woo.get();
	public long value = Woo.small();

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	public void setUp() throws Exception
	{
		super.setUp();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	public void tearDown() throws Exception
	{
		super.tearDown();
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#Service()}.
	 */
	public void testService()
	{
		assertNull(this.service.getName());
		assertNotNull(this.service.getComponent("total"));
		assertEquals("total",this.service.getComponent("total").getName());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#accept(org.yuwa.technology.livemetrics.model.MetricVisitor)}.
	 */
	public void testAccept()
	{
		service.setName(name);
		this.service.accept(this.visitor);

		MockObjectTracer expected = new MockObjectTracer();
		expected.add("visit server " + name);
		assertEquals(expected,this.tracer);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#roll()}.
	 */
	public void testRoll()
	{
//		fail("Not yet implemented");
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#add(java.lang.String, long)}.
	 */
	public void testAddStringLong()
	{
		this.service.setName(name);
		
		this.service.add(componentName, value );
		
		Component component = this.service.getComponentsByName().get(componentName);
		assertNotNull(component);
		assertEquals(this.value, component.getMinutes().get(0).getTotal());
		assertEquals(1, component.getMinutes().get(0).getCount());
		assertEquals(this.value, component.getHours().get(0).getTotal());
		assertEquals(1, component.getHours().get(0).getCount());
		assertEquals(this.value, component.getDays().get(0).getTotal());
		assertEquals(1, component.getDays().get(0).getCount());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#addToSecond(java.lang.String, long)}.
	 */
	public void testAddToSecond()
	{
		this.service.setName(name);
		
		this.service.add(componentName, value );
		
		Component component = this.service.getComponentsByName().get(componentName);
		assertNotNull(component);
		assertEquals(this.value, component.getMinutes().get(0).getTotal());
		assertEquals(1, component.getMinutes().get(0).getCount());
		assertEquals(this.value, component.getHours().get(0).getTotal());
		assertEquals(1, component.getHours().get(0).getCount());
		assertEquals(this.value, component.getDays().get(0).getTotal());
		assertEquals(1, component.getDays().get(0).getCount());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#after()}.
	 */
	public void testBefore()
	{
		this.service.setName(name);

		long now = this.service.getNow();

		pause(20L);
		
		this.service.before();
		
		assertTrue( now < this.service.getNow() );
		assertTrue( 1000L > System.currentTimeMillis() - this.service.getNow() );
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#after()}.
	 */
	public void testAfter()
	{
		// hm... how to test it does nothing ;)
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#add(org.yuwa.technology.livemetrics.model.Component)}.
	 */
	public void testAddComponent()
	{
		Component c = new Component();
		c.setName(this.componentName);
		this.service.add(c);
		
		assertNotNull(this.service.getComponentsByName().get(componentName));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#getName()}.
	 */
	public void testGetName()
	{
		this.service.setName(name);
		assertEquals(name,this.service.getName());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#getComponent(java.lang.String)}.
	 */
	public void testGetComponentNew()
	{
		Component component = this.service.getComponent(componentName);
		assertNotNull(component);
		assertEquals(componentName,component.getName());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#getComponent(java.lang.String)}.
	 */
	public void testGetComponentOld()
	{
		Component c = new Component();
		c.setName(this.componentName);
		this.service.add(c);
		
		Component component = this.service.getComponent(componentName);

		assertNotNull(component);
		assertEquals(componentName,component.getName());
		assertSame(c,component);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#getComponentsByName()}.
	 */
	public void testGetComponentsByName()
	{
		Component c = new Component();
		c.setName(this.componentName);
		this.service.add(c);
		
		assertNotNull(this.service.getComponentsByName().get(componentName));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#setComponentsByName(java.util.Map)}.
	 */
	public void testSetComponentsByNameEmpty()
	{
		assertNotNull(this.service.getComponentsByName());
		assertEquals(1,this.service.getComponentsByName().size());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#setComponentsByName(java.util.Map)}.
	 */
	public void testSetComponentsByName()
	{
		Component c = new Component();
		c.setName(this.componentName);
		Map<String,Component> map = new HashMap<String,Component>();
		map.put(componentName,c);
		this.service.setComponentsByName(map);
		
		assertNotNull(this.service.getComponentsByName());
		assertSame(map,this.service.getComponentsByName());
		assertNotNull(this.service.getComponentsByName().get(componentName));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#getHostName()}.
	 */
	public void testGetHostName()
	{
		this.service.setHostName(name);
		assertEquals(name,this.service.getHostName());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#getLegend()}.
	 */
	public void testGetLegend()
	{
		this.service.setLegend(name);
		assertEquals(name,this.service.getLegend());
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.model.Service#getNow()}.
	 */
	public void testGetNow()
	{
		assertTrue(0L < this.service.getNow());
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// utility
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	
	/**
	 * @param time TODO
	 * 
	 */
	private void pause(long time)
	{
		try
		{
			Thread.sleep(time);
		}
		catch (InterruptedException e)
		{
			// ok
		}
	}
}
