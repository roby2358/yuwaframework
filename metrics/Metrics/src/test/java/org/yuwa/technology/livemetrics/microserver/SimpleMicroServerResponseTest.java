/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jan 28, 2008 $
 */
package org.yuwa.technology.livemetrics.microserver;

import org.yuwa.technology.livemetrics.microserver.SimpleMicroServerHttpResponse;

import junit.framework.TestCase;


/**
 *
 * <p>
 * Jan 28, 2008
 *
 * @author royoung
 */
public class SimpleMicroServerResponseTest
extends TestCase
{
	private SimpleMicroServerHttpResponse response = new SimpleMicroServerHttpResponse();

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception
	{
		super.setUp();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.microserver.MicroServerHttpResponse#buildHeader(byte[])}.
	 */
	public void testBuildHeader()
	throws Exception
	{
		String encoding = "UTF-8";
		byte[] bytes = "woowoowoo".getBytes(encoding);
		this.response.setBytes(bytes);
		this.response.setContentType("text/woo");
		this.response.setDefeatCaching(true);
		this.response.setEncoding(encoding);
		this.response.setEncodingHeader("barglebargle");
		this.response.setHttpCode(1234);
		this.response.setHttpMessage("HUH");

		String header = this.response.buildHeader(bytes);
		assertEquals("HTTP/1.0 1234 HUH\r\n"
				+ "Content-Encoding: barglebargle\r\n"
				+ "Content-Type: text/woo\r\n"
				+ "Content-Length: 11\r\n"
				+ "Cache-control: no-cache\r\n"
				+ "Cache-control: no-store\r\n"
				+ "Pragma: no-cache\r\n"
				+ "Expires: 0\r\n"
				+ "\r\n",
				header);
	}
}
