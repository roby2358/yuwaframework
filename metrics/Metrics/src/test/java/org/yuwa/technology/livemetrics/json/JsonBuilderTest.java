/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package org.yuwa.technology.livemetrics.json;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.testtools.Woo;



/**
 *
 * <p>
 * Aug 13, 2007
 *
 * @author royoung
 */
public class JsonBuilderTest
extends TestCase
{
	/** our logger */
	public Logger log = Logger.getLogger(getClass());
	
	/** our builder */
	public JsonBuilder builder = new JsonBuilder();

	public void setUp() throws Exception
	{
	}
	
    // ~-~- Test Classes ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
    
    /**
     * I should really break this class up for a bunch of fine-grained tests
     */
    public static class TestJsonBuilderObject
    {
        public String a = "a!";
        public int b = 1;
        @SuppressWarnings("unused")
        private String c = "c!";
        private String d = "d!";
        private String e = "e!";
        @SuppressWarnings("unused")
        private String f = "f!";
        private SomeOther other0 = new SomeOther();
        public SomeOther other1 = new SomeOther();
        private String[] vars0 = new String[] { "a","b","c" };
        private List<String> vars1 = new ArrayList<String>( Arrays.asList(vars0) );
        public String nop = null;
        
        public String getA() { return this.a; }
        public void setA(String a) { this.a = a; }
        public int getB() { return this.b; }
        public void setB(int b) { this.b = b; }
        public String getD() { return this.d; }
        public void setD(String d) { this.d = d; }
        public String getE() { return this.e; }
        public void setF(String f) { this.f = f; }
        public SomeOther getOther0() { return this.other0; }
        public void setOther0(SomeOther other0) { this.other0 = other0; }
        public String[] getVars0() { return this.vars0; }
        public void setVars0(String[] vars0) { this.vars0 = vars0; }
        public List<String> getVars1() { return this.vars1; }
        public void setVars1(List<String> vars1) { this.vars1 = vars1; }
    }
    
    public static class Listorama
    {
        private List<SomeOther> others = new ArrayList<SomeOther>();

        public List<SomeOther> getOthers() { return this.others; }
        public void setOthers(List<SomeOther> others) { this.others = others;  }
        public void add(SomeOther so) { this.others.add(so); }
    }
    
    public static class SomeOther
    {
        private String name = "SomeOther";

        public String getName() { return this.name; }
        public void setName(String name) { this.name = name; }
        
        public String toString()
        {
            return "#SomeOther name=" + this.name + "#";
        }
    }
    
    public static class Another
    {
    	public int num1 = (int)Woo.small();
    	int num2 = (int)Woo.small();
    	@SuppressWarnings("unused")
		private int num3 = (int)Woo.small();
    	@SuppressWarnings("unused")
		private String name = "Another";
    	private String property = "property" + Woo.get();
    	private String zoop = "zoop" + Woo.get();

		public String getProperty() { return this.property; }
		public void setProperty(String property) { this.property = property; }
		public String getZoop() { return this.zoop; }
		public void setZoop(String zoop) { this.zoop = zoop; }
    }
    
    // ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	public void testJsonBuilder()
	{
        assertEquals("\n{}",this.builder.toString());
	}

	public void testCopy()
	throws Exception
	{
		Another object = new Another();
        JsonBuilder that = this.builder.copy();
        that.put(object);
        
        assertEquals( "\n{property:\""
        		+object.getProperty()
        		+"\",\nzoop:\""
        		+object.getZoop()
        		+"\"}",
        		that.toString() );

        this.builder.setNewlines(false);
        JsonBuilder that2 = this.builder.copy();
        that2.put(object);
        
        assertEquals( "{property:\""
        		+object.getProperty()
        		+"\",zoop:\""
        		+object.getZoop()
        		+"\"}",
        		that2.toString() );
        
        this.builder.setIncludeFields(true);
        JsonBuilder that3 = this.builder.copy();
        that3.put(object);
        
        assertEquals( "{num1:\""
        		+object.num1
        		+"\",property:\""
        		+object.getProperty()
        		+"\",zoop:\""
        		+object.getZoop()
        		+"\"}",
        		that3.toString() );
	}
	
    public void testPut()
    {
        builder.put("a","b");
        builder.put("b",1);
        builder.put("c",1.01f);
        
        log.debug(builder.toString());
        
        assertEquals( "\n{a:\"b\",\nb:\"1\",\nc:\"1.01\"}",builder.toString() );
    }
    
    public void testPutAdd()
    {
    	// hm... I guess if they want to put one first, then add another one,
		// we'll treat it as a list
    	builder.put("a","b");
    	builder.add("a","c");
        assertEquals( "\n{a:[\"b\",\"c\"]}",builder.toString() );
    }
    
    public void testAddPut()
    {
    	// clobber the list
    	builder.add("a","c");
    	builder.put("a","b");
        assertEquals( "\n{a:\"b\"}",builder.toString() );
    }
    
    public void testPutAddPut()
    {
    	// clobber the list
    	builder.put("a","b");
    	builder.add("a","c");
    	builder.put("a","d");
        assertEquals( "\n{a:\"d\"}",builder.toString() );
    }

    public void testPutStringObject()
    {
        SomeOther so = new SomeOther();
        so.setName("woo");
        
        this.builder.put("woo",so);
        
        assertEquals("\n{woo:#SomeOther name=woo#}",this.builder.toString());
    }

    public void testPutStringBuilder()
    throws Exception
    {
        JsonBuilder b = new JsonBuilder();
        b.put("hi","there");
        
        this.builder.put("wa","woo");
        
        assertEquals("\n{wa:\"woo\"}",this.builder.toString());
    }
    
    public void testPutNull()
    {
        builder.put("a","b");
        builder.put("b",null);
        builder.put("c",1.01f);
        
        log.debug(builder.toString());
        
        assertEquals( "\n{a:\"b\",\nb:null,\nc:\"1.01\"}",builder.toString() );
    }
    
    public void testPutMixed()
    {
        JsonBuilder another = new JsonBuilder();
        another.put("yip","eeeee");
        
        builder.put("a","b");
        builder.put("b",another);
        builder.put("c",1.01f);
        
        log.debug(builder.toString());
        
        assertEquals( "\n{a:\"b\",\nb:\n{yip:\"eeeee\"},\nc:\"1.01\"}",builder.toString() );
    }

    /**
     * @throws Exception
     */
    public void testPutNestedObjects()
    throws Exception
    {
        JsonBuilder b0 = new JsonBuilder();
        JsonBuilder b1 = new JsonBuilder();
        JsonBuilder b2 = new JsonBuilder();
        
        b0.put("name","b0");
        b1.put("name","b1");
        b2.put("name","b2");
        
        b0.put("next",b1);
        b1.put("next",b2);
        
        this.builder.put("next",b0);
        
        log.debug(this.builder.toString());
        
        assertEquals("\n{next:\n{name:\"b0\",\nnext:\n{name:\"b1\",\nnext:\n{name:\"b2\"}}}}",this.builder.toString());
    }
    
    public void testAdd()
    {
        this.builder.add("a", 1);
        this.builder.add("a", 2);
        this.builder.add("a", 3);
        this.builder.add("a", 4);
        this.builder.add("a", 5);
        
        log.debug( this.builder.toString() );
        
        assertEquals("\n{a:[\"1\",\"2\",\"3\",\"4\",\"5\"]}",this.builder.toString());
    }

    public void testAddNulls()
    {
        this.builder.add("a", "a");
        this.builder.add("a", null);
        this.builder.add("a", "b");
        this.builder.add("a", null);
        this.builder.add("a", "c");
        
        log.debug( this.builder.toString() );

        assertEquals("\n{a:[\"a\",null,\"b\",null,\"c\"]}",this.builder.toString());
    }

    public void testAddMixed()
    {
        JsonBuilder another = new JsonBuilder();
        another.put("woo","wa");
        
        this.builder.add("a", 1);
        this.builder.add("a", "woo");
        this.builder.add("a", 3);
        this.builder.add("a", another);
        this.builder.add("a", 5);
        
        log.debug( this.builder.toString() );
        
        assertEquals("\n{a:[\"1\",\"woo\",\"3\",\n{woo:\"wa\"},\"5\"]}",this.builder.toString());
    }

    public void testAddBuilders()
    throws Exception
    {
        for ( int i = 0 ; i < 3 ; i++ )
        {
            SomeOther s = new SomeOther();
            s.setName("so" + i);
            JsonBuilder b = new JsonBuilder(s);
            this.builder.add("zing",b);
        }
        
        log.debug(this.builder.toString());
        
        assertEquals("\n{zing:[\n{name:\"so0\"},\n{name:\"so1\"},\n{name:\"so2\"}]}",this.builder.toString());
    }

    /**
     * @throws Exception
     */
    public void testAddNestedObjects()
    throws Exception
    {
        JsonBuilder b0 = new JsonBuilder();
        JsonBuilder b1 = new JsonBuilder();
        JsonBuilder b2 = new JsonBuilder();
        
        b0.put("name","b0");
        b1.put("name","b1");
        b2.put("name","b2");
        
        b0.add("next",b1);
        b1.add("next",b2);
        
        this.builder.add("next",b0);
        
        log.debug(this.builder.toString());
        
        assertEquals("\n{next:[\n{name:\"b0\",\nnext:[\n{name:\"b1\",\nnext:[\n{name:\"b2\"}]}]}]}",this.builder.toString());
    }
    
    public void testJsonBuilderObject()
    throws Exception
    {
        TestJsonBuilderObject o = new TestJsonBuilderObject();
        
        this.builder = new JsonBuilder(o);
        
        assertEquals("\n{a:\"a!\",\nb:\"1\",\nd:\"d!\",\nvars1:[\"a\",\"b\",\"c\"]}",this.builder.toString());
    }

    /**
     * just for fun.. let's see what happens
     * @throws Exception
     */
    public void testAddObjects()
    throws Exception
    {
        for ( int i = 0 ; i < 3 ; i++ )
        {
            SomeOther s = new SomeOther();
            s.setName("so" + i);
            this.builder.add("so",s);
        }
        
        log.debug(this.builder.toString());
        
        assertEquals("\n{so:[#SomeOther name=so0#,#SomeOther name=so1#,#SomeOther name=so2#]}",this.builder.toString());
    }

    /**
     * @throws Exception
     */
    public void testNestedObjectLists()
    throws Exception
    {
        this.builder.add("null",null);
        this.builder.add("zing","zing");
        this.builder.add("so",new SomeOther());
        this.builder.add("soj",new JsonBuilder(new SomeOther()));
        
        log.debug(this.builder.toString());
        
        assertEquals("\n{null:[null],\nzing:[\"zing\"],\nso:[#SomeOther name=SomeOther#],\nsoj:[\n{name:\"SomeOther\"}]}",this.builder.toString());
    }

    /**
     * @throws Exception
     */
    public void testNestedObjectListsNewLines()
    throws Exception
    {
    	this.builder.setNewlines(true);
    	
        this.builder.add("null",null);
        this.builder.add("zing","zing");
        this.builder.add("so",new SomeOther());
        this.builder.add("soj",new JsonBuilder(new SomeOther()));
        
        log.debug(this.builder.toString());
        
        assertEquals("\n{null:[null],\nzing:[\"zing\"],\nso:[#SomeOther name=SomeOther#],\nsoj:[\n{name:\"SomeOther\"}]}",this.builder.toString());
    }

	/**
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public void testPutObject()
	throws IllegalAccessException, InvocationTargetException
	{
		TestJsonBuilderObject o = new TestJsonBuilderObject();
		
		this.builder.put(o);
		
		assertEquals("\n{a:\"a!\",\nb:\"1\",\nd:\"d!\",\nvars1:[\"a\",\"b\",\"c\"]}",this.builder.toString());
	}

	/**
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public void testPutObjectFields()
	throws IllegalAccessException, InvocationTargetException
	{
		this.builder.setIncludeFields(true);
		this.builder.setIncludeNulls(true);
		
		TestJsonBuilderObject o = new TestJsonBuilderObject();
		
		this.builder.put(o);
		
		log.debug( this.builder.toString() );
		
		assertEquals("\n{a:\"a!\",\nb:\"1\",\nd:\"d!\",\nnop:null,\nvars1:[\"a\",\"b\",\"c\"]}",this.builder.toString());
	}

	/**
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public void testPutObjectFieldsNoNulls()
	throws IllegalAccessException, InvocationTargetException
	{
		this.builder.setIncludeFields(true);
		this.builder.setIncludeNulls(false);
		
		TestJsonBuilderObject o = new TestJsonBuilderObject();
		
		this.builder.put(o);
		
		log.debug( this.builder.toString() );
		
		assertEquals("\n{a:\"a!\",\nb:\"1\",\nd:\"d!\",\nvars1:[\"a\",\"b\",\"c\"]}",this.builder.toString());
	}
	
	public void testToString()
	{
		// hm.. we're working this pretty hard already
	}

	public void testGetScalars()
	{
		assertNotNull(this.builder.getScalars());
        assertTrue(this.builder.getScalars().size() > 0);
		
		// TODO: test the individual values?
	}
	
	public void testAddEmptyList()
	{
		this.builder.addEmptyList("empty");

		assertEquals("\n{empty:[]}",this.builder.toString());
	}
	
	public void testAddNotSoEmptyList()
	{
		this.builder.addEmptyList("notsoempty");
		this.builder.add("notsoempty","something");

		assertEquals("\n{notsoempty:[\"something\"]}",this.builder.toString());
	}
	
	public void testEncodeQuotes()
	{
		this.builder.put("zoom", "zot'zot\"zot");
		assertEquals("\n{zoom:\"zot'zot\\\"zot\"}",this.builder.toString());
	}
	
	
	public void testEmbedded0()
	{
		this.builder.put("aaa","bbb");
		assertEquals("eval('(\n{aaa:\"bbb\"})')",this.builder.getEmbedded());
	}
	
	public void testEmbeddedEncodeQuotes()
	{
		this.builder.put("zoom", "zot'zot\"zot'zot\"zot");
		assertEquals("eval('(\n{zoom:\"zot\\\\\\'zot\\\\\\\"zot\\\\\\'zot\\\\\\\"zot\"})')",this.builder.getEmbedded());
	}
}
