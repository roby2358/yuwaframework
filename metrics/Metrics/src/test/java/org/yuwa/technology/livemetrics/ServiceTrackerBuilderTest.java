/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Apr 10, 2008 $
 */
package org.yuwa.technology.livemetrics;

import java.util.Arrays;
import java.util.List;

import org.yuwa.technology.livemetrics.MillisecondMetricTimer;
import org.yuwa.technology.livemetrics.ServiceTracker;
import org.yuwa.technology.livemetrics.ServiceTrackerBuilder;
import org.yuwa.technology.livemetrics.Timer;
import org.yuwa.technology.livemetrics.model.Component;

import junit.framework.TestCase;


/**
 *
 * <p>
 * Apr 10, 2008
 *
 * @author royoung
 */
public class ServiceTrackerBuilderTest extends TestCase
{
	public ServiceTrackerBuilder builder = new ServiceTrackerBuilder();
	
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception
	{
		this.builder.setUseListener(false);
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.ServiceTrackerBuilder#build()}.
	 */
	public void testBuildDefaultTimer()
	{
		ServiceTracker tracker = this.builder.build();
		Timer timer = tracker.startTimer("test");
		assertNotNull(timer);
		assertEquals("org.yuwa.technology.livemetrics.MicrosecondMetricTimer",timer.getClass().getName());
		timer.addMe();
		
		assertEquals(1,tracker.getService().getComponent("test").getMinutes().get(0).getCount());
	}

	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.ServiceTrackerBuilder#build()}.
	 */
	public void testBuildSetTimer()
	{
		this.builder.setProtoTimer(new MillisecondMetricTimer(ServiceTracker.NO_TRACKER,"elapsed"));
		ServiceTracker tracker = this.builder.build();
		Timer timer = tracker.startTimer("test");
		assertNotNull(timer);
		assertEquals("org.yuwa.technology.livemetrics.MillisecondMetricTimer",timer.getClass().getName());
		timer.addMe();
		
		assertEquals(1,tracker.getService().getComponent("test").getMinutes().get(0).getCount());
		assertEquals(0,tracker.getService().getComponent("zing").getMinutes().get(0).getCount());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParseHistogramConfigSuccess()
	{
		List<String> histogramComponents = Arrays.asList("something : 11 - 123 / 456");
		this.builder.setHistogramComponents(histogramComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertTrue(c.isReportHistogram());
		assertTrue(c.isReportPercentiles());
		assertNotNull(c.getMinuteHistogram());
		assertNotNull(c.getHourHistogram());
		assertNotNull(c.getDayHistogram());
		
		assertEquals(11,c.getMinuteHistogram().getMin());
		assertEquals(123,c.getMinuteHistogram().getMax());
		assertEquals(456,c.getMinuteHistogram().getBuckets());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParseHistogramConfigSuccessSpaces()
	{
		List<String> histogramComponents = Arrays.asList("                 something            :   11    -  123      /    456     ");
		this.builder.setHistogramComponents(histogramComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertTrue(c.isReportHistogram());
		assertTrue(c.isReportPercentiles());
		assertNotNull(c.getMinuteHistogram());
		assertNotNull(c.getHourHistogram());
		assertNotNull(c.getDayHistogram());
		
		assertEquals(11,c.getMinuteHistogram().getMin());
		assertEquals(123,c.getMinuteHistogram().getMax());
		assertEquals(456,c.getMinuteHistogram().getBuckets());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParseHistogramConfigSuccessNoSpaces()
	{
		List<String> histogramComponents = Arrays.asList("something:11-123/456");
		this.builder.setHistogramComponents(histogramComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertTrue(c.isReportHistogram());
		assertTrue(c.isReportPercentiles());
		assertNotNull(c.getMinuteHistogram());
		assertNotNull(c.getHourHistogram());
		assertNotNull(c.getDayHistogram());
		
		assertEquals(11,c.getMinuteHistogram().getMin());
		assertEquals(123,c.getMinuteHistogram().getMax());
		assertEquals(456,c.getMinuteHistogram().getBuckets());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParseHistogramConfigFailComponent()
	{
		List<String> histogramComponents = Arrays.asList(" : 11 - 123 / 456");
		this.builder.setHistogramComponents(histogramComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertFalse(c.isReportPercentiles());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParseHistogramConfigFailMin()
	{
		List<String> histogramComponents = Arrays.asList("something : - 123 / 456");
		this.builder.setHistogramComponents(histogramComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertFalse(c.isReportPercentiles());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParseHistogramConfigFailMinBad()
	{
		List<String> histogramComponents = Arrays.asList("something : ABC - 123 / 456");
		this.builder.setHistogramComponents(histogramComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertFalse(c.isReportPercentiles());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParseHistogramConfigFailMax()
	{
		List<String> histogramComponents = Arrays.asList("something : 11 - / 456");
		this.builder.setHistogramComponents(histogramComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertFalse(c.isReportPercentiles());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParseHistogramConfigFailBuckets()
	{
		List<String> histogramComponents = Arrays.asList("something : 11 - 123 / ");
		this.builder.setHistogramComponents(histogramComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertFalse(c.isReportPercentiles());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParsePercentilesConfigSuccess()
	{
		List<String> percentileComponents = Arrays.asList("something : 11 - 123 / 456");
		this.builder.setPercentileComponents(percentileComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertTrue(c.isReportPercentiles());
		assertNotNull(c.getMinuteHistogram());
		assertNotNull(c.getHourHistogram());
		assertNotNull(c.getDayHistogram());
		
		assertEquals(11,c.getMinuteHistogram().getMin());
		assertEquals(123,c.getMinuteHistogram().getMax());
		assertEquals(456,c.getMinuteHistogram().getBuckets());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParsePercentilesConfigSuccessSpaces()
	{
		List<String> percentileComponents = Arrays.asList("                 something            :   11    -  123      /    456     ");
		this.builder.setPercentileComponents(percentileComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertTrue(c.isReportPercentiles());
		assertNotNull(c.getMinuteHistogram());
		assertNotNull(c.getHourHistogram());
		assertNotNull(c.getDayHistogram());
		
		assertEquals(11,c.getMinuteHistogram().getMin());
		assertEquals(123,c.getMinuteHistogram().getMax());
		assertEquals(456,c.getMinuteHistogram().getBuckets());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParsePercentilesConfigSuccessNoSpaces()
	{
		List<String> percentileComponents = Arrays.asList("something:11-123/456");
		this.builder.setPercentileComponents(percentileComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertTrue(c.isReportPercentiles());
		assertNotNull(c.getMinuteHistogram());
		assertNotNull(c.getHourHistogram());
		assertNotNull(c.getDayHistogram());
		
		assertEquals(11,c.getMinuteHistogram().getMin());
		assertEquals(123,c.getMinuteHistogram().getMax());
		assertEquals(456,c.getMinuteHistogram().getBuckets());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParsePercentilesConfigFailComponent()
	{
		List<String> percentileComponents = Arrays.asList(" : 11 - 123 / 456");
		this.builder.setPercentileComponents(percentileComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertFalse(c.isReportPercentiles());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParsePercentilesConfigFailMin()
	{
		List<String> percentileComponents = Arrays.asList("something : - 123 / 456");
		this.builder.setPercentileComponents(percentileComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertFalse(c.isReportPercentiles());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParsePercentilesConfigFailMinBad()
	{
		List<String> percentileComponents = Arrays.asList("something : ABC - 123 / 456");
		this.builder.setPercentileComponents(percentileComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertFalse(c.isReportPercentiles());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParsePercentilesConfigFailMax()
	{
		List<String> percentileComponents = Arrays.asList("something : 11 - / 456");
		this.builder.setPercentileComponents(percentileComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertFalse(c.isReportPercentiles());
	}

	/**
	 * build a service with a histogram
	 */
	public void testParsePercentilesConfigFailBuckets()
	{
		List<String> percentileComponents = Arrays.asList("something : 11 - 123 / ");
		this.builder.setPercentileComponents(percentileComponents);
		ServiceTracker tracker = this.builder.build();
		
		Component c = tracker.getService().getComponent("something");
		assertNotNull(c);
		assertEquals("something",c.getName());
		assertFalse(c.isReportHistogram());
		assertFalse(c.isReportPercentiles());
	}
}
