/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Oct 27, 2008 $
 */
package org.yuwa.technology.livemetrics.model;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.yuwa.technology.livemetrics.testtools.Woo;



/**
 * 
 * <p>
 * Oct 27, 2008
 * 
 * @author royoung
 */
public class HistogramTest extends TestCase
{
	public void testMin()
	{
		long min = (int) Woo.small();
		Histogram h = new SimpleHistogram().setMin(min);

		Assert.assertEquals(min, h.getMin());
	}


	public void testMax()
	{
		long max = (int) Woo.small();
		Histogram h = new SimpleHistogram().setMax(max);

		Assert.assertEquals(max, h.getMax());
	}


	public void testBuckets()
	{
		int size = (int) Woo.get() % 1000 + 100;
		Histogram h = new SimpleHistogram().setBuckets(size);

		Assert.assertEquals(size, h.getBuckets());
	}


	public void testNoBuckets()
	{
		Histogram h = new SimpleHistogram();

		Assert.assertEquals(0, h.getBuckets());
	}


	public void testAddSimple()
	{
		Histogram h = new SimpleHistogram().setMin(0L).setMax(100L).setBuckets(10);

		h.add(0);
		h.add(9);
		h.add(10);
		h.add(49);
		h.add(50);
		h.add(51);
		h.add(89);
		h.add(90);
		h.add(91);
		h.add(100);
		h.add(10000);

		Assert.assertEquals(10, h.getValues().length);

		Assert.assertEquals(2, h.getValues()[0]);
		Assert.assertEquals(1, h.getValues()[1]);
		Assert.assertEquals(0, h.getValues()[2]);
		Assert.assertEquals(0, h.getValues()[3]);
		Assert.assertEquals(1, h.getValues()[4]);
		Assert.assertEquals(2, h.getValues()[5]);
		Assert.assertEquals(0, h.getValues()[6]);
		Assert.assertEquals(0, h.getValues()[7]);
		Assert.assertEquals(1, h.getValues()[8]);
		Assert.assertEquals(4, h.getValues()[9]);
	}


	public void testAddSimple2()
	{
		Histogram h = new SimpleHistogram().setMin(5L).setMax(100L).setBuckets(10);

		h.add(0);
		h.add(9);
		h.add(10);
		h.add(15);
		h.add(49);
		h.add(50);
		h.add(51);
		h.add(89);
		h.add(90);
		h.add(91);
		h.add(100);
		h.add(10000);

		Assert.assertEquals(10, h.getValues().length);

		Assert.assertEquals(3, h.getValues()[0]);
		Assert.assertEquals(1, h.getValues()[1]);
		Assert.assertEquals(0, h.getValues()[2]);
		Assert.assertEquals(0, h.getValues()[3]);
		Assert.assertEquals(3, h.getValues()[4]);
		Assert.assertEquals(0, h.getValues()[5]);
		Assert.assertEquals(0, h.getValues()[6]);
		Assert.assertEquals(0, h.getValues()[7]);
		Assert.assertEquals(2, h.getValues()[8]);
		Assert.assertEquals(3, h.getValues()[9]);
	}


	public void testAddSimple3()
	{
		Histogram h = new SimpleHistogram().setMin(5L).setMax(100L).setBuckets(7);

		h.add(0);
		h.add(9);
		h.add(10);
		h.add(15);

		h.add(32);
		h.add(33);

		h.add(46);
		h.add(47);

		h.add(51);
		h.add(89);
		h.add(90);
		h.add(91);
		h.add(100);
		h.add(10000);

		Assert.assertEquals(7, h.getValues().length);

		Assert.assertEquals(4, h.getValues()[0]);
		Assert.assertEquals(1, h.getValues()[1]);
		Assert.assertEquals(1, h.getValues()[2]);
		Assert.assertEquals(3, h.getValues()[3]);
		Assert.assertEquals(0, h.getValues()[4]);
		Assert.assertEquals(0, h.getValues()[5]);
		Assert.assertEquals(5, h.getValues()[6]);
	}


	public void testAddNegative0()
	{
		Histogram h = new SimpleHistogram()
		.setMin(5L)
		.setMax(100L)
		.setBuckets(7);
		
		h.add(Long.MIN_VALUE);
		h.add(-40000L);
		h.add(-300L);
		h.add(-1L);
		h.add(0L);

		Assert.assertEquals(7, h.getValues().length);

		Assert.assertEquals(5, h.getValues()[0]);
		Assert.assertEquals(0, h.getValues()[1]);
		Assert.assertEquals(0, h.getValues()[2]);
		Assert.assertEquals(0, h.getValues()[3]);
		Assert.assertEquals(0, h.getValues()[4]);
		Assert.assertEquals(0, h.getValues()[5]);
		Assert.assertEquals(0, h.getValues()[6]);
	}


	public void testAddMax0()
	{
		Histogram h = new SimpleHistogram()
		.setMin(5L)
		.setMax(100L)
		.setBuckets(7);

		h.add(100L);
		h.add(101L);
		h.add(10000L);
		h.add(100000L);
		h.add(Long.MAX_VALUE);

		Assert.assertEquals(7, h.getValues().length);

		Assert.assertEquals(0, h.getValues()[0]);
		Assert.assertEquals(0, h.getValues()[1]);
		Assert.assertEquals(0, h.getValues()[2]);
		Assert.assertEquals(0, h.getValues()[3]);
		Assert.assertEquals(0, h.getValues()[4]);
		Assert.assertEquals(0, h.getValues()[5]);
		Assert.assertEquals(5, h.getValues()[6]);
	}


	public void testGetEnd10()
	{
		Histogram h = new SimpleHistogram().setMin(5L).setMax(100L).setBuckets(10);

		Assert.assertEquals(14L, h.getEndValue(0));
		Assert.assertEquals(24L, h.getEndValue(1));
		Assert.assertEquals(33L, h.getEndValue(2));
		Assert.assertEquals(43L, h.getEndValue(3));
		Assert.assertEquals(52L, h.getEndValue(4));
		Assert.assertEquals(62L, h.getEndValue(5));
		Assert.assertEquals(71L, h.getEndValue(6));
		Assert.assertEquals(81L, h.getEndValue(7));
		Assert.assertEquals(90L, h.getEndValue(8));
		Assert.assertEquals(100L, h.getEndValue(9));
		Assert.assertEquals(109L, h.getEndValue(10));
		Assert.assertEquals(119L, h.getEndValue(11));
	}


	public void testGetEnd7()
	{
		Histogram h = new SimpleHistogram().setMax(100L).setBuckets(7);

		Assert.assertEquals(14L, h.getEndValue(0));
		Assert.assertEquals(28L, h.getEndValue(1));
		Assert.assertEquals(42L, h.getEndValue(2));
		Assert.assertEquals(57L, h.getEndValue(3));
		Assert.assertEquals(71L, h.getEndValue(4));
		Assert.assertEquals(85L, h.getEndValue(5));
		Assert.assertEquals(100L, h.getEndValue(6));
		Assert.assertEquals(114L, h.getEndValue(7));

		h.add(h.getEndValue(0));
		h.add(h.getEndValue(0) + 1);
		h.add(h.getEndValue(1));
		h.add(h.getEndValue(1) + 1);
		h.add(h.getEndValue(2));
		h.add(h.getEndValue(2) + 1);
		h.add(h.getEndValue(3));
		h.add(h.getEndValue(3) + 1);
		h.add(h.getEndValue(4));
		h.add(h.getEndValue(4) + 1);
		h.add(h.getEndValue(5));
		h.add(h.getEndValue(5) + 1);
		h.add(h.getEndValue(6));
		h.add(h.getEndValue(6) + 1);
		h.add(100);
		h.add(10000);

		Assert.assertEquals(7, h.getValues().length);

		Assert.assertEquals(1, h.getValues()[0]);
		Assert.assertEquals(2, h.getValues()[1]);
		Assert.assertEquals(2, h.getValues()[2]);
		Assert.assertEquals(2, h.getValues()[3]);
		Assert.assertEquals(2, h.getValues()[4]);
		Assert.assertEquals(2, h.getValues()[5]);
		Assert.assertEquals(5, h.getValues()[6]);
	}


	public void testGetEnd7min()
	{
		Histogram h = new SimpleHistogram()
		.setMin(5L)
		.setMax(100L)
		.setBuckets(7);

		Assert.assertEquals(18L, h.getEndValue(0));
		Assert.assertEquals(32L, h.getEndValue(1));
		Assert.assertEquals(45L, h.getEndValue(2));
		Assert.assertEquals(59L, h.getEndValue(3));
		Assert.assertEquals(72L, h.getEndValue(4));
		Assert.assertEquals(86L, h.getEndValue(5));
		Assert.assertEquals(100L, h.getEndValue(6));
		Assert.assertEquals(113L, h.getEndValue(7));

		h.add(h.getEndValue(0));
		h.add(h.getEndValue(0) + 1);
		h.add(h.getEndValue(1));
		h.add(h.getEndValue(1) + 1);
		h.add(h.getEndValue(2));
		h.add(h.getEndValue(2) + 1);
		h.add(h.getEndValue(3));
		h.add(h.getEndValue(3) + 1);
		h.add(h.getEndValue(4));
		h.add(h.getEndValue(4) + 1);
		h.add(h.getEndValue(5));
		h.add(h.getEndValue(5) + 1);
		h.add(h.getEndValue(6));
		h.add(h.getEndValue(6) + 1);
		h.add(105);
		h.add(10000);

		Assert.assertEquals(7, h.getValues().length);

		Assert.assertEquals(1, h.getValues()[0]);
		Assert.assertEquals(2, h.getValues()[1]);
		Assert.assertEquals(2, h.getValues()[2]);
		Assert.assertEquals(2, h.getValues()[3]);
		Assert.assertEquals(2, h.getValues()[4]);
		Assert.assertEquals(2, h.getValues()[5]);
		Assert.assertEquals(5, h.getValues()[6]);
	}


	public void testApproximatePercentiles1()
	{
		Histogram h = new SimpleHistogram()
		.setMax(100L)
		.setBuckets(10);

		for (long i = 0L; i < 1000L; i++)
		{
			h.add(i / 10L);
		}

		long[] percentiles = h.approximatePercentileValues();

		Assert.assertEquals(12, percentiles.length);

		Assert.assertEquals(10, percentiles[0]);
		Assert.assertEquals(20, percentiles[1]);
		Assert.assertEquals(30, percentiles[2]);
		Assert.assertEquals(40, percentiles[3]);
		Assert.assertEquals(50, percentiles[4]);
		Assert.assertEquals(60, percentiles[5]);
		Assert.assertEquals(70, percentiles[6]);
		Assert.assertEquals(80, percentiles[7]);
		Assert.assertEquals(90, percentiles[8]);
		Assert.assertEquals(100, percentiles[9]);
		Assert.assertEquals(100, percentiles[10]);
		Assert.assertEquals(100, percentiles[11]);
	}


	public void testApproximatePercentiles2()
	{
		Histogram h = new SimpleHistogram()
		.setMax(100L)
		.setBuckets(10);

		h.add(0L);
		h.add(10L);
		h.add(20L);
		h.add(30L);
		h.add(40L);

		long[] percentiles = h.approximatePercentileValues();

		Assert.assertEquals(12, percentiles.length);

		Assert.assertEquals(10, percentiles[0]);
		Assert.assertEquals(10, percentiles[1]);
		Assert.assertEquals(20, percentiles[2]);
		Assert.assertEquals(20, percentiles[3]);
		Assert.assertEquals(30, percentiles[4]);
		Assert.assertEquals(30, percentiles[5]);
		Assert.assertEquals(40, percentiles[6]);
		Assert.assertEquals(40, percentiles[7]);
		Assert.assertEquals(50, percentiles[8]);
		Assert.assertEquals(50, percentiles[9]);
		Assert.assertEquals(50, percentiles[10]);
		Assert.assertEquals(50, percentiles[11]);
	}


	public void testApproximatePercentiles3()
	{
		Histogram h = new SimpleHistogram()
		.setMax(100L)
		.setBuckets(10);

		for ( int i = 0 ; i < 10 ; i++ ) h.add(0L);
		for ( int i = 0 ; i < 4 ; i++ ) h.add(10L);
		for ( int i = 0 ; i < 3 ; i++ ) h.add(20L);
		for ( int i = 0 ; i < 2 ; i++ ) h.add(30L);
		for ( int i = 0 ; i < 1 ; i++ ) h.add(40L);

		long[] percentiles = h.approximatePercentileValues();

		Assert.assertEquals(12, percentiles.length);

		Assert.assertEquals(10, percentiles[0]);
		Assert.assertEquals(10, percentiles[1]);
		Assert.assertEquals(10, percentiles[2]);
		Assert.assertEquals(10, percentiles[3]);
		Assert.assertEquals(10, percentiles[4]);
		Assert.assertEquals(20, percentiles[5]);
		Assert.assertEquals(20, percentiles[6]);
		Assert.assertEquals(30, percentiles[7]);
		Assert.assertEquals(40, percentiles[8]);
		Assert.assertEquals(50, percentiles[9]);
		Assert.assertEquals(50, percentiles[10]);
		Assert.assertEquals(50, percentiles[11]);
	}


	public void testApproximatePercentiles4()
	{
		Histogram h = new SimpleHistogram()
		.setMax(1000L)
		.setBuckets(70);

		// even distribution
		for (long i = 0L; i < 10000L; i++)
		{
			h.add(i / 10L);
		}

		long[] percentiles = h.approximatePercentileValues();

		Assert.assertEquals(12, percentiles.length);
		Assert.assertEquals(100, percentiles[0]);
		Assert.assertEquals(200, percentiles[1]);
		Assert.assertEquals(300, percentiles[2]);
		Assert.assertEquals(400, percentiles[3]);
		Assert.assertEquals(500, percentiles[4]);
		Assert.assertEquals(600, percentiles[5]);
		Assert.assertEquals(700, percentiles[6]);
		Assert.assertEquals(800, percentiles[7]);
		Assert.assertEquals(900, percentiles[8]);
		Assert.assertEquals(1000, percentiles[9]);
		Assert.assertEquals(1000, percentiles[10]);
		Assert.assertEquals(1000, percentiles[11]);
	}


	public void testApproximatePercentiles5()
	{
		Histogram h = new SimpleHistogram()
		.setMax(100L)
		.setBuckets(10);

		for ( int i = 0 ; i < 10 ; i++ ) h.add(100L);
		for ( int i = 0 ; i < 4 ; i++ ) h.add(90L);
		for ( int i = 0 ; i < 3 ; i++ ) h.add(80L);
		for ( int i = 0 ; i < 2 ; i++ ) h.add(70L);
		for ( int i = 0 ; i < 1 ; i++ ) h.add(60L);

		long[] percentiles = h.approximatePercentileValues();

		Assert.assertEquals(12, percentiles.length);

		Assert.assertEquals(80, percentiles[0]);
		Assert.assertEquals(90, percentiles[1]);
		Assert.assertEquals(90, percentiles[2]);
		Assert.assertEquals(100, percentiles[3]);
		Assert.assertEquals(100, percentiles[4]);
		Assert.assertEquals(100, percentiles[5]);
		Assert.assertEquals(100, percentiles[6]);
		Assert.assertEquals(100, percentiles[7]);
		Assert.assertEquals(100, percentiles[8]);
		Assert.assertEquals(100, percentiles[9]);
		Assert.assertEquals(100, percentiles[10]);
		Assert.assertEquals(100, percentiles[11]);
	}


	public void testApproximatePercentiles6()
	{
		Histogram h = new SimpleHistogram()
		.setMax(10000L)
		.setBuckets(1000);

		for ( int i = 0 ; i < 1000 ; i++ ) h.add(i);
		for ( int i = 1001 ; i < 10001 ; i += 10 ) h.add(i);

		long[] percentiles = h.approximatePercentileValues();

		Assert.assertEquals(12, percentiles.length);

		Assert.assertEquals(190, percentiles[0]);
		Assert.assertEquals(380, percentiles[1]);
		Assert.assertEquals(570, percentiles[2]);
		Assert.assertEquals(760, percentiles[3]);
		Assert.assertEquals(950, percentiles[4]);
		Assert.assertEquals(2400, percentiles[5]);
		Assert.assertEquals(4300, percentiles[6]);
		Assert.assertEquals(6200, percentiles[7]);
		Assert.assertEquals(8100, percentiles[8]);
		Assert.assertEquals(9810, percentiles[9]);
		Assert.assertEquals(9990, percentiles[10]);
		Assert.assertEquals(10000, percentiles[11]);
	}


	public void testApproximatePercentiles7()
	{
		Histogram h = new SimpleHistogram()
		.setMax(10000L)
		.setBuckets(1000);

		for ( int i = 0 ; i < 1000 ; i++ ) h.add(i);
		for ( int i = 1001 ; i < 6000 ; i += 10 ) h.add(i);

		long[] percentiles = h.approximatePercentileValues();

		Assert.assertEquals(12, percentiles.length);

		Assert.assertEquals(150, percentiles[0]);
		Assert.assertEquals(300, percentiles[1]);
		Assert.assertEquals(460, percentiles[2]);
		Assert.assertEquals(600, percentiles[3]);
		Assert.assertEquals(750, percentiles[4]);
		Assert.assertEquals(910, percentiles[5]);
		Assert.assertEquals(1500, percentiles[6]);
		Assert.assertEquals(3000, percentiles[7]);
		Assert.assertEquals(4500, percentiles[8]);
		Assert.assertEquals(5850, percentiles[9]);
		Assert.assertEquals(5990, percentiles[10]);
		Assert.assertEquals(6000, percentiles[11]);
	}

	public void testStartFresh()
	{
		Histogram h = new SimpleHistogram()
		.setMin(1000L)
		.setMax(10000L)
		.setBuckets(1000)
		.setPercentiles(new float[] { 10f,40f,50f,60f,70f,90f,99f });

		SimpleHistogram that = (SimpleHistogram)h.startFresh();
		
		Assert.assertEquals(10000L, that.getMax());
		Assert.assertEquals(1000L, that.getMin());
		Assert.assertEquals(1000, that.getBuckets());
		Assert.assertEquals(7, that.getPercentiles().length);
		Assert.assertEquals(1000, that.getValues().length);
		
		Assert.assertEquals(10f, that.getPercentiles()[0] );
		Assert.assertEquals(40f, that.getPercentiles()[1] );
		Assert.assertEquals(50f, that.getPercentiles()[2] );
		Assert.assertEquals(60f, that.getPercentiles()[3] );
		Assert.assertEquals(70f, that.getPercentiles()[4] );
		Assert.assertEquals(90f, that.getPercentiles()[5] );
		Assert.assertEquals(99f, that.getPercentiles()[6] );
		
		for ( long l : that.getValues() )
		{
			Assert.assertEquals(0L, l);
		}
	}
}
