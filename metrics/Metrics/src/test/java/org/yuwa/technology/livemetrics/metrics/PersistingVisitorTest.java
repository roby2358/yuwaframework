/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Jul 7, 2008 $
 */
package org.yuwa.technology.livemetrics.metrics;

import java.io.File;
import java.io.FileReader;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.yuwa.technology.livemetrics.json.JsonBinder;
import org.yuwa.technology.livemetrics.model.Component;
import org.yuwa.technology.livemetrics.model.MockMetricsVisitor;
import org.yuwa.technology.livemetrics.model.Period;
import org.yuwa.technology.livemetrics.model.Service;
import org.yuwa.technology.livemetrics.testtools.MockObjectTracer;
import org.yuwa.technology.livemetrics.testtools.Woo;


/**
 *
 * <p>
 * Jul 7, 2008
 *
 * @author royoung
 */

public class PersistingVisitorTest extends TestCase
{
	public Logger log = Logger.getLogger(getClass());

	public MockObjectTracer tracer = new MockObjectTracer();
	public MockMetricsVisitor mock = new MockMetricsVisitor(tracer);

	public long now = System.currentTimeMillis();

	public PersistingVisitor visitor = new PersistingVisitor();
	public String path = "./test.deleteme." + Woo.get() + ".txt";

	public Service service = new Service();
	public String serviceName = "service" + Woo.get();

	public Component component = new Component();
	public String componentName = "componentName" + Woo.get();
	public long value = Woo.get();


	static
	{
		BasicConfigurator.configure();
	}

	public void setUp()
	{
		visitor.setErrorTolerance(5);
		visitor.setPath(path);
		this.visitor.setLast(now);
		visitor.setPeriod(100L);

		service.setName(serviceName);
		component.setName(componentName );
	}

	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.metrics.PersistingVisitor#isStale(long)}.
	 */
	public void testIsStale()
	{
		assertFalse(this.visitor.isStale(now - 1L));
		assertFalse(this.visitor.isStale(now + 0L));
		assertFalse(this.visitor.isStale(now + 99L));
		assertFalse(this.visitor.isStale(now + 100L));
		assertTrue(this.visitor.isStale(now + 101L));
	}

	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.metrics.PersistingVisitor#visit(org.yuwa.technology.livemetrics.model.Service)}.
	 */
	public void testVisitServiceNotStale()
	throws Exception
	{
		this.visitor.setPeriod(10000L);

		this.service.add(componentName,value);
		this.service.accept(this.visitor);
		
		Thread.sleep(100L);

		File file = new File(this.path);
		assertFalse(file.exists());
	}

	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.metrics.PersistingVisitor#visit(org.yuwa.technology.livemetrics.model.Service)}.
	 */
	@SuppressWarnings("unchecked")
	public void testVisitService()
	throws Exception
	{
		this.visitor.setPeriod(-10000L);
		this.visitor.start();

		this.service.add(componentName,value);
		this.service.accept(this.visitor);

		long start = System.nanoTime() / 1000L;
		log.debug("ok at " + start);
		
		// wait for the file to be created
		synchronized ( this.visitor.myWriter )
		{
			// wait
			long stop = System.nanoTime() / 1000L;
			log.debug("ready at " + stop + " ... " + ( stop - start ) + " micros" );
		}
		
		Thread.sleep(100L);

		File file = new File(this.path);
		FileReader reader = new FileReader(file);
		char[] buffer = new char[ (int)file.length() ];
		reader.read(buffer);
		String json = new String(buffer);
		this.log.debug("got " + json);
		JsonBinder binder = new JsonBinder(json);

		Map<String,Object> info = binder.parse();
		assertNotNull(info);
		assertNotNull(info.get("name"));
		assertEquals(serviceName,info.get("name"));

		List<Object> components = (List<Object>)info.get("components");
		Map<String,Object> component1 = (Map<String,Object>)components.get(1);
		assertEquals(componentName,component1.get("name"));

		Map<String,Object> minutes = (Map<String,Object>)component1.get("minute");
		List<Object> total = (List<Object>)minutes.get("total");

		log.debug("******* " + value + " = " + total.get(0));
		assertEquals("" + value,total.get(0));

		// whew!
		
		this.visitor.stop();
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.metrics.PersistingVisitor#visit(org.yuwa.technology.livemetrics.model.Component)}.
	 */
	public void testVisitComponent()
	{
		Component c = new Component();
		c.accept(this.visitor);
		// how to assert it did nothing?
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.metrics.PersistingVisitor#visit(org.yuwa.technology.livemetrics.model.Period)}.
	 */
	public void testVisitPeriod()
	{
		Period p = new Period(now,Period.ONE_MINUTE);
		p.accept(this.visitor);
		// how to assert it did nothing?
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.metrics.PersistingVisitor#setJsonBuilder(org.yuwa.technology.livemetrics.json.JsonBuilder)}.
	 */
	public void testSetJsonBuilder()
	{
//		TODO: fail("Not yet implemented");
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.metrics.PersistingVisitor#setPeriod(long)}.
	 */
	public void testSetPeriod()
	{
		this.visitor.setPeriod(20L);
		assertFalse(this.visitor.isStale(now));
		assertFalse(this.visitor.isStale(now + 10L));
		assertFalse(this.visitor.isStale(now + 19L));
		assertFalse(this.visitor.isStale(now + 20L));
		assertTrue(this.visitor.isStale(now + 21L));
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.metrics.PersistingVisitor#setErrorTolerance(int)}.
	 */
	public void testSetErrorTolerance()
	{
//		TODO: fail("Not yet implemented");
		// hm... how to test? need to capture logging somehow
	}


	/**
	 * Test method for {@link org.yuwa.technology.livemetrics.metrics.PersistingVisitor#setNext(org.yuwa.technology.livemetrics.model.MetricVisitor)}.
	 */
	public void testSetNext()
	{
		this.visitor.setNext(mock);

		// make sure we're not stale
		this.visitor.setPeriod(10000000000L);

		Service s = new Service();
		String servername = "service" + Woo.get();
		s.setName(servername);

		Component c = new Component();
		String componentname = "component" + Woo.get();
		c.setName(componentname);

		Period p = new Period(now,Period.ONE_HOUR);

		s.accept(this.visitor);
		c.accept(this.visitor);
		p.accept(this.visitor);

		MockObjectTracer expected = new MockObjectTracer();
		expected.add("visit server " + servername);
		expected.add("visit component " + componentname);
		expected.add("visit period " + Period.ONE_HOUR);
		assertEquals(expected,tracer);
	}


	public void pause()
	{
		try
		{
			Thread.sleep(20L);
		}
		catch (InterruptedException e)
		{
			// ok
		}
	}
}
