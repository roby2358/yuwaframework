/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Aug 25, 2008 $
 */
package org.yuwa.technology.livemetrics;

import org.yuwa.technology.livemetrics.ServiceTracker;
import org.yuwa.technology.livemetrics.ServiceTrackerBuilder;
import org.yuwa.technology.livemetrics.Timer;
import org.yuwa.technology.livemetrics.mock.MockTimer;

import junit.framework.TestCase;


/**
 *
 * <p>
 * Aug 25, 2008
 *
 * @author royoung
 */
public class AbstractTimerTest extends TestCase
{
	public ServiceTracker tracker;

	public MockTimer mockTimer;

	public void setUp()
	{
		this.mockTimer = new MockTimer(ServiceTracker.NO_TRACKER,"mock");

		ServiceTrackerBuilder builder = new ServiceTrackerBuilder();
		builder.setProtoTimer(this.mockTimer);
		this.tracker = builder.build();
	}
	
	
	/**
	 * test the start
	 */
	public void testStartMock()
	{
		Timer timer = this.tracker.startTimer("woo");

		assertTrue(timer instanceof MockTimer);
		
		long woo = this.mockTimer.getTime();
		
		assertEquals(woo - 200,timer.start());
		assertEquals(300,timer.elapsed());
	}

	
	/**
	 * set getting the current time
	 */
	public void testGetTime()
	{
		Timer timer = this.tracker.startTimer("woo");

		assertTrue(timer instanceof MockTimer);
		
		// TODO I don't think we need this... just exercises the MockTimer
//		MockTimer mock = (MockTimer)timer;
//		
//		long woo = this.mockTimer.getTime();
//		
//		assertEquals(woo + 100,mock.getTime());
	}
}
