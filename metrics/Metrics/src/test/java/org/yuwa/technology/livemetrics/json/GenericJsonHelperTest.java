/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Nov 10, 2008 $
 */
package org.yuwa.technology.livemetrics.json;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.yuwa.technology.livemetrics.json.GenericJsonHelper;
import org.yuwa.technology.livemetrics.json.JsonBinder;

import junit.framework.TestCase;

/**
 *
 * <p>
 * Nov 10, 2008
 *
 * @author royoung
 */
public class GenericJsonHelperTest extends TestCase
{
	public static final String MAP_VALUE_1 = "{ map:{ value:1, woo:2, whee:3 } }";
	public static final String LIST_1_2_3_4 = "{ list:[1,2,3,4] }";
	public static final String VALUE_1 = "{ value:1, zero:0, hoo:ya }";
	public static final String DATE_1 = "{ date:\"2008/11/10 17:03:19.676 -0800\", zero:0, hoo:ya, empty:\"\" }";
	public static final String DATE_STRING = "2008/11/10 17:03:19.676 -0800";
	public static final long DATE_LONG = 1226365399676L;

	public GenericJsonHelper helper = new GenericJsonHelper();

	public SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS ZZZZ");

	public void testHas()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(VALUE_1);
		Map<String,Object> info = binder.parse();
		assertTrue( this.helper.has(info, "value") );
	}

	public void testHasNot()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(VALUE_1);
		Map<String,Object> info = binder.parse();
		assertFalse( this.helper.has(info, "nope") );
	}
	
	public void testGetListSuccess()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(LIST_1_2_3_4);
		Map<String,Object> info = binder.parse();
		List<Object> got = this.helper.getList(info, "list");
		assertNotNull(got);
		assertEquals(4,got.size());
		assertEquals("1",got.get(0));
		assertEquals("2",got.get(1));
		assertEquals("3",got.get(2));
		assertEquals("4",got.get(3));
	}

	public void testGetListNotFound()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(LIST_1_2_3_4);
		Map<String,Object> info = binder.parse();
		List<Object> got = this.helper.getList(info, "nope");
		assertNull(got);
	}


	public void testGetListStringFail()
	throws Exception
	{
		try
		{
			JsonBinder binder = new JsonBinder(VALUE_1);
			Map<String,Object> info = binder.parse();
			this.helper.getList(info, "value");
			fail("should have failed");
		}
		catch (ClassCastException e)
		{
			// ok
		}
	}


	public void testGetListMapFail()
	throws Exception
	{
		try
		{
			JsonBinder binder = new JsonBinder(MAP_VALUE_1);
			Map<String,Object> info = binder.parse();
			this.helper.getList(info, "map");
			fail("should have failed");
		}
		catch (ClassCastException e)
		{
			// ok
		}
	}


	public void testGetMapSuccess()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(MAP_VALUE_1);
		Map<String,Object> info = binder.parse();
		Map<String,Object> got = this.helper.getMap(info, "map");
		assertNotNull(got);
		assertEquals(3,got.size());
		assertEquals("1",got.get("value"));
		assertEquals("2",got.get("woo"));
		assertEquals("3",got.get("whee"));
	}


	public void testGetMapNotFound()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(MAP_VALUE_1);
		Map<String,Object> info = binder.parse();
		Map<String,Object> got = this.helper.getMap(info, "nope");
		assertNull(got);
	}


	public void testGetMapStringFail()
	throws Exception
	{
		try
		{
			JsonBinder binder = new JsonBinder(VALUE_1);
			Map<String,Object> info = binder.parse();
			this.helper.getMap(info, "value");
			fail("should have failed");
		}
		catch (ClassCastException e)
		{
			// ok
		}
	}


	public void testGetMapListFail()
	throws Exception
	{
		try
		{
			JsonBinder binder = new JsonBinder(LIST_1_2_3_4);
			Map<String,Object> info = binder.parse();
			this.helper.getMap(info, "list");
			fail("should have failed");
		}
		catch (ClassCastException e)
		{
			// ok
		}
	}


	public void testGetString()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(VALUE_1);
		Map<String,Object> info = binder.parse();
		String got = this.helper.getString(info, "value");
		assertEquals(got,"1");
	}


	public void testGetStringNotFound()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(VALUE_1);
		Map<String,Object> info = binder.parse();
		String got = this.helper.getString(info, "nope");
		assertEquals(got,"");
	}


	public void testGetStringListHm()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(LIST_1_2_3_4);
		Map<String,Object> info = binder.parse();
		String got = this.helper.getString(info, "list");
		assertEquals(got,"[1, 2, 3, 4]");
	}


	public void testGetStringMapHm()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(MAP_VALUE_1);
		Map<String,Object> info = binder.parse();
		String got = this.helper.getString(info, "map");
		assertEquals(got,"{value=1, whee=3, woo=2}");
	}

	
	public void testAsMap()
	{
		Map<String,String> woo = new HashMap<String,String>();
		Map<String,Object> got = this.helper.asMap(woo);
		assertSame(got,woo);
	}

	
	public void testAsMapNull()
	{
		Map<String,Object> got = this.helper.asMap(null);
		assertNull(got);
	}

	public void testAsLong()
	throws Exception
	{
		long l = this.helper.asLong("1234");
		assertEquals(1234,l);
	}


	public void testGetLong()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(VALUE_1);
		Map<String,Object> info = binder.parse();
		long l = this.helper.getLong(info,"value");
		assertEquals(1L,l);
	}


	public void testGetLongZeroMissing()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(VALUE_1);
		Map<String,Object> info = binder.parse();
		long l = this.helper.getLong(info,"nope");
		assertEquals(0L,l);
	}


	public void testGetLongZero()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(VALUE_1);
		Map<String,Object> info = binder.parse();
		long l = this.helper.getLong(info,"zero");
		assertEquals(0L,l);
	}


	public void testGetLongZeroBad()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(VALUE_1);
		Map<String,Object> info = binder.parse();
		long l = this.helper.getLong(info,"hoo");
		assertEquals(0L,l);
	}

	
	public void testAsDate()
	{

		String expected = dateFormat.format(new Date(DATE_LONG));
		assertEquals(expected,DATE_STRING);
		
		Date date = this.helper.asDate(dateFormat,DATE_STRING);
		assertEquals(DATE_STRING,dateFormat.format(date));
		assertEquals(DATE_LONG,date.getTime());
	}
	
	
	public void testAsDateBad()
	{
		Date date = this.helper.asDate(dateFormat,"nope");
		assertNull(date);
	}
	
	
	public void testAsDateNull()
	{
		Date date = this.helper.asDate(dateFormat,null);
		assertNull(date);
	}
	
	
	public void testAsDateEmpty()
	{
		Date date = this.helper.asDate(dateFormat,"");
		assertNull(date);
	}

	
	public void testGetDate()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(DATE_1);
		Map<String,Object> info = binder.parse();
		Date date = this.helper.getDate(info, "date", dateFormat);
		assertEquals(DATE_STRING,dateFormat.format(date));
		assertEquals(DATE_LONG,date.getTime());
	}
	
	
	public void testGetDateBad()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(DATE_1);
		Map<String,Object> info = binder.parse();
		Date date = this.helper.getDate(info, "zero", dateFormat);
		assertNull(date);
	}
	
	
	public void testGetDateNull()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(DATE_1);
		Map<String,Object> info = binder.parse();
		Date date = this.helper.getDate(info, "nope", dateFormat);
		assertNull(date);
	}
	
	
	public void testGetDateEmpty()
	throws Exception
	{
		JsonBinder binder = new JsonBinder(DATE_1);
		Map<String,Object> info = binder.parse();
		Date date = this.helper.getDate(info, "empty", dateFormat);
		assertNull(date);
	}
}
