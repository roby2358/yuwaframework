/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Nov 25, 2008 $
 */
package org.yuwa.livemetrics.technology.lab;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

import org.yuwa.technology.livemetrics.Timer;


/**
 * a plain worker just does nothing over and over with no instrumentation
 * 
 * @author royoung
 */
public class PlainWorker
implements Worker
{
	/** the random number generator all instances will use */
	public static Random random = new Random();

	/** a counter just to keep track of something */
	public static Counter counter = new Counter();

	/** a name for this component */
	private String name = "worker";
	
	/** the first part of the delay */
	private int delay0 = 50;
	
	/** the second part of the delay */
	private int delay1 = 100;

	/** the countdown latch to use */
	private CountDownLatch countDown = new CountDownLatch(1);
	
	/** the service to resubmit when we're done, for an infinite queue */
	private ExecutorService resubmit;
	
		
	/**
	 * create a new instance just like this one
	 * @see org.yuwa.livemetrics.technology.lab.Worker#copy()
	 */
	public Worker copy()
	{
		PlainWorker that = new PlainWorker();
		that.delay0 = this.delay0;
		that.delay1 = this.delay1;
		return that;
	}

	/**
	 * just sleep for a little bit
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run()
	{
		Timer timer = MetricsController.tracker(this.name).startTimer(this.name);

		int sleep = random.nextInt(delay0) + random.nextInt(delay1);

		try
		{
			Thread.sleep(sleep);
		}
		catch ( Exception e )
		{
			// wakie up!
		}
		
		counter.increment();
		
		timer.addMe();
		
		// resubmit ourselves if we should run forever
		// -- don't use it with a countdown latch ;)
		if ( this.resubmit != null )
			this.resubmit.execute(this);
		else
			this.countDown.countDown();
	}
	
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @return the first part of the delay
	 */
	public int getDelay0()
	{
		return delay0;
	}

	/**
	 * @param delay0
	 */
	public void setDelay0(int delay0)
	{
		this.delay0 = delay0;
	}

	/**
	 * @return the second part of the delay
	 */
	public int getDelay1()
	{
		return delay1;
	}

	/**
	 * @param delay1
	 */
	public void setDelay1(int delay1)
	{
		this.delay1 = delay1;
	}

	/**
	 * set the random number generator
	 * 
	 * @param random
	 */
	public static void setRandom(Random random)
	{
		PlainWorker.random = random;
	}

	/**
	 * the executor to resubmit this job to
	 * 
	 * @param resubmit
	 */
	public void setResubmit(ExecutorService resubmit)
	{
		this.resubmit = resubmit;
	}

	/**
	 * @see org.yuwa.livemetrics.technology.lab.Worker#setCountDownLatch(java.util.concurrent.CountDownLatch)
	 */
	public void setCountDownLatch(CountDownLatch latch)
	{
		this.countDown = latch;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

}
