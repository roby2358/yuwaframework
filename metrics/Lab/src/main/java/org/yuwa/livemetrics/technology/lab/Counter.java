/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

package org.yuwa.livemetrics.technology.lab;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A counter to track counting
 * 
 * <p>
 * Nov 26, 2008
 *
 * @author royoung
 */
public class Counter
{
	/** the max value for the counter */
	private long max = 1000000000L;

	/** how many we've done */
	public AtomicLong counter = new AtomicLong();

	/**
	 * add one and loop at some value
	 */
	public void increment()
	{
		long value = counter.incrementAndGet();
		if ( value > max ) counter.set(0L);
	}
	
	/**
	 * the counter value
	 */
	public long value()
	{
		return counter.longValue();
	}

	/**
	 * @param max
	 */
	public void setMax(long max)
	{
		this.max = max;
	}

}
