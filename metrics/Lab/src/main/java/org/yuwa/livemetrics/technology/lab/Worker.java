/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Nov 26, 2008 $
 */
package org.yuwa.livemetrics.technology.lab;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

/**
 * The Worker interface represents something that needs to do something
 * <p>
 * Nov 26, 2008
 *
 * @author royoung
 */
public interface Worker
extends Runnable
{
	String getName();
	
	/**
	 * @return a copy of this object
	 */
	Worker copy();
	
	/**
	 * pass in an executor if you want the task to resubmit itself
	 * @param resubmit
	 */
	void setResubmit(ExecutorService resubmit);
	
	
	/**
	 * @param latch
	 */
	void setCountDownLatch(CountDownLatch latch);

	/**
	 * a do-nothing instance to use as a placeholder
	 */
	Worker NO_WORKER = new Worker()
	{
		public Worker copy() { return this; }
		
		public void setResubmit(ExecutorService resubmit) { }

		public void run() { }

		public void setCountDownLatch(CountDownLatch latch) { }

		public String getName() { return "none"; }
	};
}
