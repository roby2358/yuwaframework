/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Nov 25, 2008 $
 */
package org.yuwa.livemetrics.technology.lab;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * A controller to expose stuff
 * 
 * <p>
 * Nov 26, 2008
 *
 * @author royoung
 */
public class ControlController
implements InitializingBean, DisposableBean, Controller
{
	/** the list of batchers to use */
	private Map<String,Batcher> batchersByName = new HashMap<String,Batcher>();
	
	private ExecutorService pool;

	
	/**
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception
	{
		this.pool = Executors.newFixedThreadPool(batchersByName.size());
		
		for ( Batcher b : this.batchersByName.values() )
		{
			b.setResubmit(this.pool);
			this.pool.execute(b);
		}
	}
	
	
	/**
	 * @see org.springframework.beans.factory.DisposableBean#destroy()
	 */
	public void destroy() throws Exception
	{
		this.pool.shutdown();
		if ( ! this.pool.awaitTermination(3L, TimeUnit.SECONDS) )
			this.pool.shutdownNow();
	}

	
    /**
     * handle an incoming request
     * 
     * @see org.springframework.web.servlet.mvc.Controller#handleRequest(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response)
    throws Exception
    {
        ServletOutputStream out = response.getOutputStream();
		out.print("<html><body>");
		
		out.print("<div>");
		out.print("Batchers : ");
		out.print( this.batchersByName.size() );
		out.print("</div>");

		for ( Batcher b : this.batchersByName.values() )
		{
			out.print("<div>");
			out.print( b.getName() );
			out.print(" : ");
			out.print( b.getCount() );
			out.print("</div>");
		}

		// other info
		
		out.print("<div>");
		out.print("PlainWorker : ");
		out.print(PlainWorker.counter.value());
		out.print("</div>");
        
		out.print("</body></html>");

		return null;
    }

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @param batchers
	 */
	public void setBatchers(List<Batcher> batchers)
	{
		for ( Batcher b : batchers )
		{
			this.batchersByName.put(b.getName(), b);
		}
	}

}
