/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 *
 * Original author: royoung
 *
 *   $Author: royoung $
 * $Revision: #1 $
 *     $Date: Nov 25, 2008 $
 */
package org.yuwa.livemetrics.technology.lab;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.yuwa.technology.livemetrics.Timer;


/**
 * The job of the batcher is to have a bunch of threads that launch
 * sub-processes
 * 
 * @author royoung
 */
public class Batcher implements InitializingBean, DisposableBean, Worker
{
	/** the pool of threads to use */
	private ExecutorService pool;

	/** the next pool to invoke us */
	private ExecutorService resubmit;

	/** the number of threads */
	private int threads = 100;

	/** how long to wait before we give up */
	private long timeout = 300L;

	/** a prototype of the workers to run */
	private Worker protoWorker = Worker.NO_WORKER;

	/** the name of this batcher */
	private String name = "default";

	/** keep track of how many times we've run */
	private Counter counter = new Counter();

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// implementation
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception
	{
		this.pool = Executors.newFixedThreadPool(this.threads);
	}

	/**
	 * @see org.springframework.beans.factory.DisposableBean#destroy()
	 */
	public void destroy() throws Exception
	{
		this.pool.shutdown();

		try
		{
			this.pool.awaitTermination(this.timeout, TimeUnit.SECONDS);
		} catch (InterruptedException e)
		{
			// wakie up!
		}
	}

	/**
	 * @see org.yuwa.livemetrics.technology.lab.Worker#copy()
	 */
	public Worker copy()
	{
		Batcher that = new Batcher();
		that.threads = this.threads;
		that.protoWorker = this.protoWorker;
		return that;
	}

	/**
	 * @see org.yuwa.livemetrics.technology.lab.Worker#setResubmit(java.util.concurrent.ExecutorService)
	 */
	public void setResubmit(ExecutorService resubmit)
	{
		this.resubmit = resubmit;
	}

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run()
	{
		Timer timer = MetricsController.tracker(this.name).startTimer(this.name);
		
		CountDownLatch latch = new CountDownLatch(this.threads);

		for (int i = 0; i < this.threads; i++)
		{
			Worker w = this.protoWorker.copy();
			w.setCountDownLatch(latch);
			this.pool.execute(w);
		}

		try
		{
			latch.await(this.timeout, TimeUnit.SECONDS);

			counter.increment();
			
			timer.addMe();

			// see if we need to run again
			if (this.resubmit != null)
				this.resubmit.execute(this);
		} catch (InterruptedException e)
		{
			// wakie up!
		}
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * set the number of threads in this batch
	 * 
	 * @param threads
	 */
	public void setThreads(int threads)
	{
		this.threads = threads;
	}

	/**
	 * set the prototype worker from which others are set
	 * 
	 * @param protoWorker
	 */
	public void setProtoWorker(Worker protoWorker)
	{
		this.protoWorker = protoWorker;
	}

	/**
	 * how long to wait before we give up
	 * 
	 * @param timeout
	 */
	public void setTimeout(long timeout)
	{
		this.timeout = timeout;
	}

	/**
	 * @return the name of this batcher
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * set the name
	 * 
	 * @param name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @param counter
	 */
	public void setCounter(Counter counter)
	{
		this.counter = counter;
	}

	/**
	 * @return the current count
	 */
	public long getCount()
	{
		return this.counter.value();
	}

	public void setCountDownLatch(CountDownLatch latch)
	{
		// ignored at the batch level
	}

}
