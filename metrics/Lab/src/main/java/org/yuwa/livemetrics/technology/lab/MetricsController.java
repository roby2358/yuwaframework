/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package org.yuwa.livemetrics.technology.lab;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.yuwa.technology.livemetrics.MetricsReporter;
import org.yuwa.technology.livemetrics.ServiceTracker;
import org.yuwa.technology.livemetrics.ServiceTrackerBuilder;


/**
 * the metrics controller
 * 
 * <p>
 * Nov 26, 2008
 *
 * @author royoung
 */
public class MetricsController implements
		org.springframework.web.servlet.mvc.Controller
{
	private static final Map<String,ServiceTracker> trackersByName = new HashMap<String,ServiceTracker>();
	
	private final MetricsReporter reporter = new MetricsReporter();
	
	/**
	 * Get, or build, the named tracker
	 * 
	 * @param name
	 * @return the named tracker
	 */
	public static ServiceTracker tracker(String name)
	{
		ServiceTracker tracker = trackersByName.get(name);
		
		if ( null == tracker )
		{
			ServiceTrackerBuilder builder = new ServiceTrackerBuilder();
			builder.setInitLog4j(false);
			builder.setName(name);
			builder.setNewlines(true);
			builder.setUseListener(false);
			tracker = builder.build();
			
			tracker.startHistogram(name, 0L, 1000000L, 100);
			
			trackersByName.put(name,tracker);
		}
		return tracker;
	}
	
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception
	{
		String name = request.getParameter("metric");
		
		if ( null == name )
		{
			String usage = this.buildUsageString();
			response.getOutputStream().print(usage);
		}
		else
		{
			ServiceTracker tracker = tracker(name);
			byte[] history = this.buildHistory(tracker);
			response.getOutputStream().write(history);
		}
		
		return null;
	}

	/**
	 * @return a string with a usage message
	 */
	private String buildUsageString()
	{
		final String content;
		StringBuilder builder = new StringBuilder("{ message : 'Please specify metric=', metric : [\n'");

		int i  = 0;

		for ( String s : trackersByName.keySet() )
		{
			if ( i++ > 0 )
				builder.append("',\n'");

			builder.append(s);
		}

		builder.append("' ] }");

		content = builder.toString();
		return content;
	}

	/**
	 * @param tracker
	 * @return a string with the metrics history
	 * @throws UnsupportedEncodingException
	 */
	private byte[] buildHistory(ServiceTracker tracker)
	throws UnsupportedEncodingException
	{
		final byte[] history = this.reporter.buildHistory(tracker);
		return history;
	}
}
