/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 */


package org.yuwa.log4j;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;

/**
 *
 * <p>
 * Mar 19, 2010
 *
 * @author royoung
 */
public class InMemoryAppender
extends AppenderSkeleton
{
	/** the default number of lines to hold in memory */
	private static final int DEFAULT_SIZE = 1000;
	
	/** the (shared) size of the logging cache, in lines */
	private static int size = DEFAULT_SIZE;
	
	/** the common place to stash log messages... shared everywhere */
	private static final ConcurrentLinkedQueue<String> log = new ConcurrentLinkedQueue<String>();

	/** the title displayed with the queue */
	private static String title = "Log";
	
	/**
	 * "find" a reference of this class that points to the log
	 * 
	 * @return an instance of this class ;)
	 */
	public static InMemoryAppender find()
	{
		return new InMemoryAppender();
	}
	
	/**
	 * @see org.apache.log4j.AppenderSkeleton#requiresLayout()
	 */
	@Override
	public boolean requiresLayout()
	{
		return true;
	}
	
	/**
	 * @see org.apache.log4j.AppenderSkeleton#activateOptions()
	 */
	@Override
	public void activateOptions()
	{
		super.activateOptions();
	}

	/**
	 * @see org.apache.log4j.AppenderSkeleton#append(org.apache.log4j.spi.LoggingEvent)
	 */
	@Override
	protected void append(LoggingEvent event)
	{
		while ( ! log.isEmpty() && log.size() >= size )
		{
			log.remove();
		}
		
		final String line = layout.format(event);
		log.add(line);
	}

	/**
	 * @see org.apache.log4j.AppenderSkeleton#close()
	 */
	@Override
	public void close()
	{
		// nop
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// accessors
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @return the size
	 */
	public int getSize()
	{
		return InMemoryAppender.size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(int size)
	{
		// note: sets it for everyone ;)
		InMemoryAppender.size = size;
	}

	/**
	 * @return the log
	 */
	public ConcurrentLinkedQueue<String> getLog()
	{
		return log;
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title)
	{
		// note: sets it for everyone ;)
		InMemoryAppender.title = title;
	}

}
