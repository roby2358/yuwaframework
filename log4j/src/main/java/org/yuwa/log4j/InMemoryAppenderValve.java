/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 * 
 */


package org.yuwa.log4j;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ValveBase;

/**
 * This is a Tomcat valve which acts as the viewing part of a server a log4j
 * appender, holding the logging information in a common in-memory FIFO queue.
 * 
 * <p>
 * Mar 19, 2010
 * 
 * @author royoung
 */
public class InMemoryAppenderValve
extends ValveBase
{
	/** the URI that returns the in-memory log */
	private static final String LOG_GET_URI = "/-";

	/** the character encoding string for UTF-8 */
    private static final String UTF8 = "UTF-8";

	/** find an instance of the appender that knows about the queue */
	private InMemoryAppender appender = InMemoryAppender.find();

	/**
	 * @see org.apache.catalina.valves.ValveBase#invoke(org.apache.catalina.connector.Request, org.apache.catalina.connector.Response)
	 */
	@Override
	public void invoke(Request request, Response response)
	throws IOException, ServletException
	{
		if (request.getRequestURI().endsWith(LOG_GET_URI))
		{
			if ( request.getParameter("clear") != null )
				this.appender.getLog().clear();
			this.sendLogPage(response);
		}
		else
		{
			this.next.invoke(request, response);
		}
	}

	/**
	 * @param response
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	private void sendLogPage(HttpServletResponse response)
	throws IOException, UnsupportedEncodingException
	{
		// templates are for weinies
		StringBuilder page = new StringBuilder("<html>\n");
		page.append("<header>\n");
		page.append("<title>");
		page.append(appender.getTitle());
		page.append("</title>\n");
		page.append("<style type='text/css'>\n");

		page.append("body {");
		page.append("background-color:white;");
		page.append("color:#333333;");
		page.append("margin:0px;");
		page.append("font-family:Verdana, Arial, Helvetica, sans-serif;");
		page.append("font-size:12px;");
		page.append("}");

		page.append(".darkrow {");
		page.append("background-color:#ddffff;");
		page.append("}");

		page.append(".reallydarkrow {");
		page.append("background-color:#448888;");
		page.append("color:white;");
		page.append("font-weight:bold;");
		page.append("}");

		page.append("</style>\n");
		page.append("</header>\n");
		page.append("<body>\n");

		page.append("<div class='reallydarkrow'>");
		page.append(appender.getTitle());
		page.append(" ");
		page.append(appender.getLog().size());
		page.append(" rows</div>");

		final List<String> reversed = new ArrayList<String>(appender.getLog());
		Collections.reverse(reversed);
		boolean highlight = true;
		for ( String s : reversed )
		{
			page.append("<div");
			if ( highlight ^= true ) page.append(" class='darkrow'");
			page.append(">");
			page.append(s);
			page.append("</div>\n");
		}

		page.append("</body>\n");
		page.append("</html>\n\n");

		this.defeatCaching(response);
		this.writeString(response, page.toString(), UTF8);
	}

    
    /**
     * add headers to defeat caching upstream
     * 
     * @param response
     */
    private void defeatCaching(final HttpServletResponse response)
    {
        response.setHeader("Cache-control","no-cache");
        response.addHeader("Cache-control","no-store");
        response.setHeader("Pragma","no-cache");
        response.setHeader("Expires","0");  
    }

    /**
     * write the string to the response output stream
     * 
     * @param response
     * @param result
     * @throws IOException
     * @throws UnsupportedEncodingException
     */
    private void writeString(final HttpServletResponse response,
            final String result,
            final String encoding)
    throws IOException, UnsupportedEncodingException
    {
        byte[] bits = result.getBytes(encoding);
        response.setContentLength(bits.length);
        response.getOutputStream().write(bits);
    }

}
