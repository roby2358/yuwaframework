/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.util.brain.test;

import junit.framework.TestCase;

import org.bonko.util.brain.Fixed;

public class FixedTest extends TestCase
{
    private Fixed fixed = new Fixed();
    
    public int one = fixed.one();
    public int two = fixed.convert(2);
    public int ten = fixed.convert(10);
    public int hundred = fixed.convert(100);

    public void testMultiply()
    {
        assertEquals( 0, fixed.multiply(0,0) );
        assertEquals( one, fixed.multiply( one, one ));
    }
    
    public void testAdd()
    {
        assertEquals( two, fixed.add( one, one ));
    }
    
    public void testDivide()
    {
        assertEquals( ten, fixed.divide(hundred,ten) );
    }
    
    /*
     * Class under test for int convert(double)
     */
    public void testConvertDouble()
    {
        assertEquals( one, fixed.convert(1.0) );
        assertEquals( -one, fixed.convert(-1.0) );
        assertEquals( 6553600, fixed.convert(100.0) );
    }
    
    /*
     * Class under test for double convert(int)
     */
    public void testToDouble()
    {
        assertEquals( 1.0,fixed.toDouble(one) );
        assertEquals( -1.0,fixed.toDouble(-one) );
    }
    
    public void testConvertInt()
    {
        assertEquals(fixed.convert(100.0),fixed.convert(100));
        assertEquals( 6553600, fixed.convert(100) );
    }
    
}
