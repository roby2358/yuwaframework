/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.util.brain.test;

import java.util.Random;

import junit.framework.TestCase;

import org.bonko.util.brain.BasicBrainVisitor;
import org.bonko.util.brain.BrainVisitor;
import org.bonko.util.brain.InputLayer;
import org.bonko.util.brain.SimpleBrainFunctions;
import org.bonko.util.brain.SimpleNeuronLayer;

public class SimpleNeuronLayerTest extends TestCase
{
    public static int INPUT_SIZE = 3;
    public static int SIZE = 5;
    public SimpleBrainFunctions calc = new SimpleBrainFunctions();
    public SimpleNeuronLayer layer = new SimpleNeuronLayer(SIZE);
    public InputLayer inputLayer = new InputLayer(INPUT_SIZE);
    public BrainVisitor visitor = new BasicBrainVisitor() { };
    public SimpleNeuronLayer.Internals internals = layer.new Internals(visitor);
    
    
    protected void setUp() throws Exception
    {
        super.setUp();
        this.calc.setRandom( new Random(1234567L) );

        inputLayer.setFunctions(this.calc);
        inputLayer.initialize();
        
        layer.setInput(inputLayer);
        layer.setFunctions(this.calc);
        layer.initialize();
    }

    public final void testSetInput()
    {
    //TODO Implement setInput().
    }

    public final void testGetSize()
    {
        assertEquals( "size set to " + SIZE, SIZE, this.layer.getSize() );
    }

    public final void testInitialize()
    {
        //TODO
    }

    public final void testSetFunctions()
    {
    //TODO Implement setFunctions().
    }

    public final void testFeedForward()
    {
    //TODO Implement feedForward().
    }

    public final void testHasBackPropogate()
    {
    //TODO Implement hasBackPropogate().
    }

    public final void testCreateError()
    {
    //TODO Implement createError().
    }

    public final void testBackPropogate()
    {
    //TODO Implement backPropogate().
    }

}
