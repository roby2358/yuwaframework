/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

/*
 * $Id: InOrderMain.java,v 1.6 2005/06/07 02:10:06 Owner Exp $
 * 
 */
package org.bonko.util.brain.test.inOrder;

import java.util.Random;

import org.apache.log4j.BasicConfigurator;
import org.bonko.util.brain.EvaluationBrain;
import org.bonko.util.brain.InputLayer;
import org.bonko.util.brain.LayeredBrain;
import org.bonko.util.brain.SimpleBrainFunctions;
import org.bonko.util.brain.SimpleNeuronLayer;
import org.bonko.util.brain.encoding.StringEncoder;
import org.bonko.util.fullScreen.Repeater;
import org.bonko.util.fullScreen.WorkerQueue;

public class InOrderMain
{
    private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass());
    
    public static final double ALPHA_MAX = 0.3;
    public static final double ALPHA_MIN = 0.01;
    public static final int ALPHA_SCALE = 20000;
    public static final int INPUT_COUNT = 3;
    public static final int HIDDEN_COUNT = 4;
    public static final int MAX_VALUE = 7;
    public static final int MID_VALUE = 4;
    public static final int TRAINING_ITERATIONS = 100;
    
    private final WorkerQueue workerQueue = new WorkerQueue();
    private Thread workerThread = new Thread( this.workerQueue );
    private final Random random = new Random();
    private SimpleBrainFunctions calc = new SimpleBrainFunctions();
    private EvaluationBrain brain;
    private boolean halt = false;
    private int count = 0;
    
    private InOrderFrame frame;
    
    private final InOrderMain THIS = this;
    
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // PropertyConfigurator.configure(LOG4J_PROPERTIES);
        BasicConfigurator.configure();
        
        InOrderMain mainView = new InOrderMain();
        JInOrderFrame frame = new JInOrderFrame(mainView);
        
        frame.begin();
    }
    
    
    public void setFrame( InOrderFrame frame )
    {
        this.frame = frame;
    }
    
    
    public void start()
    {
        this.state.begin();
        
        this.workerThread.start();
        
        this.createBrain();
        
        this.addReadyTask();
    }
    
    
    private void addReadyTask()
    {
        THIS.WAIT_STATE.set();
        
        Runnable task = new Runnable() {
            public void run() {
                THIS.READY_STATE.set();
            }
        };
        this.workerQueue.add(task);
    }
    
    
    private void createBrain()
    {
        Runnable task = new Runnable() {
            
            public void run() {
                LayeredBrain b = new LayeredBrain();
                
                THIS.calc.setRandom( new Random() );
                b.setFunctions(THIS.calc);
                
                b.setInputLayer( new InputLayer(INPUT_COUNT) );
                b.add( new SimpleNeuronLayer(HIDDEN_COUNT) );
                b.add( new SimpleNeuronLayer(1) );
                
                THIS.brain = b;
            }
        };
        this.workerQueue.add(task);
    }
    
    
    private void randomize(InOrderObservation state)
    {
        state.setA( random.nextInt(MAX_VALUE) + 1 );
        state.setB( random.nextInt(MAX_VALUE) + 1 );
        state.setC( random.nextInt(MAX_VALUE) + 1 );
        
        this.frame.setText1( "" + state.getA() );
        this.frame.setText2( "" + state.getB() );
        this.frame.setText3( "" + state.getC() );
    }
    
    
    private void init(InOrderObservation state)
    {
        state.setA( this.intValue( this.frame.getText1() ) );
        state.setB( this.intValue( this.frame.getText2() ) );
        state.setC( this.intValue( this.frame.getText3() ) );
    }
    
    
    private void evaluate(InOrderObservation state)
    {
        brain.evaluate( state );
        
        this.frame.setOutput( state.getValues()[0] );
        this.frame.setInOrder( state.getValues()[0] > 0 );
        this.frame.setCorrect( state.isCorrect() );
        this.frame.setCount( ++this.count );
        
        if ( !state.isCorrect() )
        {
//            this.calc.taperLearningRate(ALPHA_MIN,ALPHA_MAX,this.count,ALPHA_SCALE);
//          this.calc.randomizeLearningRate(ALPHA_MIN,ALPHA_MAX);
            this.calc.setAlpha( (ALPHA_MAX + ALPHA_MIN) / 2);
            double error = brain.train( state, TRAINING_ITERATIONS );
            
            this.frame.setError( error );
        }
        
        //        if ( logger.isDebugEnabled() ) logger.debug("input [" + input[0] + " " + input[1] + " " + input[2] + "]\n" + brain );
    }
    
    
    private int intValue(String v)
    {
        return ( v == null ) ? 0 : Integer.parseInt( v );
    }
    
    
    public void evaluate() { this.state.evaluate(); }
    
    
    public void randomize() { this.state.randomize(); }
    
    
    public void dump() { this.state.dump(); }
    
    
    public void run() { this.state.run(); }
    
    
    public void stop() { this.state.stop(); }
    
    
    public void exit() { this.state.exit(); }
    
    
    private void doEvaluate() {
        
        Runnable task = new Runnable() {
            public void run() {
                if ( logger.isDebugEnabled() ) logger.debug( "evaluate" );
                
                THIS.addReadyTask();
                
                InOrderObservation state = new InOrderObservation();
                THIS.init(state);
                THIS.evaluate(state);
            }
        };
        
        this.workerQueue.add(task);
    }
    
    
    private void doRandom() {
        
        Runnable task = new Runnable() {
            public void run() {
                if ( logger.isDebugEnabled() ) logger.debug( "evaluate" );
                
                THIS.addReadyTask();
                
                InOrderObservation state = new InOrderObservation();
                THIS.randomize(state);
                THIS.evaluate(state);
            }
        };
        
        this.workerQueue.add(task);
    }
    
    
    public void doDump()
    {
        final Runnable task = new Runnable() {
            public void run() {
                StringEncoder e = new StringEncoder();
                THIS.brain.accept(e);
                System.err.println("" + e);
            }
        };
        this.workerQueue.add(task);
    }
    
    
    private void doRun() {
        if ( logger.isDebugEnabled() ) logger.debug( "run" );
        
        Runnable task = new Repeater() {
            public void run() {
                InOrderObservation state = new InOrderObservation();
                
                THIS.randomize(state);
                THIS.evaluate(state);
            }
            
            public boolean repeat() 
            {
                boolean halt = THIS.halt;
                if ( halt ) THIS.READY_STATE.set();
                return ( ! halt );
            }
        };
        
        this.RUNNING_STATE.set();
        this.halt = false;
        
        this.workerQueue.add(task);
    }
    
    
    private void doStop() {
        if ( logger.isDebugEnabled() ) logger.debug( "stop" );
        THIS.halt = true;
    }
    
    
    private void doExit() {
        if ( logger.isDebugEnabled() ) logger.debug( "exit" );
        this.workerThread.interrupt();
        this.frame.end();
        System.exit(0);
    }
    
    
    private abstract class State
    {
        public void set() {
            if ( THIS.state != null ) THIS.state.end();
            THIS.state = this;
            THIS.state.begin();
        }
        public abstract void begin();
        public void end() { }
        public void evaluate() { }
        public void randomize() { }
        public void dump() { THIS.doDump(); }
        public void run() { }
        public void stop() { }
        public void exit() { }
    }
    
    
    public State WAIT_STATE = new State() {
        public void begin() {
            THIS.frame.setEvaluateEnabled(false);
            THIS.frame.setRunEnabled(false);
            THIS.frame.setStopEnabled(false);
            THIS.frame.setExitEnabled(false);
        }
    };
    
    
    public State READY_STATE = new State() {
        public void begin() {
            THIS.frame.setEvaluateEnabled(true);
            THIS.frame.setRandomEnabled(true);
            THIS.frame.setRunEnabled(true);
            THIS.frame.setStopEnabled(false);
            THIS.frame.setExitEnabled(true);
        }
        public void evaluate() { THIS.doEvaluate(); }
        public void randomize() { THIS.doRandom(); }
        public void run() { THIS.doRun(); }
        public void exit() { THIS.doExit(); }
    };
    
    
    public State RUNNING_STATE = new State() {
        public void begin() {
            THIS.frame.setEvaluateEnabled(false);
            THIS.frame.setRandomEnabled(false);
            THIS.frame.setRunEnabled(false);
            THIS.frame.setStopEnabled(true);
            THIS.frame.setExitEnabled(false);
        }
        public void stop() { THIS.doStop(); }
    };
    
    
    private State state = WAIT_STATE;
}
