/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

/*
 * $Id: InOrderFrame.java,v 1.2 2005/05/15 23:47:28 Owner Exp $
 * 
 */

package org.bonko.util.brain.test.inOrder;




public interface InOrderFrame {

    public static final String INCORRECT = "NO";
    public static final String CORRECT = "OK";
    public static final String IN_ORDER = "In order";
    public static final String NOT_IN_ORDER = "Out of order";

    void begin();
    
    void end();
    
    void setText1(String string);

    void setText2(String string);

    void setText3(String string);

    String getText1();

    String getText2();

    String getText3();

    void setOutput(double value);

    void setInOrder(boolean b);

    void setCorrect(boolean b);

    void setCount(int i);

    void setError(double error);

    void setEvaluateEnabled(boolean b);

    void setRunEnabled(boolean b);

    void setStopEnabled(boolean b);

    void setExitEnabled(boolean b);

    void setRandomEnabled(boolean b);

    void setSaveEnabled(boolean b);

    void setLoadEnabled(boolean b);

    void setDumpEnabled(boolean b);
}
