/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.util.brain.test;

import java.util.Random;

import junit.framework.TestCase;

import org.bonko.util.brain.Fixed;
import org.bonko.util.brain.SimpleBrainFunctions;

public class SimpleBrainFunctionsTest extends TestCase
{
    private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass());
    
    public SimpleBrainFunctions calc = new SimpleBrainFunctions();
    
    Fixed fixed = new Fixed();
    
    
    protected void setUp() throws Exception
    {
        super.setUp();
        this.calc.setRandom( new Random(1234567L) );
    }
    
    public void testTrans()
    {
        assertEquals( -fixed.one(), this.calc.trans( fixed.convert(-100)) );
        assertEquals( -fixed.one(), this.calc.trans(fixed.convert(-39)) );
        assertEquals( fixed.convert(-0.4621171572600098), this.calc.trans(-fixed.one()) );
        assertEquals( fixed.convert(-0.2449186624037092), this.calc.trans(fixed.convert(-0.5)) );
        assertEquals( -12934, this.calc.trans(fixed.convert(-0.4)) );
        assertEquals( -9756, this.calc.trans(fixed.convert(-0.3)) );
        assertEquals( fixed.convert(-0.09966799462495568), this.calc.trans(fixed.convert(-0.2)) );
        assertEquals( -3273, this.calc.trans(fixed.convert(-0.1)) );
        assertEquals( 0, this.calc.trans(0) );
        assertEquals( 3273, this.calc.trans(fixed.convert(0.1)) );
        assertEquals( fixed.convert(0.0996679946249559), this.calc.trans(fixed.convert(0.2)) );
        assertEquals( 9756, this.calc.trans(fixed.convert(0.3)) );
        assertEquals( 12934, this.calc.trans(fixed.convert(0.4)) );
        assertEquals( fixed.convert(0.2449186624037092), this.calc.trans(fixed.convert(0.5)) );
        assertEquals( fixed.convert(0.4621171572600098), this.calc.trans(fixed.one()) );
        assertEquals( fixed.one(), this.calc.trans(fixed.convert(39.0)) );
        assertEquals( fixed.one(), this.calc.trans(fixed.convert(100.0)) );
    }
    
    public void testDtrans()
    {
        assertEquals( 0, this.calc.dtrans(fixed.convert(-100)) );
        assertEquals( 0, this.calc.dtrans(fixed.convert(-39) ));
        assertEquals( fixed.convert(0.3932238664829637), this.calc.dtrans(-fixed.one()) );
        assertEquals( fixed.convert(0.470007424403189), this.calc.dtrans(fixed.convert(-0.5)) );
        assertEquals( fixed.convert(0.4805214914830583), this.calc.dtrans(fixed.convert(-0.4)) );
        assertEquals( fixed.convert(0.4889166233814917), this.calc.dtrans(fixed.convert(-0.3)) );
        assertEquals( fixed.convert(0.4950331454237199), this.calc.dtrans(fixed.convert(-0.2)) );
        assertEquals( fixed.convert(0.49875208038578395), this.calc.dtrans(fixed.convert(-0.1)) );
        assertEquals( fixed.convert(0.5), this.calc.dtrans(0) );
        assertEquals( fixed.convert(0.49875208038578395), this.calc.dtrans(fixed.convert(0.1)) );
        assertEquals( fixed.convert(0.4950331454237199), this.calc.dtrans(fixed.convert(0.2)) );
        assertEquals( fixed.convert(0.4889166233814917), this.calc.dtrans(fixed.convert(0.3)) );
        assertEquals( fixed.convert(0.4805214914830583), this.calc.dtrans(fixed.convert(0.4)) );
        assertEquals( fixed.convert(0.470007424403189), this.calc.dtrans(fixed.convert(0.5)) );
        assertEquals( fixed.convert(0.3932238664829637), this.calc.dtrans(fixed.one()) );
        assertEquals( 0, this.calc.dtrans(fixed.convert(37)) );
        assertEquals( 0, this.calc.dtrans(fixed.convert(-100)) );
    }
    
    public void testRandomCell()
    {
        assertEquals( -8427, this.calc.randomCell() );
        assertEquals( 4394, this.calc.randomCell() );
        assertEquals( 8258, this.calc.randomCell() );
        assertEquals( -1717, this.calc.randomCell() );
        assertEquals( -2129, this.calc.randomCell() );
        assertEquals( -6775, this.calc.randomCell() );
        assertEquals( 5705, this.calc.randomCell() );
        assertEquals( 14581, this.calc.randomCell() );
        assertEquals( 14852, this.calc.randomCell() );
        assertEquals( -5524, this.calc.randomCell() );
        assertEquals( -9371, this.calc.randomCell() );
        assertEquals( 4865, this.calc.randomCell() );
    }
    
    
    public void testScaleError()
    {
        assertEquals( 655300, this.calc.scaleError( fixed.convert(100) ) );
        
        this.calc.setAlpha( 2.0 );
        assertEquals( fixed.convert(200.0), this.calc.scaleError( fixed.convert(100.0) ) );
    }
    
}
