/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

/*
 * $Id: JInOrderFrame.java,v 1.3 2005/05/25 01:03:47 Owner Exp $
 * 
 */
package org.bonko.util.brain.test.inOrder;

import java.awt.TextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.bonko.util.fullScreen.ApplicationFrame;
import org.bonko.util.fullScreen.ApplicationFrameFactory;
import org.bonko.util.fullScreen.GridBagManager;

public class JInOrderFrame extends JFrame implements InOrderFrame
{
    private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass());
    
    private static final long serialVersionUID = 1L;
    private static final DecimalFormat DECIMAL = new DecimalFormat(" 0.00000;-0.00000");

    private ApplicationFrame applicationFrame = ApplicationFrameFactory.createWindowedFrame(this);
    private InOrderMain mainView;
    
    private JButton runButton = new JButton("run");
    private JButton stopButton = new JButton("stop");
    private JButton exitButton = new JButton("exit");
    private TextField input1 = new TextField("1");
    private TextField input2 = new TextField("2");
    private TextField input3 = new TextField("3");
    private TextField output1 = new TextField("1");
    private TextField error1 = new TextField("1");
    private JLabel inOrderLabel = new JLabel("--");
    private JLabel correctLabel = new JLabel(CORRECT);
    private JLabel countLabel = new JLabel("0");
    private JButton evaluateButton = new JButton("evaluate");
    private JButton randomButton = new JButton("random");
    private JButton saveButton = new JButton("save");
    private JButton loadButton = new JButton("load");
    private JButton dumpButton = new JButton("dump");
    
    private final JInOrderFrame THIS = this;

    
    public void setMainView(InOrderMain v) { this.mainView = v; }

    
    public JInOrderFrame(InOrderMain m)
    {
        this.mainView = m;
        this.mainView.setFrame(this);
        this.initialize();
    }
    
    
    private void initialize()
    {
        this.setTitle("Brain Test");
        
        this.output1.setColumns(8);
        this.output1.setEditable(false);
        this.error1.setColumns(5);
        this.error1.setEditable(false);
        
        GridBagManager c = new GridBagManager(this);
        c.add( input1 );
        c.add( this.inOrderLabel );
        c.add( this.correctLabel );
        c.add( runButton );
        c.next();
        c.add( input2 );
        c.add( output1 );
        c.add( error1 );
        c.add( stopButton );
        c.next();
        c.add( input3 );
        c.add( this.countLabel );
        c.addEmpty();
        c.add( dumpButton );
        c.next();
        c.add( evaluateButton );
        c.add( randomButton );
        c.addEmpty();
        c.add( exitButton );
        c.next();
        c.addEmpty();
        c.add( saveButton );
        c.add( loadButton );
        
        this.evaluateButton.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseReleased(MouseEvent me) { THIS.mainView.evaluate(); }
                });
        
        this.randomButton.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseReleased(MouseEvent me) { THIS.mainView.randomize(); }
                });
        
        this.dumpButton.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseReleased(MouseEvent me) { THIS.mainView.dump(); }
                });
        
        this.runButton.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseReleased(MouseEvent me) { THIS.mainView.run(); }
                });
        
        this.stopButton.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseReleased(MouseEvent me) { THIS.mainView.stop(); }
                });
        
        this.exitButton.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseReleased(MouseEvent me) { THIS.mainView.exit(); }
                });
    }
    
    
    public void begin()
    {
        this.applicationFrame.showFrame();
        this.mainView.start();
    }
    
    
    public void end()
    {
        this.applicationFrame.clearFrame();
    }
  
    
    public String getText1() { return this.input1.getText(); }

    public String getText2() { return this.input2.getText(); }

    public String getText3() { return this.input3.getText(); }

    public void setText1(String string) { input1.setText(string); }

    public void setText2(String string) { input2.setText(string); }

    public void setText3(String string) { input3.setText(string); }

    public void setCorrect(boolean b) { THIS.correctLabel.setText( b ? CORRECT : INCORRECT); }


    public void setCount(int i) { this.countLabel.setText( "" + i ); }


    public void setError(double error) { this.error1.setText( DECIMAL.format( error ) ); }


    public void setInOrder(boolean b) {THIS.inOrderLabel.setText( ( b ) ? IN_ORDER : NOT_IN_ORDER ); }


    public void setOutput(double value) { THIS.output1.setText( DECIMAL.format( value ) ); }


    public void setEvaluateEnabled(boolean b) { this.evaluateButton.setEnabled(b); }


    public void setExitEnabled(boolean b) { this.exitButton.setEnabled(b); }


    public void setRandomEnabled(boolean b) { this.randomButton.setEnabled(b); }


    public void setRunEnabled(boolean b) { this.runButton.setEnabled(b); }


    public void setStopEnabled(boolean b) { this.stopButton.setEnabled(b); }


    public void setSaveEnabled(boolean b) { this.saveButton.setEnabled(b); }


    public void setLoadEnabled(boolean b) { this.loadButton.setEnabled(b); }


    public void setDumpEnabled(boolean b) { this.dumpButton.setEnabled(b); }
}
