/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

/*
 * $Id: InOrderObservation.java,v 1.5 2005/06/01 19:00:00 Owner Exp $
 * 
 */
package org.bonko.util.brain.test.inOrder;

import org.bonko.util.brain.Observation;

public class InOrderObservation implements Observation
{
    private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass());
    private int a;
    private int b;
    private int c;
    private double value;
    
    public boolean isSet() { return true; }
    
    public boolean isOrdered()
    {
        return ( a <= b && b <= c );
    }
    
    public boolean isCorrect()
    {
        boolean correct = ( this.isOrdered() && ( this.value > 0 ) ) || ( ! this.isOrdered() && ( this.value < 0 ) );
        return correct;
    }
    

    public double[] getExpectedValues()
    {
        double expected = (this.isOrdered()) ? 1  : -1;
        return new double[] { expected };
    }
    
    
    public void setExpectedValues(double[] value) { }


    public double[] toArray()
    {
        double[] in = new double[InOrderMain.INPUT_COUNT];
        
        int max = Math.max( Math.max( Math.abs(a),Math.abs(b)),Math.abs(c) );
        in[0] = (double)( this.a - InOrderMain.MID_VALUE ) / max;
        in[1] = (double)( this.b - InOrderMain.MID_VALUE ) / max;
        in[2] = (double)( this.c - InOrderMain.MID_VALUE ) / max;
        return in;
    }
    
    public int getA() { return a; }
    
    public void setA(int a) { this.a = a; }
    
    public int getB() { return b; }
    
    public void setB(int b) { this.b = b; }
    
    public int getC() { return c; }
    
    public void setC(int c) { this.c = c; }
    
    public double[] getValues() { return new double[] { this.value }; }
    
    public void setValues(double[] value) { this.value = value[0]; }
    
    public int size() { return 1; }

}
