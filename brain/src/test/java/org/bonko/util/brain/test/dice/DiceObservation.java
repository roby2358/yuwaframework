/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

/*
 * $Id: DiceObservation.java,v 1.2 2005/06/02 18:57:11 Owner Exp $
 * 
 */
package org.bonko.util.brain.test.dice;

import java.util.Random;

import org.bonko.util.brain.Observation;

public class DiceObservation implements Observation
{
    private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass());
    public static final int INPUT_COUNT = 11;
    public static final int MAX_VALUE = 12;
    public static final int MID_VALUE = 7;
    public static final int MIN_VALUE = 2;
    private final Random random;
    private int guessValue;
    private int roll;
    private double value;
    
    
    public DiceObservation(Random r)
    {
        this.random = r;
        this.roll = this.random.nextInt(6) + this.random.nextInt(6) + MIN_VALUE;
    }
    
    
    public void guess() { this.guessValue = random.nextInt(MAX_VALUE-1) + 2; }
    
    public boolean isSet() { return true; }
    
    public double[] getExpectedValues()
    {
        double e = (this.roll >= this.guessValue ) ? 1.0 : 0.0;
        return new double[] { e };
    }
    
//  public double getExpectedValue() { return ( this.roll >= this.guessValue ) ? 2 * this.value : 0.0; }
    
    public void setExpectedValues(double[] value) { }
    
    public double[] toArray()
    {
        double[] array = new double[INPUT_COUNT];
        
        array[ this.guessValue - MIN_VALUE ] = 1.0;
        
//      double[] array = new double[] { this.guessValue };
        return array;
    }
    
    public int size() { return 1; }
    
    public double[] getValues() { return new double[] { this.value }; }
    
    public void setValues(double[] value) { this.value = value[0]; }
    
    public int getGuess() { return guessValue; }
    
    public void setGuess(int guess) { this.guessValue = guess; }
    
    public int getRoll() { return roll; }
    
}
