/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

/*
 * $Id: JDiceFrame.java,v 1.2 2005/06/01 18:59:19 Owner Exp $
 * 
 */
package org.bonko.util.brain.test.dice;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.bonko.util.fullScreen.ApplicationFrame;
import org.bonko.util.fullScreen.ApplicationFrameFactory;
import org.bonko.util.fullScreen.GridBagManager;

public class JDiceFrame extends JFrame implements DiceFrame
{
    private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass());
    
    private static final long serialVersionUID = 1L;
    private static final DecimalFormat DECIMAL = new DecimalFormat(" 0.00000;-0.00000");
    
    private ApplicationFrame applicationFrame = ApplicationFrameFactory.createWindowedFrame(this);
    
    private JButton runButton = new JButton("run");
    private JButton stopButton = new JButton("stop");
    private JButton exitButton = new JButton("exit");
    private JLabel countLabel = new JLabel("0");
    private ButtonGroup buttonGroup = new ButtonGroup();
    private JRadioButton radioNone = new JRadioButton();
    private JButton evaluateButton = new JButton("evaluate");
    private JButton randomButton = new JButton("random");
    
    private JRadioButton[] radio = new JRadioButton[11];
    private JTextField[] value = new JTextField[11];
    
    private final DiceMain main;
    private final JDiceFrame THIS = this;
    
    
    public JDiceFrame(DiceMain v)
    {
        this.main = v;
        this.initialize();
    }
    
    private void initialize()
    {
        this.setTitle("Brain Test");
        
        for ( int i = 0 ; i < radio.length ; i++ )
        {
            radio[i] = new JRadioButton("" + (i + 2));
            this.buttonGroup.add( radio[i] );
            value[i] = new JTextField( DECIMAL.format(0) );
            this.initializeValueField( value[i] );
        }
        
        this.radioNone.setSelected(true);
        this.buttonGroup.add( this.radioNone );
        
        GridBagManager c = new GridBagManager(this);
        
        c.setBuildVertical();
        
        for ( JRadioButton r : radio ) c.add(r);
        
        c.add( evaluateButton );
        c.add( randomButton );
        
        c.next();
        
        for ( JTextField t : value ) c.add(t);
        
        c.add( this.countLabel );
        
        c.next();
        
        c.add( runButton );
        c.add( stopButton );
        c.add( exitButton );
        
        this.evaluateButton.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseReleased(MouseEvent me)
                    {
                        if ( ! THIS.radioNone.isSelected() ) THIS.main.evaluate();
                    }
                });
        
        this.randomButton.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseReleased(MouseEvent me) { THIS.main.randomize(); }
                });
        
        this.runButton.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseReleased(MouseEvent me) { THIS.main.run(); }
                });
        
        this.stopButton.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseReleased(MouseEvent me) { THIS.main.stop(); }
                });
        
        this.exitButton.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseReleased(MouseEvent me) { THIS.main.exit(); }
                });
    }
    
    private void initializeValueField(JTextField f)
    {
        f.setColumns(6);
        f.setEditable(false);
    }
    
    
    public void begin()
    {
        this.applicationFrame.showFrame();
        this.main.start();
    }
    
    
    public void end()
    {
        this.applicationFrame.clearFrame();
    }

    
    public int getSelectedGuess()
    {
        int i = 0;
        if ( ! this.radioNone.isSelected() )
        {
            while ( i < radio.length && ! radio[i].isSelected() ) i++;
            i += 2;
        }
        return i;
    }
    
    
    
    public void setCount(int i) { this.countLabel.setText( "" + i ); }
    
    public void selectGuess(int i) { this.radio[i - 2].setSelected(true); }
    
    public void clearGuess() { this.radioNone.setSelected(true); }
    
    public void setValue(int i, double v) { this.value[i - 2].setText( DECIMAL.format( v ) ); }
    
    
    
    public void setWaiting()
    {
        this.runButton.setEnabled(false);
        this.stopButton.setEnabled(false);
        this.evaluateButton.setEnabled(false);
        this.randomButton.setEnabled(false);
        this.exitButton.setEnabled(false);
    }
    
    public void setReady()
    {
        this.runButton.setEnabled(true);
        this.stopButton.setEnabled(false);
        this.evaluateButton.setEnabled(true);
        this.randomButton.setEnabled(true);
        this.exitButton.setEnabled(true);
    }
    
    public void setRunning()
    {
        this.clearGuess();
        this.runButton.setEnabled(false);
        this.stopButton.setEnabled(true);
        this.evaluateButton.setEnabled(false);
        this.randomButton.setEnabled(false);
        this.exitButton.setEnabled(false);
    }
}
