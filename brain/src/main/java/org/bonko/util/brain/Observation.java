/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

/*
 * $Id: Observation.java,v 1.4 2005/04/30 05:24:03 Owner Exp $
 * 
 */
package org.bonko.util.brain;




public interface Observation {

    boolean isSet();
    
    double[] toArray();

    double[] getValues();

    void setValues(double[] value);
    
    double[] getExpectedValues();
    
    void setExpectedValues(double[] values);
    
    double[] NO_VALUES = new double[0];
    
    int size();
    
    Observation NO_OBSERVATION = new Observation() {
        public boolean isSet() { return false; }
        public double[] toArray() { return NO_VALUES; }
        public double[] getValues() { return NO_VALUES; }
        public void setValues(double[] value) { }
        public double[] getExpectedValues() { return NO_VALUES; }
        public void setExpectedValues(double[] d) { }
        public int size() { return 0; }
    };
}
