/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.util.brain;

import java.io.Serializable;

public interface NeuronLayer extends Serializable, BrainObject
{
    void setInput(NeuronLayer layer);
    
    void setFunctions(BrainFunctions calc);
    
    void initialize();
    
    int getSize();
    
    int[] feedForward();

    boolean hasBackPropogate();

    void backPropogate(int[] error);
    
    int[] createError(int[] expected);
    

    NeuronLayer NO_LAYER = new NeuronLayer()
    {
        private static final long serialVersionUID = 1L;

        public void setInput(NeuronLayer layer) { }
        
        public void setFunctions(BrainFunctions calc) { }

        public void initialize() { }

        public int getSize() { return 1; }

        public int[] feedForward() { return new int[] { 0 }; }

        public boolean hasBackPropogate() { return false; }
        
        public void backPropogate(int[] error) { }
        
        public int[] createError(int[] expected) { return new int[] { 0 }; }
        
        public void accept(BrainVisitor v) { v.accept(this); }
    };
}
