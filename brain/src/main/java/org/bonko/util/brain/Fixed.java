/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.util.brain;

public class Fixed
{
    private static final int DEMONINATOR = 65536;
    private static final long SHIFT = 16;
    
    public int multiply( int a, int b )
    {
        return (int)( ( (long)a * b ) >> SHIFT );
    }
    
    
    public int add( int a, int b )
    {
        return a + b;
    }
    
    
    public int divide( int a, int b )
    {
        return (int)( ( ((long)a << SHIFT)  / b ) );
    }
    
    
    public int convert( double d )
    {
        return (int)( d * DEMONINATOR );
    }

    
    public int convert( int i )
    {
        return i * DEMONINATOR;
    }
    

    public double toDouble( int i )
    {
        return (double)i / DEMONINATOR;
    }
    
    
    public int one() { return DEMONINATOR; }
}
