/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

/*
 * $Id: EvaluationBrain.java,v 1.6 2005/06/01 18:54:35 Owner Exp $
 * 
 */
package org.bonko.util.brain;

import java.io.Serializable;



/**
 * an evaluation brain is a neural network that has some number of inputs,
 * but only a single output. The single output represents the evaluation
 * of the given input, and ranges from -1 to 1.
 * 
 * @author Owner
 *
 */
public interface EvaluationBrain extends Serializable, BrainObject
{

    /**
     * Evaluate an input object
     * 
     * @param observation -- an observation object
     * @return
     */
    void evaluate( Observation o );

    /**
     * Train the brain with the value of the observation object
     * 
     * @param observation -- the observation object with inputs and values set
     * @param count -- the number of times to run the training routine
     * @return the error following training
     */
    double train( Observation o, int count );
    
    
    /**
     * accept a visitor
     * 
     * @param v
     */
    void accept(BrainVisitor v);
    
    
    EvaluationBrain NO_BRAIN = new EvaluationBrain()
    {
        private static final long serialVersionUID = 1L;

        public void evaluate(Observation o) { }

        public double train(Observation o, int count) { return 0.0; }

        public void accept(BrainVisitor v) { v.accept(this); }
    };
}
