/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.util.brain;

import java.util.ArrayList;
import java.util.List;


/*
 * usage
 * 
 *         LayeredEvaluationBrain b = new LayeredEvaluationBrain();
 *         SimpleBrainFunctions calc = new SimpleBrainFunctions();
 *         
 *         calc.setRandom( new Random() );
 *         b.setFunctions(calc);
 *         
 *         b.setInputLayer( new InputLayer(3) );
 *         b.add( new SimpleNeuronLayer(5) );
 *         b.add( new SimpleNeuronLayer(5) );
 *         b.add( new SimpleNeuronLayer(5) );
 *         b.add( new SimpleNeuronLayer(1) );
 *         
 */
public class LayeredBrain implements EvaluationBrain
{
//  private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass());
    
    private static final long serialVersionUID = 1L;
    
    private Fixed fixed = new Fixed();
    
    private InputLayer input;
    private NeuronLayer output;
    private BrainFunctions calc;
    private List<NeuronLayer> layers = new ArrayList<NeuronLayer>();
    
    private final LayeredBrain THIS = this;
    
    public void setFunctions(BrainFunctions f) { this.calc = f; }
    
    public void setInputLayer( InputLayer layer )
    {
        this.input = layer;
        this.addLayer(layer);
    }
    
    
    public void add(NeuronLayer layer)
    {
        layer.setInput(this.output);
        this.addLayer(layer);
    }
    
    
    private void addLayer(NeuronLayer layer)
    {
        layer.setFunctions(this.calc);
        this.output = layer;
        layer.initialize();
        this.layers.add(layer);
    }
    
    
    public void evaluate(Observation p)
    {
        this.input.setInput( p.toArray() );
        int[] out = this.output.feedForward();
        double[] output = new double[ p.size() ];
        
        for ( int i = 0 ; i < output.length ; i++ )
        {
            output[i] = fixed.toDouble(out[i]);
        }
        
        p.setValues(output);
    }
    
    
    public double train(Observation p, int count)
    {
        double[] rawExpected = p.getExpectedValues();
        int[] expected = new int[ rawExpected.length ];
        int[] error = new int[ p.size() ];
        
        for ( int i = 0 ; i < expected.length ; i++ ) 
        {
            expected[i] = fixed.convert( rawExpected[i] ); 
        }
        
        for ( int i = 0 ; i < count ; i++ )
        {
            this.input.setInput( p.toArray() );
            this.output.feedForward();
            error = this.output.createError(expected);
            this.output.backPropogate(error);
        }
        
        double average = 0.0;
        
        for ( int e : error ) average += fixed.toDouble( e ); 
        average /= p.size();
        
        return average;
    }
    
    
    public void accept(BrainVisitor v) { v.accept(this); }
    
    public class Internals
    {
        public NeuronLayer getInput() { return THIS.input; }
        public NeuronLayer getOutput() { return THIS.output; }
        public List<NeuronLayer> getLayers() { return THIS.layers; }
    }
    
}
