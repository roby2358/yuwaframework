/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.util.brain.encoding;

import java.text.DecimalFormat;

import org.bonko.util.brain.BrainObject;
import org.bonko.util.brain.BrainVisitor;
import org.bonko.util.brain.Fixed;
import org.bonko.util.brain.InputLayer;
import org.bonko.util.brain.LayeredBrain;
import org.bonko.util.brain.NeuronLayer;
import org.bonko.util.brain.NoBiasNeuronLayer;
import org.bonko.util.brain.SimpleNeuronLayer;


/**
 * The string encoder encodes the given class into a string
 * 
 * @author Owner
 *
 */
public class StringEncoder implements BrainVisitor
{
    private static final DecimalFormat DECIMAL = new DecimalFormat("  00.00; -00.00");
    
    private Fixed fixed = new Fixed();
    
    private StringBuilder builder = new StringBuilder();
    
    /**
     * handling the generic case isn't supported -- make sure all BrainObjects
     * implement accept(BrainVisitor) correctly!
     */
    public void accept(BrainObject b)
    {
        throw new UnsupportedOperationException("generic handler is not supported");
    }
    
    
    public void accept(LayeredBrain b)
    {
        LayeredBrain.Internals internals = b.new Internals();
        
        this.builder.append("[ LayeredEvaluationBrain " );
        this.builder.append( internals.getLayers().size() );
        this.builder.append("\n");
        
        for ( NeuronLayer l : internals.getLayers() )
        {
            l.accept(this);
            this.builder.append("\n");
        }
        
        this.builder.append( " ]" );
    }
    
    public void accept(InputLayer l)
    {
        InputLayer.Internals i = l.new Internals(this);
        
        this.builder.append( "[ InputLayer ");
        this.builder.append( l.getSize() );
        this.builder.append( "\n");
        this.addArray( i.getOutput() );
        this.builder.append( "\n ]");
    }
    
    public void accept(SimpleNeuronLayer l)
    {
        SimpleNeuronLayer.Internals i = l.new Internals(this);
        
        this.builder.append( "[ SimpleNeuronLayer ");
        this.builder.append( l.getSize() );
        this.builder.append( "\n  in:");
        this.addArray( i.getInput() );
        this.addWeight( "\n  wt:", i.getWeight() );
        this.builder.append( "\nbias:");
        this.addArray( i.getBias() );
        this.builder.append( "\n val:");
        this.addArray( i.getValue() );
        this.builder.append( "\n out:");
        this.addArray( i.getOutput() );
        this.builder.append( "\n err:");
        this.addArray( i.getError() );
        this.builder.append( "\n err:");
        this.addArray( i.getNextError() );
        this.builder.append( "\n ]");
    }
    
    public void accept(NoBiasNeuronLayer l)
    {
        NoBiasNeuronLayer.Internals i = l.new Internals(this);
        
        this.builder.append( "[ NoBiasNeuronLayer ");
        this.builder.append( l.getSize() );
        this.builder.append( "\n  in: ");
        this.addArray( i.getInput() );
        this.addWeight( "\n  wt:", i.getWeight() );
        this.builder.append( "\n val:");
        this.addArray( i.getValue() );
        this.builder.append( "\n out:");
        this.addArray( i.getOutput() );
        this.builder.append( "\n err:");
        this.addArray( i.getError() );
        this.builder.append( "\n err:");
        this.addArray( i.getNextError() );
        this.builder.append( "\n");
    }
    
    
    private void addWeight(String label, int[][] weight)
    {
        for ( int i = 0 ; i < weight[0].length ; i++ )
        {
            this.builder.append(label);
            for ( int j = 0 ; j < weight.length ; j++ )
            {
                this.builder.append( DECIMAL.format( (double)weight[j][i] / fixed.one() ) );
            }
        }
    }
    
    
    private void addArray(int[] anArray)
    {
        for ( int i : anArray ) {
            this.builder.append( DECIMAL.format( (double)i / fixed.one() ) );
        }
    }
    
    
    public String toString() { return this.builder.toString(); }
    
}
