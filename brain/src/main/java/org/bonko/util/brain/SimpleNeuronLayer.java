/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.util.brain;




public class SimpleNeuronLayer implements NeuronLayer
{
    private static final long serialVersionUID = 1L;
    private Fixed fixed = new Fixed();
    private NeuronLayer previous = NO_LAYER;
    private int[][] weight;
    private int[] bias;
    private int[] input;
    private int[] value;
    private int[] output;
    private int[] error;
    private int[] nextError;
    private int size;
    private BrainFunctions calc;
    
    private final SimpleNeuronLayer THIS = this;
    
    public SimpleNeuronLayer(int s)
    {
        this.size = s;
    }
    
    public void setInput(NeuronLayer prev) { this.previous = prev; }
    
    public int getSize() { return this.size; }
    
    public void setSize(int s) { this.size = s; }
    
    public void setFunctions(BrainFunctions f) { this.calc = f; }
    
    public void initialize()
    {
        this.weight = new int[ previous.getSize() ][ this.size ];
        this.bias = new int[this.size];
        this.value = new int[this.size];
        this.output = new int[this.size];
        this.error = new int[this.size];
        this.nextError = new int[this.previous.getSize()];
        this.randomize();
    }
    
    
    private void randomize()
    {
        for ( int j = 0 ; j < this.size ; j++ ) {
            for ( int i = 0 ; i < this.previous.getSize() ; i++ ) {
                weight[i][j] = this.calc.randomCell();
            }
            bias[j] = this.calc.randomCell();
        }
    }
    
    
    public int[] feedForward()
    {
        this.input = this.previous.feedForward();
        
        for ( int j = 0 ; j < this.output.length ; j++ ) {
            int sum = bias[j];
            for ( int i = 0 ; i < input.length ; i++ ) {
                sum += fixed.multiply(this.input[i], weight[i][j]);
            }
            this.value[j] = sum;
            this.output[j] = this.calc.trans( sum );
        }
        
        return this.output;
    }
    
    
    public boolean hasBackPropogate() { return true; }
    
    public void backPropogate(int[] prevError)
    {
        this.error = prevError;
        
        updateWeights();
        
        if ( this.previous.hasBackPropogate() )
        {
            this.nextError = calculateError();
            this.previous.backPropogate( nextError );
        }
    }
    
    
    public int[] createError(int[] expected)
    {
        for ( int i = 0 ; i < this.output.length ; i++ )
        {
            this.error[i] = this.calc.scaleError( fixed.multiply( this.calc.dtrans(this.output[i]), ( expected[i] - this.output[i] ) ) );
        }
        return this.error;
    }
    
    
    private void updateWeights()
    {
        for ( int j = 0 ; j < this.size ; j++ )
        {
            int d = fixed.multiply( this.calc.dtrans(this.value[j]), this.error[j] );
            if ( d < -3 || d > 3 )
            {
                for ( int i = 0 ; i < this.input.length ; i++ ) {
                    this.weight[i][j] += fixed.multiply( d, this.input[i] );
                }
                this.bias[j] += d;
            }
        }
    }
    
    
    private int[] calculateError()
    {
        for ( int i = 0 ; i < nextError.length ; i++ )
        {
            int sum = 0;
            
            for ( int j = 0 ; j < this.error.length ; j++ ) {
                sum += fixed.multiply( this.error[j], this.weight[i][j] );
            }
            this.nextError[i] = this.calc.scaleError( sum ); 
        }
        
        return nextError;
    }
    
    
    public void accept(BrainVisitor v) { v.accept(this); }
    
    
    public class Internals
    {
        public Internals(BrainVisitor v) { }
        
        public NeuronLayer getPrevious() { return THIS.previous; }
        public int[][] getWeight() { return THIS.weight; }
        public int[] getBias() { return THIS.bias; }
        public int[] getInput() { return THIS.input; }
        public int[] getValue() { return THIS.value; }
        public int[] getOutput() { return THIS.output; }
        public int[] getError() { return THIS.error; }
        public int[] getNextError() { return THIS.nextError; }
    }
}
