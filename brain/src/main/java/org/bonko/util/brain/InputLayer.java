/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.util.brain;

public class InputLayer implements NeuronLayer
{
    private static final long serialVersionUID = 1L;

    private Fixed fixed = new Fixed();
    
    private int size;
    private int[] output;
    private BrainFunctions calc;
    
    private final InputLayer THIS = this;
    
    
    public InputLayer(int s)
    {
        this.size = s;
    }
    
    public int getSize() { return this.size; }
    
    public void setSize(int s) { this.size = s; }
    
    public void setFunctions(BrainFunctions f) { this.calc = f; }
    
    public void initialize()
    {
        this.output = new int[this.size];
    }
    
    
    public void setInput(double[] in)
    {
        if ( in == null ) throw new AssertionError("Assertion failed: Input array is not null");
        if ( in.length != this.size ) throw new AssertionError("Assertion failed: Input array length=" + this.size + " (is " + in.length + ")" );
        
        for ( int i = 0 ; i < this.size ; i++ ) this.output[i] = fixed.convert(in[i]);
    }
    
    
    public int[] feedForward()
    {
        return output;
    }
    
    
    public boolean hasBackPropogate() { return false; }
    
    public void backPropogate(int[] error) { }
    
    public void setInput(NeuronLayer layer) { }
    
    public int[] createError(int[] expected) { return null; }
    
    public void accept(BrainVisitor v) { v.accept(this); }
    
    public class Internals
    {
        public Internals(BrainVisitor v) { }
        
        public int[] getOutput() { return THIS.output; }
    }
}
