/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.util.brain;

import java.text.DecimalFormat;


/**
 * SimpleMemory imlements a rolling array that accepts new Observations, overwriting
 * the earliest one when the list overlaps. Training is done for the whole list,
 * although some of the values may be NO_OBSERVATION.
 * 
 * You must call setBrain to set the brain for this memory, and then call either
 * setSize or initialize to prepare the array.
 * 
 * @author Owner
 *
 */
public class EvaluationMemory implements Memory
{
    DecimalFormat DECIMAL = new DecimalFormat(" 0.0000;-0.0000");

    private static final int DEFAULT_SIZE = 16;
    EvaluationBrain brain = EvaluationBrain.NO_BRAIN; 
    private Observation[] observations;
    private int start;
    private int end;
    
    
    public void setBrain(EvaluationBrain brain) { this.brain = brain; }
    
    
    public void initialize()
    {
        if ( this.observations == null ) this.setSize(DEFAULT_SIZE);
    }
    
    
    public void setSize(int s)
    {
        this.observations = new Observation[s];
        this.clear();
    }
    
    
    public void clear()
    {
        for ( int i = 0 ; i < this.observations.length ; i++ ) observations[i] = Observation.NO_OBSERVATION;
        this.start = 0;
        this.end = 0;
    }
    

    public void add(Observation o)
    {
        if ( o == null ) throw new AssertionError("o is non-null");
        
        this.incrementEnd();
        this.observations[ this.end ] = o;
    }
    
    
    private void incrementEnd()
    {
        this.end++;
        if ( this.end >= this.observations.length ) this.end = 0;
        if ( this.end == this.start ) {
            this.incrementStart();
        }
    }
    
    
    private void incrementStart()
    {
        this.start++;
        if ( this.start >= this.observations.length ) this.start = 0;
    }
    
    
    public void setExpectedValues(double[] values)
    {
        for ( Observation a : this.observations ) {
            if ( a != Observation.NO_OBSERVATION ) a.setExpectedValues(values);
        }
    }
    
    public void train( int iterations )
    {
        for ( int i = 0 ; i < iterations ; i++ )
            for ( Observation a : this.observations ) {
                if ( a != Observation.NO_OBSERVATION ) brain.train(a, 1);
            }
    }
    
    public String toString()
    {
        final StringBuilder builder = new StringBuilder();
        
        builder.append( "[ Memory ");
        builder.append( start );
        builder.append( " - " );
        builder.append( end );
        builder.append( " [ " );
        
        for ( Observation a : this.observations )
        {
            if ( a == null ) builder.append( "? " );
            else
            {
                for ( double d : a.getValues() )
                {
                    builder.append( DECIMAL.format( d ) );
                    builder.append( " " );
                }
            }
            builder.append("\n");
        }

        builder.append( " ] ]" );
        
        return builder.toString();
    }
}
