/*
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
*/

package org.bonko.util.brain;

import java.util.Random;

public class SimpleBrainFunctions implements BrainFunctions
{
    private static final Fixed fixed = new Fixed();
    private static final int MAX = fixed.convert(0.25);
    private static final double DEFAULT_ALPHA = 0.1;
    private Random random = new Random();
    private int alpha = fixed.convert( DEFAULT_ALPHA );
    
    public void setRandom(Random r) { this.random = r; }
    
    public void setAlpha(double a) { this.alpha = fixed.convert(a); }
    
    
    public int trans(double in)
    {
        return fixed.convert( 2 / (1 + Math.exp(-in) ) - 1 );
    }
    
    public int dtrans(double in)
    {
        int t = trans(in);
        return ( ( 1 + t ) * ( 1 - t ) ) / 2;
    }
    
    
    public int trans(int in)
    {
        return fixed.convert( 2 / (1 + Math.exp(-fixed.toDouble(in)) ) - 1 );
    }
    
    public int dtrans(int in)
    {
        int t = trans(in);
        return fixed.multiply( fixed.one() + t, fixed.one() - t ) >> 1;
    }
    
    public int randomCell()
    {
        return this.random.nextInt( 2 * MAX ) - MAX; 
    }
    
    public void randomizeLearningRate(double minLearningRate, double maxLearningRate)
    {
        this.alpha = fixed.convert( (maxLearningRate - minLearningRate ) * this.random.nextDouble() + minLearningRate );
    }
    
    public void taperLearningRate( double minLearningRate, double maxLearningRate, int count, int oneOverECount )
    {
        this.alpha = fixed.convert( (maxLearningRate - minLearningRate) * Math.exp( (double)-count / oneOverECount ) + minLearningRate );
    }
    
    public int scaleError(int delta)
    {
        return fixed.multiply( this.alpha, delta );
    }
}
