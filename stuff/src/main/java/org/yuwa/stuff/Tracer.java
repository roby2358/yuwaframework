/**
 * Copyright (c) 2011 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 */
package org.yuwa.stuff;

/**
 * @author ryoung
 *
 */
public class Tracer
{

    public static void println(String string, Object... args)
    {
        final Exception e = new Exception();
        show(e, 1, "", string, args);
        show(e, 2, "  from ", "");
    }

    /**
     * @param e
     * @param index TODO
     * @param string
     * @param args
     */
    private static void show(final Exception e, int index, String padding, String string, Object... args)
    {
        final StackTraceElement st = e.getStackTrace()[index];
        final String info = new String(padding
                + st.getClassName()
                + "."
                + st.getMethodName()
                + "("
                + st.getFileName()
                + ":"
                + st.getLineNumber()
                + ") : "
                + String.format(string, (Object[])args));
        System.err.println(info);
    }

}
