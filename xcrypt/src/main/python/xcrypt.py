#! /usr/local/bin/python

'''
Copyright (c) 2010 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

original author: royoung
'''

''' This is a quick, portable crypt routine using XOR against a random string. '''

from random import getrandbits, randint
from base64 import standard_b64encode, standard_b64decode
from itertools import izip
import struct
import array
import sys


def _generate_keys() :
    ''' generate a new keys string '''
    bits = (getrandbits(8) for x in xrange(0,57 * 101))
    a = array.array('B',bits)
    encoded = standard_b64encode(a).strip()
    return encoded
    
    
def generate_keys_file():
    ''' embed the generated keys string into a python statement '''
    keys = _generate_keys()
    print 'KEYS_B64="%s"' %keys
    

def _xor(string,keys,key):
    ''' xor the string against the KEY string, using key as the offset '''
    z = (chr(ord(x) ^ ord(y)) for (x,y) in izip(string,keys[key:]))
    return ''.join(z)
    
    
def xcrypt(string, key):
    ''' run an xor on the string using the key.
    If the key is None, create one and base64 encode the result.
    If the key is provided, base64 decode the string and use the key. '''

    # you have to create a key string and put it in xcrypt_keys.py first
    import xcrypt_keys
    keys = standard_b64decode(xcrypt_keys.KEYS_B64)

    if key is None :
        key = randint(0,len(keys) - len(string) - 1)
        xored=standard_b64encode(_xor(string,keys,key)).strip()
    else:
        string = standard_b64decode(string).strip()
        xored=_xor(string,keys,key)
    return xored,key


if __name__ == '__main__' :
    ''' main function '''
    
    if len(sys.argv) > 1 and 'generate' == sys.argv[1] :
        generate_keys_file()
    else:
        if len(sys.argv) > 1 :
            input = sys.argv[1]
        else :
            input = sys.stdin.read()
        
        a,k = xcrypt(input,None)
        b,k = xcrypt(a,k)
        print k,a,b
